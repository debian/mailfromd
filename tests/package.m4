# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [mailfromd])
m4_define([AT_PACKAGE_TARNAME],   [mailfromd])
m4_define([AT_PACKAGE_VERSION],   [9.0])
m4_define([AT_PACKAGE_STRING],    [mailfromd 9.0])
m4_define([AT_PACKAGE_BUGREPORT], [bug-mailfromd@gnu.org.ua])
