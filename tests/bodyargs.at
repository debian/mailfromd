# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2023-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Arguments to body])
AT_KEYWORDS([body arguments])

MFT_TEST([
prog body
do
  echo $1
  echo $2
done],
[--lint])

MFT_TEST([
prog body
do
  echo $3
done
],
[--lint],
[EX_CONFIG],
[],
[mailfromd: prog:4.8-9: argument number too high
])

AT_DATA([filter],
[number fd

prog begin
do
  set fd open(">".getenv("TESTDIR")."/body.txt")
done

prog body
do
  write_body(fd, $1, $2)
done
])

AT_DATA([script.pre],
[\E250
HELO localhost
\E250
MAIL FROM: <foo@localhost>
\E250
RCPT TO: <bar@example.org>
\E354
DATA
From: foo
To: bar
Subject: test subject

])

AT_DATA([script.post],
[.
\E221
QUIT
])

AT_CHECK([MFT_UNPRIVILEGED_PREREQ
cat script.pre $top_srcdir/COPYING script.post > script
TESTDIR=$(pwd)
export TESTDIR
mtasim -DTESTDIR="$TESTDIR" --body-chunk=128 --stdio -Xauto --statedir -- MAILFROMD_LOGOPTS MAILFROMD_OPTIONS filter < script 2>&1 > /dev/null | sed 's/^mailfromd: info: *//' >&2
],
[0],
[],
[mailfromd_script_warning[]mailfromd_debug_header[]mailfromd_debug_footer])

AT_CHECK([cat body.txt | tr -d '\r' | cmp $top_srcdir/COPYING -])

AT_CLEANUP

