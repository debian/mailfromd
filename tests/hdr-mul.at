# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP(multiple message capturing)
AT_KEYWORDS([curhdr current_header_mul])

MFT_WITH_MAILFROMD_OPTIONS([--stderr --gacopyz-log=err],[
MFT_MTASIM([
prog eoh
do
  echo "TOTAL: " . current_header_count()
  echo "SUBJECT: " . current_header("Subject")
done

func deliver(string mailto)
do
  set m umask(0)
  set mbx mailbox_open(mailto, "w+")
  
  set msg current_message()
  mailbox_append_message(mbx, msg)
  mailbox_close(mbx)
  set m umask(m)
done

prog eom
do
  deliver("mbox:$TESTDIR/capture.mbx")
done
],
[
\E250
HELO localhost
\E250
MAIL FROM:<gray@gnu.org.ua>
\E250
RCPT TO:<gray@localhost>
\E354
DATA
Received: foo
Received: bar
From: Sergey Poznyakoff <gray@gnu.org.ua>
To: gray@localhost
Subject: Etiam tempor

Proin varius dolor nec purus varius, sed volutpat magna pharetra.
Etiam tempor iaculis nunc, at facilisis purus fringilla at.
.
\E250
MAIL FROM:<gray@gnu.org.ua>
\E250
RCPT TO:<gray@localhost>
\E354
DATA
Received: foo
Received: bar
Received: quux
From: Sergey Poznyakoff <gray@gnu.org.ua>
To: gray@localhost
Subject: Nam vitae

Mauris sit amet tortor pharetra sem placerat ultrices a ut magna.
Integer ultricies felis in arcu feugiat vestibulum. In id neque sem.
Nam vitae ipsum at mauris blandit vulputate vel a lectus.
.
\E221
QUIT
],
[EX_OK],
[],
[TOTAL: 5
SUBJECT: Etiam tempor
TOTAL: 6
SUBJECT: Nam vitae
])
])

AT_CHECK([sed -e 's/^\(From <.*>\).*/\1/' -e ['/^X-[A-Za-z]*:/d'] capture.mbx],
0,
[From <gray@gnu.org.ua>
Received: foo
Received: bar
From: Sergey Poznyakoff <gray@gnu.org.ua>
To: gray@localhost
Subject: Etiam tempor

Proin varius dolor nec purus varius, sed volutpat magna pharetra.
Etiam tempor iaculis nunc, at facilisis purus fringilla at.

From <gray@gnu.org.ua>
Received: foo
Received: bar
Received: quux
From: Sergey Poznyakoff <gray@gnu.org.ua>
To: gray@localhost
Subject: Nam vitae

Mauris sit amet tortor pharetra sem placerat ultrices a ut magna.
Integer ultricies felis in arcu feugiat vestibulum. In id neque sem.
Nam vitae ipsum at mauris blandit vulputate vel a lectus.

])

AT_CLEANUP

