# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Try-catch: returning from catch])
AT_KEYWORDS([try catch return try-catch trycatch try-catch02 trycatch02])

# Description: Check returning from the catch branch of a `try-catch'
# construct.

MFT_RUN([
dclex ex0
dclex ex1

func throwcheck()
  returns number
do
  try
  do
    throw ex1 "text"
    return 0
  done
  catch *
  do
    echo "Caught exception: $1, $2"
    return 1
  done

  return 2
done

func main(...)
  returns number
do
  echo throwcheck()
done
],
[],
[1],
[],
[Caught exception: 1, text
1
])

AT_CLEANUP
