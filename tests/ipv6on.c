#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>

int
main(int argc, char **argv)
{
	struct addrinfo *res;
	return getaddrinfo("::1", "25", NULL, &res) != 0;
}
