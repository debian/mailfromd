# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2017-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([PTR lookup])
AT_KEYWORDS([ip4 dns dns_ip4 resolv resolv_ip4 resolv_ptr resolv_ptr_ip4])
AT_CHECK([
MFT_REQUIRE_DNS
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 192.0.2.1
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 192.0.2.2
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 192.0.2.3
],
[0],
[OK
test1
OK
mail.test1
OK
mail1.test1
])
AT_CLEANUP

# #############

AT_SETUP([PTR lookup (IPv6)])
AT_KEYWORDS([ip6 dns dns_ip6 resolv resolv_ip6 resolv_ptr resolv_ptr_ip6])

AT_CHECK([MFT_REQUIRE_DNS
MFT_REQUIRE_IPV6
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 2001:db8::1
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 2001:db8::2
resolv -f resolv.conf -S .$MF_TOPDOMAIN ptr 2001:db8::3
],
[0],
[OK
test1
OK
mail.test1
OK
bkmx.test1
])
AT_CLEANUP
