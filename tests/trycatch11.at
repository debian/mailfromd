# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2011-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Try-catch: next from nested try])
AT_KEYWORDS([try catch next loop try-catch trycatch try-catch11 trycatch11])

# Description: Check whether next from a nested try branch does not
# clobber stack

AT_DATA([prog],[
require '_register'
dclex usr1

func main(...)
  returns number
do
  echo _reg(REG_TOS)
  loop for number i 0, while i < 5, set i i + 1
  do
    try
    do
      try
      do
        next
      done
      catch usr1
      do
        pass
      done
    done
    catch usr1
    do
      echo "outer catch"
    done
  done
  echo _reg(REG_TOS)
done
])

AT_CHECK([
mailfromd MAILFROMD_LOGOPTS MAILFROMD_OPTIONS --run prog 2>err || exit $?
numck -n 2 err
])

AT_CLEANUP


