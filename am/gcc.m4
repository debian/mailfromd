# This file is part of Mailfromd.  -*- autoconf -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AC_DEFUN([MF_CC_OPT],[
  m4_pushdef([mf_optname],translit($1,[-],[_]))
  AC_MSG_CHECKING(whether $CC accepts $1)
   mf_save_cc="$CC"
   CC="$CC $1"
   AC_RUN_IFELSE(
            [AC_LANG_SOURCE([int main() { return 0; }])],
            [[mf_cv_cc_]mf_optname=yes],
            [[mf_cv_cc_]mf_optname=no],
            [[mf_cv_cc_]mf_optname=no])
   CC="$mf_save_cc"
  AC_MSG_RESULT($[mf_cv_cc_]mf_optname)
  
  if test $[mf_cv_cc_]mf_optname = yes; then
         m4_if([$2],,:,[$2])
  ifelse([$3],,,else
         [$3])
  fi
  m4_popdef([mf_optname])
  ])

AC_DEFUN([MF_CC_OPT_CFLAGS],[
  MF_CC_OPT([$1],[CFLAGS="$CFLAGS $1"])
])
  
AC_DEFUN([MF_GCC_ALIAS_QUIRK],[
  # 
  # Quoting Linus Torvalds:
  #   The gcc people are more interested in trying to find out what can be
  #   allowed by the c99 specs than about making things actually _work_. The
  #   aliasing code in particular is not even worth enabling, it's just not
  #   possible to sanely tell gcc when some things can alias.
  # (see http://lkml.org/lkml/2003/2/26/158, for details)
  #
  # (The code below is borrowed from Python's configure.in)
  MF_CC_OPT_CFLAGS([-fno-strict-aliasing])
])

AC_DEFUN([MF_GCC_PAREN_QUIRK],[
  MF_CC_OPT_CFLAGS([-Wno-parentheses])
])

AC_DEFUN([MF_GCC_QUIRKS],[
  case $GCC in
  yes)
    MF_GCC_ALIAS_QUIRK
    MF_GCC_PAREN_QUIRK
    ;;
  *)  # FIXME
    ;;
  esac])
  
