# This file is part of Mailfromd.                    -*- autoconf -*-
# Copyright (C) 2022-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# mfmod.m4 serial 2
AC_DEFUN([AC_MFL_EXTRACT_PATH],
  [$2=$($MAILFROMD --no-config --show-defaults | sed -n -e '/$1 path: */{' -e 's///' -e 's/:.*$//' -e 'p' -e '}')])

AC_DEFUN([AC_MFMOD],
[AC_PATH_PROG([MAILFROMD],[mailfromd],[])
 AC_CHECK_HEADER([mailfromd/mfmod.h],[],[AC_MSG_ERROR([required header mailfromd/mfmod.h not found])])

 AC_MSG_CHECKING([for MFL loadable modules installation directory])
 AC_SUBST([MFMODDIR])
 AC_ARG_WITH([mfmoddir],
  [AS_HELP_STRING([--with-mfmoddir=DIR], [install MFL loadable modules in DIR])],
  [MFMODDIR=$withval],
  [if test -n "$MAILFROMD"; then
     AC_MFL_EXTRACT_PATH([mfmod],[MFMODDIR])
     if test -z "$MAILFROMD"; then
       AC_MSG_ERROR([can't determine mfmod installation directory: mailfromd compiled without support for dlopen?])
     fi
   else
     AC_MSG_ERROR([can't determine mfmod installation directory: mailfromd not found])
   fi])
 AC_MSG_RESULT([$MFMODDIR])

 AC_MSG_CHECKING([for MFL interface modules installation directory])
 AC_SUBST([MFLDIR])
 AC_ARG_WITH([mfldir],
  [AS_HELP_STRING([--with-mfldir=DIR], [install MFL interface modules in DIR])],
  [MFLDIR=$withval],
  [if test -n "$MAILFROMD"; then
     AC_MFL_EXTRACT_PATH([module],[MFLDIR])
     if test -z "$MFLDIR"; then
       AC_MSG_ERROR([can't determine MFL interface module installation directory])
     fi
   else
     AC_MSG_ERROR([can't determine MFL interface module installation directory: mailfromd not found])
   fi])
  AC_MSG_RESULT([$MFLDIR])])

