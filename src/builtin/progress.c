#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "progress.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2010-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */




void
#line 20
bi_progress(eval_environ_t env)
#line 20

#line 20

#line 20 "progress.bi"
{
#line 20
	
#line 20

#line 20
        
#line 20

#line 20
        
#line 20
        adjust_stack(env, 0);
#line 20

#line 20

#line 20
	if (builtin_module_trace(BUILTIN_IDX_progress))
#line 20
		prog_trace(env, "progress");;
#line 20

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      "PROGRESS");
	gacopyz_progress(env_get_context(env));
}

#line 32
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 32
	return;
#line 32
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
progress_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 20 "progress.bi"
va_builtin_install_ex("progress", bi_progress, STATMASK(smtp_state_eom), dtype_unspecified, 0, 0, 0|0, dtype_unspecified);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

