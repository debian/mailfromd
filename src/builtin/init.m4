m4_divert(-1)
# This file is part of Mailfromd.
# Copyright (C) 2006-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

m4_changequote([<,>])
m4_changecom(/*,*/)

m4_define([<__MODNO__>],0)

m4_define([<MF_BEGIN_OUTPUT>],[<m4_dnl
m4_divert(0)m4_dnl
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
>])

m4_define([<MF_DEFINES>],[<
#define BUILTIN_IDX_prog 0
m4_undivert(1)
>])

m4_define([<MF_BUILTIN_DECLS>],[<
m4_undivert(2)
>])

m4_define([<MF_BUILTIN_MODULES>],[<
static struct builtin_module builtin_module[] = {
	{ "prog", NULL, 0 },
m4_undivert(3)
	{ NULL, NULL }
};
#define BUILTIN_IDX_MAX (sizeof(builtin_module)/sizeof(builtin_module[0])-1)
>])

m4_define([<MF_BUILTIN_MODULE>],
[<m4_pushdef([<MF_MODULE_NAME>],
m4_ifelse([<$1>],,[<m4_dnl
m4_patsubst(m4___file__,\(.*/\)?\(.*\)\.bi,\2)>],[<$1>]))
m4_pushdef([<MF_MODULE_IDX>],[<[<BUILTIN_IDX_>]MF_MODULE_NAME()>])
m4_define([<__MODNO__>],m4_incr(__MODNO__))
m4_divert(1)m4_dnl
#define MF_MODULE_IDX (__MODNO__)
m4_divert(2)m4_dnl
extern void MF_MODULE_NAME[<>]_init_builtin(void);
m4_divert(3)m4_dnl
	{ "MF_MODULE_NAME", MF_MODULE_NAME[<>]_init_builtin, 0 },
m4_divert(-1)>])
