/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include "msg.h"
#include "global.h"

static size_t nmsgs = MAX_MSGS;

static struct mu_cfg_param msg_cfg_param[] = {
	{ "max-open-messages", mu_c_size, &nmsgs, 0, NULL,
	  N_("Maximum number of messages to open simultaneously.") },
	{ NULL }
};

static void *
alloc_msgs()
{
	return mu_calloc(nmsgs, sizeof(struct mf_message));
}

static void
destroy_msgs(void *data)
{
	struct mf_message *mtab = data;
	struct mf_message *p;
	for (p = mtab; p < mtab + nmsgs; p++) {
		bi_close_message(p);
	}
	free(mtab);
}

void
drop_current_message(void *data)
{
	int i;
	struct mf_message *tab = data;
	for (i = 0; i < nmsgs; i++)
		if (tab[i].msg && tab[i].type == MF_MSG_CURRENT) {
			bi_close_message(&tab[i]);
			break;
		}
}

MF_DECLARE_DATA(MSGTAB, alloc_msgs, destroy_msgs, drop_current_message)

static int
find_slot(struct mf_message *tab)
{
	int i;
	for (i = 0; i < nmsgs; i++)
		if (tab[i].msg == NULL)
			return i;
	return -1;
}

static int
do_msg_close(void *item, void *data)
{
	bi_close_message(item);
	return 0;
}

void
bi_close_message(struct mf_message *mp)
{
	if (mp->msg) {
		mu_stream_destroy(&mp->mstr);
		mu_stream_destroy(&mp->bstr);
		if (mp->type == MF_MSG_STANDALONE)
			mu_message_destroy(&mp->msg,
					   mu_message_get_owner(mp->msg));
		mu_list_foreach(mp->msglist, do_msg_close, NULL);
		mu_list_destroy(&mp->msglist);
		free(mp->buf);
		memset(mp, 0, sizeof *mp);
	}
}

int
bi_message_register(eval_environ_t env,
		    mu_list_t msglist, mu_message_t msg, int type)
{
	struct mf_message *msgtab = MF_GET_DATA;
	int idx = find_slot(msgtab);
	if (idx >= 0) {
		struct mf_message *mp = msgtab + idx;
		mp->msg = msg;
		mp->type = type;
		mu_list_create(&mp->msglist);
		if (msglist)
			mu_list_append(msglist, mp);
	}
	return idx;
}

int
bi_get_current_message(eval_environ_t env, mu_message_t *ret_msg)
{
	int i;
	struct mf_message *tab = MF_GET_DATA;
	mu_stream_t mstr;
	mu_message_t msg;
	int rc;

	for (i = 0; i < nmsgs; i++)
		if (tab[i].msg && tab[i].type == MF_MSG_CURRENT) 
			break;
 	if (i == nmsgs) {
		rc = env_get_stream(env, &mstr);
		MF_ASSERT(rc >= 0,
			  mfe_failure,
			  _("cannot obtain capture stream reference: %s"),
			  mu_strerror(rc));
		msg = MF_STREAM_TO_MESSAGE(mstr);
		i = bi_message_register(env, NULL, msg, MF_MSG_CURRENT);
		MF_ASSERT(i >= 0,
			  mfe_failure,
			  _("no more message slots available"));
	}
	if (ret_msg)
		*ret_msg = tab[i].msg;
	return i;
}


m4_define([<DCL_MSG>],[<
	struct mf_message *mtab = MF_GET_DATA;
	struct mf_message *$1;
			
	MF_ASSERT($2 >= 0 && $2 < nmsgs,
		  mfe_range,
		  _("invalid message descriptor"));
	$1 = mtab + $2;
	MF_ASSERT($1->msg,
		  mfe_failure,
		  _("message not open"))
>])

m4_define([<DCL_BODY>],[<
	DCL_MSG($1, $2);
	if (!$1->body) {
		int rc = mu_message_get_body($1->msg, &$1->body);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_body: %s",
			  mu_strerror(rc));
	}	  
>])

m4_define([<DCL_HDR>],[<
	DCL_MSG($1, $2);
	if (!$1->hdr) {
		int rc = mu_message_get_header($1->msg, &$1->hdr);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_header: %s",
			  mu_strerror(rc));
	}	  
>])

m4_define([<DCL_BODY_HDR>],[<
	DCL_MSG($1, $2);
	if (!$1->body) {
		int rc = mu_message_get_body($1->msg, &$1->body);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_body: %s",
			  mu_strerror(rc));
	}	  
	if (!$1->hdr) {
		int rc = mu_message_get_header($1->msg, &$1->hdr);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_header: %s",
			  mu_strerror(rc));
	}	  
>])

mu_message_t
bi_message_from_descr(eval_environ_t env, int nmsg)
{
	DCL_MSG(mp, nmsg);
	return mp->msg;
}

/* void message_close(number msg) */
MF_DEFUN(message_close, VOID, NUMBER nmsg)
{
	DCL_MSG(mp, nmsg);
	bi_close_message(mp);
}
END

/* number message_size(number nmsg) */
MF_DEFUN(message_size, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t size;
	DCL_MSG(mp, nmsg);
	
	rc = mu_message_size(mp->msg, &size);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s", mu_strerror(rc));
	MF_ASSERT(size < LONG_MAX,
		  mfe_range,
		  _("message size out of representable range"));
	MF_RETURN((long)size);
}	
END

/* number message_lines(number nmsg) */
MF_DEFUN(message_lines, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t lines;
	DCL_MSG(mp, nmsg);

	rc = mu_message_lines(mp->msg, &lines);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	MF_RETURN(lines);
}
END

/* number message_body_size(number nmsg) */
MF_DEFUN(message_body_size, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t size;
	DCL_BODY(mp, nmsg);
	
	rc = mu_body_size(mp->body, &size);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s", mu_strerror(rc));
	MF_ASSERT(size < LONG_MAX,
		  mfe_range,
		  _("body size out of representable range"));
	MF_RETURN((long)size);
}
END

/* boolean message_body_is_empty(number nmsg) */
MF_DEFUN(message_body_is_empty, NUMBER, NUMBER nmsg)
{
        int rc, res;
	char c;
	size_t n;
	mu_stream_t input, fstr;
	char *encoding = NULL;

	DCL_BODY_HDR(mp, nmsg);
	rc = mu_body_size(mp->body, &n);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_body_size: %s", mu_strerror(rc));
	if (n == 0)
		MF_RETURN(1);

	rc = mu_body_get_streamref(mp->body, &input);
	MF_ASSERT(rc == 0,
                  mfe_failure,
                  "mu_body_get_streamref: %s", mu_strerror(rc));

	mu_header_aget_value_unfold(mp->hdr,
				    MU_HEADER_CONTENT_TRANSFER_ENCODING,
				    &encoding);
	if (encoding) {
		rc = mu_filter_create(&fstr, input, encoding,
				      MU_FILTER_DECODE,
				      MU_STREAM_READ);
		if (rc) {
			mu_error(_("can't create filter for encoding %s: %s"),
				 encoding, mu_strerror(rc));
			fstr = input;
		} else 
			mu_stream_unref(input);
		free(encoding);
	} else
		fstr = input;
	
	res = 1;
	while (mu_stream_read(fstr, &c, 1, &n) == 0 && n > 0) {
		if (!mu_isspace(c)) {
			res = 0;
			break;
		}
	}
	mu_stream_unref(fstr);
	MF_RETURN(res);
}
END
	 
/* number message_body_lines(number nmsg) */
MF_DEFUN(message_body_lines, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t lines;
	DCL_BODY(mp, nmsg);

	rc = mu_body_lines(mp->body, &lines);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	MF_RETURN(lines);
}
END


/* Headers */

/* number message_header_size(number nmsg) */
MF_DEFUN(message_header_size, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t size;
	DCL_HDR(mp, nmsg);
	
	rc = mu_header_size(mp->hdr, &size);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s", mu_strerror(rc));
	MF_ASSERT(size < LONG_MAX,
		  mfe_range,
		  _("header size out of representable range"));
	MF_RETURN((long)size);
}
END

/* number message_header_lines(number nmsg) */
MF_DEFUN(message_header_lines, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t lines;
	DCL_HDR(mp, nmsg);

	rc = mu_header_lines(mp->hdr, &lines);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	MF_RETURN(lines);
}
END

/* number message_header_count(number nmsg[; string name]) */
MF_DEFUN(message_header_count, NUMBER, NUMBER nmsg,
	 OPTIONAL, STRING name)
{
	int rc;
	size_t count;
	DCL_HDR(mp, nmsg);

	rc = mu_header_get_field_count(mp->hdr, &count);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	
	if (MF_DEFINED(name)) {
		size_t n;

		for (n = 1; n <= count; n++) {
			const char *val;
			rc = mu_header_sget_value_n(mp->hdr, name, n, &val);
			if (rc == MU_ERR_NOENT)
				break;
			else if (rc)
				MF_THROW(mfe_failure, "%s", mu_strerror(rc));
		}
		count = n - 1;
	}
	MF_RETURN(count);
}
END

/* bool message_has_header(number msg, string header[, number idx]) */
MF_DEFUN(message_has_header, NUMBER, NUMBER nmsg, STRING header,
	 OPTIONAL, NUMBER idx)
{
	int rc;
	const char *val;
	DCL_HDR(mp, nmsg);

	rc = mu_header_sget_value_n(mp->hdr, header, MF_OPTVAL(idx, 1), &val);
 	MF_RETURN(rc == 0);
}
END

/* string message_find_header(number msg, string header[, number idx]) */
MF_DEFUN(message_find_header, STRING, NUMBER nmsg, STRING header,
	 OPTIONAL, NUMBER idx)
{
	int rc;
	const char *val;
	DCL_HDR(mp, nmsg);

	rc = mu_header_sget_value_n(mp->hdr, header, MF_OPTVAL(idx, 1), &val);
	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_not_found, _("header not found"));
	else if (rc)
		MF_THROW(mfe_failure, "%s", mu_strerror(rc));
	MF_RETURN(val);
}
END

/* string message_nth_header_name(number msg, number n) */
MF_DEFUN(message_nth_header_name, STRING, NUMBER nmsg, NUMBER idx)
{
	int rc;
	const char *val;
	DCL_HDR(mp, nmsg);
	
	rc = mu_header_sget_field_name(mp->hdr, idx, &val);
	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_range, "header index out of range");
	else if (rc)
		MF_THROW(mfe_failure, "%s", mu_strerror(rc));
	MF_RETURN(val);
}
END

/* string message_nth_header_value(number msg, number n) */
MF_DEFUN(message_nth_header_value, STRING, NUMBER nmsg, NUMBER idx)
{
	int rc;
	const char *val;
	DCL_HDR(mp, nmsg);
	
	rc = mu_header_sget_field_value(mp->hdr, idx, &val);
	if (rc == MU_ERR_NOENT)
		MF_THROW(mfe_range, "header index out of range");
	else if (rc)
		MF_THROW(mfe_failure, "%s", mu_strerror(rc));
	MF_RETURN(val);
}
END


/* Multipart messages */

/* bool message_is_multipart(number msg) */
MF_DEFUN(message_is_multipart, NUMBER, NUMBER nmsg)
{
	int rc, res;
	DCL_MSG(mp, nmsg);

	rc = mu_message_is_multipart(mp->msg, &res);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	MF_RETURN(res);
}
END

/* number message_count_parts(number msg) */
MF_DEFUN(message_count_parts, NUMBER, NUMBER nmsg)
{
	int rc;
	size_t count;
	DCL_MSG(mp, nmsg);
	rc = mu_message_get_num_parts(mp->msg, &count);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	MF_RETURN(count);
}
END

/* number message_get_part(number msg, number idx) */
MF_DEFUN(message_get_part, NUMBER, NUMBER nmsg, NUMBER idx)
{
	int rc;
	mu_message_t msg;
	DCL_MSG(mp, nmsg);

	rc = mu_message_get_part(mp->msg, idx, &msg);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "%s",
		  mu_strerror(rc));
	rc = bi_message_register(env, mp->msglist, msg, MF_MSG_MAILBOX);
	MF_ASSERT(rc >= 0,
		  mfe_failure,
		  _("no more message slots available"));
	MF_RETURN(rc);
}
END


/* Reading */

/* string message_rewind(number nmsg) */
MF_DEFUN(message_rewind, VOID, NUMBER nmsg)
{
	DCL_MSG(mp, nmsg);
	mu_stream_destroy(&mp->mstr);
}
END

/* string message_read_line(number nmsg) */
MF_DEFUN(message_read_line, STRING, NUMBER nmsg)
{
	int rc;
	size_t size;
	DCL_MSG(mp, nmsg);

	if (!mp->mstr) {
		rc = mu_message_get_streamref(mp->msg, &mp->mstr);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_stream: %s",
			  mu_strerror(rc));
	}
	rc = mu_stream_getline(mp->mstr, &mp->buf, &mp->bufsize, &size);
	MF_ASSERT(rc == 0,
		  mfe_io,
		  "mu_stream_getline: %s",
		  mu_strerror(rc));
	MF_ASSERT(size > 0,
		  mfe_eof,
		  "mu_stream_getline: %s",
		  _("end of input"));
	MF_RETURN(mp->buf);
}
END

/* string message_body_rewind(number nmsg) */
MF_DEFUN(message_body_rewind, VOID, NUMBER nmsg)
{
	DCL_BODY(mp, nmsg);
	mu_stream_destroy(&mp->bstr);
}
END

/* string message_read_body_line(number msg)  */
MF_DEFUN(message_read_body_line, STRING, NUMBER nmsg)
{
	int rc;
	size_t size;
	DCL_BODY(mp, nmsg);
	
	if (!mp->bstr) {
		rc = mu_body_get_streamref(mp->body, &mp->bstr);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_body_get_stream: %s",
			  mu_strerror(rc));
	}
	rc = mu_stream_getline(mp->bstr, &mp->buf, &mp->bufsize, &size);
	MF_ASSERT(rc == 0,
		  mfe_io,
		  "mu_stream_getline: %s",
		  mu_strerror(rc));
	MF_ASSERT(size > 0,
		  mfe_eof,
		  "mu_stream_getline: %s",
		  _("end of input"));
	MF_RETURN(mp->buf);
}
END

/* string message_to_stream(number fd, number msg; string mu_filter_chain) */
MF_DEFUN(message_to_stream, VOID, NUMBER fd, NUMBER nmsg, OPTIONAL,
	 STRING fltchain)
{
	int rc;
	int yes = 1;
	mu_stream_t dst;
	char *flts = MF_OPTVAL(fltchain);
	
	DCL_MSG(mp, nmsg);
	
	rc = mu_fd_stream_create(&dst, NULL, _bi_io_fd(env, fd, 1),
				 MU_STREAM_WRITE);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_fd_stream_create: %s",
		  mu_strerror(rc));
	mu_stream_ioctl(dst, MU_IOCTL_FD, MU_IOCTL_FD_SET_BORROW, &yes);
	if (flts) {
		struct mu_wordsplit ws;
		mu_stream_t flt;
		
		rc = mu_wordsplit(flts, &ws, MU_WRDSF_DEFFLAGS);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_wordsplit: %s",
			  mu_wordsplit_strerror(&ws));
		rc = mu_filter_chain_create(&flt, dst, MU_FILTER_ENCODE,
					    MU_STREAM_WRITE,
					    ws.ws_wordc, ws.ws_wordv);
		mu_wordsplit_free(&ws);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_filter_chain_create(\"%s\"): %s",
			  flts,
			  mu_strerror(rc));
		mu_stream_unref(dst);
		dst = flt;
	}

	if (!mp->mstr) {
		rc = mu_message_get_streamref(mp->msg, &mp->mstr);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_message_get_stream: %s",
			  mu_strerror(rc));
	}
	rc = mu_stream_copy(dst, mp->mstr, 0, NULL);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_copy: %s",
		  mu_strerror(rc));
	mu_stream_close(dst);
	mu_stream_unref(dst);
}
END

/* number message_from_stream(number fd; string mu_filter_chain) */
MF_DEFUN(message_from_stream, NUMBER, NUMBER fd, OPTIONAL,
	 STRING fltchain)
{
	int rc;
	int yes = 1;
	mu_stream_t src;
	char *flts = MF_OPTVAL(fltchain);
	mu_message_t msg;
	
	rc = mu_fd_stream_create(&src, NULL, _bi_io_fd(env, fd, 0),
				 MU_STREAM_READ|MU_STREAM_SEEK);
				    
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_fd_stream_create: %s",
		  mu_strerror(rc));
	mu_stream_ioctl(src, MU_IOCTL_FD, MU_IOCTL_FD_SET_BORROW, &yes);
	if (flts) {
		struct mu_wordsplit ws;
		mu_stream_t flt;
		
		rc = mu_wordsplit(flts, &ws, MU_WRDSF_DEFFLAGS);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_wordsplit: %s",
			  mu_wordsplit_strerror(&ws));
		rc = mu_filter_chain_create(&flt, src, MU_FILTER_ENCODE,
					    MU_STREAM_READ,
					    ws.ws_wordc, ws.ws_wordv);
		mu_wordsplit_free(&ws);
		MF_ASSERT(rc == 0,
			  mfe_failure,
			  "mu_filter_chain_create(\"%s\"): %s",
			  flts,
			  mu_strerror(rc));
		mu_stream_unref(src);
		src = flt;
	}

	msg = MF_STREAM_TO_MESSAGE(src);

	rc = bi_message_register(env, NULL, msg, MF_MSG_STANDALONE);
	if (rc < 0) {
		mu_message_destroy(&msg, mu_message_get_owner(msg));
		MF_THROW(mfe_failure,
			 _("no more message slots available"));
	}
	MF_RETURN(rc);
}
END

#if MAILUTILS_VERSION_MAJOR == 3 && MAILUTILS_VERSION_MINOR < 13

/*
 * Before mailutils commit 62a81295d771a9ca90e617b13992279932211b78,
 * mu_filter_chain_create would (1) decrement the reference counter of
 * its transport argument in case of errors, except (2) if the error
 * was caused by failure of the filter initialization function.
 * This exception is due to another bug, which happened to compensate
 * for the first one and which was fixed by mailutils commit
 * 231194ebb67b2c2865b8d4e82bd5379761742ec1, 
 *
 * See the following link for detailed description of both bugs:
 * http://git.savannah.gnu.org/cgit/mailutils.git/plain/libmailutils/tests/fltcnt.c?id=231194ebb67b2c2865b8d4e82bd5379761742ec1.
 *
 * To illustrate this on the example of ICONV filter, invoked by
 * charset_setup:
 *
 *   - If all arguments are OK, the function creates the filter, and
 *     increments transport reference counter by 1.
 *   - If unsupported encoding was given, transport reference counter
 *     is decremented by one.
 *   - If unsupported fallback method is given, reference counter
 *     is not changed.
 * 
 * The following workaround ensures the proper behavior:
 *
 *   - On success, reference counter increases by one.
 *   - One error, it remains unchanged.
 */

# include <mailutils/sys/stream.h>

static int
mfl_filter_chain_create(mu_stream_t *pret, mu_stream_t transport,
			int defmode, int flags,
			size_t argc, char **argv)
{
	int rc;
	int ref_count = transport->ref_count;	

	/*
	 * Increase reference counter to prevent transport from being
	 * freed as a result of bug [1].
	 */
	mu_stream_ref(transport);
	rc = mu_filter_chain_create(pret, transport, defmode, flags,
				    argc, argv);
	if (rc == 0)
		/* On success, decrement the counter. */
		mu_stream_unref(transport);
	else    /* We've hit bug [2].  Decrement the counter. */
		if (transport->ref_count > ref_count)
			mu_stream_unref(transport);
	return rc;
}
#else
static int
mfl_filter_chain_create(mu_stream_t *pret, mu_stream_t transport,
			int defmode, int flags,
			size_t argc, char **argv)
{
	return mu_filter_chain_create(pret, transport, defmode, flags,
				      argc, argv);
}
#endif

static int
mime_decode_filter(mu_stream_t *retflt, mu_stream_t stream, mu_message_t msg)
{
	int rc;
	mu_header_t hdr;
	char *encoding = NULL;
	
	/* Get the headers. */
	rc = mu_message_get_header(msg, &hdr);
	if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR, "mu_message_get_header", NULL, rc);
		return rc;
	}

	/* Filter the stream through the appropriate decoder. */
	rc = mu_header_aget_value_unfold(hdr,
					 MU_HEADER_CONTENT_TRANSFER_ENCODING,
					 &encoding);
	switch (rc) {
	case 0:
		mu_rtrim_class(encoding, MU_CTYPE_SPACE);
		break;
		
	case MU_ERR_NOENT:
		encoding = NULL;
		break;
		
	default:
		mu_diag_funcall(MU_DIAG_ERROR, "mu_header_aget_value_unfold",
				NULL, rc);
		return rc;
	}

	if (encoding == NULL || *encoding == '\0') {
		/* No need to filter */;
		mu_stream_ref(stream);
		*retflt = stream;
		rc = 0;
	} else if ((rc = mu_filter_create(retflt, stream, encoding,
					  MU_FILTER_DECODE,
					  MU_STREAM_READ)) == 0) {
		/* success */;
	} else if (rc == MU_ERR_NOENT) {
		mu_error("unknown encoding: %s", encoding);
	} else {
		mu_diag_funcall(MU_DIAG_ERROR,
				"mu_filter_create", encoding, rc);
	}

	free(encoding);
	return rc;
}

static int
mime_charset_filter(mu_stream_t *retflt, mu_stream_t stream,
		    mu_message_t msg, char const *charset,
		    char const *charset_fallback)
{
	int rc;
	mu_header_t hdr;
	mu_content_type_t ct;
	char *buf = NULL;
	
	rc = mu_message_get_header(msg, &hdr);
	if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR, "mu_message_get_header", NULL, rc);
		return rc;
	}

	/* Read and parse the Content-Type header. */
	rc = mu_header_aget_value_unfold(hdr, MU_HEADER_CONTENT_TYPE, &buf);
	if (rc == MU_ERR_NOENT) {
		buf = strdup("text/plain");
		if (!buf)
			return errno;
	} else if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR,
				"mu_header_aget_value_unfold", NULL, rc);
		return rc;
	}

#if MAILUTILS_VERSION_MAJOR > 3 || MAILUTILS_VERSION_MINOR >= 10
	rc = mu_content_type_parse_ext(buf, NULL,
				       MU_CONTENT_TYPE_RELAXED |
				       MU_CONTENT_TYPE_PARAM, &ct);
#else
	rc = mu_content_type_parse(buf, NULL, &ct);
#endif
	if (rc) {
		mu_diag_funcall(MU_DIAG_ERROR,
				"mu_content_type_parse_ext", buf, rc);
		free(buf);
		return rc;
	}
	free(buf);
	buf = NULL;

	/* Convert the content to the requested charset. */
	struct mu_mime_param *param;
	if (charset && charset[0]
	    && mu_assoc_lookup(ct->param, "charset", &param) == 0
	    && mu_c_strcasecmp(param->value, charset)) {
		char const *argv[] = { "iconv", NULL, NULL, NULL, NULL };
		int argc = 1;
		int rc;
      
		argv[argc++] = param->value;
		if (charset) {
			argv[argc++] = charset;
			if (charset_fallback)
				argv[argc++] = charset_fallback;
		}
		rc = mfl_filter_chain_create(retflt, stream,
					     MU_FILTER_ENCODE,
					     MU_STREAM_READ,
					     argc, (char**) argv);
		if (rc)	{
			//FIXME
			mu_error(_("can't convert from charset %s to %s: %s"),
				 param->value, charset, mu_strerror (rc));
			return rc;
		}
	} else {
		mu_stream_ref(stream);
		*retflt = stream;
	}
	mu_content_type_destroy(&ct);
	return rc;
}

struct filter_closure {
	mu_message_t msg;
	char *errmsg;
};

static int
fltfunc(mu_stream_t *dst,
	mu_stream_t src,
	int mode,
	int flags,
	size_t argc,
	char **argv,
	void *closure)
{
	struct filter_closure *cls = closure;
	int rc = ENOSYS;
	
	if (strcmp(argv[0], "mimedecode") == 0) {
		rc = mime_decode_filter(dst, src, cls->msg);
	} else if (strcmp(argv[0], "charset") == 0) {
		char *charset = NULL;
		char *charset_fallback = NULL;
		
		switch (argc) {
		case 3:
			charset_fallback = argv[2];
		case 2:
			charset = argv[1];
		case 1:
			break;
		default:
			return MU_ERR_FAILURE; //FIXME: error code
		}
		rc = mime_charset_filter(dst, src,
					 cls->msg,
					 charset,
					 charset_fallback);
	}
	return rc;
}

static void
errfunc(int ec, char const *errmsg, char const *input, int start, int end,
	void *closure)
{
	struct filter_closure *cls = closure;
	size_t n = 0;
	cls->errmsg = NULL;
	mu_asnprintf(&cls->errmsg, &n, "%s, near %.*s",
		     errmsg, end - start, input + start);
}

static void
free_errmsg_ptr(void *ptr)
{
	free(*(void**)ptr);
}

/* string message_body_to_stream(number fd, number msg; string mu_filter_chain) */
MF_DEFUN(message_body_to_stream, VOID, NUMBER fd, NUMBER nmsg, OPTIONAL,
	 STRING fltchain)
{
	int rc;
	int yes = 1;
	mu_stream_t dst;
	mu_stream_t src = NULL;
	char *flts = MF_OPTVAL(fltchain);
	
	DCL_BODY_HDR(mp, nmsg);
	
	rc = mu_fd_stream_create(&dst, NULL, _bi_io_fd(env, fd, 1),
				 MU_STREAM_WRITE);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_fd_stream_create: %s",
		  mu_strerror(rc));
	mu_stream_ioctl(dst, MU_IOCTL_FD, MU_IOCTL_FD_SET_BORROW, &yes);
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, dst, _builtin_stream_cleanup);

	rc = mu_body_get_streamref(mp->body, &src);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_body_get_streamref: %s",
		  mu_strerror(rc));

	if (flts) {
		mu_stream_t flt;
		struct filter_closure cls;
		cls.msg = mp->msg;
		cls.errmsg = NULL;

		MF_DCL_CLEANUP(CLEANUP_ALWAYS, &cls.errmsg, free_errmsg_ptr);

		rc = mfl_filter_pipe_create(&flt, src,
					    MU_FILTER_ENCODE,
					    MU_STREAM_READ,
					    flts,
					    fltfunc,
					    errfunc,
					    &cls);
		mu_stream_unref(src);
		if (rc == 0) {
			src = flt;
		} else {
			MF_THROW(mfe_failure, "%s", cls.errmsg);
		}
	}

	rc = mu_stream_copy(dst, src, 0, NULL);

	mu_stream_unref(src);

	switch (rc) {
	case 0:
		break;

	case EILSEQ:
		MF_THROW(mfe_ilseq,
			 "illegal byte sequence");
		break;

	default:
		MF_THROW(mfe_failure,
			 "mu_stream_copy: %s",
			 mu_strerror(rc));
	}
}
END

MF_DEFUN(message_content_type, STRING, NUMBER nmsg)
{
	int rc;
	char *buf;
	mu_content_type_t ct;
	DCL_HDR(mp, nmsg);

	rc = mu_header_aget_value_unfold(mp->hdr, MU_HEADER_CONTENT_TYPE, &buf);
	switch (rc) {
	case 0:
		break;

	case MU_ERR_NOENT:
		MF_RETURN("text/plain");
		
	default:
		MF_THROW(mfe_failure,
			 "mu_header_aget_value_unfold: %s",
			 mu_strerror(rc));
	}
	
#if MAILUTILS_VERSION_MAJOR > 3 || MAILUTILS_VERSION_MINOR >= 10
	rc = mu_content_type_parse_ext(buf, NULL,
				       MU_CONTENT_TYPE_RELAXED |
				       MU_CONTENT_TYPE_PARAM, &ct);
#else
	rc = mu_content_type_parse(buf, NULL, &ct);
#endif
	free(buf);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_content_type_parse_ext: %s", mu_strerror(rc));
	
	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(ct->type);
	MF_OBSTACK_1GROW('/');
	MF_OBSTACK_GROW(ct->subtype);
	mu_content_type_destroy(&ct);
	MF_OBSTACK_1GROW(0);
	MF_RETURN_OBSTACK();	
}
END

MF_INIT([<
	 mf_add_runtime_params(msg_cfg_param);
	 >])
