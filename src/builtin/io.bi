/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <mflib/status.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "global.h"
#include "msg.h"

static size_t nstreams = MAX_IOSTREAMS;

static struct mu_cfg_param io_cfg_param[] = {
	{ "max-streams", mu_c_size, &nstreams, 0, NULL,
	  N_("Maximum number of stream descriptors.") },
	{ NULL }
};

enum buffering {
	buf_none,
	buf_full,
	buf_line
};

struct io_stream {
	char *name;              /* Stream name */
	int fd;                  /* File descriptor */
	pid_t pid;               /* PID of the associated process, if any */
	struct io_stream *strout;/* Output stream */
	enum buffering buf_type; /* Buffering type */
	char *buf;               /* I/O buffer */
	size_t buf_size;         /* Buffer capacity */
	size_t buf_len;          /* Number of bytes available in buffer */
	size_t buf_pos;          /* Current position in buffer */
	int buf_dirty;           /* 1 if the buffer was modified */
	int (*shutdown)(struct io_stream *, int what);
	void (*cleanup)(void*);
	void *cleanup_data;
	char *delim;
};

static struct io_stream *
io_strout(struct io_stream *str)
{
	return (str->strout != NULL) ? str->strout : str;
}

static int
full_write(struct io_stream *str, char const *buf, size_t len)
{
	int fd = io_strout(str)->fd;
	while (len > 0) {
		ssize_t n = write(fd, buf, len);
		if (n == 0) {
			mu_error(_("%s: short write"), str->name);
			return mfe_eof;
		}
		if (n == -1) {
			mu_error(_("%s: write error: %s"),
				 str->name, mu_strerror(errno));
			return mfe_io;
		}
		len -= n;
	}
	return mfe_success;
}

static int
flush_stream(struct io_stream *str)
{
	int rc = mfe_success;
	if (str->buf_dirty) {
		rc = full_write(str, str->buf, str->buf_len);
		str->buf_dirty = 0;
	}
	str->buf_len = 0;
	str->buf_pos = 0;
	return rc;
}

static int
close_stream(struct io_stream *str)
{
	int rc;

	if ((rc = flush_stream(str)) != mfe_success)
		return rc;
	if (str->strout && (rc = close_stream(str->strout)) != mfe_success)
		return rc;
	if (str->fd != -1)
		close(str->fd);
	if (str->pid) {
		int status;
		waitpid(str->pid, &status, 0);
	}
	str->fd = -1;
	str->pid = 0;
	str->strout = NULL;
	if (str->cleanup)
		str->cleanup(str->cleanup_data);
	str->cleanup = NULL;
	str->cleanup_data = NULL;
	if (str->name) {
		free(str->name);
		str->name = NULL;
	}
	if (str->delim) {
		free(str->delim);
		str->delim = NULL;
	}
	free(str->buf);
	str->buf_type = buf_none;
	str->buf = NULL;
	str->buf_size = 0;
	str->buf_len = 0;
	str->buf_pos = 0;
	str->buf_dirty = 0;
	return mfe_success;
}

MF_VAR(io_buffering, NUMBER);
MF_VAR(io_buffer_size, NUMBER);

static void
io_set_buffer(eval_environ_t env, struct io_stream *str, long type, long size)
{
	int rc = flush_stream(str);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  str->name);
	switch (type) {
	case buf_none:
		free(str->buf);
		str->buf = NULL;
		str->buf_size = 0;
		break;

	case buf_line:
	case buf_full:
		MF_ASSERT(size > 0,
			  mfe_range,
			  _("buffer size out of allowed range"));
		if (size != str->buf_size) {
			char *p = realloc(str->buf, size);
			MF_ASSERT(p != NULL,
				  mfe_failure,
				  _("can't allocate stream buffer"));
			str->buf = p;
			str->buf_size = size;
		}
		str->buf_type = type;
		break;

	default:
		MF_THROW(mfe_range, _("bad buffering type: %ld"), type);
	}

	str->buf_len = 0;
	str->buf_pos = 0;
	str->buf_dirty = 0;
}

static inline size_t
io_buffer_avail(struct io_stream *str)
{
	return str->buf_size - str->buf_len;
}

static inline size_t
io_buffer_data(struct io_stream *str)
{
	return str->buf_len - str->buf_pos;
}

static void
io_write(eval_environ_t env, struct io_stream *iostr, char const *str, size_t size)
{
	if (iostr->buf_type == buf_none || io_strout(iostr)->fd != -1) {
		int fd = io_strout(iostr)->fd;
		while (size > 0) {
			ssize_t n = write(fd, str, size);
			if (n == 0)
				MF_THROW(mfe_eof, "short write");
			MF_ASSERT(n > 0,
				  mfe_io,
				  _("write error on %s: %s"),
				  iostr->name, mu_strerror(errno));
			size -= n;
		}
	} else if (iostr->buf_type == buf_line) {
		while (size > 0) {
			size_t len, avail;
			char *p;

			if ((p = memchr(str, '\n', size)) != NULL) {
				len = p - str + 1;
			} else {
				len = size;
			}

			avail = io_buffer_avail(iostr);
			if (avail == 0) {
				char *p = mu_2nrealloc(iostr->buf,
						       &iostr->buf_size,
						       1);
				MF_ASSERT(p != NULL,
					  mfe_failure,
					  _("cannot reallocate stream buffer"));
				iostr->buf = p;
				avail = io_buffer_avail(iostr);
			}

			if (len > avail)
				len = avail;
			memcpy(iostr->buf + iostr->buf_len, str, len);
			iostr->buf_len += len;
			iostr->buf_dirty = 1;
			if (iostr->buf[iostr->buf_len-1] == '\n') {
				int ec = flush_stream(iostr);
				MF_ASSERT(ec == mfe_success,
					  ec,
					  _("%s: error flushing stream"),
					iostr->name);
			}

			str += len;
			size -= len;
		}
	} else /* if (iostr->buf_type == buf_full) */ {
		while (size > 0) {
			size_t avail = io_buffer_avail(iostr);
			size_t len = size;

			if (avail == 0) {
				int ec = flush_stream(iostr);
				MF_ASSERT(ec == mfe_success,
					  ec,
					  _("%s: error flushing stream"),
					  iostr->name);
				avail = io_buffer_avail(iostr);
			}
			if (len > avail)
				len = avail;
			memcpy(iostr->buf + iostr->buf_len, str, len);
			iostr->buf_len += len;
			iostr->buf_dirty = 1;

			str += len;
			size -= len;
		}
	}
}

static int
io_fillbuf(eval_environ_t env, struct io_stream *iostr)
{
	if (iostr->buf_type == buf_none) {
		abort();
	} else {
		int ec = flush_stream(iostr);
		MF_ASSERT(ec == mfe_success,
			  ec,
			  _("%s: error flushing stream"), iostr->name);
		for (;;) {
			ssize_t n;
			size_t avail = io_buffer_avail(iostr);
			if (avail == 0) {
				if (iostr->buf_type == buf_full)
					break;
				else {
					char *p = mu_2nrealloc(iostr->buf,
							       &iostr->buf_size,
							       1);
					MF_ASSERT(p != NULL,
						  mfe_failure,
						  _("cannot reallocate stream buffer"));
					iostr->buf = p;
					continue;
				}
			}
			n = read(iostr->fd, iostr->buf + iostr->buf_len, avail);
			if (n == 0) {
				if (iostr->buf_len == 0)
					return 0;
				break;
			}
			MF_ASSERT(n > 0,
				  mfe_io,
				  _("read error on %s: %s"),
				  iostr->name, mu_strerror(errno));

			iostr->buf_len += n;
			if (iostr->buf_type == buf_line &&
			    memchr(iostr->buf + iostr->buf_len, '\n', n))
				break;
		}
	}
	return 1;
}

static size_t
io_read(eval_environ_t env, struct io_stream *iostr, char *str, size_t size)
{
	size_t total = 0;
	while (total < size) {
		ssize_t n;
		if (iostr->buf_type == buf_none) {
			n = read(iostr->fd, str, size - total);
			if (n == 0)
				break;
			MF_ASSERT(n > 0,
				  mfe_io,
				  _("read error on %s: %s"),
				  iostr->name, mu_strerror(errno));
		} else {
			if ((n = io_buffer_data(iostr)) == 0) {
				if (io_fillbuf(env, iostr) == 0)
					break;
				n = io_buffer_data(iostr);
			}
			if (n > size - total)
				n = size - total;
			memcpy(str, iostr->buf + iostr->buf_pos, n);
			iostr->buf_pos += n;
		}

		total += n;
	}
	return total;
}

static int
io_getc(eval_environ_t env, struct io_stream *iostr, char *retc)
{
	if (iostr->buf_type == buf_none) {
		ssize_t n = read(iostr->fd, retc, 1);
		MF_ASSERT(n >= 0,
			  mfe_io,
			  _("read error on %s: %s"),
			  iostr->name, mu_strerror(errno));
		if (n == 0)
			return 0;
	} else {
		if (io_buffer_data(iostr) == 0) {
			if (io_fillbuf(env, iostr) == 0)
				return 0;
		}
		*retc = iostr->buf[iostr->buf_pos++];
	}
	return 1;
}

static void
io_read_delim(eval_environ_t env, struct io_stream *iostr, const char *delim)
{
	size_t delim_len = strlen(delim);
	size_t i = 0;
	for (;;) {
		char c;

		if (io_getc(env, iostr, &c) == 0) {
			if (i == 0)
				MF_THROW(mfe_eof, _("EOF on %s"), iostr->name);
			break;
		}

		MF_OBSTACK_1GROW(c);
		i++;
		if (i >= delim_len &&
		    memcmp((char*)MF_OBSTACK_BASE() + i - delim_len, delim, delim_len) == 0) {
			MF_OBSTACK_RECLAIM(delim_len);
			break;
		}
	}

	MF_OBSTACK_1GROW(0);
}

static struct io_stream *io_find_avail(eval_environ_t env, int *sn);
static struct io_stream *io_get_open_stream(eval_environ_t env, long fn);


#define REDIRECT_STDIN_P(f) ((f) & (O_WRONLY|O_RDWR))
#define REDIRECT_STDOUT_P(f) (!((f) & O_WRONLY))

#define STDERR_SHUT        0
#define STDERR_NULL        1
#define STDERR_LOG         2
#define STDERR_FILE        3
#define STDERR_FILE_APPEND 4

#define LOG_TAG_PFX "mailfromd:"
#define LOG_TAG_PFX_LEN (sizeof(LOG_TAG_PFX)-1)

static void
stderr_to_log(char *arg, const char *cmd)
{
	int p[2];
	pid_t pid;

	if (pipe(p)) {
		mu_error(_("pipe failed: %s"), mu_strerror(errno));
		close(2);
		return;
	}

	pid = fork();

	if (pid == (pid_t) -1) {
		mu_error(_("fork failed: %s"), mu_strerror(errno));
		close(p[0]);
		close(p[1]);
		close(2);
		return;
	}

	/* Child */
	if (pid == 0) {
		FILE *fp;
		fd_set fdset;
		size_t len;
		char buf[1024];
		char *tag;
		int fac = mu_log_facility, pri = LOG_ERR;

		if (arg) {
			char *p = strchr(arg, '.');

			if (p)
				*p++ = 0;
			if (mu_string_to_syslog_facility(arg, &fac)) {
				mu_error(_("unknown syslog facility (%s), "
					   "redirecting stderr to %s"),
					 arg,
					 mu_syslog_facility_to_string(fac));
			}

			if (p && mu_string_to_syslog_priority(p, &pri)) {
				mu_error(_("unknown syslog priority (%s), "
					   "redirecting stderr to %s"),
					 arg,
					 mu_syslog_priority_to_string(pri));
			}
		}
		MF_DEBUG(MU_DEBUG_TRACE2,
			 ("redirecting stderr to syslog %s.%s",
			   mu_syslog_facility_to_string(fac),
			   mu_syslog_priority_to_string(pri)));

		len = strcspn(cmd, " \t");
		tag = malloc(LOG_TAG_PFX_LEN + len + 1);
		if (!tag)
			tag = (char*) cmd;
		else {
			strcpy(tag, LOG_TAG_PFX);
			memcpy(tag + LOG_TAG_PFX_LEN, cmd, len);
			tag[LOG_TAG_PFX_LEN + len] = 0;
		}
		mf_proctitle_format("%s redirector", cmd);

		FD_ZERO(&fdset);
		FD_SET(p[0], &fdset);
		logger_fdset(&fdset);
		close_fds_except(&fdset);

		fp = fdopen(p[0], "r");
		logger_open();
		while (fgets(buf, sizeof(buf), fp))
			syslog(pri, "%s", buf);
		exit(0);
	}

	/* Parent */
	close(p[0]);
	dup2(p[1], 2);
	close(p[1]);
}

static void
stderr_handler(int mode, char *arg, const char *cmd)
{
	int fd;
	int append = O_TRUNC;

	switch (mode) {
	case STDERR_SHUT:
		close(2);
		break;

	case STDERR_NULL:
		arg = "/dev/null";
	case STDERR_FILE_APPEND:
		append = O_APPEND;
	case STDERR_FILE:
		if (!arg || !*arg) {
			close(2);
			break;
		}
		MF_DEBUG(MU_DEBUG_TRACE2, ("redirecting stderr to %s", arg));
		fd = open(arg, O_CREAT|O_WRONLY|append, 0644);
		if (fd < 0) {
			mu_error(_("cannot open file %s for appending: %s"),
				 arg, mu_strerror(errno));
			close(2);
			return;
		}
		if (fd != 2) {
			dup2(fd, 2);
			close(fd);
		}
		break;

	case STDERR_LOG:
		stderr_to_log(arg, cmd);
	}
}

static void
parse_stderr_redirect(const char **pcmd, int *perr, char **parg)
{
	int err;
	size_t len;
	char *arg;
	const char *cmdline = *pcmd;

	while (*cmdline && mu_isspace(*cmdline))
		cmdline++;
	if (strncmp(cmdline, "2>file:", 7) == 0) {
		cmdline += 7;
		err = STDERR_FILE;
	} else if (strncmp(cmdline, "2>>file:", 8) == 0) {
		cmdline += 8;
		err = STDERR_FILE_APPEND;
	} else if (strncmp(cmdline, "2>null:", 7) == 0) {
		cmdline += 7;
		err = STDERR_NULL;
	} else if (strncmp(cmdline, "2>syslog:", 9) == 0) {
		cmdline += 9;
		err = STDERR_LOG;
	} else
		return;

	len = strcspn(cmdline, " \t");
	if (len > 0 && cmdline[len-1] == 0)
		return;
	if (len == 0)
		arg = NULL;
	else {
		arg = malloc(len + 1);
		if (!arg)
			return;
		memcpy(arg, cmdline, len);
		arg[len] = 0;
	}

	*pcmd = cmdline + len;
	*perr = err;
	*parg = arg;
}

static int
attach_strout(eval_environ_t env, struct io_stream *str, int fd)
{
	struct io_stream *ios;
	static char const pfx[] = "stdin of ";
	size_t len;

	if ((ios = io_find_avail(env, NULL)) == NULL) {
		return ENFILE;
	}

	len = strlen(pfx) + strlen(str->name);
	if ((ios->name = malloc(len + 1)) == NULL) {
		return errno;
	}
	strcat(strcpy(ios->name, pfx), str->name);

	ios->fd = fd;
	io_set_buffer(env, ios, MF_VAR_REF(io_buffering, long),
		      MF_VAR_REF(io_buffer_size, long));
	str->strout = ios;
	return 0;
}

static void
pipe_cleanup(void *data)
{
	int *p = data;
	close(p[0]);
	close(p[1]);
}

#define HASFD(i,n) ((i) != NULL && (i)[n] != -1)

static void
open_program_stream_ioe(eval_environ_t env,
			struct io_stream *str, const char *cmdline,
			int flags,
			int ioe[2])
{
	int rightp[2], leftp[2];
	pid_t pid;
	int err = STDERR_SHUT;
	char *arg = NULL;
	struct mu_wordsplit ws;
	char *shell;
	
	parse_stderr_redirect(&cmdline, &err, &arg);
	while (*cmdline && (*cmdline == ' ' || *cmdline == '\t'))
		cmdline++;

	MF_DCL_CLEANUP(CLEANUP_ALWAYS, arg);
	if (REDIRECT_STDIN_P(flags) && !HASFD(ioe, 0)) {
		MF_ASSERT(pipe(leftp) == 0,
			  mfe_failure,
			  _("pipe left: %s"), mu_strerror(errno));
		MF_DCL_CLEANUP(CLEANUP_THROW, leftp, pipe_cleanup);
	}

	if (REDIRECT_STDOUT_P(flags) && !HASFD(ioe, 1)) {
		MF_ASSERT(pipe(rightp) == 0,
			  mfe_failure,
			  _("pipe right: %s"), mu_strerror(errno));
		MF_DCL_CLEANUP(CLEANUP_THROW, rightp, pipe_cleanup);
	}

	switch (pid = fork()) {
		/* The child branch.  */
	case 0:
		/* attach the pipes */

		/* Right-end */
		if (HASFD(ioe, 1)) {
			dup2(ioe[1], 1);
		} else if (REDIRECT_STDOUT_P(flags)) {
			if (rightp[1] != 1)
				dup2(rightp[1], 1);
		}

		/* Left-end */
		if (HASFD(ioe, 0)) {
			dup2(ioe[0], 0);
		} else if (REDIRECT_STDIN_P(flags)) {
			if (leftp[0] != 0)
				dup2(leftp[0], 0);
		}

		if (HASFD(ioe, 2) && ioe[2] != 2)
			dup2(ioe[2], 2);
		else
			stderr_handler(err, arg, cmdline);

		/* Close unneeded descriptors */
		close_fds_above(2);

		shell = getenv("SHELL");
		if (!shell) {
			MF_DEBUG(MU_DEBUG_TRACE3, ("running %s", cmdline));
			if (mu_wordsplit(cmdline, &ws,
					 MU_WRDSF_DEFFLAGS & ~MU_WRDSF_CESCAPES)) {
				mu_error(_("cannot parse command line %s: %s"),
					 cmdline, mu_wordsplit_strerror(&ws));
				exit(127);
			}
			execvp(ws.ws_wordv[0], ws.ws_wordv);
		} else {
			char *xargv[] = {
				shell,
				"-c",
				(char*)cmdline,
				NULL
			};
			execv(shell, xargv);
		}
		mu_error(_("cannot run %s: %s"),
			 cmdline, mu_strerror(errno));
		exit(127);
		/********************/

		/* Parent branches: */
	case -1:
		/* Fork has failed */
		/* Restore things */
		MF_THROW(mfe_failure, _("fork: %s"), mu_strerror(errno));
		break;

	default:
		str->pid = pid;
		if (REDIRECT_STDOUT_P(flags) && !HASFD(ioe, 1)) {
			str->fd = rightp[0];
			close(rightp[1]);
		} else
			str->fd = -1;

		if (REDIRECT_STDIN_P(flags) && !HASFD(ioe, 0)) {
			if (str->fd == -1) {
				str->fd = leftp[1];
				str->strout = NULL;
			} else
				attach_strout(env, str, leftp[1]);
		} else
			str->strout = NULL;
	}
}

static void
open_program_stream(eval_environ_t env,
		    struct io_stream *str, const char *cmdline,
		    int flags)
{
	open_program_stream_ioe(env, str, cmdline, flags, NULL);
}

static void
open_file_stream(eval_environ_t env,
		 struct io_stream *str, const char *file, int flags)
{
	/* FIXME: file mode is hardcoded */
	if ((str->fd = open(file, flags, 0644)) == -1)
		MF_THROW(mfe_failure,
			 _("can't open %s: %s"), file, mu_strerror(errno));
}


static void
open_parsed_inet_stream(eval_environ_t env,
			struct io_stream *str,
			const char *cstr,
			char *proto, char *port, char *path,
			int flags)
{
	union {
		struct sockaddr sa;
		struct sockaddr_in s_in;
		struct sockaddr_un s_un;
		struct sockaddr_in6 s_in6;
	} addr;

	socklen_t socklen;
	int fd;
	int rc;

	if (!proto
	    || strcmp(proto, "unix") == 0 || strcmp(proto, "local") == 0) {
		struct stat st;

		MF_ASSERT(port == NULL,
			  mfe_failure,
			  _("invalid connection type: %s; "
			    "port is meaningless for UNIX sockets"),
			  cstr);

		MF_ASSERT(strlen(path) <= sizeof addr.s_un.sun_path,
			  mfe_range,
			  _("%s: UNIX socket name too long"),
			  path);

		addr.sa.sa_family = PF_UNIX;
		socklen = sizeof(addr.s_un);
		strcpy(addr.s_un.sun_path, path);

		if (stat(path, &st)) {
			MF_THROW(mfe_failure,
				 _("%s: cannot stat socket: %s"),
				 path, strerror(errno));
		} else {
			/* FIXME: Check permissions? */
			MF_ASSERT(S_ISSOCK(st.st_mode),
				  mfe_failure,
				  _("%s: not a socket"),
				  path);
		}

	} else if (strcmp(proto, "inet") == 0) {
		short pnum;
		long num;
		char *p;

		addr.sa.sa_family = PF_INET;
		socklen = sizeof(addr.s_in);

		MF_ASSERT(port != NULL,
			  mfe_failure,
			  _("invalid connection type: %s; "
			    "missing port number"),
			  cstr);

		num = pnum = strtol(port, &p, 0);
		if (*p == 0) {
			MF_ASSERT(num == pnum,
				  mfe_range,
				  _("invalid connection type: "
				    "%s; bad port number"),
				  cstr);
			pnum = htons(pnum);
		} else {
			struct servent *sp = getservbyname(port, "tcp");

			MF_ASSERT(sp != NULL,
				  mfe_failure,
				  _("invalid connection type: "
				    "%s; unknown port name"),
				  cstr);
			pnum = sp->s_port;
		}

		if (!path)
			addr.s_in.sin_addr.s_addr = INADDR_ANY;
		else {
			struct hostent *hp = gethostbyname(path);
			MF_ASSERT(hp != NULL,
				  mfe_failure,
				  _("unknown host name %s"),
				  path);
			addr.sa.sa_family = hp->h_addrtype;
			switch (hp->h_addrtype) {
			case AF_INET:
				memmove(&addr.s_in.sin_addr, hp->h_addr, 4);
				addr.s_in.sin_port = pnum;
				break;

			default:
				MF_THROW(mfe_range,
					 _("invalid connection type: "
					   "%s; unsupported address family"),
					 cstr);
			}
		}
	} else if (strcmp(proto, "inet6") == 0) {
		struct addrinfo hints;
		struct addrinfo *res;

		MF_ASSERT(port != NULL,
			  mfe_failure,
			  _("invalid connection type: %s; "
			    "missing port number"),
			  cstr);

		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_INET6;
		hints.ai_socktype = SOCK_STREAM;
		if (!path)
			hints.ai_flags |= AI_PASSIVE;

		rc = getaddrinfo(path, port, &hints, &res);

		switch (rc) {
		case 0:
			break;

		case EAI_SYSTEM:
			MF_THROW(mfe_failure,
				 _("%s:%s: cannot parse address: %s"),
				 path, port, strerror(errno));

		case EAI_BADFLAGS:
		case EAI_SOCKTYPE:
			MF_THROW(mfe_failure,
				 _("%s:%d: internal error converting %s:%s"),
				 __FILE__, __LINE__, path, port);

		case EAI_MEMORY:
			mu_alloc_die();

		default:
			MF_THROW(mfe_failure,
				 "%s:%s: %s",
				 path, port, gai_strerror(rc));
		}

		socklen = res->ai_addrlen;
		if (socklen > sizeof(addr)) {
			freeaddrinfo(res);
			MF_THROW(mfe_failure,
				 _("%s:%s: address length too big (%lu)"),
				 path, port,
				 (unsigned long) socklen);
		}
		memcpy(&addr, res->ai_addr, res->ai_addrlen);
		freeaddrinfo(res);
	} else {
		MF_THROW(mfe_range,
			 _("unsupported protocol: %s"),
			 proto);
	}

	fd = socket(addr.sa.sa_family, SOCK_STREAM, 0);
	MF_ASSERT(fd != -1,
		  mfe_failure,
		  _("unable to create new socket: %s"),
		  strerror(errno));

	/* FIXME: Bind to the source ? */

	rc = connect(fd, &addr.sa, socklen);
	if (rc) {
		close(fd);
		MF_THROW(mfe_failure,
			 _("cannot connect to %s: %s"),
			 cstr, strerror(errno));
	}

	str->fd = fd;
}

static int
shutdown_inet_stream(struct io_stream *str, int how)
{
	switch (how) {
	case 0:
		how = SHUT_RD;
		break;

	case 1:
		how = SHUT_WR;
		break;

	case 2:
		how = SHUT_RDWR;
		break;

	default:
		return EINVAL;
	}
	if (shutdown(str->fd, how))
		return errno;
	return 0;
}

static void
open_inet_stream(eval_environ_t env,
		 struct io_stream *str, const char *addr, int flags)
{
	char *proto, *port, *path;

	if (gacopyz_parse_connection(addr, &proto, &port, &path)
	    != MI_SUCCESS)
		MF_THROW(mfe_failure,
			 _("can't parse connection string %s"), addr);
	else {
		MF_DCL_CLEANUP(CLEANUP_ALWAYS, proto);
		MF_DCL_CLEANUP(CLEANUP_ALWAYS, port);
		MF_DCL_CLEANUP(CLEANUP_ALWAYS, path);
		open_parsed_inet_stream(env,
					str, addr,
					proto, port, path, flags);
		str->shutdown = shutdown_inet_stream;
	}
}


static void *
alloc_streams()
{
	struct io_stream *p, *stab = mu_calloc(nstreams, sizeof *stab);
	for (p = stab; p < stab + nstreams; p++)
		p->fd = -1;
	return stab;
}

static void
destroy_streams(void *data)
{
	struct io_stream *stab = data;
	struct io_stream *p;
	for (p = stab; p < stab + nstreams; p++) {
		close_stream(p);
		free(p->buf);
	}
	free(stab);
}

MF_DECLARE_DATA(IO, alloc_streams, destroy_streams)

static inline int
io_stream_is_avail(struct io_stream *str)
{
	return str->fd == -1 && str->pid == 0;
}

static struct io_stream *
io_find_avail(eval_environ_t env, int *sn)
{
	int i;
	struct io_stream *iotab = MF_GET_DATA;

	for (i = 0; i < nstreams; i++) {
		if (io_stream_is_avail(&iotab[i])) {
			if (sn)
				*sn = i;
			return &iotab[i];
		}
	}
	return NULL;
}

static struct io_stream *
io_get_open_stream(eval_environ_t env, long fn)
{
	struct io_stream *iotab = MF_GET_DATA;
	MF_ASSERT(fn >= 0 && fn < nstreams && !io_stream_is_avail(&iotab[fn]),
		  mfe_range,
		  _("invalid file descriptor"));
	return &iotab[fn];
}

int
_bi_io_fd(eval_environ_t env, int fn, int what)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	int descr;

	descr = what == 0 ? ios->fd : io_strout(ios)->fd;
	MF_ASSERT(descr >= 0,
		  mfe_range,
		  _("invalid file descriptor"));
	return descr;
}

MF_DEFUN(setbuf, VOID, NUMBER fn, OPTIONAL, NUMBER type, NUMBER size)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	io_set_buffer(env, ios,
		      MF_OPTVAL(type, MF_VAR_REF(io_buffering, long)),
		      MF_OPTVAL(size, MF_VAR_REF(io_buffer_size, long)));
}
END

MF_DEFUN(getbuftype, NUMBER, NUMBER fn)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	MF_RETURN(ios->buf_type);
}
END

MF_DEFUN(getbufsize, NUMBER, NUMBER fn)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	MF_RETURN(ios->buf_size);
}
END

MF_DEFUN(get_output, NUMBER, NUMBER fn)
{
	struct io_stream *iotab = MF_GET_DATA;
	struct io_stream *ios = io_get_open_stream(env, fn);
	int n;
	if (ios->strout == NULL)
		n = -1;
	else
		n = ios->strout - iotab;
	MF_RETURN(n);
}
END

static void
stream_cleanup(void *data)
{
	struct io_stream *ios = data;
	close_stream(ios);
}

MF_DEFUN(open, NUMBER, STRING name)
{
	int i;
	int flags = 0;
	void (*opf)(eval_environ_t env,
		   struct io_stream *, const char *, int) = open_file_stream;
	struct io_stream *ios;

	if ((ios = io_find_avail(env, &i)) == NULL)
		MF_THROW(mfe_failure, _("no more files available"));

	MF_DEBUG(MU_DEBUG_TRACE1, ("opening stream %s", name));
	ios->name = mu_strdup(name);
	ios->delim = NULL;
	if (*name == '>') {
		flags |= O_RDWR|O_CREAT;
		name++;
		if (*name == '>') {
			flags |= O_APPEND;
			name++;
		} else
			flags |= O_TRUNC;
	} else if (*name == '|') {
		opf = open_program_stream;
		flags = O_WRONLY;
		name++;
		if (*name == '&') {
			flags = O_RDWR;
			name++;
		} else if (*name == '<') {
			flags = O_RDONLY;
			name++;
		}
	} else if (*name == '@') {
		name++;
		opf = open_inet_stream;
		flags = O_RDWR;
	} else
		flags = O_RDONLY;

	for (;*name && mu_isspace(*name); name++)
		;

	MF_DCL_CLEANUP(CLEANUP_THROW, ios, stream_cleanup);
	opf(env, ios, name, flags);
	io_set_buffer(env, ios, MF_VAR_REF(io_buffering, long),
		      MF_VAR_REF(io_buffer_size, long));

	MF_DEBUG(MU_DEBUG_TRACE1, ("open(%s) = %d", name, i));
	MF_RETURN(i);
}
END

MF_DEFUN(spawn, NUMBER, STRING name, OPTIONAL,
	 NUMBER fin, NUMBER fout, NUMBER ferr)
{
	int i;
	struct io_stream *ios;
	int ioe[3];
	int flags;

	if ((ios = io_find_avail(env, &i)) == NULL)
		MF_THROW(mfe_failure, _("no more files available"));

	MF_DEBUG(MU_DEBUG_TRACE1, ("spawning %s", name));
	ios->name = mu_strdup(name);
	ios->delim = NULL;
	MF_DCL_CLEANUP(CLEANUP_THROW, ios, stream_cleanup);

	flags = O_WRONLY;
	if (*name == '|')
		name++;
	if (*name == '&') {
		flags = O_RDWR;
		name++;
	} else if (*name == '<') {
		flags = O_RDONLY;
		name++;
	}

	for (;*name && mu_isspace(*name); name++)
		;

	if (MF_DEFINED(fin))
		ioe[0] = _bi_io_fd(env, MF_OPTVAL(fin), 0);
	else
		ioe[0] = -1;
	if (MF_DEFINED(fout))
		ioe[1] = _bi_io_fd(env, MF_OPTVAL(fout), 1);
	else
		ioe[1] = -1;
	if (MF_DEFINED(ferr))
		ioe[2] = _bi_io_fd(env, MF_OPTVAL(fout), 1);
	else
		ioe[2] = -1;

	open_program_stream_ioe(env, ios, name, flags, ioe);
	io_set_buffer(env, ios, MF_VAR_REF(io_buffering, long),
		      MF_VAR_REF(io_buffer_size, long));
	MF_DEBUG(MU_DEBUG_TRACE1, ("spawn(%s) = %d", name, i));
	MF_RETURN(i);
}
END

MF_DSEXP
MF_DEFUN(tempfile, NUMBER, OPTIONAL, STRING tempdir)
{
	struct io_stream *ios;
	int i;
	char *dir = MF_OPTVAL(tempdir, "/tmp");
	size_t dirlen = strlen(dir);
	mode_t u;
	int fd;
	char *template;
#define PATTERN "mfdXXXXXX"

	if ((ios = io_find_avail(env, &i)) == NULL)
		MF_THROW(mfe_failure, _("no more files available"));

	while (dirlen > 0 && dir[dirlen-1] == '/')
		dirlen--;

	template = MF_ALLOC_HEAP_TEMP((dirlen ? dirlen + 1 : 0) +
				      sizeof(PATTERN));
	if (dirlen) {
		memcpy(template, dir, dirlen);
		template[dirlen++] = '/';
	}
	strcpy(template + dirlen, PATTERN);
	u = umask(077);
	fd = mkstemp(template);
	umask(u);
	MF_ASSERT(fd >= 0,
		  mfe_failure,
		  "mkstemp failed: %s",
		  mu_strerror(errno));
	unlink(template);

	ios->fd = fd;
	ios->name = mu_strdup(template);
	io_set_buffer(env, ios, MF_VAR_REF(io_buffering, long),
		      MF_VAR_REF(io_buffer_size, long));

	MF_RETURN(i);
#undef PATTERN
}
END

MF_DEFUN(close, VOID, NUMBER fn)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	int rc;

	rc = close_stream(ios);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  ios->name);
}
END

static struct builtin_const_trans shutdown_modes[] = {
	MF_TRANS(SHUT_RD),
	MF_TRANS(SHUT_WR),
	MF_TRANS(SHUT_RDWR)
};

MF_DEFUN(shutdown, VOID, NUMBER fn, NUMBER how)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	int mode;
	int rc;

	MF_ASSERT(how >= 0 && how <= 2,
		  mfe_range,
		  _("invalid file descriptor"));
	MF_ASSERT(_builtin_const_to_c(shutdown_modes,
				      MU_ARRAY_SIZE(shutdown_modes),
				      how,
				      &mode) == 0,
		  mfe_failure,
		  "bad shutdown mode");

	rc = flush_stream(ios);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  ios->name);
	if (ios->shutdown) {
		int rc = ios->shutdown(ios, mode);
		MF_ASSERT(rc == 0,
			  mfe_io,
			  "shutdown failed: %s",
			  mu_strerror(rc));
	} else {
		switch (how) {
		case _MFL_SHUT_RDWR:
			close_stream(ios);
			break;

		case _MFL_SHUT_WR:
			if (ios->strout) {
				close_stream(ios->strout);
				ios->strout = NULL;
			} //FIXME: else?
			break;

		case _MFL_SHUT_RD:
			close(ios->fd);
			ios->fd = -1;
		}
	}
}
END

MF_DEFUN(write, VOID, NUMBER fn, STRING str, OPTIONAL, NUMBER size)
{
	struct io_stream *ios = io_get_open_stream(env, fn);

	MF_DEBUG(MU_DEBUG_TRACE1, ("writing %s to %lu", str, fn));
	if (!MF_DEFINED(size))
		size = strlen(str);

	io_write(env, ios, str, size);
}
END

MF_STATE(body)
MF_DEFUN(write_body, VOID, NUMBER fn, POINTER str, NUMBER n)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	io_write(env, ios, str, n);
}
END

MF_DEFUN(read, STRING, NUMBER fn, NUMBER size)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	char *s;

	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(NULL, size + 1, s);
	size = io_read(env, ios, s, size);

	/* FIXME: This is for backward compatibility. Now I'm not sure
	   this is right. */
	MF_ASSERT(size > 0, mfe_eof, _("EOF on %s"), ios->name);

	s[size] = 0;
	MF_OBSTACK_TRUNCATE(size + 1);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(rewind, VOID, NUMBER fn)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	int rc;

	rc = flush_stream(ios);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  ios->name);
	if (lseek(ios->fd, 0, SEEK_SET) == -1)
		MF_THROW(mfe_io,
			 "seek failed: %s",
			 mu_strerror(errno));
}
END

#define MINBUFSIZE 128
#define MAXBUFSIZE 65535

MF_DEFUN(copy, NUMBER, NUMBER dst, NUMBER src)
{
	struct io_stream *ios;
	int ifd, ofd;
	char *buffer;
	size_t bufsize = MAXBUFSIZE;
	char bs[MINBUFSIZE];
	off_t cur, end;
	size_t total = 0;
	ssize_t rdbytes;
	int rc;

	ios = io_get_open_stream(env, src);
	rc = flush_stream(ios);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  ios->name);
	ifd = ios->fd;

	ios = io_get_open_stream(env, dst);
	rc = flush_stream(ios);
	MF_ASSERT(rc == mfe_success,
		  rc,
		  _("%s: error flushing stream"),
		  ios->name);
	ofd = io_strout(ios)->fd;

	cur = lseek(ifd, 0, SEEK_CUR);
	if (cur != -1) {
		end = lseek(ifd, 0, SEEK_END);
		if (end != -1) {
			if (end < MAXBUFSIZE)
				bufsize = end;
			lseek(ifd, cur, SEEK_SET);
		}
	}

	for (; (buffer = malloc(bufsize)) == NULL; bufsize >>= 1)
		if (bufsize < MINBUFSIZE) {
			buffer = bs;
			bufsize = MINBUFSIZE;
			break;
		}

	while ((rdbytes = read(ifd, buffer, bufsize)) > 0) {
		char *p = buffer;
		while (rdbytes) {
			ssize_t wrbytes = write(ofd, p, rdbytes);
			if (wrbytes == -1) {
				if (buffer != bs)
					free(buffer);
				MF_THROW(mfe_io,
					 "write error: %s",
					 mu_strerror(errno));
			} else if (wrbytes == 0) {
				if (buffer != bs)
					free(buffer);
				MF_THROW(mfe_io,
					 "short write");
			}
			p += wrbytes;
			rdbytes -= wrbytes;
			total += wrbytes;
		}
	}
	if (buffer != bs)
		free(buffer);
	MF_RETURN(total);
}
END

MF_DEFUN(getdelim, STRING, NUMBER fn, STRING delim)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	MF_OBSTACK_BEGIN();
	io_read_delim(env, ios, delim);
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(getline, STRING, NUMBER fn)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	MF_OBSTACK_BEGIN();
	io_read_delim(env, ios, ios->delim ? ios->delim : "\n");
	MF_RETURN_OBSTACK();
}
END

MF_DEFUN(fd_set_delimiter, VOID, NUMBER fn, STRING delim)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	free(ios->delim);
	ios->delim = mu_strdup(delim);
}
END

MF_DEFUN(fd_delimiter, STRING, NUMBER fn, STRING delim)
{
	struct io_stream *ios = io_get_open_stream(env, fn);
	MF_RETURN(ios->delim ? ios->delim : "\n");
}
END

MF_INIT([<
	 long n;

	 mf_add_runtime_params(io_cfg_param);
	 n = buf_none;
	 ds_init_variable("io_buffering", &n);
	 n = sysconf(_SC_PAGESIZE);
	 ds_init_variable("io_buffer_size", &n);
	 >])
