/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

/* #pragma prereq <version> */
MF_PRAGMA(prereq, 1, 0)
{
	int res;
	
	if (mf_vercmp(PACKAGE_VERSION, argv[1], &res) < 0) {
		if (res == 0)
			abort();
		parse_error(_("invalid version syntax"));
	} else if (res < 0) {
		parse_error(_("this source file requires %s version %s or later,"),
			    PACKAGE_NAME, argv[1]);
		parse_error(_("but you use version %s"), PACKAGE_VERSION);
	}
}

MF_DEFUN(vercmp, NUMBER, STRING a, STRING b)
{
	int res;

	if (mf_vercmp(a, b, &res) < 0)
		MF_THROW(mfe_range,
			 _("not a valid version number: %s (argument %d)"),
			 res == 0 ? a : b, res + 1);
	MF_RETURN(res);
}
END

