#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "from.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2015-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_set_from(eval_environ_t env)
#line 19

#line 19

#line 19 "from.bi"
{
#line 19
	
#line 19

#line 19
        char *  addr;
#line 19
        char *  args;
#line 19
        
#line 19
        long __bi_argcnt;
#line 19
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 19
        get_string_arg(env, 1, &addr);
#line 19
        if (__bi_argcnt > 1)
#line 19
                get_string_arg(env, 2, &args);
#line 19
        
#line 19
        adjust_stack(env, __bi_argcnt + 1);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_from))
#line 19
		prog_trace(env, "set_from %s %s",addr, ((__bi_argcnt > 1) ? args : ""));;
#line 19

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %s",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(set_from),
	      addr);
	env_msgmod_append(env, set_from, addr, ((__bi_argcnt > 1) ? args : NULL), 0);
}

#line 32
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 32
	return;
#line 32
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
from_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 19 "from.bi"
va_builtin_install_ex("set_from", bi_set_from, 0, dtype_unspecified, 2, 1, 0|0, dtype_string, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

