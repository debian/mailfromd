#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "curhdr.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "msg.h"

/* number current_header_count([string name]) */




void
#line 25
bi_current_header_count(eval_environ_t env)
#line 25

#line 25

#line 25 "curhdr.bi"
{
#line 25
	
#line 25

#line 25
        char *  name;
#line 25
        
#line 25
        long __bi_argcnt;
#line 25
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 25
        if (__bi_argcnt > 0)
#line 25
                get_string_arg(env, 1, &name);
#line 25
        
#line 25
        adjust_stack(env, __bi_argcnt + 1);
#line 25

#line 25

#line 25
	if (builtin_module_trace(BUILTIN_IDX_curhdr))
#line 25
		prog_trace(env, "current_header_count %s",((__bi_argcnt > 0) ? name : ""));;
#line 25

{
	mu_header_t hdr;
	size_t total;
	size_t count;
	int rc;

	rc = env_get_header(env, &hdr);
		if (!(rc == 0))
#line 33
		(
#line 33
	env_throw_bi(env, mfe_failure, "current_header_count", "env_get_header: %s",mu_strerror(rc))
#line 33
)
#line 36
;
	
	rc = mu_header_get_field_count(hdr, &total);
	
		if (!(rc == 0))
#line 40
		(
#line 40
	env_throw_bi(env, mfe_failure, "current_header_count", "mu_header_get_field_count: %s",mu_strerror(rc))
#line 40
)
#line 43
;
	if ((__bi_argcnt > 0)) {
		size_t i;
		count = 0;
		for (i = 1; i <= total; i++) {
			const char *sptr;
			if (mu_header_sget_field_name(hdr, i, &sptr) == 0
			    && strcasecmp(sptr, name) == 0)
				count++;
		}
	} else
		count = total;
	
#line 55
do {
#line 55
  push(env, (STKVAL)(mft_number)(count));
#line 55
  goto endlab;
#line 55
} while (0);
}
endlab:
#line 57
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 57
	return;
#line 57
}

/* string current_header_nth_name(number index) */




void
#line 64
bi_current_header_nth_name(eval_environ_t env)
#line 64

#line 64

#line 64 "curhdr.bi"
{
#line 64
	
#line 64

#line 64
        long  index;
#line 64
        
#line 64

#line 64
        get_numeric_arg(env, 0, &index);
#line 64
        
#line 64
        adjust_stack(env, 1);
#line 64

#line 64

#line 64
	if (builtin_module_trace(BUILTIN_IDX_curhdr))
#line 64
		prog_trace(env, "current_header_nth_name %lu",index);;
#line 64

{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
		if (!(rc == 0))
#line 71
		(
#line 71
	env_throw_bi(env, mfe_failure, "current_header_nth_name", "env_get_header: %s",mu_strerror(rc))
#line 71
)
#line 74
;
	
	rc = mu_header_sget_field_name(hdr, index, &sptr);

	if (rc == MU_ERR_NOENT)
		(
#line 79
	env_throw_bi(env, mfe_not_found, "current_header_nth_name", _("requested item (#%ld) not found"),index)
#line 79
);
#line 82
	else if (rc != 0)
		(
#line 83
	env_throw_bi(env, mfe_failure, "current_header_nth_name", "mu_header_sget_field_name: %s",mu_strerror(rc))
#line 83
);
#line 86
	
#line 86
do {
#line 86
  pushs(env, sptr);
#line 86
  goto endlab;
#line 86
} while (0);
}
endlab:
#line 88
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 88
	return;
#line 88
}

/* string current_header_nth_value(number index) */




void
#line 95
bi_current_header_nth_value(eval_environ_t env)
#line 95

#line 95

#line 95 "curhdr.bi"
{
#line 95
	
#line 95

#line 95
        long  index;
#line 95
        
#line 95

#line 95
        get_numeric_arg(env, 0, &index);
#line 95
        
#line 95
        adjust_stack(env, 1);
#line 95

#line 95

#line 95
	if (builtin_module_trace(BUILTIN_IDX_curhdr))
#line 95
		prog_trace(env, "current_header_nth_value %lu",index);;
#line 95

{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
		if (!(rc == 0))
#line 102
		(
#line 102
	env_throw_bi(env, mfe_failure, "current_header_nth_value", "env_get_header: %s",mu_strerror(rc))
#line 102
)
#line 105
;
	
	rc = mu_header_sget_field_value(hdr, index, &sptr);

	if (rc == MU_ERR_NOENT)
		(
#line 110
	env_throw_bi(env, mfe_not_found, "current_header_nth_value", _("requested item (#%ld) not found"),index)
#line 110
);
#line 113
	else if (rc != 0)
		(
#line 114
	env_throw_bi(env, mfe_failure, "current_header_nth_value", "mu_header_sget_field_name: %s",mu_strerror(rc))
#line 114
);
#line 117
	
#line 117
do {
#line 117
  pushs(env, sptr);
#line 117
  goto endlab;
#line 117
} while (0);
}
endlab:
#line 119
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 119
	return;
#line 119
}

/* string current_header(string name[, number index]) */

	


void
#line 126
bi_current_header(eval_environ_t env)
#line 126

#line 126

#line 126 "curhdr.bi"
{
#line 126
	
#line 126

#line 126
        char * MFL_DATASEG name;
#line 126
        long  index;
#line 126
        
#line 126
        long __bi_argcnt;
#line 126
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 126
        get_string_arg(env, 1, &name);
#line 126
        if (__bi_argcnt > 1)
#line 126
                get_numeric_arg(env, 2, &index);
#line 126
        
#line 126
        adjust_stack(env, __bi_argcnt + 1);
#line 126

#line 126

#line 126
	if (builtin_module_trace(BUILTIN_IDX_curhdr))
#line 126
		prog_trace(env, "current_header %s %lu",name, ((__bi_argcnt > 1) ? index : 0));;
#line 126

{
	mu_header_t hdr;
	const char *sptr;
	int rc;

	rc = env_get_header(env, &hdr);
		if (!(rc == 0))
#line 133
		(
#line 133
	env_throw_bi(env, mfe_failure, "current_header", "env_get_header: %s",mu_strerror(rc))
#line 133
)
#line 136
;
	
	rc = mu_header_sget_value_n(hdr, name, ((__bi_argcnt > 1) ? index : 1), &sptr);

	if (rc == MU_ERR_NOENT)
		(
#line 141
	env_throw_bi(env, mfe_not_found, "current_header", _("requested item (%s,%ld) not found"),name,((__bi_argcnt > 1) ? index : 1))
#line 141
);
#line 144
	else if (rc != 0)
		(
#line 145
	env_throw_bi(env, mfe_failure, "current_header", "mu_header_sget_field_name: %s",mu_strerror(rc))
#line 145
);
#line 148
	
#line 148
do {
#line 148
  pushs(env, sptr);
#line 148
  goto endlab;
#line 148
} while (0);
}
endlab:
#line 150
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 150
	return;
#line 150
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
curhdr_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 25 "curhdr.bi"
va_builtin_install_ex("current_header_count", bi_current_header_count, STATMASK(smtp_state_eoh) | STATMASK(smtp_state_body) | STATMASK(smtp_state_eom), dtype_number, 1, 1, MFD_BUILTIN_CAPTURE|0, dtype_string);
#line 64 "curhdr.bi"
va_builtin_install_ex("current_header_nth_name", bi_current_header_nth_name, STATMASK(smtp_state_eoh) | STATMASK(smtp_state_body) | STATMASK(smtp_state_eom), dtype_string, 1, 0, MFD_BUILTIN_CAPTURE|0, dtype_number);
#line 95 "curhdr.bi"
va_builtin_install_ex("current_header_nth_value", bi_current_header_nth_value, STATMASK(smtp_state_eoh) | STATMASK(smtp_state_body) | STATMASK(smtp_state_eom), dtype_string, 1, 0, MFD_BUILTIN_CAPTURE|0, dtype_number);
#line 126 "curhdr.bi"
va_builtin_install_ex("current_header", bi_current_header, STATMASK(smtp_state_eoh) | STATMASK(smtp_state_body) | STATMASK(smtp_state_eom), dtype_string, 2, 1, MFD_BUILTIN_CAPTURE|0, dtype_string, dtype_number);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

