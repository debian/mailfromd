#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "email.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2010-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */


#include "mflib/email.h"

void
#line 20
bi_domainpart(eval_environ_t env)
#line 20

#line 20

#line 20 "email.bi"
{
#line 20
	
#line 20

#line 20
        char * MFL_DATASEG str;
#line 20
        
#line 20

#line 20
        get_string_arg(env, 0, &str);
#line 20
        
#line 20
        adjust_stack(env, 1);
#line 20

#line 20

#line 20
	if (builtin_module_trace(BUILTIN_IDX_email))
#line 20
		prog_trace(env, "domainpart %s",str);;
#line 20

{
	char *p = strchr(str, '@');
	
#line 23
do {
#line 23
  pushs(env, p ? p+1 : str);
#line 23
  goto endlab;
#line 23
} while (0);
}
endlab:
#line 25
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 25
	return;
#line 25
}

void
#line 27
bi_localpart(eval_environ_t env)
#line 27

#line 27

#line 27 "email.bi"
{
#line 27
	
#line 27

#line 27
        char * MFL_DATASEG str;
#line 27
        
#line 27

#line 27
        get_string_arg(env, 0, &str);
#line 27
        
#line 27
        adjust_stack(env, 1);
#line 27

#line 27

#line 27
	if (builtin_module_trace(BUILTIN_IDX_email))
#line 27
		prog_trace(env, "localpart %s",str);;
#line 27

{
	char *p = strchr(str, '@');

	if (p) {
		heap_obstack_begin(env);
		heap_obstack_grow(env, str, p - str);
		do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
		
#line 35
do {
#line 35
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 35
  goto endlab;
#line 35
} while (0);
	} else
		
#line 37
do {
#line 37
  pushs(env, str);
#line 37
  goto endlab;
#line 37
} while (0);
}
endlab:
#line 39
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 39
	return;
#line 39
}

void
#line 41
bi_dequote(eval_environ_t env)
#line 41

#line 41

#line 41 "email.bi"
{
#line 41
	
#line 41

#line 41
        char * MFL_DATASEG str;
#line 41
        
#line 41

#line 41
        get_string_arg(env, 0, &str);
#line 41
        
#line 41
        adjust_stack(env, 1);
#line 41

#line 41

#line 41
	if (builtin_module_trace(BUILTIN_IDX_email))
#line 41
		prog_trace(env, "dequote %s",str);;
#line 41

{
	size_t len = strlen(str);
	if (len > 1 && str[0] == '<' && str[len-1] == '>') {
		heap_obstack_begin(env);
		heap_obstack_grow(env, str + 1, len - 2);
		do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
		
#line 48
do {
#line 48
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 48
  goto endlab;
#line 48
} while (0);
	} else
		
#line 50
do {
#line 50
  pushs(env, str);
#line 50
  goto endlab;
#line 50
} while (0);
}
endlab:
#line 52
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 52
	return;
#line 52
}

void
#line 54
bi_email_map(eval_environ_t env)
#line 54

#line 54

#line 54 "email.bi"
{
#line 54
	
#line 54

#line 54
        char *  str;
#line 54
        
#line 54

#line 54
        get_string_arg(env, 0, &str);
#line 54
        
#line 54
        adjust_stack(env, 1);
#line 54

#line 54

#line 54
	if (builtin_module_trace(BUILTIN_IDX_email))
#line 54
		prog_trace(env, "email_map %s",str);;
#line 54

{
	mu_address_t addr;
	int f = 0;
	int rc = mu_address_create_hint(&addr, str, NULL, 0);
	if (rc)
		
#line 60
do {
#line 60
  push(env, (STKVAL)(mft_number)(0));
#line 60
  goto endlab;
#line 60
} while (0);
	if (addr->next)
		f |= EMAIL_MULTIPLE;
	if (addr->comments)
		f |= EMAIL_COMMENTS;
	if (addr->personal)
		f |= EMAIL_PERSONAL;
	if (addr->local_part)
		f |= EMAIL_LOCAL;
	if (addr->domain)
		f |= EMAIL_DOMAIN;
	if (addr->route)
		f |= EMAIL_ROUTE;
	mu_address_destroy(&addr);
	
#line 74
do {
#line 74
  push(env, (STKVAL)(mft_number)(f));
#line 74
  goto endlab;
#line 74
} while (0);
}
endlab:
#line 76
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 76
	return;
#line 76
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
email_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 20 "email.bi"
va_builtin_install_ex("domainpart", bi_domainpart, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 27 "email.bi"
va_builtin_install_ex("localpart", bi_localpart, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 41 "email.bi"
va_builtin_install_ex("dequote", bi_dequote, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 54 "email.bi"
va_builtin_install_ex("email_map", bi_email_map, 0, dtype_number, 1, 0, 0|0, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

