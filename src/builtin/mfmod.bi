/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2022-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE
MF_COND(WITH_MFMOD)

#include <mflib/status.h>
#include "global.h"
#include "mfmod/mfmod.h"
#include "msg.h"
#include <dlfcn.h>
#include <mailutils/wordsplit.h>

static size_t nmfmods = MAX_MFMODS;
char *mfmod_path = MFMODDIR;
static struct mu_cfg_param mfmod_cfg_param[] = {
	{ "max-mfmods", mu_c_size, &nmfmods, 0, NULL,
	  N_("Maximum number of dynamically loaded modules.") },
	{ "mfmod-path", mu_c_string, &mfmod_path, 0, NULL,
	  N_("Search path for dynamically loaded modules.") },
	{ NULL }
};

struct modinfo {
	void *handle;
	char *libname;
};

static inline int
mfmod_is_used(struct modinfo *dltab, int i)
{
	return i >= 0 && i < nmfmods && dltab[i].handle != NULL;
}


static void *
alloc_mfmods(void)
{
	return mu_calloc(nmfmods, sizeof(struct modinfo));
}

static struct modinfo *dltab;

#if 0
// See FIXME below

static void
mfmod_close(struct modinfo *mod)
{
	dlclose(mod->handle);
	free(mod->libname);
	memset(mod, 0, sizeof(*mod));
}

static void
destroy_mfmods(void)
{
	if (dltab) {
		size_t i;
		for (i = 0; i < nmfmods; i++) {
			if (mfmod_is_used(dltab, i))
				mfmod_close(&dltab[i]);
		}
		free(dltab);
	}
}
#endif

static struct modinfo *
get_dltab(void)
{
	if (!dltab) {
		dltab = alloc_mfmods();
		/*
		 * FIXME: Used to have atexit(destroy_mfmods) here.
		 * However, shutdown handlers, that are currently run
		 * via mu_onexit (i.e. atexit) as well get executed after
		 * destroy_mfmod, which causes trouble.
		 * I guess it's OK not to close dlhandles, since we're
		 * going to exit anyway.
		 */
	}
	return dltab;
}

static int
find_free_slot(struct modinfo *dltab)
{
	int i;
	for (i = 0; i < nmfmods; i++)
		if (!mfmod_is_used(dltab, i))
			return i;
	return -1;
}

static char *
mfmod_find(char const *name)
{
	char *retval = NULL;

	if (name[0] == '/')
		retval = mu_strdup(name);
	else {
		struct mu_wordsplit ws;
		size_t i;
	
		ws.ws_delim = ":";
		if (mu_wordsplit(mfmod_path, &ws, MU_WRDSF_DELIM|WRDSF_NOCMD|WRDSF_NOVAR)) {
			mu_error("can't split mfmod_path: %s", mu_wordsplit_strerror(&ws));
			mu_wordsplit_free(&ws);
			return NULL;
		}
		
		for (i = 0; i < ws.ws_wordc; i++) {
			char *libname = mu_make_file_name(ws.ws_wordv[0], name);
			if (access(libname, X_OK) == 0) {
				retval = libname;
				break;
			}
			free(libname);
		}
		mu_wordsplit_free(&ws);
	}
	return retval;
}

MF_DEFUN(dlopen, NUMBER, STRING libname)
{
	struct modinfo *dltab = get_dltab();
	long n = find_free_slot(dltab);
	void *dlh;
	char *filename;
	
	MF_ASSERT(n != -1,
		  mfe_failure, _("no more library slots available"));
	filename = mfmod_find(libname);
	MF_ASSERT(filename != NULL,
		  mfe_failure,
		  _("module file %s not found in mfmod_path"), libname);
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, filename, free);
	dlh = dlopen(filename, RTLD_LAZY | RTLD_LOCAL);
	MF_ASSERT(dlh != NULL, mfe_failure,
		  _("can't load %s: %s"), libname, dlerror());
	dltab[n].handle = dlh;
	dltab[n].libname = mu_strdup(libname);
	MF_RETURN(n);
}
END

static void
_cleanup_vparam(void *ptr)
{
	free(ptr);
}

static void
_cleanup_retval(void *ptr)
{
	MFMOD_PARAM *p = ptr;
	free(p->string);
}

MF_DEFUN_VARARGS_NO_PROM(dlcall, ANY, NUMBER dlh, STRING symname,
			 STRING *types)
{
	long i;
	MFMOD_FUNC func;
	struct modinfo *dltab = get_dltab();
	struct modinfo *mod;
	size_t nparam, ntypes;
	MFMOD_PARAM *vparam, retval;
	int rc;
	int varargs;
	
	MF_ASSERT(mfmod_is_used(dltab, dlh),
		  mfe_failure,
		  _("invalid dynamic library handle"));
	mod = &dltab[dlh];

	dlerror(); /* Clear error state */
	func = dlsym(mod->handle, symname);
	if (!func) {
		char *err = dlerror();
		if (err)
			MF_THROW(mfe_failure, _("can't find symbol %s in %s: %s"),
				 symname, mod->libname, err);
		else
			MF_THROW(mfe_failure, _("%s in module %s has NULL value"),
				 symname, mod->libname);
	}

	nparam = MF_VA_COUNT();
	ntypes = strlen(types);
	varargs = 0;
	if (ntypes > 1) {
		switch (types[ntypes-1]) {
		case '+':
			--ntypes;
			varargs = nparam > ntypes;
			break;
		case '*':
			--ntypes;
			varargs = nparam >= ntypes-1;
			break;
		}
	}
		
	MF_ASSERT(nparam == ntypes || varargs,
		  mfe_failure,
		  _("number of arguments doesn't satisfy type string"));

	vparam = mu_calloc(nparam, sizeof(vparam[0]));
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, vparam, _cleanup_vparam);

	MF_VA_START();
	for (i = 0; i < nparam; i++) {
		switch (types[(varargs && i >= ntypes) ? ntypes-1 : i]) {
		case 'n':
		case 'i':
		case 'd':
			vparam[i].type = mfmod_number;
			MF_VA_ARG(i, NUMBER, vparam[i].number);
			break;

		case 's':
			vparam[i].type = mfmod_string;
			MF_VA_ARG(i, STRING, vparam[i].string);
			break;

		case 'm': {
			long n;
			mu_message_t msg;
			
			MF_VA_ARG(i, NUMBER, n);
			msg = bi_message_from_descr(env, n);
			vparam[i].type = mfmod_message;
			vparam[i].message = msg;
			break;
		}
				
		default:
			MF_THROW(mfe_failure,
				 "unrecognized data type in %s at position: %ld",
				 types, i);
		}
	}
	MF_VA_END();

	rc = func(nparam, vparam, &retval);
	if (rc < 0) {
		MF_THROW(mfe_failure,
			 _("call to %s in module %s failed"), symname,
			 mod->libname);
	} else if (rc) {
		if (retval.type == mfmod_string && retval.string) {
			MF_DCL_CLEANUP(CLEANUP_THROW, &retval, _cleanup_retval);
			MF_THROW(rc,
				 _("call to %s in module %s failed: %s"),
				 symname, mod->libname, retval.string);
		} else {
			MF_THROW(rc,
				 _("call to %s in module %s failed"), symname,
				 mod->libname);
		}
	}

	switch (retval.type) {
	case mfmod_number:
		push(env, (STKVAL)(mft_number)retval.number);
		break;

	case mfmod_string:
		pushs(env, retval.string);
		break;

	case mfmod_message:
		rc = bi_message_register(env, NULL, retval.message, MF_MSG_STANDALONE);
		if (rc < 0) {
			mu_message_destroy(&retval.message, mu_message_get_owner(retval.message));
			MF_THROW(mfe_failure,
				 _("no more message slots available"));
		}
		push(env, (STKVAL)(mft_number)rc);
		break;

	default:
		MF_THROW(mfe_failure,
			 [<_("unsupported return type %d in call to %s, module %s failed")>],
			 retval.type, symname, mod->libname);
	}
}
END

int
mfmod_error(MFMOD_PARAM *r, int errcode, char const *fmt, ...)
{
	va_list ap;
	char *buf = NULL;
	size_t size = 0;
	int rc;

	va_start(ap, fmt);
	rc = mu_vasnprintf (&buf, &size, fmt, ap);
	va_end(ap);

	if (rc)
		return -1;
	r->type = mfmod_string;
	r->string = buf;
	return errcode;
}

char const *
mfmod_data_type_str(int t)
{
	static char const *type_str[] = {
		[mfmod_number] = "number",
		[mfmod_string] = "string",
		[mfmod_message] = "message"
	};
	if (t >= 0 && t < sizeof(type_str)/sizeof(type_str[0]) && type_str[t])
		return type_str[t];
	return "UNKNOWN";
}

int
mfmod_error_argtype(MFMOD_PARAM *p, MFMOD_PARAM *r, int n, int exptype)
{
	return mfmod_error(r, mfe_inval,
			   "bad type of argument #%d: expected %s, but given %s",
			   n+1, mfmod_data_type_str(exptype),
			   mfmod_data_type_str(p[n].type));
}

MF_INIT([<
	 mf_add_runtime_params(mfmod_cfg_param);
	 >])
