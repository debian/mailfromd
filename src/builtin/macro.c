#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "macro.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



/* #pragma miltermacros <handler> <name> [name...] */
#line 20 "macro.bi"

#line 20
static void _pragma_miltermacros (int argc, char **argv, const char *text)
#line 20

{
	enum smtp_state state;
	int i = 1;
	
	state = string_to_state(argv[i]);
	if (state == smtp_state_none) {
		parse_error(_("unknown smtp state tag: %s"), argv[i]);
		return;
	}
	for (i++; i < argc; i++)
		register_macro(state, argv[i]);
}

void
#line 34
bi_getmacro(eval_environ_t env)
#line 34

#line 34

#line 34 "macro.bi"
{
#line 34
	
#line 34

#line 34
        char * MFL_DATASEG name;
#line 34
        
#line 34

#line 34
        get_string_arg(env, 0, &name);
#line 34
        
#line 34
        adjust_stack(env, 1);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_macro))
#line 34
		prog_trace(env, "getmacro %s",name);;
#line 34

{
	const char *s = env_get_macro(env, name);
		if (!(s))
#line 37
		(
#line 37
	env_throw_bi(env, mfe_macroundef, "getmacro", _("macro not defined: %s"),name)
#line 37
)
#line 39
;
	
#line 40
do {
#line 40
  pushs(env, s);
#line 40
  goto endlab;
#line 40
} while (0);
}
endlab:
#line 42
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 42
	return;
#line 42
}

void
#line 44
bi_macro_defined(eval_environ_t env)
#line 44

#line 44

#line 44 "macro.bi"
{
#line 44
	
#line 44

#line 44
        char *  name;
#line 44
        
#line 44

#line 44
        get_string_arg(env, 0, &name);
#line 44
        
#line 44
        adjust_stack(env, 1);
#line 44

#line 44

#line 44
	if (builtin_module_trace(BUILTIN_IDX_macro))
#line 44
		prog_trace(env, "macro_defined %s",name);;
#line 44

{
	int ret = !!env_get_macro(env, name);
	
#line 47
do {
#line 47
  push(env, (STKVAL)(mft_number)(ret));
#line 47
  goto endlab;
#line 47
} while (0);
}
endlab:
#line 49
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 49
	return;
#line 49
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
macro_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 20 "macro.bi"

#line 20
install_pragma("miltermacros", 3, 0, _pragma_miltermacros);
#line 34 "macro.bi"
va_builtin_install_ex("getmacro", bi_getmacro, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 44 "macro.bi"
va_builtin_install_ex("macro_defined", bi_macro_defined, 0, dtype_number, 1, 0, 0|0, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

