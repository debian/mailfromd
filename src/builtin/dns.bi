/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <inttypes.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <limits.h>
#include "srvcfg.h"
#include "global.h"
#include <mflib/status.h>

MF_DEFUN(primitive_hostname, STRING, STRING string)
{
	char *hbuf;
	mf_status stat;

	stat = resolve_ipstr(string, &hbuf);
	MF_ASSERT(stat == mf_success,
		  mf_status_to_exception(stat),
		  _("cannot resolve IP %s"),
		  string);

	pushs(env, hbuf);
	free(hbuf);
}
END

static int
has_suffix(char const *str, char const *suf)
{
	size_t len = strlen(str);
	size_t slen = strlen(suf);
	return len > slen && memcmp(str + len - slen, suf, slen) == 0;
}

static void
cleanup_reply(void *reply)
{
	dns_reply_free(reply);
}

MF_DSEXP
MF_DEFUN(primitive_resolve, STRING, STRING string, OPTIONAL, STRING domain,
	NUMBER resolve_family)
{
	struct dns_reply reply;
	if (MF_OPTVAL(domain,"")[0]) {
		dns_status status;
		char ipstr[IPMAX_INADDR_BUFSIZE];

		if (strcasecmp(domain, IPV4_INADDR_DOMAIN) == 0 ||
		    strcasecmp(domain, IPV6_INADDR_DOMAIN) == 0) {
			status = dns_reverse_name(string, ipstr, sizeof(ipstr));
			MF_ASSERT(status == dns_success,
				  mf_status_to_exception(dns_to_mf_status(status)),
				  _("cannot create reverse name from %s and %s"),
				  string, domain);
			MF_ASSERT(has_suffix(ipstr, domain),
				  dns_failure,
				  _("domain %s is not suitable for PTR record %s"),
				  domain, string);
			status = ptr_lookup(ipstr, &reply);
			MF_ASSERT(status == dns_success,
				  mf_status_to_exception(dns_to_mf_status(status)),
				  _("cannot resolve host name %s"), ipstr);
		} else if (dns_str_is_ipv4(string) || dns_str_is_ipv6(string)) {
			char *qname;
			int n = dns_reverse_ipstr(string, ipstr, sizeof(ipstr));
			MF_ASSERT(n > 0,
				  dns_failure,
				  _("cannot reverse IP %s"),
				  string);
			qname = MF_ALLOC_HEAP_TEMP(n + strlen(domain));
			memcpy(qname, ipstr, n);
			strcpy(qname + n, domain);
			status = a_lookup(qname, &reply);
			if (status == dns_not_found) {
				status = aaaa_lookup(qname, &reply);
			}
			MF_ASSERT(status == dns_success,
				  mf_status_to_exception(dns_to_mf_status(status)),
				  _("cannot resolve host name %s"), ipstr);
		} else {
			char *qname;
			qname = MF_ALLOC_HEAP_TEMP(strlen(string) + strlen(domain) + 2);
			strcpy(qname, string);
			strcat(qname, ".");
			strcat(qname, domain);
			string = qname;
			goto resolve;
		}
		MF_DCL_CLEANUP(CLEANUP_ALWAYS, &reply, cleanup_reply);
		switch (reply.type) {
		case dns_reply_str:
			MF_RETURN(reply.data.str[0]);
		case dns_reply_ip:
			MF_RETURN(inet_ntop(AF_INET, &reply.data.ip[0], ipstr,
					    sizeof(ipstr)));
		case dns_reply_ip6:
			MF_RETURN(inet_ntop(AF_INET6, &reply.data.ip6[0], ipstr,
					    sizeof(ipstr)));
		default:
			runtime_error(env, _("bad DNS reply type: %d"), reply.type);
		}
	} else {
		char *ipstr;
		mf_status status;
		int res;
resolve:
		res = MF_OPTVAL(resolve_family, _MFL_RESOLVE_DFL);
		if (res == _MFL_RESOLVE_DFL) {
			res = (MF_SOCKET_FAMILY() == AF_INET)
				? resolve_ip4 : resolve_ip6;
		} else if (res != _MFL_RESOLVE_IP4 && res != _MFL_RESOLVE_IP6) {
			MF_THROW(mfe_inval,
				 _("invalid resolve family value"));
		}
		status = resolve_hostname(string, res, &ipstr);
		MF_ASSERT(status == mf_success,
			  mf_status_to_exception(status),
			  _("cannot resolve %s"), string);
		MF_DCL_CLEANUP(CLEANUP_ALWAYS, ipstr, free);
		MF_RETURN(ipstr);
	}
}
END

MF_DEFUN(primitive_hasmx, NUMBER, STRING string)
{
	struct dns_reply repl;
	mf_status mxstat;

	mxstat = dns_to_mf_status(mx_lookup(string, resolve_none, &repl));

	MF_ASSERT(mxstat == mf_success || mxstat == mf_not_found,
		  mf_status_to_exception(mxstat),
		  _("cannot get MX records for %s"),
		  string);
	if (mxstat == mf_success) {
		dns_reply_free(&repl);
		MF_RETURN(1);
	}
	MF_RETURN(0);
}
END

static dns_status
resolve_host(const char *string, int family, struct dns_reply *reply, int *isip)
{
	struct addrinfo *res, hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_flags = AI_NUMERICHOST;
	hints.ai_family = family;
	if (getaddrinfo(string, NULL, &hints, &res) ==  0) {
		if (isip)
			*isip = 1;
		dns_reply_init(reply,
			       res->ai_family == AF_INET
				 ? dns_reply_ip : dns_reply_ip6, 1);
		switch (res->ai_family) {
		case AF_INET:
			reply->data.ip[0] = ((struct sockaddr_in*)res->ai_addr)->sin_addr;
			break;

		case AF_INET6:
			reply->data.ip6[0] = ((struct sockaddr_in6*)res->ai_addr)->sin6_addr;
			break;
		}
		freeaddrinfo(res);
		return dns_success;
	} else if (isip)
		*isip = 0;

	return (family == AF_INET6 ? aaaa_lookup : a_lookup)(string, reply);
}

static int
dns_replies_intersect(struct dns_reply const *a, struct dns_reply const *b)
{
	int i, j;

	if (a->type == b->type) {
		switch (a->type) {
		case dns_reply_str:
			for (i = 0; i < a->count; i++)
				for (j = 0; j < b->count; j++)
					if (strcmp(a->data.str[i], b->data.str[j]) == 0)
						return 1;
			break;

		case dns_reply_ip:
			for (i = 0; i < a->count; i++)
				for (j = 0; j < b->count; j++)
					if (a->data.ip[i].s_addr == b->data.ip[j].s_addr)
						return 1;
			break;

		case dns_reply_ip6:
			for (i = 0; i < a->count; i++)
				for (j = 0; j < b->count; j++)
					if (memcmp(&a->data.ip6[i],
						   &b->data.ip6[j],
						   sizeof(a->data.ip6[0])) == 0)
						return 1;
			break;
		}
	}
	return 0;
}

MF_DEFUN(primitive_ismx, NUMBER, STRING domain, STRING ipstr)
{
	struct dns_reply areply, mxreply;
	dns_status status;
	int ip;
	int rc = 0;
	int not_found = 0;

	/*
	 * FIXME: The approach below results in two DNS MX queries.
	 * Two ways to avoid it: (1) cache hostnames obtained from the
	 * first MX query and reuse them later to query AAAA records,
	 * and (2) combine dns_reply_ip and dns_reply_ip6 into single
	 * reply type and introduce a resolve_any constant, meaning
	 * "resolve to IPv4 or IPv6".
	 */
	status = resolve_host(ipstr, AF_INET, &areply, &ip);
	switch (status) {
	case dns_success:
		status = mx_lookup(domain, resolve_ip4, &mxreply);
		switch (status) {
		case dns_success:
			rc = dns_replies_intersect(&areply, &mxreply);
			break;
		case dns_not_found:
			not_found = 1;
			break;
		default:
			dns_reply_free(&areply);
			MF_THROW(mf_status_to_exception(dns_to_mf_status(status)),
				 _("cannot get MXs for %s"), domain);
		}
		dns_reply_free(&areply);
		dns_reply_free(&mxreply);
		break;
	case dns_not_found:
		break;
	default:
		MF_THROW(mf_status_to_exception(dns_to_mf_status(status)),
			 _("cannot resolve host name %s"), ipstr);
	}

	if (rc == 0 && !ip) {
		status = resolve_host(ipstr, AF_INET6, &areply, NULL);
		switch (status) {
		case dns_success:
			status = mx_lookup(domain, resolve_ip6, &mxreply);
			switch (status) {
			case dns_success:
				rc = dns_replies_intersect(&areply, &mxreply);
				break;
			case dns_not_found:
				if (not_found)
					MF_THROW(mf_status_to_exception(mfe_not_found),
						 _("%s has no MX records"),
						 ipstr);
				break;
			default:
				dns_reply_free(&areply);
				MF_THROW(mf_status_to_exception(dns_to_mf_status(status)),
					 _("cannot resolve host name %s"), ipstr);
			}
			dns_reply_free(&areply);
			dns_reply_free(&mxreply);
			break;
		case dns_not_found:
			break;
		default:
			MF_THROW(mf_status_to_exception(dns_to_mf_status(status)),
				 _("cannot resolve host name %s"), ipstr);
		}
	}
	MF_RETURN(rc);
}
END

MF_DEFUN(relayed, NUMBER, STRING s)
{
	MF_RETURN(relayed_domain_p(s));
}
END

MF_DEFUN(ptr_validate, NUMBER, STRING s)
{
	int rc, res;
	switch (rc = ptr_validate(s, NULL)) {
	case dns_success:
		res = 1;
		break;
	case dns_not_found:
		res = 0;
		break;
	default:
		MF_THROW(mf_status_to_exception(dns_to_mf_status(rc)),
			 _("failed to get PTR record for %s"), s);
	}
	MF_RETURN(res);
}
END

MF_DEFUN(primitive_hasns, NUMBER, STRING dom)
{
	struct dns_reply repl;
	mf_status stat = dns_to_mf_status(ns_lookup(dom, resolve_none, &repl));
	MF_ASSERT(stat == mf_success || stat == mf_not_found,
		  mf_status_to_exception(stat),
		  _("cannot get NS records for %s"),
		  dom);
	if (stat == mf_success) {
		dns_reply_free(&repl);
		MF_RETURN(1);
	}
	MF_RETURN(0);
}
END

static int
cmp_ip(void const *a, void const *b)
{
	uint32_t ipa = ntohl(((struct in_addr*)a)->s_addr);
	uint32_t ipb = ntohl(((struct in_addr*)b)->s_addr);
	if (ipa < ipb)
		return -1;
	if (ipa > ipb)
		return 1;
	return 0;
}

static int
cmp_ip6(void const *a, void const *b)
{
	return memcmp(a, b, sizeof(struct in6_addr));
}

static int
cmp_str(void const *a, void const *b)
{
	char * const *stra = a;
	char * const *strb = b;
	return strcmp(*stra, *strb);
}

static int
cmp_hostname(const void *a, const void *b)
{
	return strcasecmp(*(const char**) a, *(const char**) b);
}

typedef long DNS_REPLY_COUNT;
#define DNS_REPLY_MAX LONG_MAX

struct dns_reply_storage {
	DNS_REPLY_COUNT reply_count;
	size_t reply_max;
	struct dns_reply *reply_tab;
};

/*
 * Return true if N is in valid range for accessing reply storage RS and
 * the entry it refers to is in use.
 */
static inline int
dns_reply_entry_ok(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	return n >= 0 && n < rs->reply_count && rs->reply_tab[n].type != -1;
}

static inline void
dns_reply_entry_release(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	rs->reply_tab[n].type = -1;
}

static inline struct dns_reply *
dns_reply_entry_locate(struct dns_reply_storage *rs, DNS_REPLY_COUNT n)
{
	return dns_reply_entry_ok(rs, n) ? &rs->reply_tab[n] : NULL;
}

static void *
dns_reply_storage_alloc(void)
{
	struct dns_reply_storage *rs = mu_alloc(sizeof(*rs));
	rs->reply_count = 0;
	rs->reply_max = 0;
	rs->reply_tab = NULL;
	return rs;
}

static void
dns_reply_storage_destroy(void *data)
{
	struct dns_reply_storage *rs = data;
	DNS_REPLY_COUNT i;

	for (i = 0; i < rs->reply_count; i++) {
		if (dns_reply_entry_ok(rs, i)) {
			dns_reply_free(&rs->reply_tab[i]);
			dns_reply_entry_release(rs, i);
		}
	}
	free(rs->reply_tab);
	free(rs);
}

MF_DECLARE_DATA(DNS, dns_reply_storage_alloc, dns_reply_storage_destroy)

static inline DNS_REPLY_COUNT
dns_reply_entry_alloc(struct dns_reply_storage *rs,
		      struct dns_reply **return_reply)
{
	DNS_REPLY_COUNT i;

	for (i = 0; i < rs->reply_count; i++) {
		if (!dns_reply_entry_ok(rs, i)) {
			*return_reply = &rs->reply_tab[i];
			return i;
		}
	}
	if (rs->reply_count == DNS_REPLY_MAX)
		return -1;
	if (rs->reply_count == rs->reply_max) {
		size_t n = rs->reply_max;
		rs->reply_tab = mu_2nrealloc(rs->reply_tab, &rs->reply_max,
					     sizeof(rs->reply_tab[0]));
		for (; n < rs->reply_max; n++)
			dns_reply_entry_release(rs, n);
	}
	*return_reply = &rs->reply_tab[rs->reply_count];
	return rs->reply_count++;
}

MF_DEFUN(dns_reply_release, VOID, NUMBER n)
{
	struct dns_reply_storage *repl = MF_GET_DATA;
	if (dns_reply_entry_ok(repl, n)) {
		dns_reply_free(&repl->reply_tab[n]);
		dns_reply_entry_release(repl, n);
	}
}
END

MF_DEFUN(dns_reply_count, NUMBER, NUMBER n)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply;

	if (n == -1)
		MF_RETURN(0);
	if (n < 0)
		MF_THROW(mfe_failure,
			 _("invalid DNS reply number: %ld"), n);
	reply = dns_reply_entry_locate(rs, n);
	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	MF_RETURN(reply->count);
}
END

MF_DEFUN(dns_reply_string, STRING, NUMBER n, NUMBER i)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply = dns_reply_entry_locate(rs, n);
	char ipstr[IPMAX_DOTTED_BUFSIZE];

	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	if (i < 0 || i >= reply->count)
		MF_THROW(mfe_range,
			 _("index out of range: %ld"), i);
	switch (reply->type) {
	case dns_reply_str:
		MF_RETURN(reply->data.str[i]);
	case dns_reply_ip:
		MF_RETURN(inet_ntop(AF_INET, &reply->data.ip[i], ipstr,
				    sizeof(ipstr)));
	case dns_reply_ip6:
		MF_RETURN(inet_ntop(AF_INET6, &reply->data.ip6[i], ipstr,
				    sizeof(ipstr)));
	default:
		runtime_error(env, _("bad DNS reply type: %d"), reply->type);
	}
}
END

MF_DEFUN(dns_reply_ip, NUMBER, NUMBER n, NUMBER i)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	struct dns_reply *reply = dns_reply_entry_locate(rs, n);
	if (!reply)
		MF_THROW(mfe_failure,
			 _("no such reply: %ld"), n);
	if (i < 0 || i >= reply->count)
		MF_THROW(mfe_range,
			 _("index out of range: %ld"), i);
	if (reply->type != dns_reply_ip) {
		MF_THROW(mfe_failure,
			 _("can't use dns_reply_ip on non-IPv4 replies"));
	}
	MF_RETURN(reply->data.ip[i].s_addr);
}
END

enum {
	DNS_TYPE_A = 1,
	DNS_TYPE_NS = 2,
	DNS_TYPE_PTR = 12,
	DNS_TYPE_MX = 15,
	DNS_TYPE_TXT = 16,
	DNS_TYPE_AAAA = 28,
};

static char *dns_type_name[] = {
	[DNS_TYPE_A] = "a",
	[DNS_TYPE_NS] = "ns",
	[DNS_TYPE_PTR] = "ptr",
	[DNS_TYPE_MX] = "mx",
	[DNS_TYPE_TXT] = "txt",
	[DNS_TYPE_AAAA] = "aaaa",
};

MF_DEFUN(dns_query, NUMBER, NUMBER type, STRING domain, OPTIONAL, NUMBER sort,
	 NUMBER resolve)
{
	struct dns_reply_storage *rs = MF_GET_DATA;
	dns_status dnsstat;
	mf_status stat;
	DNS_REPLY_COUNT ret;
	struct dns_reply *reply;
	int (*cmpfun)(const void *, const void *) = NULL;
	int res = resolve_none;

	ret = dns_reply_entry_alloc(rs, &reply);
	MF_ASSERT(ret >= 0,
		  mfe_failure,
		  _("DNS reply table full"));

	res = MF_OPTVAL(resolve, _MFL_RESOLVE_NONE);
	if (res == _MFL_RESOLVE_DFL) {
		res = (MF_SOCKET_FAMILY() == AF_INET)
			? resolve_ip4 : resolve_ip6;
	}

	switch (type) {
	case DNS_TYPE_PTR:
		dnsstat = dns_resolve_ipstr(domain, reply);
		cmpfun = cmp_hostname;
		break;

	case DNS_TYPE_A:
		dnsstat = resolve_host(domain, AF_INET, reply, NULL);
		break;

	case DNS_TYPE_AAAA:
		dnsstat = resolve_host(domain, AF_INET6, reply, NULL);
		break;

	case DNS_TYPE_TXT:
		dnsstat = txt_lookup(domain, reply);
		break;

	case DNS_TYPE_MX:
		dnsstat = mx_lookup(domain, res, reply);
		break;

	case DNS_TYPE_NS:
		dnsstat = ns_lookup(domain, res, reply);
		break;

	default:
		dns_reply_entry_release(rs, ret);
		MF_THROW(mfe_failure,
			 _("unsupported type: %ld"), type);
	}

	stat = dns_to_mf_status(dnsstat);

	if (!mf_resolved(stat)) {
		dns_reply_entry_release(rs, ret);
		MF_THROW(mf_status_to_exception(stat),
			 _("cannot get %s records for %s"),
			 dns_type_name[type], domain);
	}
	if (stat == mf_not_found) {
		dns_reply_entry_release(rs, ret);
		MF_RETURN(-1);
	}
	if (MF_OPTVAL(sort)) {
		if (!cmpfun) {
			switch (reply->type) {
			case dns_reply_str:
				cmpfun = cmp_str;
				break;
			case dns_reply_ip:
				cmpfun = cmp_ip;
				break;
			case dns_reply_ip6:
				cmpfun = cmp_ip6;
				break;
			}
		}
		qsort(reply->data.ptr, reply->count, dns_reply_elsize(reply), cmpfun);
	}
	MF_RETURN(ret);
}
END
