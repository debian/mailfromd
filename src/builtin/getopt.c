#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "getopt.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>

static size_t optarg_loc
#line 23 "getopt.bi"
;
static size_t optind_loc
#line 24 "getopt.bi"
;
static size_t opterr_loc
#line 25 "getopt.bi"
;
static size_t optopt_loc
#line 26 "getopt.bi"
;

void
#line 28
bi_getopt(eval_environ_t env)
#line 28

#line 28

#line 28 "getopt.bi"
{
#line 28
	
#line 28

#line 28
        long  argc;
#line 28
        long  argoff;
#line 28
        
#line 28
        long __bi_argcnt;
#line 28
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 28
        get_numeric_arg(env, 1, &argc);
#line 28
        get_numeric_arg(env, 2, &argoff);
#line 28
        
#line 28
        adjust_stack(env, __bi_argcnt + 1);
#line 28

#line 28

#line 28
	if (builtin_module_trace(BUILTIN_IDX_getopt))
#line 28
		prog_trace(env, "getopt %lu %lu",argc, argoff);;
#line 28

{
	int rc;
	int long_idx;
	char s[2] = { 0, 0 };
	static char xargc;
	static char **xargv;
	static struct option *option;
	static char *optstr;
	static char *loptstr;
	
	if (argc) {
		size_t i, n;
		size_t serial = 256;
		
		xargc = argc + 1;
		xargv = mu_calloc(xargc+1, sizeof(xargv[0]));
		xargv[0] = script_file;
		for (i = 0; i < argc; i++)
			xargv[i+1] = env_vaptr(env, argoff + i);
		xargv[i+1] = NULL;

		n = (__bi_argcnt - 2);
		if (n) {
			size_t i, j;
			size_t len;
			char *str;
			size_t size, lsize, lcnt;
			
			
#line 57
unroll_stack(env, __bi_argcnt + 1);

			size = lsize = lcnt = 0;
			for (i = 0; i < n; i++) {
				
#line 61
 ((__bi_argcnt > (i+2)) ?   get_string_arg(env, (i+2) + 1, &str) :   ((
#line 61
	env_throw_bi(env, mfe_range, "getopt", "Argument %u is not supplied",(unsigned) (i+2))
#line 61
),(char * MFL_DATASEG) 0));
				len = strcspn(str, "|");
				size += len;
				str += len;
				if (*str) {
					lcnt++;
					lsize += strlen(str);
				}
			}

			optstr = mu_realloc(optstr, size + 1);
			loptstr = mu_realloc(loptstr, lsize + 1);
			option = mu_realloc(option,
					  (lcnt + 1) * sizeof(option[0]));
			
			size = 0;
			lsize = 0;
			for (i = j = 0; i < n; i++) {
				size_t len;
				int val;
				char *flags;
					
				
#line 83
 ((__bi_argcnt > (i+2)) ?   get_string_arg(env, (i+2) + 1, &str) :   ((
#line 83
	env_throw_bi(env, mfe_range, "getopt", "Argument %u is not supplied",(unsigned) (i+2))
#line 83
),(char * MFL_DATASEG) 0));
				len = strcspn(str, "|");

				if (len > 0) {
					flags = str + 1;
					if (i == 0 || str[0] != '-') {
						memcpy(optstr + size, str,
						       len);
						size += len;
						val = str[0];
					} else
						val = serial++;
				} else {
					flags = "";
					val = serial++;
				}

				str += len;
				
				if (*str) {
					option[j].name = loptstr + lsize;
					strcpy(loptstr + lsize, str + 1);
					lsize += strlen(str) + 1;
					if (flags[0] == ':') 
						option[j].has_arg =
							(flags[1] == ':') ?
							  optional_argument :
							  required_argument;
					else
						option[j].has_arg =
							no_argument;
					option[j].flag = NULL;
					option[j].val = val;
					j++;
				}
			}
			adjust_stack(env, __bi_argcnt + 1);
			optstr[size] = 0;
			loptstr[lsize] = 0;
			memset(&option[j], 0, sizeof option[0]);
		} else
			option = NULL;
	}

	if (xargc == 0 || optstr == NULL || option == 0)
		
#line 128
do {
#line 128
  pushs(env, "");
#line 128
  goto endlab;
#line 128
} while (0);
	
	optind = mf_c_val(*env_data_ref(env, optind_loc),int) ;
	opterr = mf_c_val(*env_data_ref(env, opterr_loc),int) ;
	
	rc = getopt_long(xargc, xargv, optstr, option, &long_idx);
	mf_c_val(*env_data_ref(env, optind_loc),int) = (optind);
	mf_c_val(*env_data_ref(env, opterr_loc),int) = (opterr);
	s[0] = optopt;
	
#line 137
{ size_t __off;
#line 137
  const char *__s = s;
#line 137
  if (__s)
#line 137
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 137
  else
#line 137
     __off = 0;
#line 137
  mf_c_val(*env_data_ref(env, optopt_loc),size) = (__off); }
#line 137
;
	
#line 138
{ size_t __off;
#line 138
  const char *__s = optarg;
#line 138
  if (__s)
#line 138
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 138
  else
#line 138
     __off = 0;
#line 138
  mf_c_val(*env_data_ref(env, optarg_loc),size) = (__off); }
#line 138
;
	if (rc == EOF)
		
#line 140
do {
#line 140
  pushs(env, "");
#line 140
  goto endlab;
#line 140
} while (0);
	if (rc < 256) {
		s[0] = rc;
		
#line 143
do {
#line 143
  pushs(env, s);
#line 143
  goto endlab;
#line 143
} while (0);
	}
	
#line 145
do {
#line 145
  pushs(env, option[long_idx].name);
#line 145
  goto endlab;
#line 145
} while (0);
}
endlab:
#line 147
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 147
	return;
#line 147
}

 
#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
getopt_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 23 "getopt.bi"
	builtin_variable_install("optarg", dtype_string, SYM_VOLATILE, &optarg_loc);
#line 24 "getopt.bi"
	builtin_variable_install("optind", dtype_number, SYM_VOLATILE, &optind_loc);
#line 25 "getopt.bi"
	builtin_variable_install("opterr", dtype_number, SYM_VOLATILE, &opterr_loc);
#line 26 "getopt.bi"
	builtin_variable_install("optopt", dtype_string, SYM_VOLATILE, &optopt_loc);
#line 28 "getopt.bi"
va_builtin_install_ex("getopt", bi_getopt, 0, dtype_string, 2, 0, 0|MFD_BUILTIN_VARIADIC, dtype_number, dtype_number);

#line 1020 "../../src/builtin/snarf.m4"
	int n = 1;
#line 1020
	ds_init_variable("opterr", &n);
#line 1020
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

