/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2007-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

MF_DEFUN(ntohl, NUMBER, NUMBER n)
{
	MF_RETURN(ntohl((uint32_t) n));
}
END

MF_DEFUN(htonl, NUMBER, NUMBER n)
{
	MF_RETURN(htonl((uint32_t) n));
}
END

/* FIXME: The following functions assume binary complement arithmetic.
   This is hardly a limitation, the similar approach works in GNU Radius
   for years. Nevertheless, this assumption should be noted. */

MF_DEFUN(ntohs, NUMBER, NUMBER n)
{
	MF_RETURN((uint32_t) ntohs(((uint16_t) n) & 0xffff));
}
END

MF_DEFUN(htons, NUMBER, NUMBER n)
{
	MF_RETURN((uint32_t) htons(((uint16_t) n) & 0xffff));
}
END

MF_DEFUN(inet_aton, NUMBER, STRING s)
{
	struct in_addr addr;

	MF_ASSERT(inet_aton(s, &addr),
		  mfe_invip,
		  _("invalid IP address (%s)"),
		  s);
	MF_RETURN(ntohl(addr.s_addr));
}
END

MF_DEFUN(inet_ntoa, STRING, NUMBER ip)
{
	struct in_addr addr;

	addr.s_addr = htonl(ip);
	MF_RETURN(inet_ntoa(addr));
}
END

MF_DEFUN(len_to_netmask, NUMBER, NUMBER x)
{
	unsigned long n = (unsigned long) x;
	unsigned long netmask;

	MF_ASSERT(n <= 32, mfe_range,
		  _("invalid netmask: %lu"), n);
	n = 32 - n;
	if (n == 32)
		netmask = 0;
	else
		netmask = (0xfffffffful >> n) << n;
	MF_RETURN(netmask);
}
END

MF_DEFUN(netmask_to_len, NUMBER, NUMBER x)
{
	unsigned long n = (unsigned long) x;
	unsigned long i;

	for (i = 32; i > 0; i--) {
		if (n & 1)
			break;
		n >>= 1;
	}
	MF_RETURN(i);
}
END

MF_DEFUN(reverse_ipstr, STRING, STRING ipstr)
{
	char revip[IPMAX_DOTTED_BUFSIZE];
	int n = dns_reverse_ipstr(ipstr, revip, sizeof(revip));
	MF_ASSERT(n > 0,
		  mfe_invip,
		  _("invalid IP address: %s"), ipstr);
	revip[n-1] = 0;
	MF_RETURN(revip);
}
END

MF_DEFUN(is_ipstr, NUMBER, STRING ipstr)
{
	MF_RETURN(dns_str_is_ipv4(ipstr) || dns_str_is_ipv6(ipstr));
}
END

MF_DEFUN(is_ip4str, NUMBER, STRING ipstr)
{
	MF_RETURN(dns_str_is_ipv4(ipstr));
}
END

MF_DEFUN(is_ip6str, NUMBER, STRING ipstr)
{
	MF_RETURN(dns_str_is_ipv6(ipstr));
}
END

MF_DEFUN(match_cidr, NUMBER, STRING ipstr, STRING cidrstr)
{
	struct mu_cidr ip, cidr;

	MF_ASSERT(mu_cidr_from_string(&ip, ipstr) == 0,
		  mfe_invip,
		  _("not a valid IP or CIDR: %s"), ipstr);
	MF_ASSERT(mu_cidr_from_string(&cidr, cidrstr) == 0,
		  mfe_invcidr,
		  _("not a valid IP or CIDR: %s"), cidrstr);
	MF_RETURN(mu_cidr_match(&cidr, &ip) == 0);
}
END
