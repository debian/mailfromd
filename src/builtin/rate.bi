/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

MF_DEFUN(rate, NUMBER, STRING key, NUMBER interval, OPTIONAL, NUMBER mincnt,
	 NUMBER threshold)
{
	long ret;

	MF_ASSERT(get_rate(key, &ret, interval,
			   MF_OPTVAL(mincnt),
			   MF_OPTVAL(threshold)) == mf_success,
		  mfe_dbfailure,
		  _("cannot get rate for %s"), key);

	MF_RETURN(ret);
}
END

MF_DEFUN(tbf_rate, NUMBER, STRING key, NUMBER cost,
	 NUMBER interval, NUMBER burst_size)
{
        int result;
		
	MF_ASSERT(check_tbf_rate(key, &result, cost, interval,
				 burst_size) == mf_success,
		  mfe_dbfailure,
		  _("cannot check TBF rate for %s"), key);
	
	MF_RETURN(result);
}
END

