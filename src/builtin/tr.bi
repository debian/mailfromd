/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

m4_pushdef([<CCLASS>],
[<MF_STASH([<{ "$1", sizeof("$1")-1, match_$1 },
>])m4_dnl
static int match_$1(int c) { return mu_is$1(c); }>])m4_dnl
CCLASS(alnum)
CCLASS(alpha)
CCLASS(blank)
CCLASS(cntrl)
CCLASS(digit)
CCLASS(graph)
CCLASS(lower)
CCLASS(print)
CCLASS(punct)
CCLASS(space)
CCLASS(upper)
CCLASS(xdigit)
m4_popdef([<CCLASS>])

typedef int (*class_matcher_fn)(int c);
struct cclass_fun {
	char const *name;
	int len;
	class_matcher_fn matcher;
};

static struct cclass_fun cclass_funtab[] = {
MF_UNSTASH
{ NULL }
};

static class_matcher_fn
find_cclass(unsigned char const *name, int len)
{
	struct cclass_fun *cp;
	for (cp = cclass_funtab; cp->name; cp++)
		if (len == cp->len && memcmp(name, cp->name, len) == 0)
			return cp->matcher;
	return NULL;
}

static unsigned char const *
scan_cclass(eval_environ_t env, unsigned char const *s)
{
	unsigned char const *start = s;

	s++;
	if (*s == '!')
		s++;
	if (*s == ']')
		s++;
	while (*s != ']') {
		MF_ASSERT(*s != 0,
			  mfe_inval,
			  _("unfinished character class near %s"),
			  start);
		if (*s == '[') {
			if (s[1] == ':') {
				int n = strcspn((char*)s+2, ":");
				MF_ASSERT(s[2+n] == ':' && s[3+n] == ']',
					  mfe_inval,
					  _("unclosed character class near %s"),
					  s);
				MF_ASSERT(find_cclass(s + 2, n),
					  mfe_inval,
					  _("unrecognized character class %*.*s"),
					  n, n, s + 2);
				s += n + 4;
				continue;
			}
		}
		if (s[1] == '-') {
			if (s[2] != ']') {
				MF_ASSERT(s[2] > s[0],
					  mfe_range,
					  _("descending range in character class near %s"),
					  s);
				s += 3;
				continue;
			}
		}
		s++;
	}
	return s;
}

static int
match_cclass(int c, unsigned char const **class_ptr)
{
	unsigned char const *cl = *class_ptr + 1;
	int result = 1;

	if (*cl == '!') {
		result = 0;
		cl++;
	}
	if (*cl == ']') {
		cl++;
		if (c == ']')
			goto end;
	}
	for (;;) {
		if (*cl == ']') {
			*class_ptr = cl;
			return !result;
		}

		if (cl[0] == '[' && cl[1] == ':') {
			int n = strcspn((char*)cl+2, ":");
			class_matcher_fn f = find_cclass(cl+2, n);
			cl += n + 4;
			if (f(c))
				break;
		} else if (cl[1] == '-' && cl[2] != ']') {
			cl += 3;
			if (cl[-3] <= c && c <= cl[-1])
				break;
		} else {
			cl++;
			if (cl[-1] == c)
				break;
		}
	}

end:
	/* Skip the rest of class */
	while (*cl != ']') {
		if (cl[0] == '[' && cl[1] == ':')
			cl += strcspn((char*)cl+2, ":") + 4;
		else if (cl[1] == '-' && cl[2] != ']')
			cl += 3;
		else
			cl++;
	}
	*class_ptr = cl;
	return result;
}

m4_dnl GENTR(TR|RM|SQ)
m4_dnl ---------------
m4_dnl Generate code for tr, dc, or sq function, depending on the
m4_dnl argument:
m4_dnl
m4_dnl * TR
m4_dnl   Generate the tr (transliterate) function.
m4_dnl * RM
m4_dnl   Generate the dc (delete character) function.
m4_dnl * SQ
m4_dnl   Generate the sq (squeeze repeated characters) function.
m4_dnl
m4_pushdef([<GENTR>],
[<	char *s, *start;

	/* Operate on usigned characters. */
	unsigned char const *subj, *src;
m4_ifelse([<$1>],[<TR>],[<m4_dnl
	unsigned char const *dst;
>])m4_dnl

	/* Check consistency of both sets. */
	m4_ifelse([<$1>],[<TR>],[<m4_dnl
	MF_ASSERT(set2[0] != 0,
		  mfe_inval,
			  _("replacement set cannot be empty"));>])

	for (src = (unsigned char const *)set1[<>]m4_dnl,
m4_ifelse([<$1>],[<TR>],[<,
		  dst = (unsigned char const *)set2>]);
	     *src;
	     src++) {
		if (*src == '[') {
			src = scan_cclass(env, src);
		} else if (src > (unsigned char *)set1 &&
			   src[0] == '-' && src[1]) {
			MF_ASSERT(src[1] > src[-1],
				  mfe_range,
				  _("ranges in source set must be in "
				    "ascending order"));
			m4_ifelse([<$1>],[<TR>],[<m4_dnl
				MF_ASSERT(dst[0] == src[0] && dst[1],
					  mfe_inval,
					  _("ranges should appear at the same "
					    "offset in both source and "
					    "replacement sets"));
				MF_ASSERT((dst[1] > dst[-1] ?
					   (dst[1] - dst[-1]) :
					   (dst[-1] - dst[1])) == src[1] - src[-1],
					  mfe_range,
					  _("source and replacement ranges "
					    "differ in size"));
>])m4_dnl
		}
		m4_ifelse([<$1>],[<TR>],[<m4_dnl
			dst++;
			if (*dst == 0)
				break;
>])m4_dnl
	}

	/* Allocate result string. */
	MF_OBSTACK_BEGIN();
	MF_OBSTACK_GROW(NULL, strlen(input) + 1, start);
	s = start;

m4_pushdef([<TRCH>],m4_ifelse([<$1>],[<TR>],[<*s = *dst>],m4_dnl
[<$1>],[<RM>],[<s-->],m4_dnl
[<$1>],[<SQ>],[<if (s > start && c == s[-1]) s-->]))

	/* Now translate the input. */
	for (subj = (unsigned char const *)input; *subj; subj++, s++) {
		int c = *subj;

		/* Store untranslated character. */
		*s = c;

		/* Walk over source and destination (replacement) sets. */
		src = (unsigned char const *)set1;
m4_ifelse([<$1>],[<TR>],[<m4_dnl
		dst = (unsigned char const *)set2;
>])m4_dnl
		if (*src == '-') {
			if (c == '-') {
				TRCH();
				continue;
			}
			src++;
m4_ifelse([<$1>],[<TR>],[<m4_dnl
			dst++;
>])
		}
		while (*src) {
			if (*src == '[') {
				if (match_cclass(*s, &src)) {
					TRCH();
					break;
				}
			} else if (src > (unsigned char const *)set1 &&
				   src[0] == '-' && src[1]) {
				if (src[-1] < c && c <= src[1]) {
m4_ifelse([<$1>],[<TR>],[<m4_dnl
						if (dst[1] > dst[-1])
							*s = dst[-1] + c - src[-1];
						else
							*s = dst[-1] - c + src[-1];>],m4_dnl
[<$1>],[<RM>],[<m4_dnl
						s--;>],m4_dnl
[<$1>],[<SQ>],[<m4_dnl
					if (s > start && c == s[-1]) s--;>])
					break;
				} else {
					src++;
m4_ifelse([<$1>],[<TR>],[<m4_dnl
					dst++;
>])m4_dnl
				}
			} else if (src[0] == c) {//FIXME
				TRCH();
				break;
			}
			src++;
m4_ifelse([<$1>],[<TR>],[<m4_dnl
			if (dst[1])
				dst++;
>])m4_dnl
		}
	}
	*s = 0;
	MF_OBSTACK_TRUNCATE(s - start + 1);
	MF_RETURN_OBSTACK();
m4_popdef([<TRCH>])
>])

MF_DEFUN(tr, STRING, STRING input, STRING set1, STRING set2)
{
GENTR([<TR>])
}
END

MF_DEFUN(sq, STRING, STRING input, STRING set1)
{
GENTR([<SQ>])
}
END

MF_DEFUN(dc, STRING, STRING input, STRING set1)
{
GENTR([<RM>])
}
END

m4_popdef([<GENTR>])
