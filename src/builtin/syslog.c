#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "syslog.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "syslog.h"
#include "mflib/syslog.h"

static struct builtin_const_trans syslog_prio[] = {
	{ _MFL_LOG_EMERG, LOG_EMERG },
	{ _MFL_LOG_ALERT, LOG_ALERT },
	{ _MFL_LOG_CRIT, LOG_CRIT },
	{ _MFL_LOG_ERR, LOG_ERR },
	{ _MFL_LOG_WARNING, LOG_WARNING },
	{ _MFL_LOG_NOTICE, LOG_NOTICE },
	{ _MFL_LOG_INFO, LOG_INFO },
	{ _MFL_LOG_DEBUG, LOG_DEBUG }
};

static struct builtin_const_trans syslog_fac[] = {
	{ _MFL_LOG_KERN, LOG_KERN },
	{ _MFL_LOG_USER, LOG_USER },
	{ _MFL_LOG_MAIL, LOG_MAIL },
	{ _MFL_LOG_DAEMON, LOG_DAEMON },
	{ _MFL_LOG_AUTH, LOG_AUTH },
	{ _MFL_LOG_SYSLOG, LOG_SYSLOG },
	{ _MFL_LOG_LPR, LOG_LPR },
	{ _MFL_LOG_NEWS, LOG_NEWS },
	{ _MFL_LOG_UUCP, LOG_UUCP },
	{ _MFL_LOG_CRON, LOG_CRON },
	{ _MFL_LOG_AUTHPRIV, LOG_AUTHPRIV },
	{ _MFL_LOG_FTP, LOG_FTP },
	{ _MFL_LOG_LOCAL0, LOG_LOCAL0 },
	{ _MFL_LOG_LOCAL1, LOG_LOCAL1 },
	{ _MFL_LOG_LOCAL2, LOG_LOCAL2 },
	{ _MFL_LOG_LOCAL3, LOG_LOCAL3 },
	{ _MFL_LOG_LOCAL4, LOG_LOCAL4 },
	{ _MFL_LOG_LOCAL5, LOG_LOCAL5 },
	{ _MFL_LOG_LOCAL6, LOG_LOCAL6 },
	{ _MFL_LOG_LOCAL7, LOG_LOCAL7 },
};

void
#line 56
bi_syslog(eval_environ_t env)
#line 56

#line 56

#line 56 "syslog.bi"
{
#line 56
	
#line 56

#line 56
        long  priority;
#line 56
        char *  text;
#line 56
        
#line 56

#line 56
        get_numeric_arg(env, 0, &priority);
#line 56
        get_string_arg(env, 1, &text);
#line 56
        
#line 56
        adjust_stack(env, 2);
#line 56

#line 56

#line 56
	if (builtin_module_trace(BUILTIN_IDX_syslog))
#line 56
		prog_trace(env, "syslog %lu %s",priority, text);;
#line 56

{
	int prio = 0;
	int f = 0;

 	prio = priority & _MFL_LOG_PRIMASK;
	_builtin_const_to_c(syslog_prio, MU_ARRAY_SIZE(syslog_prio),
			    prio, &prio);

	f = priority & ~_MFL_LOG_PRIMASK;
	_builtin_const_to_c(syslog_fac, MU_ARRAY_SIZE(syslog_fac),
			    f, &f);
	logger_text(prio | f, text);
}

#line 70
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 70
	return;
#line 70
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
syslog_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 56 "syslog.bi"
va_builtin_install_ex("syslog", bi_syslog, 0, dtype_unspecified, 2, 0, 0|0, dtype_number, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

