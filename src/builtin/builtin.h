/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/


struct builtin_module {
	char *name;
	void (*init)(void);
	int trace;
};

struct builtin_const_trans
{
	int const_mfl;
	int const_c;
};

int _builtin_const_to_c(struct builtin_const_trans *tab, size_t count,
			int num, int *ret);
int _builtin_c_to_const(struct builtin_const_trans *tab, size_t count,
			int num, int *ret);
int _builtin_const_to_bitmap(struct builtin_const_trans *tab, size_t count,
			     int num);


#define BUILTIN_IDX_prog 0
#define BUILTIN_IDX_body (1)
#define BUILTIN_IDX_burst (2)
#define BUILTIN_IDX_callout (3)
#define BUILTIN_IDX_ctype (4)
#define BUILTIN_IDX_curhdr (5)
#define BUILTIN_IDX_db (6)
#define BUILTIN_IDX_debug (7)
#define BUILTIN_IDX_dkim (8)
#define BUILTIN_IDX_dns (9)
#define BUILTIN_IDX_email (10)
#define BUILTIN_IDX_filter (11)
#define BUILTIN_IDX_from (12)
#define BUILTIN_IDX_geoip2 (13)
#define BUILTIN_IDX_gethostname (14)
#define BUILTIN_IDX_getopt (15)
#define BUILTIN_IDX_getpw (16)
#define BUILTIN_IDX_gettext (17)
#define BUILTIN_IDX_header (18)
#define BUILTIN_IDX_io (19)
#define BUILTIN_IDX_ipaddr (20)
#define BUILTIN_IDX_macro (21)
#define BUILTIN_IDX_mail (22)
#define BUILTIN_IDX_mbox (23)
#define BUILTIN_IDX_mfmod (24)
#define BUILTIN_IDX_mmq (25)
#define BUILTIN_IDX_msg (26)
#define BUILTIN_IDX_prereq (27)
#define BUILTIN_IDX_progress (28)
#define BUILTIN_IDX_rate (29)
#define BUILTIN_IDX_rcpt (30)
#define BUILTIN_IDX_sa (31)
#define BUILTIN_IDX_sieve (32)
#define BUILTIN_IDX_spf (33)
#define BUILTIN_IDX_sprintf (34)
#define BUILTIN_IDX_string (35)
#define BUILTIN_IDX_syslog (36)
#define BUILTIN_IDX_system (37)
#define BUILTIN_IDX_tr (38)
#define BUILTIN_IDX_other (39)
#define BUILTIN_IDX_vars (40)
#define BUILTIN_IDX_qrnt (41)



#ifdef DEFINE_BUILTIN_MODULE

extern void body_init_builtin(void);
extern void burst_init_builtin(void);
extern void callout_init_builtin(void);
extern void ctype_init_builtin(void);
extern void curhdr_init_builtin(void);
extern void db_init_builtin(void);
extern void debug_init_builtin(void);
extern void dkim_init_builtin(void);
extern void dns_init_builtin(void);
extern void email_init_builtin(void);
extern void filter_init_builtin(void);
extern void from_init_builtin(void);
extern void geoip2_init_builtin(void);
extern void gethostname_init_builtin(void);
extern void getopt_init_builtin(void);
extern void getpw_init_builtin(void);
extern void gettext_init_builtin(void);
extern void header_init_builtin(void);
extern void io_init_builtin(void);
extern void ipaddr_init_builtin(void);
extern void macro_init_builtin(void);
extern void mail_init_builtin(void);
extern void mbox_init_builtin(void);
extern void mfmod_init_builtin(void);
extern void mmq_init_builtin(void);
extern void msg_init_builtin(void);
extern void prereq_init_builtin(void);
extern void progress_init_builtin(void);
extern void rate_init_builtin(void);
extern void rcpt_init_builtin(void);
extern void sa_init_builtin(void);
extern void sieve_init_builtin(void);
extern void spf_init_builtin(void);
extern void sprintf_init_builtin(void);
extern void string_init_builtin(void);
extern void syslog_init_builtin(void);
extern void system_init_builtin(void);
extern void tr_init_builtin(void);
extern void other_init_builtin(void);
extern void vars_init_builtin(void);
extern void qrnt_init_builtin(void);



static struct builtin_module builtin_module[] = {
	{ "prog", NULL, 0 },
	{ "body", body_init_builtin, 0 },
	{ "burst", burst_init_builtin, 0 },
	{ "callout", callout_init_builtin, 0 },
	{ "ctype", ctype_init_builtin, 0 },
	{ "curhdr", curhdr_init_builtin, 0 },
	{ "db", db_init_builtin, 0 },
	{ "debug", debug_init_builtin, 0 },
	{ "dkim", dkim_init_builtin, 0 },
	{ "dns", dns_init_builtin, 0 },
	{ "email", email_init_builtin, 0 },
	{ "filter", filter_init_builtin, 0 },
	{ "from", from_init_builtin, 0 },
	{ "geoip2", geoip2_init_builtin, 0 },
	{ "gethostname", gethostname_init_builtin, 0 },
	{ "getopt", getopt_init_builtin, 0 },
	{ "getpw", getpw_init_builtin, 0 },
	{ "gettext", gettext_init_builtin, 0 },
	{ "header", header_init_builtin, 0 },
	{ "io", io_init_builtin, 0 },
	{ "ipaddr", ipaddr_init_builtin, 0 },
	{ "macro", macro_init_builtin, 0 },
	{ "mail", mail_init_builtin, 0 },
	{ "mbox", mbox_init_builtin, 0 },
	{ "mfmod", mfmod_init_builtin, 0 },
	{ "mmq", mmq_init_builtin, 0 },
	{ "msg", msg_init_builtin, 0 },
	{ "prereq", prereq_init_builtin, 0 },
	{ "progress", progress_init_builtin, 0 },
	{ "rate", rate_init_builtin, 0 },
	{ "rcpt", rcpt_init_builtin, 0 },
	{ "sa", sa_init_builtin, 0 },
	{ "sieve", sieve_init_builtin, 0 },
	{ "spf", spf_init_builtin, 0 },
	{ "sprintf", sprintf_init_builtin, 0 },
	{ "string", string_init_builtin, 0 },
	{ "syslog", syslog_init_builtin, 0 },
	{ "system", system_init_builtin, 0 },
	{ "tr", tr_init_builtin, 0 },
	{ "other", other_init_builtin, 0 },
	{ "vars", vars_init_builtin, 0 },
	{ "qrnt", qrnt_init_builtin, 0 },

	{ NULL, NULL }
};
#define BUILTIN_IDX_MAX (sizeof(builtin_module)/sizeof(builtin_module[0])-1)

#endif

extern int provide_callout;

void builtin_setup(void);
void builtin_set_module_trace(const char *name, size_t len, int val);
void builtin_set_all_module_trace(int val);
int builtin_module_trace(unsigned idx);
void _builtin_stream_cleanup(void *);
mu_message_t _builtin_mu_stream_to_message(mu_stream_t str,
					   eval_environ_t env,
				           const char *func_name);
