#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "body.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "msg.h"


void
#line 22
bi_body_string(eval_environ_t env)
#line 22

#line 22

#line 22 "body.bi"
{
#line 22
	
#line 22

#line 22
        void * MFL_DATASEG text;
#line 22
        long  length;
#line 22
        
#line 22

#line 22
        get_pointer_arg(env, 0, &text);
#line 22
        get_numeric_arg(env, 1, &length);
#line 22
        
#line 22
        adjust_stack(env, 2);
#line 22

#line 22

#line 22
	if (builtin_module_trace(BUILTIN_IDX_body))
#line 22
		prog_trace(env, "body_string %p %lu",text, length);;
#line 22

{
	heap_obstack_begin(env);
	heap_obstack_grow(env, text, length);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 27
do {
#line 27
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 27
  goto endlab;
#line 27
} while (0);
}
endlab:
#line 29
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 29
	return;
#line 29
}


void
#line 32
bi_body_has_nulls(eval_environ_t env)
#line 32

#line 32

#line 32 "body.bi"
{
#line 32
	
#line 32

#line 32
        void *  text;
#line 32
        long  length;
#line 32
        
#line 32

#line 32
        get_pointer_arg(env, 0, &text);
#line 32
        get_numeric_arg(env, 1, &length);
#line 32
        
#line 32
        adjust_stack(env, 2);
#line 32

#line 32

#line 32
	if (builtin_module_trace(BUILTIN_IDX_body))
#line 32
		prog_trace(env, "body_has_nulls %p %lu",text, length);;
#line 32

{
	
#line 34
do {
#line 34
  push(env, (STKVAL)(mft_number)(memchr(text, 0, length) != NULL));
#line 34
  goto endlab;
#line 34
} while (0);
}
endlab:
#line 36
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 36
	return;
#line 36
}
	
/* number current_message() */


void
#line 41
bi_current_message(eval_environ_t env)
#line 41

#line 41

#line 41 "body.bi"
{
#line 41
	
#line 41

#line 41
        
#line 41

#line 41
        
#line 41
        adjust_stack(env, 0);
#line 41

#line 41

#line 41
	if (builtin_module_trace(BUILTIN_IDX_body))
#line 41
		prog_trace(env, "current_message");;
#line 41

{
	
#line 43
do {
#line 43
  push(env, (STKVAL)(mft_number)(bi_get_current_message(env, NULL)));
#line 43
  goto endlab;
#line 43
} while (0);
}
endlab:
#line 45
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 45
	return;
#line 45
}

void
#line 47
bi_replbody(eval_environ_t env)
#line 47

#line 47

#line 47 "body.bi"
{
#line 47
	
#line 47

#line 47
        char *  text;
#line 47
        
#line 47

#line 47
        get_string_arg(env, 0, &text);
#line 47
        
#line 47
        adjust_stack(env, 1);
#line 47

#line 47

#line 47
	if (builtin_module_trace(BUILTIN_IDX_body))
#line 47
		prog_trace(env, "replbody %s",text);;
#line 47

{
	env_msgmod_append(env, body_repl, "BODY", text, 0);
}

#line 51
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 51
	return;
#line 51
}

void
#line 53
bi_replbody_fd(eval_environ_t env)
#line 53

#line 53

#line 53 "body.bi"
{
#line 53
	
#line 53

#line 53
        long  fd;
#line 53
        
#line 53

#line 53
        get_numeric_arg(env, 0, &fd);
#line 53
        
#line 53
        adjust_stack(env, 1);
#line 53

#line 53

#line 53
	if (builtin_module_trace(BUILTIN_IDX_body))
#line 53
		prog_trace(env, "replbody_fd %lu",fd);;
#line 53

{
	env_msgmod_append(env, body_repl_fd, "BODYFD", NULL, _bi_io_fd(env, fd, 0));
}

#line 57
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 57
	return;
#line 57
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
body_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 22 "body.bi"
va_builtin_install_ex("body_string", bi_body_string, STATMASK(smtp_state_body), dtype_string, 2, 0, 0|0, dtype_pointer, dtype_number);
#line 32 "body.bi"
va_builtin_install_ex("body_has_nulls", bi_body_has_nulls, STATMASK(smtp_state_body), dtype_number, 2, 0, 0|0, dtype_pointer, dtype_number);
#line 41 "body.bi"
va_builtin_install_ex("current_message", bi_current_message, STATMASK(smtp_state_eom), dtype_number, 0, 0, MFD_BUILTIN_CAPTURE|0, dtype_unspecified);
#line 47 "body.bi"
va_builtin_install_ex("replbody", bi_replbody, 0, dtype_unspecified, 1, 0, 0|0, dtype_string);
#line 53 "body.bi"
va_builtin_install_ex("replbody_fd", bi_replbody_fd, 0, dtype_unspecified, 1, 0, 0|0, dtype_number);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

