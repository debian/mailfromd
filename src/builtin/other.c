#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 51 "other.bi"
static mu_debug_handle_t debug_handle;
#line 1020 "../../src/builtin/snarf.m4"

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "other.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "mflib/_register.h"

static int
valid_user_p(eval_environ_t env, const char *name)
{
	int rc;
	struct mu_auth_data *auth_data = NULL;

	rc = mu_get_auth(&auth_data, mu_auth_key_name, name);
	mu_auth_data_free(auth_data);
	switch (rc) {
	case 0:
		rc = 1;
		break;
		
	case MU_ERR_AUTH_FAILURE:
		rc = 0;
		break;

	case EAGAIN:
                (
#line 39
	env_throw_bi(env, mfe_temp_failure, NULL, _("temporary failure querying for username %s"),name)
#line 39
);
#line 42
		break;

	default:
                (
#line 45
	env_throw_bi(env, mfe_failure, NULL, _("failure querying for username %s"),name)
#line 45
);
#line 48
		break;
	}

	
#line 51 "other.bi"

#line 51
mu_debug(debug_handle, MU_DEBUG_TRACE1,("Checking user %s: %s", name, rc ? "true" : "false"));
#line 53
	return rc;
}

void
#line 56
bi_validuser(eval_environ_t env)
#line 56

#line 56

#line 56 "other.bi"
{
#line 56
	
#line 56

#line 56
        char *  name;
#line 56
        
#line 56

#line 56
        get_string_arg(env, 0, &name);
#line 56
        
#line 56
        adjust_stack(env, 1);
#line 56

#line 56

#line 56
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 56
		prog_trace(env, "validuser %s",name);;
#line 56

{
	
#line 58
do {
#line 58
  push(env, (STKVAL)(mft_number)(valid_user_p(env, name)));
#line 58
  goto endlab;
#line 58
} while (0);
}
endlab:
#line 60
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 60
	return;
#line 60
}

void
#line 62
bi_interval(eval_environ_t env)
#line 62

#line 62

#line 62 "other.bi"
{
#line 62
	
#line 62

#line 62
        char *  str;
#line 62
        
#line 62

#line 62
        get_string_arg(env, 0, &str);
#line 62
        
#line 62
        adjust_stack(env, 1);
#line 62

#line 62

#line 62
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 62
		prog_trace(env, "interval %s",str);;
#line 62

{
       time_t t;
       const char *endp;

       	if (!(parse_time_interval(str, &t, &endp) == 0))
#line 67
		(
#line 67
	env_throw_bi(env, mfe_invtime, "interval", _("unrecognized time format (near `%s')"),endp)
#line 67
)
#line 69
;
       
#line 70
do {
#line 70
  push(env, (STKVAL)(mft_number)(t));
#line 70
  goto endlab;
#line 70
} while (0);
}
endlab:
#line 72
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 72
	return;
#line 72
}

void
#line 74
bi_milter_state_name(eval_environ_t env)
#line 74

#line 74

#line 74 "other.bi"
{
#line 74
	
#line 74

#line 74
        long  n;
#line 74
        
#line 74

#line 74
        get_numeric_arg(env, 0, &n);
#line 74
        
#line 74
        adjust_stack(env, 1);
#line 74

#line 74

#line 74
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 74
		prog_trace(env, "milter_state_name %lu",n);;
#line 74

{
	const char *s = state_to_string(n);
		if (!(s != NULL))
#line 77
		(
#line 77
	env_throw_bi(env, mfe_inval, "milter_state_name", _("unrecongized state id %ld"),n)
#line 77
)
#line 79
;
	
#line 80
do {
#line 80
  pushs(env, s);
#line 80
  goto endlab;
#line 80
} while (0);
}
endlab:
#line 82
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 82
	return;
#line 82
}

void
#line 84
bi_milter_state_code(eval_environ_t env)
#line 84

#line 84

#line 84 "other.bi"
{
#line 84
	
#line 84

#line 84
        char *  state;
#line 84
        
#line 84

#line 84
        get_string_arg(env, 0, &state);
#line 84
        
#line 84
        adjust_stack(env, 1);
#line 84

#line 84

#line 84
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 84
		prog_trace(env, "milter_state_code %s",state);;
#line 84

{
	
#line 86
do {
#line 86
  push(env, (STKVAL)(mft_number)(string_to_state(state)));
#line 86
  goto endlab;
#line 86
} while (0);
}
endlab:
#line 88
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 88
	return;
#line 88
}

void
#line 90
bi_milter_action_name(eval_environ_t env)
#line 90

#line 90

#line 90 "other.bi"
{
#line 90
	
#line 90

#line 90
        long  n;
#line 90
        
#line 90

#line 90
        get_numeric_arg(env, 0, &n);
#line 90
        
#line 90
        adjust_stack(env, 1);
#line 90

#line 90

#line 90
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 90
		prog_trace(env, "milter_action_name %lu",n);;
#line 90

{
	const char *s = sfsistat_str(n);
		if (!(s != NULL))
#line 93
		(
#line 93
	env_throw_bi(env, mfe_inval, "milter_action_name", _("unrecognized action code %ld"),n)
#line 93
)
#line 95
;
	
#line 96
do {
#line 96
  pushs(env, s);
#line 96
  goto endlab;
#line 96
} while (0);
}
endlab:
#line 98
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 98
	return;
#line 98
}

void
#line 100
bi_milter_action_code(eval_environ_t env)
#line 100

#line 100

#line 100 "other.bi"
{
#line 100
	
#line 100

#line 100
        char *  action;
#line 100
        
#line 100

#line 100
        get_string_arg(env, 0, &action);
#line 100
        
#line 100
        adjust_stack(env, 1);
#line 100

#line 100

#line 100
	if (builtin_module_trace(BUILTIN_IDX_other))
#line 100
		prog_trace(env, "milter_action_code %s",action);;
#line 100

{
	
#line 102
do {
#line 102
  push(env, (STKVAL)(mft_number)(sfsistat_code(action)));
#line 102
  goto endlab;
#line 102
} while (0);
}
endlab:
#line 104
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 104
	return;
#line 104
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
other_init_builtin(void)
#line 1020
{
#line 1020
		debug_handle = mu_debug_register_category("bi_other");
#line 1020

#line 1020
	#line 56 "other.bi"
va_builtin_install_ex("validuser", bi_validuser, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 62 "other.bi"
va_builtin_install_ex("interval", bi_interval, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 74 "other.bi"
va_builtin_install_ex("milter_state_name", bi_milter_state_name, 0, dtype_string, 1, 0, 0|0, dtype_number);
#line 84 "other.bi"
va_builtin_install_ex("milter_state_code", bi_milter_state_code, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 90 "other.bi"
va_builtin_install_ex("milter_action_name", bi_milter_action_name, 0, dtype_string, 1, 0, 0|0, dtype_number);
#line 100 "other.bi"
va_builtin_install_ex("milter_action_code", bi_milter_action_code, 0, dtype_number, 1, 0, 0|0, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

