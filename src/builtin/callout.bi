/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* Run-time support for callout functions */
MF_BUILTIN_MODULE
#include "filenames.h"
#include "callout.h"
#include "srvcfg.h"

MF_VAR(ehlo_domain, STRING, SYM_PRECIOUS);
MF_VAR(mailfrom_address, STRING, SYM_PRECIOUS);

int provide_callout;

/* #pragma provide-callout */
MF_PRAGMA(provide-callout, 1, 1)
{
	provide_callout = 1;
}

MF_DEFUN(default_callout_server_url, STRING)
{
	MF_RETURN(callout_server_url ?
		  callout_server_url : DEFAULT_CALLOUT_SOCKET);
}
END

MF_DEFUN(callout_transcript, NUMBER, OPTIONAL, NUMBER val)
{
	int oldval = smtp_transcript;
	if (MF_DEFINED(val))
		smtp_transcript = val;
	MF_RETURN(oldval);
}
END

MF_DEFUN(callout_starttls, STRING, OPTIONAL, STRING val)
{
	int oldval = smtp_use_tls;
	if (MF_DEFINED(val)) {
		MF_ASSERT(smtp_str_to_starttls(val, &smtp_use_tls) == 0,
			  mfe_inval,
			  "bad starttls value");
	}
	MF_RETURN(smtp_starttls_to_str(oldval));
}
END

MF_INIT([<
	 if (ehlo_domain)
		 ds_init_variable("ehlo_domain", ehlo_domain);
	 if (mailfrom_address)
		 ds_init_variable("mailfrom_address", mailfrom_address);
>])
	


