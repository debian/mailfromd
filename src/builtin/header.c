#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "header.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2008-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



void
#line 19
bi_header_add(eval_environ_t env)
#line 19

#line 19

#line 19 "header.bi"
{
#line 19
	
#line 19

#line 19
        char *  name;
#line 19
        char *  value;
#line 19
        long  idx;
#line 19
        
#line 19
        long __bi_argcnt;
#line 19
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 19
        get_string_arg(env, 1, &name);
#line 19
        get_string_arg(env, 2, &value);
#line 19
        if (__bi_argcnt > 2)
#line 19
                get_numeric_arg(env, 3, &idx);
#line 19
        
#line 19
        adjust_stack(env, __bi_argcnt + 1);
#line 19

#line 19

#line 19
	if (builtin_module_trace(BUILTIN_IDX_header))
#line 19
		prog_trace(env, "header_add %s %s %lu",name, value, ((__bi_argcnt > 2) ? idx : 0));;
#line 19

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	if ((__bi_argcnt > 2)) {
		trace("%s%s:%u: %s %ld \"%s: %s\"",
		      mailfromd_msgid(env_get_context(env)),
		      locus.beg.mu_file, locus.beg.mu_line,
		      msgmod_opcode_str(header_insert),
		      idx,
		      name, value);
		env_msgmod_append(env, header_insert, name, value, idx);
	} else {
		trace("%s%s:%u: %s \"%s: %s\"",
		      mailfromd_msgid(env_get_context(env)),
		      locus.beg.mu_file, locus.beg.mu_line,
		      msgmod_opcode_str(header_add),
		      name, value);
		env_msgmod_append(env, header_add, name, value, 1);
	}
}

#line 42
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 42
	return;
#line 42
}

void
#line 44
bi_header_insert(eval_environ_t env)
#line 44

#line 44

#line 44 "header.bi"
{
#line 44
	
#line 44

#line 44
        char *  name;
#line 44
        char *  value;
#line 44
        long  idx;
#line 44
        
#line 44

#line 44
        get_string_arg(env, 0, &name);
#line 44
        get_string_arg(env, 1, &value);
#line 44
        get_numeric_arg(env, 2, &idx);
#line 44
        
#line 44
        adjust_stack(env, 3);
#line 44

#line 44

#line 44
	if (builtin_module_trace(BUILTIN_IDX_header))
#line 44
		prog_trace(env, "header_insert %s %s %lu",name, value, idx);;
#line 44

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s %ld \"%s: %s\"",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_insert),
	      idx,
	      name, value);
	env_msgmod_append(env, header_insert, name, value, idx);
}

#line 58
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 58
	return;
#line 58
}

void
#line 60
bi_header_delete(eval_environ_t env)
#line 60

#line 60

#line 60 "header.bi"
{
#line 60
	
#line 60

#line 60
        char *  name;
#line 60
        long  idx;
#line 60
        
#line 60
        long __bi_argcnt;
#line 60
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 60
        get_string_arg(env, 1, &name);
#line 60
        if (__bi_argcnt > 1)
#line 60
                get_numeric_arg(env, 2, &idx);
#line 60
        
#line 60
        adjust_stack(env, __bi_argcnt + 1);
#line 60

#line 60

#line 60
	if (builtin_module_trace(BUILTIN_IDX_header))
#line 60
		prog_trace(env, "header_delete %s %lu",name, ((__bi_argcnt > 1) ? idx : 0));;
#line 60

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s \"%s\" (%lu)",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_delete),
	      name, ((__bi_argcnt > 1) ? idx : 1));
	env_msgmod_append(env, header_delete, name, NULL, ((__bi_argcnt > 1) ? idx : 1));
}

#line 73
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 73
	return;
#line 73
}

void
#line 75
bi_header_replace(eval_environ_t env)
#line 75

#line 75

#line 75 "header.bi"
{
#line 75
	
#line 75

#line 75
        char *  name;
#line 75
        char *  value;
#line 75
        long  idx;
#line 75
        
#line 75
        long __bi_argcnt;
#line 75
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 75
        get_string_arg(env, 1, &name);
#line 75
        get_string_arg(env, 2, &value);
#line 75
        if (__bi_argcnt > 2)
#line 75
                get_numeric_arg(env, 3, &idx);
#line 75
        
#line 75
        adjust_stack(env, __bi_argcnt + 1);
#line 75

#line 75

#line 75
	if (builtin_module_trace(BUILTIN_IDX_header))
#line 75
		prog_trace(env, "header_replace %s %s %lu",name, value, ((__bi_argcnt > 2) ? idx : 0));;
#line 75

{
	struct mu_locus_range locus;

	env_get_locus(env, &locus);

	trace("%s%s:%u: %s \"%s: %s\" (%lu)",
	      mailfromd_msgid(env_get_context(env)),
	      locus.beg.mu_file, locus.beg.mu_line,
	      msgmod_opcode_str(header_replace),
	      name, value, ((__bi_argcnt > 2) ? idx : 1));
	env_msgmod_append(env, header_replace, name, value, ((__bi_argcnt > 2) ? idx : 1));
}

#line 88
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 88
	return;
#line 88
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
header_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 19 "header.bi"
va_builtin_install_ex("header_add", bi_header_add, 0, dtype_unspecified, 3, 1, 0|0, dtype_string, dtype_string, dtype_number);
#line 44 "header.bi"
va_builtin_install_ex("header_insert", bi_header_insert, 0, dtype_unspecified, 3, 0, 0|0, dtype_string, dtype_string, dtype_number);
#line 60 "header.bi"
va_builtin_install_ex("header_delete", bi_header_delete, 0, dtype_unspecified, 2, 1, 0|0, dtype_string, dtype_number);
#line 75 "header.bi"
va_builtin_install_ex("header_replace", bi_header_replace, 0, dtype_unspecified, 3, 1, 0|0, dtype_string, dtype_string, dtype_number);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

