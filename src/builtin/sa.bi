/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

MF_BUILTIN_MODULE

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <signal.h>

#include <mailutils/stream.h>

#include "msg.h"
#include "mflib/sa.h"

MF_VAR(sa_score, NUMBER);
MF_VAR(sa_threshold, NUMBER);
MF_VAR(sa_keywords, STRING);
MF_VAR(clamav_virus_name, STRING);

static void
set_xscript(mu_stream_t *pstr)
{
	mu_stream_t tstr = *pstr;
	
	if (mu_debug_level_p(debug_handle, MU_DEBUG_PROT)) {
		int rc;
		mu_stream_t dstr;

		rc = mu_dbgstream_create(&dstr, MU_DIAG_DEBUG);
		if (rc)
			mu_error(_("cannot create debug stream; "
				   "transcript disabled: %s"),
				 mu_strerror(rc));
		else {
			rc = mu_xscript_stream_create(pstr, tstr, dstr, NULL);
			mu_stream_unref(dstr);
			if (rc)
				mu_error(_("cannot create transcript "
					   "stream: %s"),
					 mu_strerror(rc));
			else
				mu_stream_unref(tstr);
		}
	}
}

static int
spamd_send_stream(mu_stream_t out, mu_stream_t in)
{
  int rc;
  struct mu_buffer_query newbuf, oldbuf;
  int bufchg = 0;
  int xlev;
  int xlevchg = 0;
  
  /* Ensure effective transport buffering */
  if (mu_stream_ioctl(out, MU_IOCTL_TRANSPORT_BUFFER,
		      MU_IOCTL_OP_GET, &oldbuf) == 0) {
	  newbuf.type = MU_TRANSPORT_OUTPUT;
	  newbuf.buftype = mu_buffer_full;
	  newbuf.bufsize = 64*1024;
	  mu_stream_ioctl(out, MU_IOCTL_TRANSPORT_BUFFER, MU_IOCTL_OP_SET, 
			  &newbuf);
	  bufchg = 1;
  }

  if (!mu_debug_level_p(debug_handle, MU_DEBUG_TRACE9)) {
	  /* Mark out the following data as payload */
	  xlev = MU_XSCRIPT_PAYLOAD;
	  if (mu_stream_ioctl(out, MU_IOCTL_XSCRIPTSTREAM,
			      MU_IOCTL_XSCRIPTSTREAM_LEVEL, &xlev) == 0)
		  xlevchg = 1;
  }
  
  rc = mu_stream_copy(out, in, 0, NULL);

  /* Restore prior transport buffering and xscript level */
  if (bufchg)
    mu_stream_ioctl(out, MU_IOCTL_TRANSPORT_BUFFER, MU_IOCTL_OP_SET,  &oldbuf);
  if (xlevchg)
    mu_stream_ioctl(out, MU_IOCTL_XSCRIPTSTREAM,
		    MU_IOCTL_XSCRIPTSTREAM_LEVEL, &xlev);
  return rc;
}


static int
spamd_connect(eval_environ_t env, mu_stream_t *pstream,
	      const char *host, int port)
{
	int rc;
	char *fname;
	mu_stream_t tstr, istr, ostr;
	
	if (port) {
		fname = "mu_tcp_stream_create";
		rc = mu_tcp_stream_create(&tstr, host, port, MU_STREAM_RDWR);
	} else {
		fname = "mu_socket_stream_create";
		rc = mu_socket_stream_create(&tstr, host, MU_STREAM_RDWR);
	}

	MF_ASSERT(rc == 0, mfe_failure,
		  "%s: %s",
		  fname, mu_strerror(rc));

	mu_stream_set_buffer (tstr, mu_buffer_line, 0);

	rc = mu_filter_create(&istr, tstr, "CRLF",
			      MU_FILTER_DECODE, MU_FILTER_READ);
	if (rc) {
		mu_stream_unref(tstr);
		MF_THROW(mfe_failure,
			 "%s input filter: %s",
			 fname, mu_strerror(rc));
	}
	mu_stream_set_buffer(istr, mu_buffer_line, 0);
	
	rc = mu_filter_create(&ostr, tstr, "CRLF",
			      MU_FILTER_ENCODE, MU_FILTER_WRITE);
	if (rc) {
		mu_stream_unref(tstr);
		MF_THROW(mfe_failure,
			 "%s output filter: %s",
			 fname, mu_strerror(rc));
	}
	mu_stream_set_buffer(ostr, mu_buffer_line, 0);
	mu_stream_unref(tstr);
	
	rc = mu_iostream_create(&tstr, istr, ostr);
	mu_stream_unref(istr);
	mu_stream_unref(ostr);
	MF_ASSERT(rc == 0, mfe_failure,
		  "I/O stream: %s",
		  mu_strerror(rc));
	mu_stream_set_buffer(tstr, mu_buffer_line, 0);
	set_xscript(&tstr);
	*pstream = tstr;
	return rc;
}

static int
spamd_get_line(mu_stream_t stream, char **pbuffer, size_t *psize)
{
	int rc = mu_stream_getline(stream, pbuffer, psize, NULL);
	if (rc == 0)
		mu_rtrim_class (*pbuffer, MU_CTYPE_SPACE);
	return rc;
}

#define char_to_num(c) (c-'0')

static void
decode_float(long *vn, char *str, int digits)
{
	long v;
	size_t frac = 0;
	size_t base = 1;
	int i;
	int negative = 0;
  
	for (i = 0; i < digits; i++)
		base *= 10;
  
	v = strtol(str, &str, 10);
	if (v < 0) {
		negative = 1;
		v = - v;
	}
  
	v *= base;
	if (*str == '.') {
		for (str++, i = 0; *str && i < digits; i++, str++)
			frac = frac * 10 + char_to_num (*str);
		if (*str) {
			if (char_to_num(*str) >= 5)
				frac++;
		}
		else
			for (; i < digits; i++)
				frac *= 10;
	}
	*vn = v + frac;
	if (negative)
		*vn = - *vn;
}

static int
decode_boolean (char *str)
{
	if (strcasecmp (str, "true") == 0)
		return 1;
	else if (strcasecmp (str, "false") == 0)
		return 0;
	/*else?*/
	return 0;
}

typedef void (*signal_handler_fn)(int);

static signal_handler_fn
set_signal_handler (int sig, signal_handler_fn h)
{
	struct sigaction act, oldact;
	act.sa_handler = h;
	sigemptyset (&act.sa_mask);
	act.sa_flags = 0;
	sigaction (sig, &act, &oldact);
	return oldact.sa_handler;
}

static int got_sigpipe;

static void
sigpipe_handler (int sig)
{
	got_sigpipe = 1;
}

mu_stream_t
open_connection(eval_environ_t env, char *urlstr, char **phost)
{
	char *path = NULL;
	short port = 0;
	mu_stream_t str = NULL;
	int rc;
	
	if (urlstr[0] == '/') {
		path = strdup(urlstr);
		if (!path)
			runtime_error(env, _("not enough memory"));
	} else {
		const char *buf;
		mu_url_t url = NULL;
		
		rc = mu_url_create(&url, urlstr);
		if (rc)
			MF_THROW(mfe_failure, 
				 _("cannot create URL from `%s': %s"),
				 urlstr, mu_strerror(rc));
		
		if (rc = mu_url_sget_scheme(url, &buf)) {
			mu_url_destroy(&url);
			MF_THROW(mfe_url,
				     _("%s: cannot get scheme: %s"),
				     urlstr, mu_strerror(rc));
		}

		if (strcmp(buf, "file") == 0
		    || strcmp(buf, "socket") == 0) {
			if (rc = mu_url_aget_path(url, &path)) {
				MF_THROW(mfe_url,
					 _("%s: cannot get path: %s"),
					 urlstr, mu_strerror(rc));
			}
		} else if (strcmp(buf, "tcp") == 0) {
			unsigned n;
			
			if (rc = mu_url_get_port(url, &n)) {
				mu_url_destroy(&url);
				MF_THROW(mfe_url,
					 _("%s: cannot get port: %s"),
					 urlstr, mu_strerror(rc));
			}

			if (n == 0 || (port = n) != n) {
				mu_url_destroy(&url);
				MF_THROW(mfe_range,
					 _("port out of range: %u"),
					 n);
			}
			
			if (rc = mu_url_aget_host(url, &path)) {
				mu_url_destroy(&url);
				MF_THROW(mfe_url,
					 _("%s: cannot get host: %s"),
					 urlstr, mu_strerror(rc));
			}
		} else
			MF_THROW(mfe_url,
				 _("invalid URL: %s"), buf);

		mu_url_destroy(&url);
	}

	rc = spamd_connect(env, &str, path, port);
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, str, _builtin_stream_cleanup);
	if (rc == 0 && phost) {
		if (port)
			*phost = path;
		else
			*phost = NULL;
	}
	return str;
}	

MF_DSEXP
MF_DEFUN(spamc, NUMBER, NUMBER nmsg, STRING urlstr, NUMBER prec, NUMBER command)
{
	mu_message_t msg;
	mu_stream_t mstr;
	mu_off_t msize;
	size_t lines;
	mu_stream_t ostr;
	signal_handler_fn handler;
	char *buffer = NULL;
	size_t bufsize = 0;
	size_t n;
	char version_str[19];
	char spam_str[6], score_str[21], threshold_str[21];
	long version;
	int result;
	long score, threshold;
	char *cmdstr;
	int rc;
	
	msg = bi_message_from_descr(env, nmsg);
	rc = mu_message_get_streamref(msg, &mstr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_get_streamref: %s",
		  mu_strerror (rc));
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, mstr, _builtin_stream_cleanup);
	
	rc = mu_stream_size(mstr, &msize);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_size: %s",
		  mu_strerror (rc));
	
	ostr = open_connection(env, urlstr, NULL);

	/* And that, finally, gets the number of lines */
	rc = mu_message_lines(msg, &lines);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_message_lines: %s",
		  mu_strerror (rc));
	msize += lines;
	
	switch (command) {
	case SA_REPORT:
		cmdstr = "REPORT";
		break;
	case SA_SYMBOLS:
		cmdstr = "SYMBOLS";
		break;
	case SA_LEARN_SPAM:
	case SA_LEARN_HAM:
	case SA_FORGET:
		cmdstr = "TELL";
		break;
	default:
		MF_THROW(mfe_failure,
			 _("unknown command: %ld"), command);
	}
	mu_stream_printf(ostr, "%s SPAMC/1.2\n", cmdstr);

	switch (command) {
	case SA_LEARN_SPAM:
		mu_stream_printf(ostr,
				 "Message-class: spam\n"
				 "Set: local\n");
		break;
	case SA_LEARN_HAM:
		mu_stream_printf(ostr,
				 "Message-class: ham\n"
				 "Set: local\n");
		break;
	case SA_FORGET:
		mu_stream_printf(ostr,
				 "Remove: local\n");
	}
	
	mu_stream_printf(ostr, "Content-length: %lu\n",
			 (unsigned long) msize);
	/*FIXME: spamd_send_command(ostr, "User: %s", ??) */
	got_sigpipe = 0;
	handler = set_signal_handler(SIGPIPE, sigpipe_handler);
	mu_stream_write(ostr, "\n", 1, NULL);
	rc = spamd_send_stream(ostr, mstr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  _("send stream failed: %s"),
		  mu_strerror(rc));

	mu_stream_shutdown(ostr, MU_STREAM_WRITE);
	set_signal_handler(SIGPIPE, handler);

	MF_DCL_CLEANUP(CLEANUP_ALWAYS, buffer);
	spamd_get_line(ostr, &buffer, &bufsize);
	if (got_sigpipe)
		MF_THROW(mfe_failure, _("remote side has closed connection"));

	MF_ASSERT(sscanf(buffer, "SPAMD/%18s %d %*s", version_str,
			 &result) == 2,
		  mfe_failure,
		  _("spamd responded with bad string '%s'"),
		  buffer);

	decode_float(&version, version_str, 1);
	MF_ASSERT(version >= 10,
		  mfe_failure,
		  _("unsupported SPAMD version: %s"),
		  version_str);

	MF_ASSERT(result == 0, mfe_failure, "%s", buffer);
	
	spamd_get_line(ostr, &buffer, &bufsize);

	switch (command) {
	case SA_REPORT:
	case SA_SYMBOLS:
		MF_ASSERT(sscanf(buffer, "Spam: %5s ; %20s / %20s",
				 spam_str, score_str, threshold_str) == 3,
			  mfe_failure,
			  _("spamd responded with bad Spam header '%s'"),
			  buffer);

		result = decode_boolean(spam_str);
		decode_float(&score, score_str, prec);
		decode_float(&threshold, threshold_str, prec);
		
		MF_VAR_REF(sa_score, long, score);
		MF_VAR_REF(sa_threshold, long, threshold);

		/* Skip newline */
		spamd_get_line(ostr, &buffer, &bufsize);
		break;
		
	case SA_LEARN_SPAM:
	case SA_LEARN_HAM:
		result = !!strcmp(buffer, "DidSet: local");
		break;
		
	case SA_FORGET:
		result = !!strcmp(buffer, "DidRemove: local");
		break;
	}
	
	switch (command) {
	case SA_REPORT:
		MF_OBSTACK_BEGIN();
		while (mu_stream_getline(ostr, &buffer, &bufsize, &n) == 0
		       && n > 0) 
			MF_OBSTACK_GROW(buffer, n);
		MF_OBSTACK_1GROW(0);
		MF_VAR_REF(sa_keywords, ptr, mf_c_val(MF_OBSTACK_FINISH, ptr));//FIXME
		break;

	case SA_SYMBOLS:
		/* Read symbol list */
		spamd_get_line(ostr, &buffer, &bufsize);
		MF_VAR_SET_STRING(sa_keywords, buffer);
	}

	/* Just in case */
	while (mu_stream_getline(ostr, &buffer, &bufsize, &n) == 0
	       && n > 0) 
		/* Drain input */;
	
	MF_RETURN(result);
}
END

static int
clamav_open_data_stream(mu_stream_t *retstr, const char *host, unsigned port)
{
	mu_stream_t tstr, dstr;
	int rc = mu_tcp_stream_create(&dstr, host, port, 0);
	if (rc)
		return rc;
	rc = mu_filter_create(&tstr, dstr, "CRLF",
			      MU_FILTER_ENCODE, MU_FILTER_WRITE);
	mu_stream_unref(dstr);
	if (rc)
		return rc;
	set_xscript(&tstr);
	*retstr = tstr;
	return 0;
}
	
MF_DSEXP
MF_DEFUN(clamav, NUMBER, NUMBER nmsg, STRING urlstr)
{
	mu_message_t msg;
	mu_stream_t mstr;
	mu_stream_t cstr, dstr;
	char *buffer = NULL;
	size_t bufsize = 0;
	char *host;
	unsigned short port;
	int rc;
	signal_handler_fn handler;
	char *p;

	msg = bi_message_from_descr(env, nmsg);
	rc = mu_message_get_streamref(msg, &mstr);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_stream_get_streamref: %s",
		  mu_strerror (rc));
	MF_DCL_CLEANUP(CLEANUP_ALWAYS, mstr, _builtin_stream_cleanup);

	MF_DCL_CLEANUP(CLEANUP_ALWAYS, buffer);
	cstr = open_connection(env, urlstr, &host);
	
	mu_stream_printf(cstr, "STREAM\n");
	spamd_get_line(cstr, &buffer, &bufsize);
	MF_ASSERT(sscanf(buffer, "PORT %hu", &port) == 1,
		  mfe_failure,
		  _("bad response from clamav: expected `PORT' but found `%s'"),
		  buffer);

	if (!host) 
		host = strdup("127.0.0.1"); /* FIXME */
	rc = clamav_open_data_stream(&dstr, host, port);
	free(host);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  "mu_tcp_stream_create: %s",
		  mu_strerror(rc));
	
	handler = set_signal_handler(SIGPIPE, sigpipe_handler);
	rc = spamd_send_stream(dstr, mstr);
	mu_stream_shutdown(dstr, MU_STREAM_WRITE);
	mu_stream_destroy(&dstr);
	set_signal_handler(SIGPIPE, handler);
	MF_ASSERT(rc == 0,
		  mfe_failure,
		  _("sending to stream failed: %s"),
		  mu_strerror(rc));

	rc = spamd_get_line(cstr, &buffer, &bufsize);
	//FIXME MF_CLEANUP(cstr);
	MF_ASSERT(rc == 0, mfe_failure, _("error reading clamav response: %s"),
		  mu_strerror(rc));

	p = strrchr(buffer, ' ');
        MF_ASSERT(p, mfe_failure,
	          _("unknown clamav response: %s"), buffer);
	++p;
	if (strncmp(p, "OK", 2) == 0)
		rc = 0;
	else if (strncmp(p, "FOUND", 5) == 0) {
		char *s;
			
		*--p = '\0';
		s = strrchr(buffer, ' ');
		if (!s)
			s = buffer;
		else
			s++;
		MF_VAR_SET_STRING(clamav_virus_name, s);

		MF_DEBUG(MU_DEBUG_TRACE1,
		         ("%sclamav found %s",
		          mailfromd_msgid(env_get_context(env)), s));
		rc = 1;
	} else if (strncmp(p, "ERROR", 5) == 0) {
		/* FIXME: mf code */
		MF_THROW(mfe_failure, _("clamav error: %s"), buffer);
	} else {
		MF_THROW(mfe_failure,
			 _("unknown clamav response: %s"),
			 buffer);
	}
	MF_RETURN(rc);
}
END

