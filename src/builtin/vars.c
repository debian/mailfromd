#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "vars.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



#include "filenames.h" /* For DEFAULT_FROM_ADDRESS */
static size_t rcpt_count_loc
#line 20 "vars.bi"
;
static size_t milter_client_family_loc
#line 21 "vars.bi"
;
static size_t milter_client_address_loc
#line 22 "vars.bi"
;
static size_t milter_server_family_loc
#line 23 "vars.bi"
;
static size_t milter_server_address_loc
#line 24 "vars.bi"
;
static size_t milter_server_id_loc
#line 25 "vars.bi"
;
static size_t milter_state_loc
#line 26 "vars.bi"
;

void
set_milter_state(eval_environ_t env, enum smtp_state state)
{
	mf_c_val(*env_data_ref(env, milter_state_loc),long) = (state);
}

/* Functions to access %rcpt_count */
unsigned long
get_rcpt_count(eval_environ_t env)
{
 	return mf_c_val(*env_data_ref(env, rcpt_count_loc),long) ;
}

void
clear_rcpt_count(eval_environ_t env)
{
	mf_c_val(*env_data_ref(env, rcpt_count_loc),long) = (0);
}

void
incr_rcpt_count(eval_environ_t env)
{
	env_var_inc(env, rcpt_count_loc);
}

/* define_milter_address name */
#line 96

	  

#line 98

#line 98
void
#line 98
set_milter_server_address(eval_environ_t env, milter_sockaddr_t *addr,
#line 98
		      socklen_t len)
#line 98
{
#line 98
	char *path;
#line 98
	
#line 98
	switch (addr->sa.sa_family) {
#line 98
	case PF_INET:
#line 98
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_INET);
#line 98
		
#line 98
{ size_t __off;
#line 98
  const char *__s = inet_ntoa(addr->sin.sin_addr);
#line 98
  if (__s)
#line 98
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 98
  else
#line 98
     __off = 0;
#line 98
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 98
;
#line 98
		break;
#line 98

#line 98
	case PF_INET6: {
#line 98
		char hostbuf[NI_MAXHOST];
#line 98

#line 98
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_INET6);
#line 98
		if (getnameinfo(&addr->sa, sizeof(addr->sin6),
#line 98
				hostbuf, sizeof hostbuf,
#line 98
				NULL, 0,
#line 98
				NI_NUMERICHOST|NI_NUMERICSERV))
#line 98
			hostbuf[0] = 0;
#line 98
		
#line 98
{ size_t __off;
#line 98
  const char *__s = hostbuf;
#line 98
  if (__s)
#line 98
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 98
  else
#line 98
     __off = 0;
#line 98
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 98
;
#line 98
		break;
#line 98
	}
#line 98
		
#line 98
	case PF_UNIX:
#line 98
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = (MFAM_UNIX);
#line 98
		if (len == sizeof (addr->sa.sa_family))
#line 98
			path = "";
#line 98
		else
#line 98
			path = addr->sunix.sun_path;
#line 98
		
#line 98
{ size_t __off;
#line 98
  const char *__s = path;
#line 98
  if (__s)
#line 98
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 98
  else
#line 98
     __off = 0;
#line 98
  mf_c_val(*env_data_ref(env, milter_server_address_loc),size) = (__off); }
#line 98
;
#line 98
		break;
#line 98
		
#line 98
	default:
#line 98
		/* FIXME */
#line 98
		mf_c_val(*env_data_ref(env, milter_server_family_loc),long) = ((long)addr->sa.sa_family);
#line 98
	}
#line 98
}
#line 98

#line 98

#line 98

#line 98


#line 99

#line 99
void
#line 99
set_milter_client_address(eval_environ_t env, milter_sockaddr_t *addr,
#line 99
		      socklen_t len)
#line 99
{
#line 99
	char *path;
#line 99
	
#line 99
	switch (addr->sa.sa_family) {
#line 99
	case PF_INET:
#line 99
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_INET);
#line 99
		
#line 99
{ size_t __off;
#line 99
  const char *__s = inet_ntoa(addr->sin.sin_addr);
#line 99
  if (__s)
#line 99
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 99
  else
#line 99
     __off = 0;
#line 99
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 99
;
#line 99
		break;
#line 99

#line 99
	case PF_INET6: {
#line 99
		char hostbuf[NI_MAXHOST];
#line 99

#line 99
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_INET6);
#line 99
		if (getnameinfo(&addr->sa, sizeof(addr->sin6),
#line 99
				hostbuf, sizeof hostbuf,
#line 99
				NULL, 0,
#line 99
				NI_NUMERICHOST|NI_NUMERICSERV))
#line 99
			hostbuf[0] = 0;
#line 99
		
#line 99
{ size_t __off;
#line 99
  const char *__s = hostbuf;
#line 99
  if (__s)
#line 99
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 99
  else
#line 99
     __off = 0;
#line 99
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 99
;
#line 99
		break;
#line 99
	}
#line 99
		
#line 99
	case PF_UNIX:
#line 99
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = (MFAM_UNIX);
#line 99
		if (len == sizeof (addr->sa.sa_family))
#line 99
			path = "";
#line 99
		else
#line 99
			path = addr->sunix.sun_path;
#line 99
		
#line 99
{ size_t __off;
#line 99
  const char *__s = path;
#line 99
  if (__s)
#line 99
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 99
  else
#line 99
     __off = 0;
#line 99
  mf_c_val(*env_data_ref(env, milter_client_address_loc),size) = (__off); }
#line 99
;
#line 99
		break;
#line 99
		
#line 99
	default:
#line 99
		/* FIXME */
#line 99
		mf_c_val(*env_data_ref(env, milter_client_family_loc),long) = ((long)addr->sa.sa_family);
#line 99
	}
#line 99
}
#line 99

#line 99

#line 99

#line 99



#line 101

#line 101
void
#line 101
set_milter_server_id(eval_environ_t env, const char *id)
#line 101
{
#line 101
	
#line 101
{ size_t __off;
#line 101
  const char *__s = id ? id : "";
#line 101
  if (__s)
#line 101
     strcpy((char*)env_data_ref(env, __off = heap_reserve(env, strlen(__s) + 1)), __s);
#line 101
  else
#line 101
     __off = 0;
#line 101
  mf_c_val(*env_data_ref(env, milter_server_id_loc),size) = (__off); }
#line 101
;
#line 101
}
#line 101

#line 101

#line 101

#line 107

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
vars_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 20 "vars.bi"
	builtin_variable_install("rcpt_count", dtype_number, SYM_VOLATILE, &rcpt_count_loc);
#line 21 "vars.bi"
	builtin_variable_install("milter_client_family", dtype_number, SYM_VOLATILE|SYM_PRECIOUS, &milter_client_family_loc);
#line 22 "vars.bi"
	builtin_variable_install("milter_client_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_client_address_loc);
#line 23 "vars.bi"
	builtin_variable_install("milter_server_family", dtype_number, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_family_loc);
#line 24 "vars.bi"
	builtin_variable_install("milter_server_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_address_loc);
#line 25 "vars.bi"
	builtin_variable_install("milter_server_id", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &milter_server_id_loc);
#line 26 "vars.bi"
	builtin_variable_install("milter_state", dtype_number, SYM_VOLATILE|SYM_PRECIOUS, &milter_state_loc);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

