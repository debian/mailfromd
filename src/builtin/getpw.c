#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "getpw.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2009-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */



static void
return_passwd(eval_environ_t env, struct passwd *pw)
{
	char buf[NUMERIC_BUFSIZE_BOUND];
	
	
#line 24
do {
#line 24
  char *__s = pw->pw_name;
#line 24
  heap_obstack_grow(env, __s, strlen(__s));
#line 24
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 26
do {
#line 26
  char *__s = pw->pw_passwd;
#line 26
  heap_obstack_grow(env, __s, strlen(__s));
#line 26
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	snprintf(buf, sizeof(buf), "%lu", (unsigned long) pw->pw_uid);
	
#line 29
do {
#line 29
  char *__s = buf;
#line 29
  heap_obstack_grow(env, __s, strlen(__s));
#line 29
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	snprintf(buf, sizeof(buf), "%lu", (unsigned long) pw->pw_gid);
	
#line 32
do {
#line 32
  char *__s = buf;
#line 32
  heap_obstack_grow(env, __s, strlen(__s));
#line 32
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 34
do {
#line 34
  char *__s = pw->pw_gecos;
#line 34
  heap_obstack_grow(env, __s, strlen(__s));
#line 34
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 36
do {
#line 36
  char *__s = pw->pw_dir;
#line 36
  heap_obstack_grow(env, __s, strlen(__s));
#line 36
} while (0);
	do { char __c = ':'; heap_obstack_grow(env, &__c, 1); } while(0);
	
#line 38
do {
#line 38
  char *__s = pw->pw_shell;
#line 38
  heap_obstack_grow(env, __s, strlen(__s));
#line 38
} while (0);
	do { char __c = 0; heap_obstack_grow(env, &__c, 1); } while(0);
}

void
#line 42
bi_mappwnam(eval_environ_t env)
#line 42

#line 42

#line 42 "getpw.bi"
{
#line 42
	
#line 42

#line 42
        char *  name;
#line 42
        
#line 42

#line 42
        get_string_arg(env, 0, &name);
#line 42
        
#line 42
        adjust_stack(env, 1);
#line 42

#line 42

#line 42
	if (builtin_module_trace(BUILTIN_IDX_getpw))
#line 42
		prog_trace(env, "mappwnam %s",name);;
#line 42

{
	struct passwd *pw = getpwnam(name);
	
#line 45
do {
#line 45
  push(env, (STKVAL)(mft_number)(pw != NULL));
#line 45
  goto endlab;
#line 45
} while (0);
}
endlab:
#line 47
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 47
	return;
#line 47
}

void
#line 49
bi_getpwnam(eval_environ_t env)
#line 49

#line 49

#line 49 "getpw.bi"
{
#line 49
	
#line 49

#line 49
        char * MFL_DATASEG name;
#line 49
        
#line 49

#line 49
        get_string_arg(env, 0, &name);
#line 49
        
#line 49
        adjust_stack(env, 1);
#line 49

#line 49

#line 49
	if (builtin_module_trace(BUILTIN_IDX_getpw))
#line 49
		prog_trace(env, "getpwnam %s",name);;
#line 49

{
	struct passwd *pw = getpwnam(name);
		if (!(pw != NULL))
#line 52
		(
#line 52
	env_throw_bi(env, mfe_not_found, "getpwnam", _("%s: user not found"),name)
#line 52
)
#line 54
;
	heap_obstack_begin(env);
	return_passwd(env, pw);
	
#line 57
do {
#line 57
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 57
  goto endlab;
#line 57
} while (0);
}
endlab:
#line 59
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 59
	return;
#line 59
}

void
#line 61
bi_mappwuid(eval_environ_t env)
#line 61

#line 61

#line 61 "getpw.bi"
{
#line 61
	
#line 61

#line 61
        long  uid;
#line 61
        
#line 61

#line 61
        get_numeric_arg(env, 0, &uid);
#line 61
        
#line 61
        adjust_stack(env, 1);
#line 61

#line 61

#line 61
	if (builtin_module_trace(BUILTIN_IDX_getpw))
#line 61
		prog_trace(env, "mappwuid %lu",uid);;
#line 61

{
	struct passwd *pw = getpwuid(uid);
	
#line 64
do {
#line 64
  push(env, (STKVAL)(mft_number)(pw != NULL));
#line 64
  goto endlab;
#line 64
} while (0);
}
endlab:
#line 66
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 66
	return;
#line 66
}

void
#line 68
bi_getpwuid(eval_environ_t env)
#line 68

#line 68

#line 68 "getpw.bi"
{
#line 68
	
#line 68

#line 68
        long  uid;
#line 68
        
#line 68

#line 68
        get_numeric_arg(env, 0, &uid);
#line 68
        
#line 68
        adjust_stack(env, 1);
#line 68

#line 68

#line 68
	if (builtin_module_trace(BUILTIN_IDX_getpw))
#line 68
		prog_trace(env, "getpwuid %lu",uid);;
#line 68

{
	struct passwd *pw = getpwuid(uid);
		if (!(pw != NULL))
#line 71
		(
#line 71
	env_throw_bi(env, mfe_not_found, "getpwuid", _("%lu: uid not found"),(unsigned long) uid)
#line 71
)
#line 73
;
	heap_obstack_begin(env);
	return_passwd(env, pw);
	
#line 76
do {
#line 76
  push(env, (STKVAL) (heap_obstack_finish(env)));
#line 76
  goto endlab;
#line 76
} while (0);
}
endlab:
#line 78
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 78
	return;
#line 78
}

#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
getpw_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 42 "getpw.bi"
va_builtin_install_ex("mappwnam", bi_mappwnam, 0, dtype_number, 1, 0, 0|0, dtype_string);
#line 49 "getpw.bi"
va_builtin_install_ex("getpwnam", bi_getpwnam, 0, dtype_string, 1, 0, 0|0, dtype_string);
#line 61 "getpw.bi"
va_builtin_install_ex("mappwuid", bi_mappwuid, 0, dtype_number, 1, 0, 0|0, dtype_number);
#line 68 "getpw.bi"
va_builtin_install_ex("getpwuid", bi_getpwuid, 0, dtype_string, 1, 0, 0|0, dtype_number);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

