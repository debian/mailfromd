#line 1020 "../../src/builtin/snarf.m4"
/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#line 1020
#ifdef HAVE_CONFIG_H
#line 1020
# include <config.h>
#line 1020
#endif
#line 1020
#include <sys/types.h>
#line 1020

#line 1020
#include "mailfromd.h"
#line 1020
#include "prog.h"
#line 1020
#include "builtin.h"
#line 1020

#line 1020

#line 1060 "../../src/builtin/snarf.m4"

/* End of snarf.m4 */
#line 1 "callout.bi"
/* This file is part of Mailfromd.             -*- c -*-
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/* Run-time support for callout functions */

#include "filenames.h"
#include "callout.h"
#include "srvcfg.h"

static size_t ehlo_domain_loc
#line 23 "callout.bi"
;
static size_t mailfrom_address_loc
#line 24 "callout.bi"
;

int provide_callout;

/* #pragma provide-callout */
#line 29 "callout.bi"

#line 29
static void _pragma_provide_callout (int argc, char **argv, const char *text)
#line 29

{
	provide_callout = 1;
}

void
#line 34
bi_default_callout_server_url(eval_environ_t env)
#line 34

#line 34

#line 34 "callout.bi"
{
#line 34
	
#line 34

#line 34
        
#line 34

#line 34
        
#line 34
        adjust_stack(env, 0);
#line 34

#line 34

#line 34
	if (builtin_module_trace(BUILTIN_IDX_callout))
#line 34
		prog_trace(env, "default_callout_server_url");;
#line 34

{
	
#line 36
do {
#line 36
  pushs(env, callout_server_url ?
#line 36
		  callout_server_url : DEFAULT_CALLOUT_SOCKET);
#line 36
  goto endlab;
#line 36
} while (0);
#line 38
}
endlab:
#line 39
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 39
	return;
#line 39
}

void
#line 41
bi_callout_transcript(eval_environ_t env)
#line 41

#line 41

#line 41 "callout.bi"
{
#line 41
	
#line 41

#line 41
        long  val;
#line 41
        
#line 41
        long __bi_argcnt;
#line 41
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 41
        if (__bi_argcnt > 0)
#line 41
                get_numeric_arg(env, 1, &val);
#line 41
        
#line 41
        adjust_stack(env, __bi_argcnt + 1);
#line 41

#line 41

#line 41
	if (builtin_module_trace(BUILTIN_IDX_callout))
#line 41
		prog_trace(env, "callout_transcript %lu",((__bi_argcnt > 0) ? val : 0));;
#line 41

{
	int oldval = smtp_transcript;
	if ((__bi_argcnt > 0))
		smtp_transcript = val;
	
#line 46
do {
#line 46
  push(env, (STKVAL)(mft_number)(oldval));
#line 46
  goto endlab;
#line 46
} while (0);
}
endlab:
#line 48
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 48
	return;
#line 48
}

void
#line 50
bi_callout_starttls(eval_environ_t env)
#line 50

#line 50

#line 50 "callout.bi"
{
#line 50
	
#line 50

#line 50
        char * MFL_DATASEG val;
#line 50
        
#line 50
        long __bi_argcnt;
#line 50
        get_numeric_arg(env, 0, &__bi_argcnt);
#line 50
        if (__bi_argcnt > 0)
#line 50
                get_string_arg(env, 1, &val);
#line 50
        
#line 50
        adjust_stack(env, __bi_argcnt + 1);
#line 50

#line 50

#line 50
	if (builtin_module_trace(BUILTIN_IDX_callout))
#line 50
		prog_trace(env, "callout_starttls %s",((__bi_argcnt > 0) ? val : ""));;
#line 50

{
	int oldval = smtp_use_tls;
	if ((__bi_argcnt > 0)) {
			if (!(smtp_str_to_starttls(val, &smtp_use_tls) == 0))
#line 54
		(
#line 54
	env_throw_bi(env, mfe_inval, "callout_starttls", "bad starttls value")
#line 54
)
#line 56
;
	}
	
#line 58
do {
#line 58
  pushs(env, smtp_starttls_to_str(oldval));
#line 58
  goto endlab;
#line 58
} while (0);
}
endlab:
#line 60
        env_function_cleanup_flush(env, CLEANUP_RETURN);
#line 60
	return;
#line 60
}

 
#line 68
	


#line 1020 "../../src/builtin/snarf.m4"

#line 1020

#line 1020

#line 1020
void
#line 1020
callout_init_builtin(void)
#line 1020
{
#line 1020
	
#line 1020
	#line 23 "callout.bi"
	builtin_variable_install("ehlo_domain", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &ehlo_domain_loc);
#line 24 "callout.bi"
	builtin_variable_install("mailfrom_address", dtype_string, SYM_VOLATILE|SYM_PRECIOUS, &mailfrom_address_loc);
#line 29 "callout.bi"

#line 29
install_pragma("provide-callout", 1, 1, _pragma_provide_callout);
#line 34 "callout.bi"
va_builtin_install_ex("default_callout_server_url", bi_default_callout_server_url, 0, dtype_string, 0, 0, 0|0, dtype_unspecified);
#line 41 "callout.bi"
va_builtin_install_ex("callout_transcript", bi_callout_transcript, 0, dtype_number, 1, 1, 0|0, dtype_number);
#line 50 "callout.bi"
va_builtin_install_ex("callout_starttls", bi_callout_starttls, 0, dtype_string, 1, 1, 0|0, dtype_string);

#line 1020 "../../src/builtin/snarf.m4"
	
#line 1020
	 if (ehlo_domain)
#line 1020
		 ds_init_variable("ehlo_domain", ehlo_domain);
#line 1020
	 if (mailfrom_address)
#line 1020
		 ds_init_variable("mailfrom_address", mailfrom_address);
#line 1020

#line 1020
}
#line 1020 "../../src/builtin/snarf.m4"

