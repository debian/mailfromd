/* This file is part of Mailfromd.
   Copyright (C) 2005-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/alloc.h>

typedef enum {
	tls_never,
	tls_always,
	tls_ondemand
} smtp_starttls_t;

extern time_t negative_expire_interval;
extern char *ehlo_domain;
extern char *mailfrom_address;
extern int enable_vrfy;
extern smtp_starttls_t smtp_use_tls;
extern mu_debug_handle_t callout_debug_handle;

const char *mf_status_str(mf_status s);

/* callout.c */
typedef void (*smtp_io_callback_t) (void *, const char *, const char *);
	
typedef struct smtp_io_data *smtp_io_t;

smtp_io_t smtp_io_create(const char *id, time_t timeout[],
			 smtp_io_callback_t callback,
			 void *closure);
void smtp_io_setup_callout(struct smtp_io_data *iop,
			   const char *email,
			   const char *ehlo,
			   const char *mailfrom,
			   smtp_starttls_t tls);
void smtp_io_set_timeouts(struct smtp_io_data *io, time_t *to);

void smtp_io_free(struct smtp_io_data *iop);

const char *smtp_io_id(struct smtp_io_data *iop);
const char *smtp_io_email(struct smtp_io_data *iop);

mf_status callout_host(struct smtp_io_data *, const char *);
mf_status callout_mx(struct smtp_io_data *, const char *, int *);
mf_status callout_strict(struct smtp_io_data *, const char *);
mf_status callout_standard(struct smtp_io_data *);

void transcript(const char *id, char *prefix, const char *msg);

extern int callout_session_server(const char *id, int fd,
				  struct sockaddr const *sa, socklen_t len,
				  void *server_data, void *srvman_data);

void libcallout_init(void);

int smtp_str_to_starttls(char const *str, smtp_starttls_t *val);
char const *smtp_starttls_to_str(smtp_starttls_t t);
