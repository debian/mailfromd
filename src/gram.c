/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "gram.y" /* yacc.c:339  */

/* This file is part of Mailfromd.
   Copyright (C) 2005-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <fnmatch.h>
#include <netdb.h>
#include <limits.h>
#include "mailfromd.h"
#include <mailutils/yyloc.h>
#include "global.h"
#include "prog.h"
#include "optab.h"

static void free_node(NODE *node); 
static void set_poll_arg(struct poll_data *poll, int kw, NODE *expr);
static int codegen(prog_counter_t *pc, NODE *node, struct exmask *exmask,
		   size_t nautos);
static void mark(NODE *node);
static void dataseg_layout(void);
static void regex_layout(void);
static int optimize_tree(NODE *node);
static void compile_tree(NODE *node);
static NODE *reverse(NODE *in);
static size_t nodelistlength(NODE *list);
static NODE *function_call(struct function *function, size_t count,
                           NODE *subtree);
static NODE *declare_function(struct function *func,
		              struct mu_locus_range const *loc,
                              size_t nautos);
static data_type_t node_type(NODE *node);
static NODE *cast_arg_list(NODE *args, size_t parmc, data_type_t *parmtype,
			   data_type_t vartype);
void add_xref(struct variable *var, struct mu_locus_range const *locus);
static struct variable *vardecl(const char *name, data_type_t type,
                                storage_class_t sc,
		                struct mu_locus_range const *loc);
static struct variable *externdecl(const char *name, struct value *value,
		                   struct mu_locus_range const *loc);
static int initialize_variable(struct variable *var, struct value *val,
                               struct mu_locus_range const *locus);
static void apply_deferred_init(void);
static NODE *create_asgn_node(struct variable *var, NODE *expr,
		              struct mu_locus_range const *loc);

NODE *defined_parm(struct variable *var, struct mu_locus_range const *locus);
 
static void register_auto(struct variable *var);
static void unregister_auto(struct variable *var);
static size_t forget_autos(size_t nparam, size_t prev, size_t hidden_arg);
static void optimize(NODE *node);

static NODE *root_node[smtp_state_count];
prog_counter_t entry_point[smtp_state_count];

int regex_flags;        /* Should default to REG_NOSUB ? */
unsigned error_count;   /* Number of detected errors */
size_t variable_count = 0;  /* Number of variables in the data segment. */
size_t precious_count = 0;

static enum smtp_state state_tag; /* State tag of the currently processed
                                      PROG */

static struct function *func;   /* The currently compiled function */
static prog_counter_t jump_pc;  /* Pointer to the chain of jmp instructions */
static prog_counter_t action_hook_pc; /* Chain of action hook addresses */
/* Outermost context decides how to code exits from catches.
   Innermost context is used to access parameters. */
enum lexical_context outer_context, inner_context;

size_t catch_nesting;  /* Nesting level for catch statements */

static struct stmtlist genstmt;     /* List of generated statements */

/* State handlers and their positional parameters */ 
struct state_parms {
        int cnt;                /* Number or positional parameters */
        data_type_t types[4];   /* Their data types */
} state_parms[smtp_state_count] = {
        [smtp_state_connect] = { 4, { dtype_string, dtype_number, dtype_number, dtype_string } },
        [smtp_state_helo] = { 1, { dtype_string } },
        [smtp_state_envfrom] = { 2, { dtype_string, dtype_string } },
        [smtp_state_envrcpt] = { 2, { dtype_string, dtype_string } },
        [smtp_state_header] = { 2, { dtype_string, dtype_string } },
        [smtp_state_body] = { 2, { dtype_pointer, dtype_number } },
	[smtp_state_action] = { 4, { dtype_number, dtype_string, dtype_string, dtype_string } },
};

struct parmtype {
        struct parmtype *next;
        data_type_t type;
};

static int
parmcount_none()
{
        return 0;
}

static data_type_t
parmtype_none(int n)
{
        return dtype_unspecified;
}

static int
parmcount_handler()
{
        return state_parms[state_tag].cnt;
}

static data_type_t
parmtype_handler(int n)
{
        return state_parms[state_tag].types[n-1];
}

static int
parmcount_catch()
{
        return 3;
}

static data_type_t
parmtype_catch(int n)
{
        switch (n) {
        case 1:
                return dtype_number;
        case 2:
                return dtype_string;
	case 3:
		return dtype_number;
        }
        abort();
}

static int
parmcount_function()
{
        return func->parmcount;
}

static data_type_t
parmtype_function(int n)
{
        if (func->varargs && n > func->parmcount)
                return func->vartype;
        return func->parmtype[n-1];
}

struct parminfo {
        int (*parmcount)(void);
        data_type_t (*parmtype)(int n);
} parminfo[] = {
        { parmcount_none, parmtype_none },
        { parmcount_handler, parmtype_handler },
        { parmcount_catch, parmtype_catch },
        { parmcount_function, parmtype_function },
};

#define PARMCOUNT() parminfo[inner_context].parmcount()
#define PARMTYPE(n) parminfo[inner_context].parmtype(n)
#define FUNC_HIDDEN_ARGS(f) (((f)->optcount || (f)->varargs) ? 1 : 0)


data_type_t
string_to_type(const char *s)
{
        if (strcmp(s, "n") == 0)
                return dtype_number;
        else if (strcmp(s, "s") == 0)
                return dtype_string;
        else
                return dtype_unspecified;
}

const char *
type_to_string(data_type_t t)
{
        switch (t) {
        case dtype_number:
                return "number";
        case dtype_string:
                return "string";
        case dtype_unspecified:
                return "unspecified";
        case dtype_pointer:
                return "pointer";
	case dtype_any:
		return "any";
        default:
                abort();
        }
}

static int
check_func_usage(struct function *fp, struct mu_locus_range const *locus)
{
	switch (outer_context) {
	case context_handler:
		if (fp->statemask && !(STATMASK(state_tag) & fp->statemask)) {
                        parse_error_locus(locus,
                                          _("function `%s' cannot be used in "
					    "prog `%s'"),
				    fp->sym.name,
				    xstate_to_string(state_tag));
			return 1;
		}
		break;
		
	case context_function:
		func->statemask |= fp->statemask;
		break;

	default:
		break;
	}
	return 0;
}

static int
check_builtin_usage(const struct builtin *bp,
		    struct mu_locus_range const *locus)
{
	switch (outer_context) {
	case context_handler:
		if (bp->statemask && !(STATMASK(state_tag) & bp->statemask)) {
			parse_error_locus(locus,
					  _("built-in function `%s' cannot be used in "
					    "prog `%s'"),
					  bp->name,
					  xstate_to_string(state_tag));
			return 1;
		}
		break;
		
	case context_function:
		func->statemask |= bp->statemask;
		break;
		
	default:
		break;
	}
			
	if (bp->flags & MFD_BUILTIN_CAPTURE)
		capture_on();
	return 0;
}

static void jump_fixup(prog_counter_t pos, prog_counter_t endpos);

#define LITERAL_TEXT(lit) ((lit) ? (lit)->text : NULL)
#define LITERAL_OFF(lit) ((lit) ? (lit)->off : 0)

static int
_create_alias(void *item, void *data)
{
        struct literal *lit = item;
        struct function *fun = data;
        /* FIXME: Ideally we should pass a locus of `lit' instead of
           fun->locus. However, its only purpose is for use in redefinition
           diagnostic messages and these are never produced, because the
           grammar catches these errors earlier. This might change in
           the future.  2008-09-14 */
        install_alias(lit->text, fun, &fun->sym.locus);
        return 0;
}

#line 352 "gram.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "gram.h".  */
#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 290 "gram.y" /* yacc.c:355  */

#define YYLTYPE struct mu_locus_range

#line 386 "gram.c" /* yacc.c:355  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_ACCEPT = 258,
    T_REJECT = 259,
    T_TEMPFAIL = 260,
    T_CONTINUE = 261,
    T_DISCARD = 262,
    T_ADD = 263,
    T_REPLACE = 264,
    T_DELETE = 265,
    T_PROG = 266,
    T_IF = 267,
    T_FI = 268,
    T_ELSE = 269,
    T_ELIF = 270,
    T_ON = 271,
    T_HOST = 272,
    T_FROM = 273,
    T_AS = 274,
    T_DO = 275,
    T_DONE = 276,
    T_POLL = 277,
    T_MATCHES = 278,
    T_FNMATCHES = 279,
    T_MXMATCHES = 280,
    T_MXFNMATCHES = 281,
    T_WHEN = 282,
    T_PASS = 283,
    T_SET = 284,
    T_CATCH = 285,
    T_TRY = 286,
    T_THROW = 287,
    T_ECHO = 288,
    T_RETURNS = 289,
    T_RETURN = 290,
    T_FUNC = 291,
    T_SWITCH = 292,
    T_CASE = 293,
    T_DEFAULT = 294,
    T_CONST = 295,
    T_FOR = 296,
    T_LOOP = 297,
    T_WHILE = 298,
    T_BREAK = 299,
    T_NEXT = 300,
    T_ARGCOUNT = 301,
    T_ALIAS = 302,
    T_DOTS = 303,
    T_ARGX = 304,
    T_VAPTR = 305,
    T_PRECIOUS = 306,
    T_OR = 307,
    T_AND = 308,
    T_EQ = 309,
    T_NE = 310,
    T_LT = 311,
    T_LE = 312,
    T_GT = 313,
    T_GE = 314,
    T_NOT = 315,
    T_LOGAND = 316,
    T_LOGOR = 317,
    T_LOGXOR = 318,
    T_LOGNOT = 319,
    T_REQUIRE = 320,
    T_IMPORT = 321,
    T_STATIC = 322,
    T_PUBLIC = 323,
    T_MODULE = 324,
    T_BYE = 325,
    T_DCLEX = 326,
    T_SHL = 327,
    T_SHR = 328,
    T_SED = 329,
    T_ARGV = 330,
    T_VOID = 331,
    T_COMPOSE = 332,
    T_MODBEG = 333,
    T_MODEND = 334,
    T_STRING = 335,
    T_SYMBOL = 336,
    T_IDENTIFIER = 337,
    T_ARG = 338,
    T_NUMBER = 339,
    T_BACKREF = 340,
    T_BUILTIN = 341,
    T_FUNCTION = 342,
    T_TYPE = 343,
    T_TYPECAST = 344,
    T_VARIABLE = 345,
    T_BOGUS = 346,
    T_UMINUS = 347
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 294 "gram.y" /* yacc.c:355  */

	struct literal *literal;
	struct stmtlist stmtlist;
	NODE *node;
	struct return_node ret;
	struct poll_data poll;
	struct pollarg {
		int kw;
		NODE *expr;
	} pollarg;
	struct arglist {
		NODE *head;
		NODE *tail;
		size_t count;
	} arglist;
	long number;
	const struct builtin *builtin;
	struct variable *var;
	enum smtp_state state;
	struct {
		int qualifier;
	} matchtype;
	data_type_t type;
	struct parmtype *parm;
	struct parmlist {
		struct parmtype *head, *tail;
		size_t count;
		size_t optcount;
		int varargs;
		data_type_t vartype;
	} parmlist;
	enum lexical_context tie_in;
	struct function *function;
	struct {
		struct valist *head, *tail;
	} valist_list;
	struct {
		int all;
		struct valist *valist;
	} catchlist;
	struct valist *valist;
	struct value value;
	struct {
		struct case_stmt *head, *tail;
	} case_list ;
	struct case_stmt *case_stmt;
	struct loop_node loop;
	struct enumlist {
		struct constant *cv;
		struct enumlist *prev;
	} enumlist;
	mu_list_t list;
	struct import_rule_list import_rule_list;
	char *string;

#line 547 "gram.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 578 "gram.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  30
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1728

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  105
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  97
/* YYNRULES -- Number of rules.  */
#define YYNRULES  239
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  409

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   347

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    97,     2,     2,
     100,   101,    95,    93,    99,    94,    92,    96,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   103,   102,
       2,     2,     2,     2,   104,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    98
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   480,   480,   506,   507,   513,   517,   518,   529,   542,
     546,   554,   557,   560,   571,   589,   593,   599,   600,   606,
     611,   619,   620,   623,   646,   664,   668,   672,   678,   679,
     685,   699,   736,   764,   767,   776,   780,   784,   791,   796,
     809,   823,   863,   887,   895,   913,   923,   929,   961,   965,
     968,   969,   975,   991,   996,   999,  1006,  1015,  1016,  1024,
    1028,  1034,  1045,  1048,  1051,  1056,  1063,  1070,  1073,  1079,
    1091,  1097,  1110,  1111,  1112,  1113,  1114,  1115,  1116,  1117,
    1127,  1128,  1129,  1130,  1133,  1140,  1164,  1170,  1182,  1195,
    1203,  1207,  1215,  1223,  1231,  1239,  1247,  1255,  1261,  1269,
    1270,  1279,  1286,  1293,  1303,  1306,  1309,  1316,  1324,  1332,
    1341,  1354,  1377,  1378,  1379,  1382,  1392,  1395,  1402,  1408,
    1453,  1457,  1464,  1473,  1484,  1490,  1501,  1506,  1513,  1526,
    1530,  1536,  1540,  1549,  1560,  1567,  1570,  1574,  1577,  1580,
    1600,  1630,  1635,  1640,  1648,  1651,  1657,  1671,  1696,  1702,
    1709,  1716,  1723,  1730,  1737,  1744,  1758,  1766,  1773,  1780,
    1784,  1787,  1790,  1791,  1798,  1804,  1811,  1818,  1825,  1832,
    1839,  1846,  1853,  1860,  1869,  1874,  1878,  1882,  1883,  1889,
    1893,  1901,  1905,  1910,  1914,  1915,  1919,  1925,  1930,  1933,
    1934,  1942,  1947,  1950,  1954,  1958,  1977,  1978,  1984,  2018,
    2033,  2073,  2081,  2091,  2096,  2097,  2106,  2122,  2125,  2131,
    2137,  2146,  2150,  2156,  2164,  2172,  2180,  2179,  2210,  2214,
    2221,  2237,  2249,  2268,  2296,  2355,  2363,  2369,  2375,  2384,
    2393,  2399,  2404,  2411,  2416,  2421,  2426,  2434,  2438,  2446
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"accept\"", "\"reject\"",
  "\"tempfail\"", "\"continue\"", "\"discard\"", "\"add\"", "\"replace\"",
  "\"delete\"", "\"prog\"", "\"if\"", "\"fi\"", "\"else\"", "\"elif\"",
  "\"on\"", "\"host\"", "\"from\"", "\"as\"", "\"do\"", "\"done\"",
  "\"poll\"", "\"matches\"", "\"fnmatches\"", "\"mx matches\"",
  "\"mx fnmatches\"", "\"when\"", "\"pass\"", "\"set\"", "\"catch\"",
  "\"try\"", "\"throw\"", "\"echo\"", "\"returns\"", "\"return\"",
  "\"func\"", "\"switch\"", "\"case\"", "\"default\"", "\"const\"",
  "\"for\"", "\"loop\"", "\"while\"", "\"break\"", "\"next\"", "\"$#\"",
  "\"alias\"", "\"...\"", "\"$(n)\"", "\"vaptr\"", "\"precious\"",
  "\"or\"", "\"and\"", "\"==\"", "\"!=\"", "\"<\"", "\"<=\"", "\">\"",
  "\">=\"", "\"!\"", "\"&\"", "\"|\"", "\"^\"", "\"~\"", "\"require\"",
  "\"import\"", "\"static\"", "\"public\"", "\"module\"", "\"bye\"",
  "\"dclex\"", "\"<<\"", "\">>\"", "\"sed\"", "\"$@\"", "\"void\"",
  "\"composed string\"", "T_MODBEG", "T_MODEND", "\"string\"",
  "\"MTA macro\"", "\"identifier\"", "\"$n\"", "\"number\"",
  "\"back reference\"", "\"builtin function\"", "\"function\"",
  "\"data type\"", "\"typecast\"", "\"variable\"", "T_BOGUS", "'.'", "'+'",
  "'-'", "'*'", "'/'", "'%'", "T_UMINUS", "','", "'('", "')'", "';'",
  "':'", "'@'", "$accept", "input", "program", "decllist", "modcntl",
  "opt_moddecl", "moddecl", "require", "bye", "imports", "literal", "decl",
  "varname", "vardecl", "qualifiers", "qualifier", "constdecl",
  "qualconst", "constdefn", "enumlist", "exdecl", "fundecl", "parmdecl",
  "params", "opt_parmlist", "parmlist", "fparmlist", "dots", "parm",
  "aliasdecl", "aliases", "alias", "retdecl", "state_ident", "stmtlist",
  "stmt", "void_funcall", "asgn", "autodcl", "action", "sendmail_action",
  "maybe_xcode_expr", "header_action", "maybe_triplet", "triplet", "code",
  "xcode", "condition", "if_cond", "else_cond", "case_cond",
  "cond_branches", "cond_branch", "valist", "value", "string", "matches",
  "fnmatches", "loopstmt", "loop_ident", "opt_ident", "opt_loop_parms",
  "loop_parm_list", "loop_parm", "opt_while", "jumpstmt", "expr",
  "maybe_expr", "simp_expr", "atom_expr", "atom", "strcat", "strval",
  "argref", "paren_argref", "funcall", "funcarglist", "argv", "argvarg",
  "arglist", "variable", "catch", "try_block", "simple_catch", "@1",
  "catchlist", "throw", "return", "bool_expr", "on_cond", "on", "do",
  "pollstmt", "pollarglist", "pollarg", "branches", "branch", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,    46,    43,    45,    42,    47,    37,   347,    44,
      40,    41,    59,    58,    64
};
# endif

#define YYPACT_NINF -288

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-288)))

#define YYTABLE_NINF -240

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-240)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     256,   -53,   123,    11,   123,   123,    35,    78,  -288,    27,
    -288,   394,    30,  -288,  -288,    17,  -288,    -7,  -288,  -288,
     102,  -288,  -288,    75,  -288,  -288,  1493,  -288,    36,  -288,
    -288,  -288,  -288,    44,    82,    11,  -288,  -288,  -288,  -288,
      11,  -288,    11,  1493,  -288,  1340,   123,  -288,    66,   104,
    1493,  1585,    86,  -288,  -288,  -288,  -288,  -288,  -288,   108,
     111,   127,  -288,  -288,  1585,  1585,  1493,    85,   557,   163,
    -288,  -288,    79,  -288,  -288,  -288,  -288,  -288,   115,  -288,
    -288,   394,  -288,   140,   203,  1493,  1493,  -288,   -19,   557,
     158,   -30,    45,  -288,  -288,    70,    70,    70,  1493,  -288,
    -288,    11,   -58,   226,    70,  1493,  1493,  1493,    11,    11,
      11,   151,    11,    76,  -288,   949,  -288,  -288,  -288,  -288,
    -288,  -288,  -288,  -288,  -288,  -288,  -288,  -288,  -288,  -288,
     231,  -288,  -288,  -288,  -288,    20,   -41,  -288,  1493,    74,
    -288,  -288,   697,  -288,  1493,  1355,  1401,  1493,  -288,  -288,
     289,  -288,  -288,  -288,  -288,  -288,  1493,  1493,  1493,  1493,
    1493,  1493,  1493,  1493,  1493,  1493,  1585,  1585,  1585,  1585,
    1585,  1585,  1585,  1585,  1585,  1585,  1585,  -288,  -288,   173,
     -14,  1340,   557,  -288,  1493,  -288,  -288,  -288,  -288,  1539,
    1493,  -288,  1493,  -288,  -288,  -288,  -288,  1493,  1493,  -288,
     557,  1340,  1493,  -288,   211,  -288,   245,  1340,  1493,   557,
     557,   423,  -288,   556,  -288,  -288,  -288,   103,  1493,  -288,
    -288,  -288,   454,   246,   246,  -288,   123,   363,   167,   557,
     129,   180,  -288,   172,  -288,   184,  -288,   189,   581,  -288,
     643,   697,   758,   758,   368,   368,   368,   368,   758,   758,
     530,  1083,   413,   182,   182,  1056,   221,   221,  -288,  -288,
    -288,   202,  -288,   -13,   190,  -288,   193,   132,  -288,  -288,
    -288,  1010,   204,  1493,   557,   557,   200,   205,   557,   557,
     642,   557,    70,  -288,  1071,   557,    26,  1340,  1493,   364,
     283,   207,  -288,   206,   557,  1493,  1493,  1493,  1493,  1669,
     107,  -288,  -288,   282,   282,  -288,  -288,  -288,  1493,  -288,
    1493,  -288,  -288,  1447,  -288,  -288,  -288,  -288,  -288,   263,
     232,     2,  -288,   239,   557,  1539,  1539,  1340,  1493,   311,
    -288,  1340,  -288,    70,   223,   131,  -288,   425,   557,  1340,
     825,  -288,   557,   557,   557,   557,   107,  -288,    70,   119,
    -288,   128,   557,   636,  -288,    11,   294,   263,  -288,    11,
     230,  -288,  -288,  -288,   240,   235,  -288,  -288,   238,  1157,
    1340,  -288,  1218,   -21,  1340,  -288,  -288,  1279,  -288,    -4,
    -288,  -288,  -288,  -288,  -288,   251,  -288,  -288,   265,  1493,
    1493,   642,  -288,  1340,   703,   307,  1340,  -288,  -288,   250,
     252,  -288,   764,  1493,  -288,   888,  -288,  -288,   557
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      33,     0,     0,     0,     0,     0,     0,     0,     2,    33,
       6,    33,     9,     5,    25,     0,    26,     0,    27,    69,
       0,    21,    22,     0,    28,    29,     0,    15,     0,    46,
       1,     8,     7,    33,    11,     0,    40,    35,    36,    37,
       0,    34,     0,     0,    38,    33,     0,   185,     0,     0,
       0,     0,     0,   192,   191,   181,   194,   182,   183,     0,
       0,     0,   211,   212,     0,     0,     0,     0,    32,   159,
     162,   177,   188,   189,   184,   174,   193,    13,     0,    18,
       4,    33,    12,     0,     0,    30,    42,    43,     0,    41,
     104,   104,   104,    97,    98,     0,     0,     0,     0,   226,
      90,     0,     0,     0,     0,     0,   221,     0,   135,   135,
     135,     0,     0,     0,    81,    33,    70,    80,    74,    75,
      73,    88,    89,    72,   112,   113,    82,    83,    79,    76,
       0,   213,    77,    78,   114,     0,     0,    19,     0,     0,
     196,   187,   148,   180,     0,     0,     0,     0,   179,   178,
       0,   186,   129,   131,   130,   132,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   190,    14,    33,
      48,    33,    31,    39,    44,    45,   110,    92,   105,   106,
     160,    93,   160,    95,   126,   127,   128,     0,     0,   103,
     223,    33,     0,   218,   219,   124,     0,    33,     0,    91,
     222,     0,   136,    33,   134,   146,   147,     0,    86,    23,
      71,   214,     0,     0,     0,    16,     0,     0,     0,   209,
       0,   207,   199,     0,   203,   204,   202,     0,     0,   175,
     157,   158,   149,   150,   151,   152,   153,   154,   155,   156,
     169,   170,   171,   172,   173,   164,   163,   165,   166,   167,
     168,     0,    59,     0,     0,    49,     0,    54,    50,    51,
      55,    33,   182,   107,   109,   161,     0,     0,   101,   102,
      33,    85,     0,   216,    33,   220,     0,    33,     0,    33,
       0,   138,   139,     0,    87,     0,     0,     0,     0,   228,
     230,   231,   227,     0,     0,    20,   195,   197,     0,   200,
       0,   206,   198,     0,   201,   176,    10,    60,    61,    62,
       0,     0,    24,     0,   108,   160,   160,    33,     0,     0,
     125,    33,   215,     0,     0,     0,   120,    33,   143,    33,
      33,    84,   234,   236,   235,   233,   229,   232,     0,     0,
     237,     0,   210,     0,   205,     0,    67,    63,    64,     0,
      57,    52,    58,    56,     0,     0,   100,    99,     0,    33,
      33,   115,    33,     0,    33,   119,   121,    33,   140,     0,
     225,   238,   224,   208,    66,     0,    47,    65,     0,   160,
     160,    33,   217,    33,    33,   144,    33,    68,   111,     0,
       0,   117,    33,     0,   133,    33,    94,    96,   145
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -288,  -288,  -288,     5,     0,  -288,   324,  -288,   185,  -288,
       3,     6,     1,  -288,    14,   333,    19,  -288,   -22,  -288,
    -288,  -288,  -288,  -288,  -288,    55,    58,    60,    61,  -288,
    -288,     8,  -288,  -288,   -34,   -83,  -288,  -288,  -288,  -288,
    -288,    53,  -288,    69,  -288,  -288,   194,  -288,  -288,    -6,
    -288,  -288,    56,  -287,   -99,    84,  -288,  -288,  -288,  -288,
      90,  -288,  -288,    62,  -288,  -288,   -26,  -180,    46,  -288,
    -288,  -288,   326,   -48,  -288,   -39,   254,    91,  -288,   259,
    -288,  -288,  -288,   277,  -288,  -288,  -288,  -288,    83,  -288,
    -288,   186,  -288,   114,  -279,   110,  -112
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     7,     8,     9,    10,    81,    11,    12,    80,   136,
      23,    13,   212,    14,   113,    41,   114,    17,    44,    88,
      18,    84,   264,   265,   266,   267,   268,   269,   270,   356,
     357,   358,   386,    20,   289,   116,   117,   118,   119,   120,
     121,   365,   122,   187,   188,   189,   366,   123,   124,   329,
     125,   335,   336,   204,   196,   197,   164,   165,   126,   213,
     214,   290,   291,   292,   404,   127,   275,   367,    69,    70,
      71,    72,    73,    74,   141,    75,   233,   234,   311,   235,
      76,   129,   130,   131,   331,   206,   132,   133,   201,   134,
     135,   303,   224,   300,   301,   349,   350
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      68,   140,   183,   205,    26,   208,   128,    27,    28,    31,
     276,   115,   277,    42,    15,    32,    33,    89,    43,    16,
      87,   347,   194,    15,   142,    15,   195,    -3,    16,    19,
      16,   282,   220,    31,   262,   317,    83,   203,     1,    32,
     150,    85,   222,    86,   -17,     2,   373,    15,   282,   137,
     262,   225,    16,    35,   186,     1,     3,    36,   226,   182,
      89,   379,     2,    24,   333,   334,   185,   347,    37,    24,
     190,    25,   200,     3,   263,    24,   128,    25,    30,   209,
     210,   211,   393,    25,    38,    39,   179,    37,   -53,   184,
     263,   228,     4,    24,    52,    15,   223,   143,     6,   396,
      16,    25,   202,    38,    39,    40,    59,    60,    34,     4,
     148,   149,   227,   218,    79,     6,    36,    29,   229,   229,
     229,   238,    45,    48,   295,   296,   297,    37,    77,   186,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     380,    46,   128,    38,    39,   192,   348,   271,   298,   382,
     194,     5,   375,    48,   195,   348,    53,    56,    89,    54,
     191,   193,   128,   274,    62,    63,   138,   280,   128,   333,
     334,   278,   279,   284,   128,   151,   281,    52,   293,    31,
     198,   199,   285,   330,     1,    32,   144,    56,   220,    59,
      60,     2,   294,    15,    62,    63,   299,   220,    16,   215,
     216,   220,     3,    21,   139,    22,   220,   178,   145,   399,
     400,   146,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   181,   166,   167,   168,   147,   308,   305,
     309,   321,   128,   -57,   205,   169,   170,   381,     4,   381,
     180,   128,   186,    79,     6,   128,   207,   324,   128,   205,
     128,   217,   -17,   337,   220,   171,   172,   173,   174,   175,
     176,   102,   338,   282,   318,   283,   302,     1,   307,   342,
     343,   344,   345,   312,     2,   172,   173,   174,   175,   176,
     310,   316,   352,   313,   353,     3,   220,   352,   128,   220,
     314,   319,   128,   369,   220,   320,   323,   372,   128,   325,
     128,   128,   200,   339,   326,   377,   340,   341,   220,   348,
     355,   220,   152,   153,   154,   155,   174,   175,   176,   220,
     359,     4,   220,   364,   371,     5,   374,     6,   385,   321,
     128,   128,   388,   128,   389,   128,   391,   390,   128,   397,
     394,   156,   157,   158,   159,   160,   161,   162,   163,   398,
     403,   406,   128,   407,   128,   128,   384,   128,    82,   402,
     318,    78,   405,   128,   261,   387,   128,    90,    91,    92,
      93,    94,    95,    96,    97,   360,    98,   408,   361,   368,
      99,   362,   363,   273,  -141,   401,   152,   153,   154,   155,
     239,   376,   100,   101,   102,   103,   104,   105,   177,   106,
     237,   107,   378,   230,   354,     1,   108,   221,   109,   110,
     304,   370,     2,   346,   351,   156,   157,   158,   159,   160,
     161,   162,   163,     3,  -240,  -240,  -240,  -240,    90,    91,
      92,    93,    94,    95,    96,    97,     0,    98,    52,     0,
     111,    99,     0,   286,     0,  -142,   152,   153,   154,   155,
      59,    60,   112,   100,   101,   102,   103,   104,   105,     4,
     106,     0,   107,  -141,   306,     6,     0,   108,     0,   109,
     110,   295,   296,   297,   166,   156,   157,   158,   159,   160,
     161,   162,   163,     0,     0,   169,   170,     0,     0,     0,
       0,     0,     0,     0,     0,   298,     0,     0,     0,    52,
      47,   111,     0,    48,    49,     0,   172,   173,   174,   175,
     176,    59,    60,   112,    50,     0,     0,     0,    51,     0,
       0,     0,     0,     0,  -142,     0,     0,     0,    52,     0,
       0,    53,     0,     0,    54,    55,     0,    56,    57,    58,
      59,    60,     0,    61,    62,    63,     0,    64,    65,     0,
       0,     0,     0,     0,    66,     0,     0,     0,    67,    90,
      91,    92,    93,    94,    95,    96,    97,     0,    98,     0,
       0,     0,    99,     0,     0,     0,  -137,     0,     0,     0,
     152,   153,   154,   155,   100,   101,   102,   103,   104,   105,
       0,   106,     0,   107,     0,     0,     0,   287,   108,   288,
     109,   110,   169,   170,   152,   153,   154,   155,     0,   156,
     157,   158,   159,   160,   161,   162,   163,     0,     0,     0,
       0,     0,     0,   172,   173,   174,   175,   176,     0,     0,
      52,     0,   111,   156,   157,   158,   159,   160,   161,   162,
     163,     0,    59,    60,   112,    90,    91,    92,    93,    94,
      95,    96,    97,     0,    98,  -116,   327,   328,    99,   152,
     153,   154,   155,     0,     0,     0,   152,   153,   154,   155,
     100,   101,   102,   103,   104,   105,     0,   106,     0,   107,
       0,     0,   315,     0,   108,     0,   109,   110,   156,   157,
     158,   159,   160,   161,   162,   163,   157,   158,   159,   160,
     161,   162,   163,     0,     0,     0,    90,    91,    92,    93,
      94,    95,    96,    97,     0,    98,    52,     0,   111,    99,
     152,   153,   154,   155,  -123,     0,     0,     0,    59,    60,
     112,   100,   101,   102,   103,   104,   105,   383,   106,     0,
     107,  -123,  -123,     0,     0,   108,     0,   109,   110,     0,
       0,   158,   159,   160,   161,   162,   163,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    90,    91,    92,
      93,    94,    95,    96,    97,     0,    98,    52,     0,   111,
      99,  -240,  -240,  -240,  -240,  -122,     0,     0,     0,    59,
      60,   112,   100,   101,   102,   103,   104,   105,     0,   106,
       0,   107,  -122,  -122,     0,     0,   108,     0,   109,   110,
       0,     0,  -240,  -240,   160,   161,   162,   163,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    90,    91,
      92,    93,    94,    95,    96,    97,     0,    98,    52,     0,
     111,    99,     0,     0,     0,     0,     0,     0,     0,     0,
      59,    60,   112,   100,   101,   102,   103,   104,   105,     0,
     106,     0,   107,     0,     0,     0,   287,   108,   288,   109,
     110,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    90,    91,    92,    93,    94,    95,    96,    97,    52,
      98,   111,     0,     0,    99,     0,     0,     0,     0,  -239,
       0,    59,    60,   112,     0,  -239,   100,   101,   102,   103,
     104,   105,     0,   106,     0,   107,     0,     0,     0,     0,
     108,     0,   109,   110,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    90,    91,    92,    93,    94,    95,    96,    97,
       0,    98,    52,     0,   111,    99,     0,     0,     0,     0,
     219,     0,     0,     0,    59,    60,   112,   100,   101,   102,
     103,   104,   105,     0,   106,     0,   107,     0,     0,     0,
       0,   108,     0,   109,   110,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    90,    91,    92,    93,    94,    95,    96,
      97,     0,    98,    52,     0,   111,    99,     0,     0,     0,
       0,   322,     0,     0,     0,    59,    60,   112,   100,   101,
     102,   103,   104,   105,     0,   106,     0,   107,     0,     0,
       0,     0,   108,     0,   109,   110,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    90,    91,    92,    93,    94,    95,
      96,    97,     0,    98,    52,     0,   111,    99,     0,     0,
       0,     0,   332,     0,     0,     0,    59,    60,   112,   100,
     101,   102,   103,   104,   105,     0,   106,     0,   107,     0,
       0,     0,     0,   108,     0,   109,   110,   166,   167,   168,
       0,     0,     0,     0,     0,     0,     0,     0,   169,   170,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   166,    52,   168,   111,     0,   172,
     173,   174,   175,   176,     0,   169,   170,    59,    60,   112,
      90,    91,    92,    93,    94,    95,    96,    97,     0,    98,
    -118,     0,     0,    99,     0,     0,   172,   173,   174,   175,
     176,     0,     0,     0,     0,   100,   101,   102,   103,   104,
     105,     0,   106,     0,   107,     0,     0,     0,     0,   108,
       0,   109,   110,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    90,    91,    92,    93,    94,    95,    96,    97,     0,
      98,    52,     0,   111,    99,     0,     0,     0,     0,   392,
       0,     0,     0,    59,    60,   112,   100,   101,   102,   103,
     104,   105,     0,   106,     0,   107,     0,     0,     0,     0,
     108,     0,   109,   110,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    90,    91,    92,    93,    94,    95,    96,    97,
       0,    98,    52,     0,   111,    99,     0,     0,     0,     0,
     395,     0,     0,     0,    59,    60,   112,   100,   101,   102,
     103,   104,   105,     0,   106,     0,   107,     0,     0,     0,
       0,   108,     0,   109,   110,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    90,    91,    92,    93,    94,    95,    96,
      97,     0,    98,    52,     0,   111,    99,     0,     0,     0,
       0,     0,     0,     0,     0,    59,    60,   112,   100,   101,
     102,   103,   104,   105,     0,   106,     0,   107,     0,     0,
       0,     0,   108,     0,   109,   110,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    47,     0,     0,    48,    49,     0,     0,     0,     0,
       0,     0,     0,     0,    52,    50,   111,     0,     0,    51,
       0,     0,     0,     0,     0,     0,    59,    60,   112,    52,
     231,     0,    53,     0,     0,    54,    55,     0,    56,    57,
      58,    59,    60,     0,    61,    62,    63,    47,    64,    65,
      48,    49,     0,     0,     0,    66,   232,     0,     0,    67,
       0,    50,     0,     0,     0,    51,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    52,   231,     0,    53,     0,
       0,    54,    55,     0,    56,    57,    58,    59,    60,     0,
      61,    62,    63,    47,    64,    65,    48,    49,     0,     0,
       0,    66,   236,     0,     0,    67,     0,    50,     0,     0,
       0,    51,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    52,   231,     0,    53,     0,     0,    54,    55,     0,
      56,    57,    58,    59,    60,     0,    61,    62,    63,    47,
      64,    65,    48,    49,     0,     0,     0,    66,     0,     0,
       0,    67,     0,    50,     0,     0,     0,    51,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    52,     0,     0,
      53,     0,     0,    54,    55,     0,    56,    57,    58,    59,
      60,     0,    61,    62,    63,    47,    64,    65,    48,    49,
       0,     0,     0,    66,     0,     0,     0,    67,     0,    50,
       0,     0,     0,    51,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    52,     0,     0,    53,     0,     0,    54,
      55,     0,    56,   272,    58,    59,    60,     0,    61,    62,
      63,    47,    64,    65,    48,    49,     0,     0,     0,    66,
       0,     0,     0,    67,     0,     0,     0,     0,     0,    51,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    52,
       0,     0,    53,     0,     0,    54,    55,     0,    56,    57,
      58,    59,    60,     0,    61,    62,    63,     0,    64,    65,
       0,     0,     0,     0,     0,    66,   295,   296,   297,    67,
       0,     0,   152,   153,   154,   155,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     298,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   156,   157,   158,   159,   160,   161,   162,   163
};

static const yytype_int16 yycheck[] =
{
      26,    49,    21,   102,     3,   104,    45,     4,     5,     9,
     190,    45,   192,    20,     0,     9,    11,    43,    17,     0,
      42,   300,    80,     9,    50,    11,    84,     0,     9,    82,
      11,    52,   115,    33,    48,    48,    35,    95,    11,    33,
      66,    40,    22,    42,     0,    18,   333,    33,    52,    46,
      48,    92,    33,    36,    84,    11,    29,    40,    99,    85,
      86,   348,    18,    82,    38,    39,    88,   346,    51,    82,
     100,    90,    98,    29,    88,    82,   115,    90,     0,   105,
     106,   107,   103,    90,    67,    68,    81,    51,   102,    88,
      88,   139,    65,    82,    74,    81,   135,    51,    71,   103,
      81,    90,   101,    67,    68,    88,    86,    87,    78,    65,
      64,    65,   138,   112,    70,    71,    40,    82,   144,   145,
     146,   147,    20,    49,    17,    18,    19,    51,    92,    84,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
      21,    66,   181,    67,    68,   100,    27,   181,    41,    21,
      80,    69,    21,    49,    84,    27,    77,    83,   184,    80,
      91,    92,   201,   189,    90,    91,   100,   201,   207,    38,
      39,   197,   198,   207,   213,    90,   202,    74,   217,   179,
      96,    97,   208,   282,    11,   179,   100,    83,   271,    86,
      87,    18,   218,   179,    90,    91,   222,   280,   179,   109,
     110,   284,    29,    80,   100,    82,   289,    92,   100,   389,
     390,   100,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,    20,    61,    62,    63,   100,    99,   226,
     101,    99,   271,   101,   333,    72,    73,   349,    65,   351,
     100,   280,    84,    70,    71,   284,    20,   273,   287,   348,
     289,   100,    79,   287,   337,    92,    93,    94,    95,    96,
      97,    30,   288,    52,   263,    20,    20,    11,   101,   295,
     296,   297,   298,   101,    18,    93,    94,    95,    96,    97,
     100,    79,   308,    99,   310,    29,   369,   313,   327,   372,
     101,   101,   331,   327,   377,   102,    92,   331,   337,    99,
     339,   340,   328,    20,    99,   339,    99,   101,   391,    27,
      47,   394,    23,    24,    25,    26,    95,    96,    97,   402,
      88,    65,   405,    84,    13,    69,   103,    71,    34,    99,
     369,   370,    92,   372,    99,   374,   370,    99,   377,    88,
     374,    52,    53,    54,    55,    56,    57,    58,    59,    84,
      43,   101,   391,   101,   393,   394,   355,   396,    34,   393,
     359,    28,   396,   402,   179,   357,   405,     3,     4,     5,
       6,     7,     8,     9,    10,   320,    12,   403,   320,   326,
      16,   321,   321,   189,    20,   391,    23,    24,    25,    26,
     101,   335,    28,    29,    30,    31,    32,    33,    72,    35,
     146,    37,   340,   144,   313,    11,    42,   130,    44,    45,
     224,   328,    18,   299,   304,    52,    53,    54,    55,    56,
      57,    58,    59,    29,    56,    57,    58,    59,     3,     4,
       5,     6,     7,     8,     9,    10,    -1,    12,    74,    -1,
      76,    16,    -1,    20,    -1,    20,    23,    24,    25,    26,
      86,    87,    88,    28,    29,    30,    31,    32,    33,    65,
      35,    -1,    37,    99,   101,    71,    -1,    42,    -1,    44,
      45,    17,    18,    19,    61,    52,    53,    54,    55,    56,
      57,    58,    59,    -1,    -1,    72,    73,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    41,    -1,    -1,    -1,    74,
      46,    76,    -1,    49,    50,    -1,    93,    94,    95,    96,
      97,    86,    87,    88,    60,    -1,    -1,    -1,    64,    -1,
      -1,    -1,    -1,    -1,    99,    -1,    -1,    -1,    74,    -1,
      -1,    77,    -1,    -1,    80,    81,    -1,    83,    84,    85,
      86,    87,    -1,    89,    90,    91,    -1,    93,    94,    -1,
      -1,    -1,    -1,    -1,   100,    -1,    -1,    -1,   104,     3,
       4,     5,     6,     7,     8,     9,    10,    -1,    12,    -1,
      -1,    -1,    16,    -1,    -1,    -1,    20,    -1,    -1,    -1,
      23,    24,    25,    26,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    -1,    -1,    -1,    41,    42,    43,
      44,    45,    72,    73,    23,    24,    25,    26,    -1,    52,
      53,    54,    55,    56,    57,    58,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    93,    94,    95,    96,    97,    -1,    -1,
      74,    -1,    76,    52,    53,    54,    55,    56,    57,    58,
      59,    -1,    86,    87,    88,     3,     4,     5,     6,     7,
       8,     9,    10,    -1,    12,    13,    14,    15,    16,    23,
      24,    25,    26,    -1,    -1,    -1,    23,    24,    25,    26,
      28,    29,    30,    31,    32,    33,    -1,    35,    -1,    37,
      -1,    -1,   101,    -1,    42,    -1,    44,    45,    52,    53,
      54,    55,    56,    57,    58,    59,    53,    54,    55,    56,
      57,    58,    59,    -1,    -1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    -1,    12,    74,    -1,    76,    16,
      23,    24,    25,    26,    21,    -1,    -1,    -1,    86,    87,
      88,    28,    29,    30,    31,    32,    33,   101,    35,    -1,
      37,    38,    39,    -1,    -1,    42,    -1,    44,    45,    -1,
      -1,    54,    55,    56,    57,    58,    59,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    -1,    12,    74,    -1,    76,
      16,    23,    24,    25,    26,    21,    -1,    -1,    -1,    86,
      87,    88,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    39,    -1,    -1,    42,    -1,    44,    45,
      -1,    -1,    54,    55,    56,    57,    58,    59,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,
       5,     6,     7,     8,     9,    10,    -1,    12,    74,    -1,
      76,    16,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      86,    87,    88,    28,    29,    30,    31,    32,    33,    -1,
      35,    -1,    37,    -1,    -1,    -1,    41,    42,    43,    44,
      45,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    74,
      12,    76,    -1,    -1,    16,    -1,    -1,    -1,    -1,    21,
      -1,    86,    87,    88,    -1,    27,    28,    29,    30,    31,
      32,    33,    -1,    35,    -1,    37,    -1,    -1,    -1,    -1,
      42,    -1,    44,    45,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    12,    74,    -1,    76,    16,    -1,    -1,    -1,    -1,
      21,    -1,    -1,    -1,    86,    87,    88,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    -1,    -1,    -1,
      -1,    42,    -1,    44,    45,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    12,    74,    -1,    76,    16,    -1,    -1,    -1,
      -1,    21,    -1,    -1,    -1,    86,    87,    88,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    -1,    -1,
      -1,    -1,    42,    -1,    44,    45,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     3,     4,     5,     6,     7,     8,
       9,    10,    -1,    12,    74,    -1,    76,    16,    -1,    -1,
      -1,    -1,    21,    -1,    -1,    -1,    86,    87,    88,    28,
      29,    30,    31,    32,    33,    -1,    35,    -1,    37,    -1,
      -1,    -1,    -1,    42,    -1,    44,    45,    61,    62,    63,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    72,    73,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    61,    74,    63,    76,    -1,    93,
      94,    95,    96,    97,    -1,    72,    73,    86,    87,    88,
       3,     4,     5,     6,     7,     8,     9,    10,    -1,    12,
      13,    -1,    -1,    16,    -1,    -1,    93,    94,    95,    96,
      97,    -1,    -1,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    -1,    -1,    -1,    -1,    42,
      -1,    44,    45,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    -1,
      12,    74,    -1,    76,    16,    -1,    -1,    -1,    -1,    21,
      -1,    -1,    -1,    86,    87,    88,    28,    29,    30,    31,
      32,    33,    -1,    35,    -1,    37,    -1,    -1,    -1,    -1,
      42,    -1,    44,    45,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,     4,     5,     6,     7,     8,     9,    10,
      -1,    12,    74,    -1,    76,    16,    -1,    -1,    -1,    -1,
      21,    -1,    -1,    -1,    86,    87,    88,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    -1,    -1,    -1,
      -1,    42,    -1,    44,    45,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    -1,    12,    74,    -1,    76,    16,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    86,    87,    88,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    -1,    -1,
      -1,    -1,    42,    -1,    44,    45,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    46,    -1,    -1,    49,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    74,    60,    76,    -1,    -1,    64,
      -1,    -1,    -1,    -1,    -1,    -1,    86,    87,    88,    74,
      75,    -1,    77,    -1,    -1,    80,    81,    -1,    83,    84,
      85,    86,    87,    -1,    89,    90,    91,    46,    93,    94,
      49,    50,    -1,    -1,    -1,   100,   101,    -1,    -1,   104,
      -1,    60,    -1,    -1,    -1,    64,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    74,    75,    -1,    77,    -1,
      -1,    80,    81,    -1,    83,    84,    85,    86,    87,    -1,
      89,    90,    91,    46,    93,    94,    49,    50,    -1,    -1,
      -1,   100,   101,    -1,    -1,   104,    -1,    60,    -1,    -1,
      -1,    64,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    74,    75,    -1,    77,    -1,    -1,    80,    81,    -1,
      83,    84,    85,    86,    87,    -1,    89,    90,    91,    46,
      93,    94,    49,    50,    -1,    -1,    -1,   100,    -1,    -1,
      -1,   104,    -1,    60,    -1,    -1,    -1,    64,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    74,    -1,    -1,
      77,    -1,    -1,    80,    81,    -1,    83,    84,    85,    86,
      87,    -1,    89,    90,    91,    46,    93,    94,    49,    50,
      -1,    -1,    -1,   100,    -1,    -1,    -1,   104,    -1,    60,
      -1,    -1,    -1,    64,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    74,    -1,    -1,    77,    -1,    -1,    80,
      81,    -1,    83,    84,    85,    86,    87,    -1,    89,    90,
      91,    46,    93,    94,    49,    50,    -1,    -1,    -1,   100,
      -1,    -1,    -1,   104,    -1,    -1,    -1,    -1,    -1,    64,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    74,
      -1,    -1,    77,    -1,    -1,    80,    81,    -1,    83,    84,
      85,    86,    87,    -1,    89,    90,    91,    -1,    93,    94,
      -1,    -1,    -1,    -1,    -1,   100,    17,    18,    19,   104,
      -1,    -1,    23,    24,    25,    26,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    53,    54,    55,    56,    57,    58,    59
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    11,    18,    29,    65,    69,    71,   106,   107,   108,
     109,   111,   112,   116,   118,   119,   121,   122,   125,    82,
     138,    80,    82,   115,    82,    90,   117,   115,   115,    82,
       0,   109,   116,   108,    78,    36,    40,    51,    67,    68,
      88,   120,    20,   117,   123,    20,    66,    46,    49,    50,
      60,    64,    74,    77,    80,    81,    83,    84,    85,    86,
      87,    89,    90,    91,    93,    94,   100,   104,   171,   173,
     174,   175,   176,   177,   178,   180,   185,    92,   120,    70,
     113,   110,   111,   117,   126,   117,   117,   123,   124,   171,
       3,     4,     5,     6,     7,     8,     9,    10,    12,    16,
      28,    29,    30,    31,    32,    33,    35,    37,    42,    44,
      45,    76,    88,   119,   121,   139,   140,   141,   142,   143,
     144,   145,   147,   152,   153,   155,   163,   170,   180,   186,
     187,   188,   191,   192,   194,   195,   114,   115,   100,   100,
     178,   179,   171,   173,   100,   100,   100,   100,   173,   173,
     171,    90,    23,    24,    25,    26,    52,    53,    54,    55,
      56,    57,    58,    59,   161,   162,    61,    62,    63,    72,
      73,    92,    93,    94,    95,    96,    97,   177,    92,   108,
     100,    20,   171,    21,   117,   123,    84,   148,   149,   150,
     100,   148,   100,   148,    80,    84,   159,   160,   160,   160,
     171,   193,   117,    95,   158,   159,   190,    20,   159,   171,
     171,   171,   117,   164,   165,   165,   165,   100,   117,    21,
     140,   188,    22,   180,   197,    92,    99,   171,   178,   171,
     184,    75,   101,   181,   182,   184,   101,   181,   171,   101,
     171,   171,   171,   171,   171,   171,   171,   171,   171,   171,
     173,   173,   173,   173,   173,   173,   173,   173,   173,   173,
     173,   113,    48,    88,   127,   128,   129,   130,   131,   132,
     133,   139,    84,   151,   171,   171,   172,   172,   171,   171,
     139,   171,    52,    20,   139,   171,    20,    41,    43,   139,
     166,   167,   168,   180,   171,    17,    18,    19,    41,   171,
     198,   199,    20,   196,   196,   115,   101,   101,    99,   101,
     100,   183,   101,    99,   101,   101,    79,    48,   117,   101,
     102,    99,    21,    92,   171,    99,    99,    14,    15,   154,
     159,   189,    21,    38,    39,   156,   157,   139,   171,    20,
      99,   101,   171,   171,   171,   171,   198,   199,    27,   200,
     201,   200,   171,   171,   182,    47,   134,   135,   136,    88,
     130,   131,   132,   133,    84,   146,   151,   172,   146,   139,
     193,    13,   139,   158,   103,    21,   157,   139,   168,   158,
      21,   201,    21,   101,   117,    34,   137,   136,    92,    99,
      99,   139,    21,   103,   139,    21,   103,    88,    84,   172,
     172,   154,   139,    43,   169,   139,   101,   101,   171
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   105,   106,   107,   107,   108,   108,   108,   108,   109,
     109,   110,   110,   111,   111,   112,   112,   113,   113,   114,
     114,   115,   115,   116,   116,   116,   116,   116,   117,   117,
     118,   118,   118,   119,   119,   120,   120,   120,   121,   121,
     122,   123,   124,   124,   124,   124,   125,   126,   127,   127,
     128,   128,   128,   129,   129,   130,   130,   131,   131,   132,
     132,   133,   134,   134,   135,   135,   136,   137,   137,   138,
     139,   139,   140,   140,   140,   140,   140,   140,   140,   140,
     140,   140,   140,   140,   141,   142,   143,   143,   144,   144,
     144,   144,   145,   145,   145,   145,   145,   145,   145,   146,
     146,   147,   147,   147,   148,   148,   149,   149,   149,   149,
     150,   151,   152,   152,   152,   153,   154,   154,   154,   155,
     156,   156,   157,   157,   158,   158,   159,   159,   160,   161,
     161,   162,   162,   163,   164,   165,   165,   166,   166,   167,
     167,   168,   168,   168,   169,   169,   170,   170,   171,   171,
     171,   171,   171,   171,   171,   171,   171,   171,   171,   171,
     172,   172,   173,   173,   173,   173,   173,   173,   173,   173,
     173,   173,   173,   173,   174,   174,   174,   174,   174,   174,
     174,   175,   175,   175,   175,   175,   175,   175,   175,   176,
     176,   177,   177,   178,   178,   178,   179,   179,   180,   180,
     180,   180,   180,   181,   181,   181,   182,   183,   183,   184,
     184,   185,   185,   186,   186,   187,   189,   188,   190,   190,
     191,   192,   192,   193,   194,   194,   195,   196,   197,   197,
     197,   198,   198,   199,   199,   199,   199,   200,   200,   201
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     3,     1,     1,     2,     2,     1,
       6,     0,     1,     3,     4,     2,     5,     0,     1,     1,
       3,     1,     1,     5,     6,     1,     1,     1,     1,     1,
       3,     4,     3,     0,     2,     1,     1,     1,     2,     4,
       2,     2,     1,     1,     2,     2,     2,     6,     0,     1,
       1,     1,     3,     0,     1,     1,     3,     1,     3,     1,
       2,     2,     0,     1,     1,     2,     2,     0,     2,     1,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     3,     2,     3,     1,     1,
       1,     2,     2,     2,     8,     2,     8,     1,     1,     1,
       1,     3,     3,     2,     0,     1,     1,     2,     3,     2,
       1,     5,     1,     1,     1,     5,     0,     4,     2,     5,
       1,     2,     4,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     7,     1,     0,     1,     0,     1,     1,
       3,     1,     2,     2,     0,     2,     2,     2,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     1,
       0,     1,     1,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     1,     3,     4,     1,     2,     2,
       2,     1,     1,     1,     1,     1,     2,     2,     1,     1,
       2,     1,     1,     1,     1,     4,     1,     3,     4,     3,
       4,     4,     3,     1,     1,     3,     2,     0,     3,     1,
       3,     1,     1,     1,     2,     4,     0,     6,     1,     1,
       3,     1,     2,     1,     5,     5,     1,     1,     2,     3,
       2,     1,     2,     2,     2,     2,     2,     1,     2,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 481 "gram.y" /* yacc.c:1646  */
    {
                     if (error_count == 0) {
                             if (genstmt.head) {
                                     genstmt.tail->next = (yyvsp[0].stmtlist).head;
                                     genstmt.tail = (yyvsp[0].stmtlist).tail;
                             } else
                                     genstmt = (yyvsp[0].stmtlist);
                             optimize_tree(genstmt.head);
                             if (error_count)
                                     YYERROR;
                             mark(genstmt.head);
                             if (error_count)
                                     YYERROR;
                             apply_deferred_init();
			     fixup_exceptions();
                             dataseg_layout();
                             if (optimization_level)
                                     regex_layout();
                             compile_tree(genstmt.head);
                             if (!optimization_level)
                                     regex_layout();
                     }
             }
#line 2364 "gram.c" /* yacc.c:1646  */
    break;

  case 4:
#line 508 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 2372 "gram.c" /* yacc.c:1646  */
    break;

  case 5:
#line 514 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = (yyvsp[0].node);
             }
#line 2380 "gram.c" /* yacc.c:1646  */
    break;

  case 7:
#line 519 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) {
                             if ((yyvsp[-1].stmtlist).tail) 
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].node);
                             else
                                     (yyvsp[-1].stmtlist).head = (yyvsp[0].node);
                             (yyvsp[-1].stmtlist).tail = (yyvsp[0].node);
                     }
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 2395 "gram.c" /* yacc.c:1646  */
    break;

  case 8:
#line 530 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].stmtlist).tail) {
                             if ((yyvsp[-1].stmtlist).tail) {
                                     (yyval.stmtlist).head = (yyvsp[-1].stmtlist).head;
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].stmtlist).head;
                                     (yyval.stmtlist).tail = (yyvsp[0].stmtlist).tail;
                             } else
                                     (yyval.stmtlist) = (yyvsp[0].stmtlist);
                     } 
             }
#line 2410 "gram.c" /* yacc.c:1646  */
    break;

  case 9:
#line 543 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = NULL;
             }
#line 2418 "gram.c" /* yacc.c:1646  */
    break;

  case 10:
#line 547 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.stmtlist) = (yyvsp[-2].stmtlist);
                     pop_top_module();
             }
#line 2427 "gram.c" /* yacc.c:1646  */
    break;

  case 11:
#line 554 "gram.y" /* yacc.c:1646  */
    {
                     parse_warning(_("missing module declaration"));
             }
#line 2435 "gram.c" /* yacc.c:1646  */
    break;

  case 13:
#line 561 "gram.y" /* yacc.c:1646  */
    {
		     if (top_module->dclname) {
			     struct mu_locus_range lr;
			     lr.beg = (yylsp[-2]).beg;
			     lr.end = (yylsp[0]).end;
                             parse_error_locus(&lr,
                                               _("duplicate module declaration"));
		     } else
			     top_module->dclname = (yyvsp[-1].literal)->text;
	     }
#line 2450 "gram.c" /* yacc.c:1646  */
    break;

  case 14:
#line 572 "gram.y" /* yacc.c:1646  */
    {
		     if (top_module->dclname) {
			     struct mu_locus_range lr;
			     lr.beg = (yylsp[-3]).beg;
			     lr.end = (yylsp[0]).end;
                             parse_error_locus(&lr,
                                               _("duplicate module declaration"));
		     } else {
			     if (((yyvsp[-1].number) & (SYM_STATIC|SYM_PUBLIC)) == 0)
                                     parse_error_locus(&(yylsp[-1]),
                                                       _("invalid module declaration"));
			     top_module->dclname = (yyvsp[-2].literal)->text;
			     top_module->flags = (yyvsp[-1].number);
		     }
	     }
#line 2470 "gram.c" /* yacc.c:1646  */
    break;

  case 15:
#line 590 "gram.y" /* yacc.c:1646  */
    {
                     require_module((yyvsp[0].literal)->text, NULL);
             }
#line 2478 "gram.c" /* yacc.c:1646  */
    break;

  case 16:
#line 594 "gram.y" /* yacc.c:1646  */
    {
                     require_module((yyvsp[-3].literal)->text, (yyvsp[-1].import_rule_list).head);
             }
#line 2486 "gram.c" /* yacc.c:1646  */
    break;

  case 18:
#line 601 "gram.y" /* yacc.c:1646  */
    {
                     lex_bye();
             }
#line 2494 "gram.c" /* yacc.c:1646  */
    break;

  case 19:
#line 607 "gram.y" /* yacc.c:1646  */
    {
                     struct import_rule *rule = import_rule_create((yyvsp[0].literal));
                     (yyval.import_rule_list).head = (yyval.import_rule_list).tail = rule;
             }
#line 2503 "gram.c" /* yacc.c:1646  */
    break;

  case 20:
#line 612 "gram.y" /* yacc.c:1646  */
    {
                     struct import_rule *rule = import_rule_create((yyvsp[0].literal));
                     (yyval.import_rule_list).tail->next = rule;
                     (yyval.import_rule_list).tail = rule;
             }
#line 2513 "gram.c" /* yacc.c:1646  */
    break;

  case 23:
#line 624 "gram.y" /* yacc.c:1646  */
    {
		     mf_yyltype_t loc;

		     loc.beg = (yylsp[-4]).beg;
		     loc.end = (yylsp[0]).end;

		     if (root_node[state_tag]) {
			     root_node[state_tag]->v.progdecl.tree.tail->next = (yyvsp[-1].stmtlist).head;
			     root_node[state_tag]->v.progdecl.tree.tail = (yyvsp[-1].stmtlist).tail;
			     (yyval.node) = NULL;
		     } else {
			     (yyval.node) = alloc_node(node_type_progdecl, &loc);
			     (yyval.node)->v.progdecl.tag = state_tag;
			     (yyval.node)->v.progdecl.tree = (yyvsp[-1].stmtlist);
			     (yyval.node)->v.progdecl.auto_count = 0;
			     root_node[state_tag] = (yyval.node);
			     (yyval.node)->v.progdecl.auto_count =
				     forget_autos(PARMCOUNT(), 0, 0);
		     }
		     outer_context = inner_context = context_none;
		     state_tag = smtp_state_none;
	     }
#line 2540 "gram.c" /* yacc.c:1646  */
    break;

  case 24:
#line 647 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-5].number) & SYM_PRECIOUS)
                             parse_error_locus(&(yylsp[-4]),
					       _("`precious' used with func"));
		     if (((yyvsp[-5].number) & (SYM_STATIC|SYM_PUBLIC)) ==
			 (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-4]),
					       _("`static' and `public' "
						 "used together"));
		     func->sym.flags = (yyvsp[-5].number);
		     func->node = (yyvsp[-1].stmtlist).head;
                     (yyval.node) = declare_function(func, &(yylsp[-4]),
			   forget_autos(PARMCOUNT() + FUNC_HIDDEN_ARGS(func),
					0, FUNC_HIDDEN_ARGS(func)));
		     outer_context = inner_context = context_none;
		     func = NULL;
	     }
#line 2562 "gram.c" /* yacc.c:1646  */
    break;

  case 25:
#line 665 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2570 "gram.c" /* yacc.c:1646  */
    break;

  case 26:
#line 669 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2578 "gram.c" /* yacc.c:1646  */
    break;

  case 27:
#line 673 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = NULL;
	     }
#line 2586 "gram.c" /* yacc.c:1646  */
    break;

  case 29:
#line 680 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.literal) = literal_lookup((yyvsp[0].var)->sym.name);
	     }
#line 2594 "gram.c" /* yacc.c:1646  */
    break;

  case 30:
#line 686 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var;
		     
		     if (((yyvsp[-2].number) & (SYM_STATIC|SYM_PUBLIC))
			 == (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-2]),
                                               _("`static' and `public' "
						 "used together"));
		     var = vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_extern, &(yylsp[0]));
		     if (!var) 
			     YYERROR;
		     var->sym.flags |= (yyvsp[-2].number);
	     }
#line 2612 "gram.c" /* yacc.c:1646  */
    break;

  case 31:
#line 700 "gram.y" /* yacc.c:1646  */
    {
                     struct value value;
                     struct variable *var;

		     if (((yyvsp[-3].number) & (SYM_STATIC|SYM_PUBLIC))
			 == (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-3]),
                                               _("`static' and `public' "
						 "used together"));
		     
                     var = vardecl((yyvsp[-1].literal)->text, (yyvsp[-2].type), storage_extern, &(yylsp[-2]));
                     if (!var)
                             YYERROR;
                     var->sym.flags |= (yyvsp[-3].number);

                     if (optimization_level)
                             optimize((yyvsp[0].node));
                     value.type = node_type((yyvsp[0].node));

                     switch ((yyvsp[0].node)->type) {
                     case node_type_string:
                             value.v.literal = (yyvsp[0].node)->v.literal;
                             break;

                     case node_type_number:
                             value.v.number = (yyvsp[0].node)->v.number;
                             break;

                     default:
                             yyerror("initializer element is not constant");
                             YYERROR;

		     }
		     if (initialize_variable(var, &value, &(yylsp[-1])))
			     YYERROR;
	     }
#line 2653 "gram.c" /* yacc.c:1646  */
    break;

  case 32:
#line 738 "gram.y" /* yacc.c:1646  */
    {
		     struct value value;
		     
		     if (optimization_level)
			     optimize((yyvsp[0].node));
		     value.type = node_type((yyvsp[0].node));
		     
		     switch ((yyvsp[0].node)->type) {
		     case node_type_string:
			     value.v.literal = (yyvsp[0].node)->v.literal;
			     break;

		     case node_type_number:
			     value.v.number = (yyvsp[0].node)->v.number;
			     break;

		     default:
			     yyerror("initializer element is not constant");
			     YYERROR;
		     }
                     if (!externdecl((yyvsp[-1].literal)->text, &value, &(yylsp[-1])))
			     YYERROR;
	     }
#line 2681 "gram.c" /* yacc.c:1646  */
    break;

  case 33:
#line 764 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = 0;
             }
#line 2689 "gram.c" /* yacc.c:1646  */
    break;

  case 34:
#line 768 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[-1].number) & (yyvsp[0].number))
                             parse_warning(_("duplicate `%s'"),
                                           symbit_to_qualifier((yyvsp[0].number)));
                     (yyval.number) = (yyvsp[-1].number) | (yyvsp[0].number);
             }
#line 2700 "gram.c" /* yacc.c:1646  */
    break;

  case 35:
#line 777 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_PRECIOUS;
             }
#line 2708 "gram.c" /* yacc.c:1646  */
    break;

  case 36:
#line 781 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_STATIC;
             }
#line 2716 "gram.c" /* yacc.c:1646  */
    break;

  case 37:
#line 785 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.number) = SYM_PUBLIC;
             }
#line 2724 "gram.c" /* yacc.c:1646  */
    break;

  case 38:
#line 792 "gram.y" /* yacc.c:1646  */
    {
		     (yyvsp[0].enumlist).cv->sym.flags = (yyvsp[-1].number);
		     (yyval.node) = NULL;
	     }
#line 2733 "gram.c" /* yacc.c:1646  */
    break;

  case 39:
#line 797 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist;
		     while ((elist = (yyvsp[-1].enumlist).prev)) {
			     elist->cv->sym.flags = (yyvsp[-3].number);
			     elist = elist->prev;
			     free((yyvsp[-1].enumlist).prev);
			     (yyvsp[-1].enumlist).prev = elist;
		     }
		     (yyval.node) = NULL;
	     }
#line 2748 "gram.c" /* yacc.c:1646  */
    break;

  case 40:
#line 810 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-1].number) & SYM_PRECIOUS)
                             parse_error_locus(&(yylsp[-1]),
					       _("`precious' used with const"));
		     if (((yyvsp[-1].number) & (SYM_STATIC|SYM_PUBLIC)) ==
			 (SYM_STATIC|SYM_PUBLIC))
                             parse_error_locus(&(yylsp[-1]),
					       _("`static' and `public' "
						 "used together"));
		     (yyval.number) = (yyvsp[-1].number);
	     }
#line 2764 "gram.c" /* yacc.c:1646  */
    break;

  case 41:
#line 825 "gram.y" /* yacc.c:1646  */
    {
		     struct value value;
		     struct variable *pvar;

		     /* FIXME: This is necessary because constants can be
			referred to the same way as variables. */
		     if (pvar = variable_lookup((yyvsp[-1].literal)->text)) {
			     parse_warning(_("constant name `%s' clashes with a variable name"),
					   (yyvsp[-1].literal)->text);
			     parse_warning_locus(&pvar->sym.locus,
						 _("this is the location of the "
						   "previous definition"));
		     }
			     
		     if (optimization_level)
			     optimize((yyvsp[0].node));
		     
		     switch ((yyvsp[0].node)->type) {
		     case node_type_string:
			     value.type = dtype_string;
			     value.v.literal = (yyvsp[0].node)->v.literal;
			     break;

		     case node_type_number:
			     value.type = dtype_number;
			     value.v.number = (yyvsp[0].node)->v.number;
			     break;
			     
		     default:
			     yyerror(_("initializer element is not constant"));
			     YYERROR;
		     }

		     (yyval.enumlist).cv = define_constant((yyvsp[-1].literal)->text, &value, 0, &(yylsp[-1]));
		     (yyval.enumlist).prev =  NULL;
	     }
#line 2805 "gram.c" /* yacc.c:1646  */
    break;

  case 42:
#line 864 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist;
		     struct value value;
		     struct variable *pvar;

		     if (pvar = variable_lookup((yyvsp[0].literal)->text)) {
			     parse_warning(_("constant name `%s' clashes with a variable name"),
					   (yyvsp[0].literal)->text);
			     parse_warning_locus(&pvar->sym.locus,
						 _("this is the location of the "
						   "previous definition"));
		     }

		     value.type = dtype_number;
		     value.v.number = 0;

		     elist = mu_alloc(sizeof(*elist));
		     
		     elist->cv = define_constant((yyvsp[0].literal)->text, &value, 0, &(yylsp[0]));
		     elist->prev = NULL;
		     (yyval.enumlist).cv = NULL;
		     (yyval.enumlist).prev = elist;
	     }
#line 2833 "gram.c" /* yacc.c:1646  */
    break;

  case 43:
#line 888 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist = mu_alloc(sizeof(*elist));
		     elist->cv = (yyvsp[0].enumlist).cv;
		     elist->prev = NULL;
		     (yyval.enumlist).cv = NULL;
		     (yyval.enumlist).prev = elist;
	     }
#line 2845 "gram.c" /* yacc.c:1646  */
    break;

  case 44:
#line 896 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-1].enumlist).prev->cv->value.type == dtype_number) {
			     struct enumlist *elist = mu_alloc(sizeof(*elist));
			     struct value value;

			     value.type = dtype_number;
			     value.v.number = (yyvsp[-1].enumlist).prev->cv->value.v.number + 1;
			     
			     elist->cv = define_constant((yyvsp[0].literal)->text, &value, 0,
							 &(yylsp[0]));
			     elist->prev = (yyvsp[-1].enumlist).prev;
			     (yyval.enumlist).prev = elist;
		     } else {
			     yyerror(_("initializer element is not numeric"));
			     YYERROR;
		     }
	     }
#line 2867 "gram.c" /* yacc.c:1646  */
    break;

  case 45:
#line 914 "gram.y" /* yacc.c:1646  */
    {
		     struct enumlist *elist = mu_alloc(sizeof(*elist));
		     elist->cv = (yyvsp[0].enumlist).cv;
		     elist->prev = (yyvsp[-1].enumlist).prev;
		     (yyvsp[-1].enumlist).prev = elist;
		     (yyval.enumlist) = (yyvsp[-1].enumlist);
	     }
#line 2879 "gram.c" /* yacc.c:1646  */
    break;

  case 46:
#line 924 "gram.y" /* yacc.c:1646  */
    {
		     define_exception((yyvsp[0].literal), &(yylsp[-1]));
	     }
#line 2887 "gram.c" /* yacc.c:1646  */
    break;

  case 47:
#line 930 "gram.y" /* yacc.c:1646  */
    {
		     data_type_t *ptypes = NULL;
		     
		     if ((yyvsp[-3].parmlist).count) {
			     int i;
			     struct parmtype *p;
			     
			     ptypes = mu_alloc((yyvsp[-3].parmlist).count * sizeof *ptypes);
			     for (i = 0, p = (yyvsp[-3].parmlist).head; p; i++) {
				     struct parmtype *next = p->next;
				     ptypes[i] = p->type;
				     free(p);
				     p = next;
			     }
		     }

		     (yyval.function) = func = function_install((yyvsp[-5].literal)->text,
						  (yyvsp[-3].parmlist).count, (yyvsp[-3].parmlist).optcount,
						  (yyvsp[-3].parmlist).varargs,
						  (yyvsp[-3].parmlist).vartype,
						  ptypes, (yyvsp[0].type),
                                                  &(yylsp[-5]));
		     if ((yyvsp[-1].list)) {
			     mu_list_foreach((yyvsp[-1].list), _create_alias, (yyval.function));
			     mu_list_destroy(&(yyvsp[-1].list));
		     }
		     outer_context = inner_context = context_function;
	     }
#line 2920 "gram.c" /* yacc.c:1646  */
    break;

  case 48:
#line 961 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = (yyval.parmlist).optcount = 0;
                     (yyval.parmlist).varargs = 0;
             }
#line 2929 "gram.c" /* yacc.c:1646  */
    break;

  case 51:
#line 970 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = (yyval.parmlist).optcount = 0;
                     (yyval.parmlist).varargs = 1;
		     (yyval.parmlist).vartype = (yyvsp[0].type);
             }
#line 2939 "gram.c" /* yacc.c:1646  */
    break;

  case 52:
#line 976 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).count += (yyvsp[0].parmlist).count;
                     (yyvsp[-2].parmlist).optcount = (yyvsp[0].parmlist).count;
		     (yyvsp[-2].parmlist).varargs = (yyvsp[0].parmlist).varargs;
		     (yyvsp[-2].parmlist).vartype = (yyvsp[0].parmlist).vartype;
                     if ((yyvsp[-2].parmlist).tail) 
                             (yyvsp[-2].parmlist).tail->next = (yyvsp[0].parmlist).head;
                     else
                             (yyvsp[-2].parmlist).head = (yyvsp[0].parmlist).head;
                     (yyvsp[-2].parmlist).tail = (yyvsp[0].parmlist).tail;
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 2956 "gram.c" /* yacc.c:1646  */
    break;

  case 53:
#line 991 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.parmlist).count = 0;
                     (yyval.parmlist).varargs = 0;
                     (yyval.parmlist).head = (yyval.parmlist).tail = NULL;
             }
#line 2966 "gram.c" /* yacc.c:1646  */
    break;

  case 55:
#line 1000 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.parmlist).count = 1;
		     (yyval.parmlist).varargs = 0;
                     (yyval.parmlist).optcount = 0;
		     (yyval.parmlist).head = (yyval.parmlist).tail = (yyvsp[0].parm);
	     }
#line 2977 "gram.c" /* yacc.c:1646  */
    break;

  case 56:
#line 1007 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).count++;
                     (yyvsp[-2].parmlist).tail->next = (yyvsp[0].parm);
                     (yyvsp[-2].parmlist).tail = (yyvsp[0].parm);
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 2988 "gram.c" /* yacc.c:1646  */
    break;

  case 58:
#line 1017 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].parmlist).varargs = 1;
		     (yyvsp[-2].parmlist).vartype = (yyvsp[0].type);
                     (yyval.parmlist) = (yyvsp[-2].parmlist);
             }
#line 2998 "gram.c" /* yacc.c:1646  */
    break;

  case 59:
#line 1025 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.type) = dtype_string;
	     }
#line 3006 "gram.c" /* yacc.c:1646  */
    break;

  case 60:
#line 1029 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.type) = (yyvsp[-1].type);
	     }
#line 3014 "gram.c" /* yacc.c:1646  */
    break;

  case 61:
#line 1035 "gram.y" /* yacc.c:1646  */
    {
		     if (!vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_param, &(yylsp[0])))
			     YYERROR;
		     (yyval.parm) = mu_alloc(sizeof *(yyval.parm));
		     (yyval.parm)->next = NULL;
		     (yyval.parm)->type = (yyvsp[-1].type);
	     }
#line 3026 "gram.c" /* yacc.c:1646  */
    break;

  case 62:
#line 1045 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.list) = NULL;
             }
#line 3034 "gram.c" /* yacc.c:1646  */
    break;

  case 64:
#line 1052 "gram.y" /* yacc.c:1646  */
    {
                     mu_list_create(&(yyval.list));
                     mu_list_append((yyval.list), (yyvsp[0].literal));
             }
#line 3043 "gram.c" /* yacc.c:1646  */
    break;

  case 65:
#line 1057 "gram.y" /* yacc.c:1646  */
    {
                     mu_list_append((yyvsp[-1].list), (yyvsp[0].literal));
                     (yyval.list) = (yyvsp[-1].list);
             }
#line 3052 "gram.c" /* yacc.c:1646  */
    break;

  case 66:
#line 1064 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.literal) = (yyvsp[0].literal);
             }
#line 3060 "gram.c" /* yacc.c:1646  */
    break;

  case 67:
#line 1070 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.type) = dtype_unspecified;
             }
#line 3068 "gram.c" /* yacc.c:1646  */
    break;

  case 68:
#line 1074 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.type) = (yyvsp[0].type);
	     }
#line 3076 "gram.c" /* yacc.c:1646  */
    break;

  case 69:
#line 1080 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.state) = string_to_state((yyvsp[0].literal)->text);
		     if ((yyval.state) == smtp_state_none)
                             parse_error_locus(&(yylsp[0]),
					       _("unknown smtp state tag: %s"),
					       (yyvsp[0].literal)->text);
		     state_tag = (yyval.state);
		     outer_context = inner_context = context_handler;
	     }
#line 3090 "gram.c" /* yacc.c:1646  */
    break;

  case 70:
#line 1092 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) 
                             (yyvsp[0].node)->next = NULL;
                     (yyval.stmtlist).head = (yyval.stmtlist).tail = (yyvsp[0].node);
             }
#line 3100 "gram.c" /* yacc.c:1646  */
    break;

  case 71:
#line 1098 "gram.y" /* yacc.c:1646  */
    {
                     if ((yyvsp[0].node)) {
                             if ((yyvsp[-1].stmtlist).tail) 
                                     (yyvsp[-1].stmtlist).tail->next = (yyvsp[0].node);
                             else
                                     (yyvsp[-1].stmtlist).head = (yyvsp[0].node);
                             (yyvsp[-1].stmtlist).tail = (yyvsp[0].node);
                     }
                     (yyval.stmtlist) = (yyvsp[-1].stmtlist);
             }
#line 3115 "gram.c" /* yacc.c:1646  */
    break;

  case 79:
#line 1118 "gram.y" /* yacc.c:1646  */
    {
		     if (node_type((yyvsp[0].node)) != dtype_unspecified) {
			     parse_warning(_("return from %s is ignored"),
					   (yyvsp[0].node)->type == node_type_builtin ?
					   (yyvsp[0].node)->v.builtin.builtin->name :
					   (yyvsp[0].node)->v.call.func->sym.name);
			     (yyvsp[0].node)->value_ignored = 1;
		     }
	     }
#line 3129 "gram.c" /* yacc.c:1646  */
    break;

  case 84:
#line 1134 "gram.y" /* yacc.c:1646  */
    {
		     (yyvsp[-1].node)->value_ignored = 1;
		     (yyval.node) = (yyvsp[-1].node);
	     }
#line 3138 "gram.c" /* yacc.c:1646  */
    break;

  case 85:
#line 1141 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var;
		     data_type_t t = node_type((yyvsp[0].node));

		     if (t == dtype_unspecified) {
                             parse_error_locus(&(yylsp[0]),
					   _("unspecified value not ignored as "
					     "it should be"));
			     YYERROR;
		     }

		     var = variable_lookup((yyvsp[-1].literal)->text);
		     if (!var) {
                             var = vardecl((yyvsp[-1].literal)->text, t, storage_auto, &(yylsp[-1]));
			     if (!var) 
				     YYERROR;
		     }
                     (yyval.node) = create_asgn_node(var, (yyvsp[0].node), &(yylsp[-2]));
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 3164 "gram.c" /* yacc.c:1646  */
    break;

  case 86:
#line 1165 "gram.y" /* yacc.c:1646  */
    {
		     if (!vardecl((yyvsp[0].literal)->text, (yyvsp[-1].type), storage_auto, &(yylsp[0])))
			     YYERROR;
		     (yyval.node) = NULL;
	     }
#line 3174 "gram.c" /* yacc.c:1646  */
    break;

  case 87:
#line 1171 "gram.y" /* yacc.c:1646  */
    {
		     struct variable *var = vardecl((yyvsp[-1].literal)->text, (yyvsp[-2].type),
						    storage_auto, &(yylsp[-1]));
		     if (!var)
			     YYERROR;
                     (yyval.node) = create_asgn_node(var, (yyvsp[0].node), &(yylsp[-2]));
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 3188 "gram.c" /* yacc.c:1646  */
    break;

  case 88:
#line 1183 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_handler) {
			     if (state_tag == smtp_state_begin)
                                     parse_error_locus(&(yylsp[0]),
                                                _("Sendmail action is not "
						   "allowed in begin block"));
			     else if (state_tag == smtp_state_end)
                                     parse_error_locus(&(yylsp[0]),
                                               _("Sendmail action is not "
						   "allowed in end block"));
		     }
	     }
#line 3205 "gram.c" /* yacc.c:1646  */
    break;

  case 89:
#line 1196 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_handler
			 && state_tag == smtp_state_end)
                            parse_error_locus(&(yylsp[0]),
                                          _("header action is not allowed "
					   "in end block"));
	     }
#line 3217 "gram.c" /* yacc.c:1646  */
    break;

  case 90:
#line 1204 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = alloc_node(node_type_noop, &(yylsp[0]));
	     }
#line 3225 "gram.c" /* yacc.c:1646  */
    break;

  case 91:
#line 1208 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = alloc_node(node_type_echo, &(yylsp[-1]));
		    (yyval.node)->v.node = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3234 "gram.c" /* yacc.c:1646  */
    break;

  case 92:
#line 1216 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].ret).code || (yyvsp[0].ret).xcode || (yyvsp[0].ret).message)
			     parse_warning(_("arguments are ignored for accept"));
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     (yyval.node)->v.ret.stat = SMFIS_ACCEPT;
	     }
#line 3246 "gram.c" /* yacc.c:1646  */
    break;

  case 93:
#line 1224 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     if (!(yyval.node)->v.ret.code)
			     (yyval.node)->v.ret.code = create_node_string(DEFAULT_REJECT_CODE, &(yylsp[-1]));
		     (yyval.node)->v.ret.stat = SMFIS_REJECT;
	     }
#line 3258 "gram.c" /* yacc.c:1646  */
    break;

  case 94:
#line 1232 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-7]));
		     (yyval.node)->v.ret.stat = SMFIS_REJECT;
		     (yyval.node)->v.ret.code = (yyvsp[-5].node) ? cast_to(dtype_string, (yyvsp[-5].node)) : create_node_string(DEFAULT_REJECT_CODE, &(yylsp[-7]));;
		     (yyval.node)->v.ret.xcode = (yyvsp[-3].node) ? cast_to(dtype_string, (yyvsp[-3].node)) : NULL;
		     (yyval.node)->v.ret.message = (yyvsp[-1].node) ? cast_to(dtype_string, (yyvsp[-1].node)) : NULL;
	     }
#line 3270 "gram.c" /* yacc.c:1646  */
    break;

  case 95:
#line 1240 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-1]));
		     (yyval.node)->v.ret = (yyvsp[0].ret);
		     if (!(yyval.node)->v.ret.code)
			     (yyval.node)->v.ret.code = create_node_string(DEFAULT_TEMPFAIL_CODE, &(yylsp[-1]));
		     (yyval.node)->v.ret.stat = SMFIS_TEMPFAIL;
	     }
#line 3282 "gram.c" /* yacc.c:1646  */
    break;

  case 96:
#line 1248 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[-7]));
		     (yyval.node)->v.ret.stat = SMFIS_TEMPFAIL;
		     (yyval.node)->v.ret.code = (yyvsp[-5].node) ? cast_to(dtype_string, (yyvsp[-5].node)) : create_node_string(DEFAULT_TEMPFAIL_CODE, &(yylsp[-7]));
		     (yyval.node)->v.ret.xcode = (yyvsp[-3].node) ? cast_to(dtype_string, (yyvsp[-3].node)) : NULL;
		     (yyval.node)->v.ret.message = (yyvsp[-1].node) ? cast_to(dtype_string, (yyvsp[-1].node)) : NULL;
	     }
#line 3294 "gram.c" /* yacc.c:1646  */
    break;

  case 97:
#line 1256 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[0]));
		     memset(&(yyval.node)->v.ret, 0, sizeof (yyval.node)->v.ret);
		     (yyval.node)->v.ret.stat = SMFIS_CONTINUE;
	     }
#line 3304 "gram.c" /* yacc.c:1646  */
    break;

  case 98:
#line 1262 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_result, &(yylsp[0]));
		     memset(&(yyval.node)->v.ret, 0, sizeof (yyval.node)->v.ret);
		     (yyval.node)->v.ret.stat = SMFIS_DISCARD;
	     }
#line 3314 "gram.c" /* yacc.c:1646  */
    break;

  case 100:
#line 1271 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 3323 "gram.c" /* yacc.c:1646  */
    break;

  case 101:
#line 1280 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-2]));
		     (yyval.node)->v.hdr.opcode = header_add;
		     (yyval.node)->v.hdr.name = (yyvsp[-1].literal);
		     (yyval.node)->v.hdr.value = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3334 "gram.c" /* yacc.c:1646  */
    break;

  case 102:
#line 1287 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-2]));
		     (yyval.node)->v.hdr.opcode = header_replace;
		     (yyval.node)->v.hdr.name = (yyvsp[-1].literal);
		     (yyval.node)->v.hdr.value = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3345 "gram.c" /* yacc.c:1646  */
    break;

  case 103:
#line 1294 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_header, &(yylsp[-1]));
		     (yyval.node)->v.hdr.opcode = header_delete;
		     (yyval.node)->v.hdr.name = (yyvsp[0].literal);
		     (yyval.node)->v.hdr.value = NULL;
	     }
#line 3356 "gram.c" /* yacc.c:1646  */
    break;

  case 104:
#line 1303 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.ret), 0, sizeof (yyval.ret));
             }
#line 3364 "gram.c" /* yacc.c:1646  */
    break;

  case 106:
#line 1310 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.ret).code->v.literal = (yyvsp[0].literal);
		     (yyval.ret).xcode = NULL;
		     (yyval.ret).message = NULL;
	     }
#line 3375 "gram.c" /* yacc.c:1646  */
    break;

  case 107:
#line 1317 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).code->v.literal = (yyvsp[-1].literal);
                     (yyval.ret).xcode = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.ret).xcode->v.literal = (yyvsp[0].literal);
		     (yyval.ret).message = NULL;
	     }
#line 3387 "gram.c" /* yacc.c:1646  */
    break;

  case 108:
#line 1325 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-2]));
		     (yyval.ret).code->v.literal = (yyvsp[-2].literal);
                     (yyval.ret).xcode = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).xcode->v.literal = (yyvsp[-1].literal);
		     (yyval.ret).message = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3399 "gram.c" /* yacc.c:1646  */
    break;

  case 109:
#line 1333 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.ret).code = alloc_node(node_type_string, &(yylsp[-1]));
		     (yyval.ret).code->v.literal = (yyvsp[-1].literal);
		     (yyval.ret).xcode = NULL;
		     (yyval.ret).message = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3410 "gram.c" /* yacc.c:1646  */
    break;

  case 110:
#line 1342 "gram.y" /* yacc.c:1646  */
    {
                     char buf[4];

                     if ((yyvsp[0].number) < 200 || (yyvsp[0].number) > 599) {
                             yyerror(_("invalid SMTP reply code"));
                             buf[0] = 0;
                     } else
                             snprintf(buf, sizeof(buf), "%lu", (yyvsp[0].number));
                     (yyval.literal) = string_alloc(buf, strlen(buf));
             }
#line 3425 "gram.c" /* yacc.c:1646  */
    break;

  case 111:
#line 1355 "gram.y" /* yacc.c:1646  */
    {
                     char buf[sizeof("5.999.999")];

                     /* RFC 1893:
                        The syntax of the new status codes is defined as:

                        status-code = class "." subject "." detail
                        class = "2"/"4"/"5"
                        subject = 1*3digit
                        detail = 1*3digit
                     */                        
                     if (((yyvsp[-4].number) != 2 && (yyvsp[-4].number) != 4 && (yyvsp[-4].number) !=5)
                         || (yyvsp[-2].number) > 999 || (yyvsp[0].number) > 999) {
                             yyerror(_("invalid extended reply code"));
                             buf[0] = 0;
                     } else
                             snprintf(buf, sizeof(buf), "%lu.%lu.%lu",
                                      (yyvsp[-4].number), (yyvsp[-2].number), (yyvsp[0].number));
                     (yyval.literal) = string_alloc(buf, strlen(buf));
             }
#line 3450 "gram.c" /* yacc.c:1646  */
    break;

  case 115:
#line 1383 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_if, &(yylsp[-4]));
		     (yyval.node)->v.cond.cond = (yyvsp[-3].node);
		     (yyval.node)->v.cond.if_true = (yyvsp[-2].stmtlist).head;
		     (yyval.node)->v.cond.if_false = (yyvsp[-1].node);
	     }
#line 3461 "gram.c" /* yacc.c:1646  */
    break;

  case 116:
#line 1392 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3469 "gram.c" /* yacc.c:1646  */
    break;

  case 117:
#line 1396 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_if, &(yylsp[-3]));
		     (yyval.node)->v.cond.cond = (yyvsp[-2].node);
		     (yyval.node)->v.cond.if_true = (yyvsp[-1].stmtlist).head;
		     (yyval.node)->v.cond.if_false = (yyvsp[0].node);
	     }
#line 3480 "gram.c" /* yacc.c:1646  */
    break;

  case 118:
#line 1403 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].stmtlist).head;
             }
#line 3488 "gram.c" /* yacc.c:1646  */
    break;

  case 119:
#line 1409 "gram.y" /* yacc.c:1646  */
    {
		     struct case_stmt *defcase = NULL, *pcase, *prev;
		     
                     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = (yyvsp[-3].node);
		     
		     /* Make sure there is only one default case and
			place it at the beginning of the list */
		     pcase = (yyvsp[-1].case_list).head;
		     if (!pcase->valist) {
			     defcase = pcase;
			     (yyvsp[-1].case_list).head = (yyvsp[-1].case_list).head->next;
		     }
		     prev = pcase;
		     pcase = pcase->next;
		     while (pcase) {
			     if (!pcase->valist) {
				     if (defcase) {
					     parse_error_locus(&pcase->locus,
					      _("duplicate default statement"));
					     parse_error_locus(&defcase->locus,
					      _("previously defined here"));
					     YYERROR;
				     }
				     defcase = pcase;
				     prev->next = pcase->next;
			     } else
				     prev = pcase;
			     pcase = pcase->next;
		     }

		     if (!defcase) {
			     defcase = mu_alloc(sizeof *defcase);
			     mu_locus_range_init(&defcase->locus);
                             mu_locus_range_copy(&defcase->locus, &(yylsp[0]));
			     defcase->valist = NULL;
			     defcase->node = alloc_node(node_type_noop,
							&defcase->locus);
		     }
		     defcase->next = (yyvsp[-1].case_list).head;
		     (yyval.node)->v.switch_stmt.cases = defcase;
	     }
#line 3535 "gram.c" /* yacc.c:1646  */
    break;

  case 120:
#line 1454 "gram.y" /* yacc.c:1646  */
    {
                       (yyval.case_list).head = (yyval.case_list).tail = (yyvsp[0].case_stmt);
               }
#line 3543 "gram.c" /* yacc.c:1646  */
    break;

  case 121:
#line 1458 "gram.y" /* yacc.c:1646  */
    {
                       (yyval.case_list).tail->next = (yyvsp[0].case_stmt);
                       (yyval.case_list).tail = (yyvsp[0].case_stmt);
               }
#line 3552 "gram.c" /* yacc.c:1646  */
    break;

  case 122:
#line 1465 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-3]));
		     (yyval.case_stmt)->valist = (yyvsp[-2].valist_list).head;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 3565 "gram.c" /* yacc.c:1646  */
    break;

  case 123:
#line 1474 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-2]));
		     (yyval.case_stmt)->valist = NULL;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 3578 "gram.c" /* yacc.c:1646  */
    break;

  case 124:
#line 1485 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.valist_list).head = (yyval.valist_list).tail = mu_alloc(sizeof((yyval.valist_list).head[0]));
                     (yyval.valist_list).head->next = NULL;
                     (yyval.valist_list).head->value = (yyvsp[0].value);
             }
#line 3588 "gram.c" /* yacc.c:1646  */
    break;

  case 125:
#line 1491 "gram.y" /* yacc.c:1646  */
    {
                     struct valist *p = mu_alloc(sizeof(*p));
                     p->value = (yyvsp[0].value);
                     p->next = NULL;
                     (yyvsp[-2].valist_list).tail->next = p;
                     (yyvsp[-2].valist_list).tail = p;
                     (yyval.valist_list) = (yyvsp[-2].valist_list);
             }
#line 3601 "gram.c" /* yacc.c:1646  */
    break;

  case 126:
#line 1502 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.value).type = dtype_string;
		     (yyval.value).v.literal = (yyvsp[0].literal);
	     }
#line 3610 "gram.c" /* yacc.c:1646  */
    break;

  case 127:
#line 1507 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.value).type = dtype_number;
                     (yyval.value).v.number = (yyvsp[0].number);
             }
#line 3619 "gram.c" /* yacc.c:1646  */
    break;

  case 128:
#line 1514 "gram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].value).type != dtype_string) {
                             parse_error_locus(&(yylsp[0]),
                                         _("expected string, but found %s"),
					 type_to_string((yyvsp[0].value).type));
			     /* Make sure we return something usable: */
			     (yyval.literal) = string_alloc("ERROR", 5);
		     } else
			     (yyval.literal) = (yyvsp[0].value).v.literal; 
	     }
#line 3634 "gram.c" /* yacc.c:1646  */
    break;

  case 129:
#line 1527 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = 0;
	     }
#line 3642 "gram.c" /* yacc.c:1646  */
    break;

  case 130:
#line 1531 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = QUALIFIER_MX;
	     }
#line 3650 "gram.c" /* yacc.c:1646  */
    break;

  case 131:
#line 1537 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = 0;
	     }
#line 3658 "gram.c" /* yacc.c:1646  */
    break;

  case 132:
#line 1541 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.matchtype).qualifier = QUALIFIER_MX;
	     }
#line 3666 "gram.c" /* yacc.c:1646  */
    break;

  case 133:
#line 1550 "gram.y" /* yacc.c:1646  */
    {
		     leave_loop();
		     (yyvsp[-4].loop).end_while = (yyvsp[0].node);
                     (yyval.node) = alloc_node(node_type_loop, &(yylsp[-6]));
		     (yyvsp[-4].loop).body = (yyvsp[-2].stmtlist).head;
		     (yyvsp[-4].loop).ident = (yyvsp[-5].literal);
		     (yyval.node)->v.loop = (yyvsp[-4].loop);
	     }
#line 3679 "gram.c" /* yacc.c:1646  */
    break;

  case 134:
#line 1561 "gram.y" /* yacc.c:1646  */
    {
                     enter_loop((yyvsp[0].literal), NULL, NULL);
             }
#line 3687 "gram.c" /* yacc.c:1646  */
    break;

  case 135:
#line 1567 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.literal) = NULL;
	     }
#line 3695 "gram.c" /* yacc.c:1646  */
    break;

  case 137:
#line 1574 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.loop), 0, sizeof (yyval.loop));
             }
#line 3703 "gram.c" /* yacc.c:1646  */
    break;

  case 139:
#line 1581 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.loop), 0, sizeof (yyval.loop));
                     switch ((yyvsp[0].pollarg).kw) {
                     case 0:
                             (yyval.loop).stmt = (yyvsp[0].pollarg).expr;
                             break;

                     case T_FOR:
                             (yyval.loop).for_stmt = (yyvsp[0].pollarg).expr;
                             break;

                     case T_WHILE:
                             (yyval.loop).beg_while = (yyvsp[0].pollarg).expr;
                             break;

                     default:
                             abort();
                     }
             }
#line 3727 "gram.c" /* yacc.c:1646  */
    break;

  case 140:
#line 1601 "gram.y" /* yacc.c:1646  */
    {
		     switch ((yyvsp[0].pollarg).kw) {
		     case 0:
			     if ((yyval.loop).stmt) 
                                     parse_error_locus(&(yylsp[0]),
                                                _("duplicate loop increment"));
			     (yyval.loop).stmt = (yyvsp[0].pollarg).expr;
			     break;

		     case T_FOR:
			     if ((yyval.loop).for_stmt) 
                                     parse_error_locus(&(yylsp[0]),
                                                _("duplicate for statement"));
			     (yyval.loop).for_stmt = (yyvsp[0].pollarg).expr;
			     break;

		     case T_WHILE:
			     if ((yyval.loop).beg_while) 
                                     parse_error_locus(&(yylsp[0]),
                                              _("duplicate while statement"));
			     (yyval.loop).beg_while = (yyvsp[0].pollarg).expr;
			     break;

		     default:
			     abort();
		     }
	     }
#line 3759 "gram.c" /* yacc.c:1646  */
    break;

  case 141:
#line 1631 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = 0;
                     (yyval.pollarg).expr = (yyvsp[0].stmtlist).head;
             }
#line 3768 "gram.c" /* yacc.c:1646  */
    break;

  case 142:
#line 1636 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FOR;
                     (yyval.pollarg).expr = (yyvsp[0].stmtlist).head;
             }
#line 3777 "gram.c" /* yacc.c:1646  */
    break;

  case 143:
#line 1641 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_WHILE;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 3786 "gram.c" /* yacc.c:1646  */
    break;

  case 144:
#line 1648 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3794 "gram.c" /* yacc.c:1646  */
    break;

  case 145:
#line 1652 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].node);
             }
#line 3802 "gram.c" /* yacc.c:1646  */
    break;

  case 146:
#line 1658 "gram.y" /* yacc.c:1646  */
    {
		     if (!within_loop((yyvsp[0].literal))) {
			     if ((yyvsp[0].literal))
                                     parse_error_locus(&(yylsp[0]),
                                                       _("no such loop: %s"), 
				                 (yyvsp[0].literal)->text);
                             parse_error_locus(&(yylsp[-1]),
                                        _("`break' used outside of `loop'"));
			     YYERROR;
		     }
                     (yyval.node) = alloc_node(node_type_break, &(yylsp[-1]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 3820 "gram.c" /* yacc.c:1646  */
    break;

  case 147:
#line 1672 "gram.y" /* yacc.c:1646  */
    {
		     if (!within_loop((yyvsp[0].literal))) {
			     if ((yyvsp[0].literal)) {
                                     parse_error_locus(&(yylsp[0]),
                                                       _("no such loop: %s"),
                                                       (yyvsp[0].literal)->text);
                                     parse_error_locus(&(yylsp[-1]),
                                                       _("`next' used outside `loop'"));
				     YYERROR;
			     } else {
                                     parse_error_locus(&(yylsp[-1]),
                                           _("`next' is used outside `loop'; "
                                             "did you mean `pass'?"));
				     YYERROR;
			     }
		     } else {
                             (yyval.node) = alloc_node(node_type_next, &(yylsp[-1]));
			     (yyval.node)->v.literal = (yyvsp[0].literal);
		     }
	     }
#line 3845 "gram.c" /* yacc.c:1646  */
    break;

  case 148:
#line 1697 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_not;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3855 "gram.c" /* yacc.c:1646  */
    break;

  case 149:
#line 1703 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_eq;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3866 "gram.c" /* yacc.c:1646  */
    break;

  case 150:
#line 1710 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_ne;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3877 "gram.c" /* yacc.c:1646  */
    break;

  case 151:
#line 1717 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_lt;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3888 "gram.c" /* yacc.c:1646  */
    break;

  case 152:
#line 1724 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_le;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3899 "gram.c" /* yacc.c:1646  */
    break;

  case 153:
#line 1731 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_gt;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3910 "gram.c" /* yacc.c:1646  */
    break;

  case 154:
#line 1738 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_ge;
		     (yyval.node)->v.bin.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.bin.arg[1] = cast_to(node_type((yyvsp[-2].node)), (yyvsp[0].node));
	     }
#line 3921 "gram.c" /* yacc.c:1646  */
    break;

  case 155:
#line 1745 "gram.y" /* yacc.c:1646  */
    {
		     NODE *p;
		     
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_match;
		     (yyval.node)->v.bin.qualifier = (yyvsp[-1].matchtype).qualifier;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
                     (yyval.node)->v.bin.arg[1] = p = alloc_node(node_type_regcomp,
                                                       &(yylsp[-1]));
		     p->v.regcomp_data.expr = cast_to(dtype_string, (yyvsp[0].node));
		     p->v.regcomp_data.flags = regex_flags;
		     p->v.regcomp_data.regind = -1;
	     }
#line 3939 "gram.c" /* yacc.c:1646  */
    break;

  case 156:
#line 1759 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_fnmatch;
		     (yyval.node)->v.bin.qualifier = (yyvsp[-1].matchtype).qualifier;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 3951 "gram.c" /* yacc.c:1646  */
    break;

  case 157:
#line 1767 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_or;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3962 "gram.c" /* yacc.c:1646  */
    break;

  case 158:
#line 1774 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-1]));
		     (yyval.node)->v.bin.opcode = bin_and;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3973 "gram.c" /* yacc.c:1646  */
    break;

  case 160:
#line 1784 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = NULL;
             }
#line 3981 "gram.c" /* yacc.c:1646  */
    break;

  case 163:
#line 1792 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_add;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 3992 "gram.c" /* yacc.c:1646  */
    break;

  case 164:
#line 1799 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_concat, &(yylsp[-1]));
		     (yyval.node)->v.concat.arg[0] = cast_to(dtype_string, (yyvsp[-2].node));
		     (yyval.node)->v.concat.arg[1] = cast_to(dtype_string, (yyvsp[0].node));
	     }
#line 4002 "gram.c" /* yacc.c:1646  */
    break;

  case 165:
#line 1805 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_sub;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4013 "gram.c" /* yacc.c:1646  */
    break;

  case 166:
#line 1812 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_mul;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4024 "gram.c" /* yacc.c:1646  */
    break;

  case 167:
#line 1819 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_div;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4035 "gram.c" /* yacc.c:1646  */
    break;

  case 168:
#line 1826 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_mod;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4046 "gram.c" /* yacc.c:1646  */
    break;

  case 169:
#line 1833 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logand;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4057 "gram.c" /* yacc.c:1646  */
    break;

  case 170:
#line 1840 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logor;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4068 "gram.c" /* yacc.c:1646  */
    break;

  case 171:
#line 1847 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_logxor;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4079 "gram.c" /* yacc.c:1646  */
    break;

  case 172:
#line 1854 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_shl;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4090 "gram.c" /* yacc.c:1646  */
    break;

  case 173:
#line 1861 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_bin, &(yylsp[-2]));
		     (yyval.node)->v.bin.opcode = bin_shr;
		     (yyval.node)->v.bin.arg[0] = cast_to(dtype_number, (yyvsp[-2].node));
		     (yyval.node)->v.bin.arg[1] = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4101 "gram.c" /* yacc.c:1646  */
    break;

  case 174:
#line 1870 "gram.y" /* yacc.c:1646  */
    {
		     if (node_type((yyvsp[0].node)) == dtype_unspecified)
			     parse_error(_("unspecified value not ignored as it should be"));
	     }
#line 4110 "gram.c" /* yacc.c:1646  */
    break;

  case 175:
#line 1875 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[-1].node);
             }
#line 4118 "gram.c" /* yacc.c:1646  */
    break;

  case 176:
#line 1879 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = cast_to((yyvsp[-3].type), (yyvsp[-1].node));
             }
#line 4126 "gram.c" /* yacc.c:1646  */
    break;

  case 178:
#line 1884 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_minus;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4136 "gram.c" /* yacc.c:1646  */
    break;

  case 179:
#line 1890 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[0].node);
             }
#line 4144 "gram.c" /* yacc.c:1646  */
    break;

  case 180:
#line 1894 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = alloc_node(node_type_un, &(yylsp[-1]));
		     (yyval.node)->v.un.opcode = unary_lognot;
		     (yyval.node)->v.un.arg = cast_to(dtype_number, (yyvsp[0].node));
	     }
#line 4154 "gram.c" /* yacc.c:1646  */
    break;

  case 181:
#line 1902 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_symbol((yyvsp[0].literal), &(yylsp[0]));
	     }
#line 4162 "gram.c" /* yacc.c:1646  */
    break;

  case 182:
#line 1906 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_number, &(yylsp[0]));
		     (yyval.node)->v.number = (yyvsp[0].number);
	     }
#line 4171 "gram.c" /* yacc.c:1646  */
    break;

  case 183:
#line 1911 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_backref((yyvsp[0].number), &(yylsp[0]));
	     }
#line 4179 "gram.c" /* yacc.c:1646  */
    break;

  case 185:
#line 1916 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_argcount(&(yylsp[0]));
	     }
#line 4187 "gram.c" /* yacc.c:1646  */
    break;

  case 186:
#line 1920 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_offset, &(yylsp[-1]));
		     (yyval.node)->v.var_ref.variable = (yyvsp[0].var);
		     (yyval.node)->v.var_ref.nframes = catch_nesting;
	     }
#line 4197 "gram.c" /* yacc.c:1646  */
    break;

  case 187:
#line 1926 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_vaptr, &(yylsp[-1]));
		     (yyval.node)->v.node = (yyvsp[0].node);
	     }
#line 4206 "gram.c" /* yacc.c:1646  */
    break;

  case 190:
#line 1935 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_concat, &(yylsp[0]));
		     (yyval.node)->v.concat.arg[0] = (yyvsp[-1].node);
		     (yyval.node)->v.concat.arg[1] = (yyvsp[0].node);
	     }
#line 4216 "gram.c" /* yacc.c:1646  */
    break;

  case 191:
#line 1943 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_string, &(yylsp[0]));
		     (yyval.node)->v.literal = (yyvsp[0].literal);
	     }
#line 4225 "gram.c" /* yacc.c:1646  */
    break;

  case 193:
#line 1951 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = create_node_variable((yyvsp[0].var), &(yylsp[0]));
	     }
#line 4233 "gram.c" /* yacc.c:1646  */
    break;

  case 194:
#line 1955 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = create_node_arg((yyvsp[0].number), &(yylsp[0]));
 	     }
#line 4241 "gram.c" /* yacc.c:1646  */
    break;

  case 195:
#line 1959 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_function && func->varargs) {
			     (yyval.node) = alloc_node(node_type_argx, &(yylsp[-3]));
			     (yyval.node)->v.argx.nargs = PARMCOUNT() +
				                 FUNC_HIDDEN_ARGS(func);
			     (yyval.node)->v.argx.node = (yyvsp[-1].node);
			     (yyval.node)->v.argx.dtype = func->vartype;
		     } else {
                             (yyval.node) = alloc_node(node_type_noop, &(yylsp[-3]));
                             parse_error_locus(&(yylsp[-3]),
                                         _("$(expr) is allowed only "
					   "in a function with "
					   "variable number of "
					   "arguments"));
		     }
	     }
#line 4262 "gram.c" /* yacc.c:1646  */
    break;

  case 197:
#line 1979 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.node) = (yyvsp[-1].node);
             }
#line 4270 "gram.c" /* yacc.c:1646  */
    break;

  case 198:
#line 1985 "gram.y" /* yacc.c:1646  */
    {
		     if (check_builtin_usage((yyvsp[-3].builtin), &(yylsp[-3])))
			     YYERROR;

		     if ((yyvsp[-3].builtin)->flags & MFD_BUILTIN_REGEX_FLAGS) {
			     NODE *p = alloc_node(node_type_number, &(yylsp[-3]));
			     p->v.number = regex_flags;
			     p->next = (yyvsp[-1].arglist).head;
			     (yyvsp[-1].arglist).head = p;
			     (yyvsp[-1].arglist).count++;
		     }

		     if ((yyvsp[-1].arglist).count < (yyvsp[-3].builtin)->parmcount - (yyvsp[-3].builtin)->optcount) {
                             parse_error_locus(&(yylsp[-3]),
                                       _("too few arguments in call to `%s'"),
					 (yyvsp[-3].builtin)->name);
			     YYERROR;
		     } else if ((yyvsp[-1].arglist).count > (yyvsp[-3].builtin)->parmcount
				&& !((yyvsp[-3].builtin)->flags & MFD_BUILTIN_VARIADIC)) {
                             parse_error_locus(&(yylsp[-3]),
                                      _("too many arguments in call to `%s'"),
					 (yyvsp[-3].builtin)->name);
			     YYERROR;
		     } else {
                             (yyval.node) = alloc_node(node_type_builtin, &(yylsp[-3]));
			     (yyval.node)->v.builtin.builtin = (yyvsp[-3].builtin);
			     (yyval.node)->v.builtin.args =
					     reverse(cast_arg_list((yyvsp[-1].arglist).head,
 							       (yyvsp[-3].builtin)->parmcount,
							       (yyvsp[-3].builtin)->parmtype,
							       ((yyval.node)->v.builtin.builtin->flags & MFD_BUILTIN_NO_PROMOTE) ? dtype_any : dtype_string));
		     }
	     }
#line 4308 "gram.c" /* yacc.c:1646  */
    break;

  case 199:
#line 2019 "gram.y" /* yacc.c:1646  */
    {
		     if (check_builtin_usage((yyvsp[-2].builtin), &(yylsp[-2])))
			     YYERROR;
		     if ((yyvsp[-2].builtin)->parmcount - (yyvsp[-2].builtin)->optcount) {
                             parse_error_locus(&(yylsp[-2]),
                                       _("too few arguments in call to `%s'"),
					 (yyvsp[-2].builtin)->name);
			     YYERROR;
		     } else {
                             (yyval.node) = alloc_node(node_type_builtin, &(yylsp[-2]));
			     (yyval.node)->v.builtin.builtin = (yyvsp[-2].builtin);
			     (yyval.node)->v.builtin.args = NULL;
		     }
	     }
#line 4327 "gram.c" /* yacc.c:1646  */
    break;

  case 200:
#line 2034 "gram.y" /* yacc.c:1646  */
    {
		     NODE *arg, *expr;

		     if ((yyvsp[-1].arglist).count < 2) {
			     parse_error_locus(&(yylsp[-3]),
					       _("too few arguments in call to `%s'"),
					       "sed");
			     YYERROR;
		     }

		     arg = cast_to(dtype_string, (yyvsp[-1].arglist).head);
		     expr = (yyvsp[-1].arglist).head->next;
		     /* Break the link between the args lest any eventual
			optimizations cause grief later in mark phase. */
		     (yyvsp[-1].arglist).head->next = NULL;

		     do {
			     NODE *comp, *next;
			     struct mu_locus_range lr;

			     comp = alloc_node(node_type_sedcomp, &expr->locus);
			     comp->v.sedcomp.index = next_transform_index();
			     comp->v.sedcomp.expr = cast_to(dtype_string, expr);
			     comp->v.sedcomp.flags = regex_flags;

			     lr.beg = (yylsp[-3]).beg;
			     lr.end = expr->locus.end;

			     (yyval.node) = alloc_node(node_type_sed, &lr);
			     (yyval.node)->v.sed.comp = comp;
			     (yyval.node)->v.sed.arg = arg;

			     next = expr->next;
			     expr->next = NULL;
			     expr = next;

			     arg = (yyval.node);
		     } while (expr);
	     }
#line 4371 "gram.c" /* yacc.c:1646  */
    break;

  case 201:
#line 2074 "gram.y" /* yacc.c:1646  */
    {
                     if (check_func_usage((yyvsp[-3].function), &(yylsp[-3])))
			     YYERROR;
		     (yyval.node) = function_call((yyvsp[-3].function), (yyvsp[-1].arglist).count, (yyvsp[-1].arglist).head);
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 4383 "gram.c" /* yacc.c:1646  */
    break;

  case 202:
#line 2082 "gram.y" /* yacc.c:1646  */
    {
                     if (check_func_usage((yyvsp[-2].function), &(yylsp[-2])))
			     YYERROR;
		     (yyval.node) = function_call((yyvsp[-2].function), 0, NULL);
		     if (!(yyval.node))
			     YYERROR;
	     }
#line 4395 "gram.c" /* yacc.c:1646  */
    break;

  case 203:
#line 2092 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.arglist).head = (yyval.arglist).tail = (yyvsp[0].node);
                     (yyval.arglist).count = 1;
	     }
#line 4404 "gram.c" /* yacc.c:1646  */
    break;

  case 205:
#line 2098 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].arglist).tail->next = (yyvsp[0].node);
                     (yyvsp[-2].arglist).tail = (yyvsp[0].node);
                     (yyvsp[-2].arglist).count++;
                     (yyval.arglist) = (yyvsp[-2].arglist);
	     }
#line 4415 "gram.c" /* yacc.c:1646  */
    break;

  case 206:
#line 2107 "gram.y" /* yacc.c:1646  */
    {
		     if (inner_context == context_function && func->varargs) {
			     (yyval.node) = alloc_node(node_type_argv, &(yylsp[-1]));
			     (yyval.node)->v.argv.nargs = PARMCOUNT();
			     (yyval.node)->v.argv.node = (yyvsp[0].node);
		     } else {
			     (yyval.node) = alloc_node(node_type_noop, &(yylsp[-1]));
			     parse_error_locus(&(yylsp[-1]),
					       _("$@ is allowed only in variadic functions"));
			     
		     }
	     }
#line 4432 "gram.c" /* yacc.c:1646  */
    break;

  case 207:
#line 2122 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = NULL;
             }
#line 4440 "gram.c" /* yacc.c:1646  */
    break;

  case 208:
#line 2126 "gram.y" /* yacc.c:1646  */
    {
                    (yyval.node) = (yyvsp[-1].node);
             }
#line 4448 "gram.c" /* yacc.c:1646  */
    break;

  case 209:
#line 2132 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[0].node)->next = NULL;
                     (yyval.arglist).head = (yyval.arglist).tail = (yyvsp[0].node);
                     (yyval.arglist).count = 1;
             }
#line 4458 "gram.c" /* yacc.c:1646  */
    break;

  case 210:
#line 2138 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-2].arglist).tail->next = (yyvsp[0].node);
                     (yyvsp[-2].arglist).tail = (yyvsp[0].node);
                     (yyvsp[-2].arglist).count++;
                     (yyval.arglist) = (yyvsp[-2].arglist);
             }
#line 4469 "gram.c" /* yacc.c:1646  */
    break;

  case 211:
#line 2147 "gram.y" /* yacc.c:1646  */
    {
		     add_xref((yyvsp[0].var), &(yylsp[0]));
	     }
#line 4477 "gram.c" /* yacc.c:1646  */
    break;

  case 212:
#line 2151 "gram.y" /* yacc.c:1646  */
    {
                     YYERROR;
             }
#line 4485 "gram.c" /* yacc.c:1646  */
    break;

  case 213:
#line 2157 "gram.y" /* yacc.c:1646  */
    {
		     if (outer_context == context_function) {
			     func->exmask->all |= (yyval.node)->v.catch.exmask->all;
			     bitmask_merge(&func->exmask->bm,
					   &(yyval.node)->v.catch.exmask->bm);
		     }
	     }
#line 4497 "gram.c" /* yacc.c:1646  */
    break;

  case 214:
#line 2165 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_try, &(yylsp[-1]));
		     (yyval.node)->v.try.node = (yyvsp[-1].node);
		     (yyval.node)->v.try.catch = (yyvsp[0].node);
	     }
#line 4507 "gram.c" /* yacc.c:1646  */
    break;

  case 215:
#line 2173 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = (yyvsp[-1].stmtlist).head;
	     }
#line 4515 "gram.c" /* yacc.c:1646  */
    break;

  case 216:
#line 2180 "gram.y" /* yacc.c:1646  */
    { (yyval.tie_in) = inner_context;
                   inner_context = context_catch; 
                   catch_nesting++; }
#line 4523 "gram.c" /* yacc.c:1646  */
    break;

  case 217:
#line 2184 "gram.y" /* yacc.c:1646  */
    {
		      int i;
		      struct valist *p;

		      inner_context = (yyvsp[-2].tie_in);
		      catch_nesting--;
                      (yyval.node) = alloc_node(node_type_catch, &(yylsp[-5]));
		      (yyval.node)->v.catch.exmask = exmask_create();
		      (yyval.node)->v.catch.context = outer_context;/*??*/
		      (yyval.node)->v.catch.exmask->all = (yyvsp[-4].catchlist).all;
		      if (!(yyvsp[-4].catchlist).all) {
			      for (i = 0, p = (yyvsp[-4].catchlist).valist; p; p = p->next, i++) {
				      if (p->value.type != dtype_number) {
					      parse_error_locus(&(yylsp[-5]),
								_("expected numeric value, but found `%s'"),
								p->value.v.literal->text);
					      continue;
				      }
				      bitmask_set(&(yyval.node)->v.catch.exmask->bm,
						  p->value.v.number);
			      }
		      }
		      (yyval.node)->v.catch.node = (yyvsp[-1].stmtlist).head;
	      }
#line 4552 "gram.c" /* yacc.c:1646  */
    break;

  case 218:
#line 2211 "gram.y" /* yacc.c:1646  */
    {
		      (yyval.catchlist).all = 1;
	      }
#line 4560 "gram.c" /* yacc.c:1646  */
    break;

  case 219:
#line 2215 "gram.y" /* yacc.c:1646  */
    {
		      (yyval.catchlist).all = 0;
		      (yyval.catchlist).valist = (yyvsp[0].valist_list).head;
	      }
#line 4569 "gram.c" /* yacc.c:1646  */
    break;

  case 220:
#line 2222 "gram.y" /* yacc.c:1646  */
    {
                      (yyval.node) = alloc_node(node_type_throw, &(yylsp[-2]));
		      if ((yyvsp[-1].value).type != dtype_number) 
                              parse_error_locus(&(yylsp[-1]),
                                           _("exception code not a number"));
		      else if ((yyvsp[-1].value).v.number > exception_count)
                              parse_error_locus(&(yylsp[-1]),
                                           _("invalid exception number: %lu"),
					  (yyvsp[-1].value).v.number);

		      (yyval.node)->v.throw.code = (yyvsp[-1].value).v.number;
		      (yyval.node)->v.throw.expr = cast_to(dtype_string, (yyvsp[0].node));
	      }
#line 4587 "gram.c" /* yacc.c:1646  */
    break;

  case 221:
#line 2238 "gram.y" /* yacc.c:1646  */
    {
		      if (!func) 
			      parse_error_locus(&(yylsp[0]),
				      _("`return' outside of a function"));
		      else if (func->rettype != dtype_unspecified) 
			      parse_error_locus(&(yylsp[0]),
				      _("`return' with no value, in function "
				      "returning non-void"));
		      (yyval.node) = alloc_node(node_type_return, &(yylsp[0]));
		      (yyval.node)->v.node = NULL;
	      }
#line 4603 "gram.c" /* yacc.c:1646  */
    break;

  case 222:
#line 2250 "gram.y" /* yacc.c:1646  */
    {
		      if (!func) 
			      parse_error_locus(&(yylsp[-1]),
					_("`return' outside of a function"));
		      else {
			      (yyval.node) = alloc_node(node_type_return, &(yylsp[-1]));
			      if (func->rettype == dtype_unspecified) { 
				      parse_error_locus(&(yylsp[-1]),
				       _("`return' with a value, in function "
				       "returning void"));
				      (yyval.node)->v.node = NULL;
			      }
			      else
				      (yyval.node)->v.node = cast_to(func->rettype, (yyvsp[0].node));
		      }
	      }
#line 4624 "gram.c" /* yacc.c:1646  */
    break;

  case 223:
#line 2269 "gram.y" /* yacc.c:1646  */
    {
		      switch (node_type((yyvsp[0].node))) {
		      case dtype_string:
			      (yyval.node) = alloc_node(node_type_bin, &(yylsp[0]));
			      (yyval.node)->v.bin.opcode = bin_ne;
			      (yyval.node)->v.bin.arg[0] = (yyvsp[0].node);
			      (yyval.node)->v.bin.arg[1] = alloc_node(node_type_string, &(yylsp[0]));
			      (yyval.node)->v.bin.arg[1]->v.literal = literal_lookup("");
			      break;
			      
		      case dtype_number:
		      case dtype_pointer:
			      (yyval.node) = (yyvsp[0].node);
			      break;
			      
		      default:
			      parse_error_locus(&(yylsp[0]),
						_("unspecified data type in conditional expression: please, report"));
			      YYERROR;
		      }
	      }
#line 4650 "gram.c" /* yacc.c:1646  */
    break;

  case 224:
#line 2297 "gram.y" /* yacc.c:1646  */
    {
		     NODE *sel, *np;
		     NODE *head = NULL, *tail;
		     struct function *fp;
		     
		     fp = function_lookup((yyvsp[-3].poll).client_addr ?
					  "strictpoll" : "stdpoll");
		     if (!fp) {
			     parse_error_locus(&(yylsp[-4]),
					       _("`on poll' used without prior `require poll'"));
			     YYERROR;
		     }

		     /* Build argument list */
		     if ((yyvsp[-3].poll).client_addr) {
			     head = tail = (yyvsp[-3].poll).client_addr;
			     tail = (yyvsp[-3].poll).email;
			     if (!tail) {
                                     parse_error_locus(&(yylsp[-3]),
                                          _("recipient address not specified "
                                            "in `on poll' construct"));
				     YYERROR;
			     }
			     tail->next = NULL;
			     head->next = tail;
		     } else
			     head = tail = (yyvsp[-3].poll).email;

		     if ((yyvsp[-3].poll).ehlo)
			     np = (yyvsp[-3].poll).ehlo;
		     else {
			     /* FIXME: Pass NULL? */
                             np = alloc_node(node_type_variable, &(yylsp[-3]));
			     np->v.var_ref.variable =
				     variable_lookup("ehlo_domain");
			     np->v.var_ref.nframes = 0;
		     }
		     tail->next = np;
		     tail = np;

		     if ((yyvsp[-3].poll).mailfrom)
			     np = (yyvsp[-3].poll).mailfrom;
		     else {
			     /* FIXME: Pass NULL? */
                             np = alloc_node(node_type_variable, &(yylsp[-3]));
			     np->v.var_ref.variable =
				     variable_lookup("mailfrom_address");
			     np->v.var_ref.nframes = 0;
		     }
		     tail->next = np;
		     tail = np;

		     sel = function_call(fp, nodelistlength(head), head);

		     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = sel;
		     (yyval.node)->v.switch_stmt.cases = (yyvsp[-1].case_list).head;
	     }
#line 4713 "gram.c" /* yacc.c:1646  */
    break;

  case 225:
#line 2356 "gram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = alloc_node(node_type_switch, &(yylsp[-4]));
		     (yyval.node)->v.switch_stmt.node = (yyvsp[-3].node);
		     (yyval.node)->v.switch_stmt.cases = (yyvsp[-1].case_list).head;
             }
#line 4723 "gram.c" /* yacc.c:1646  */
    break;

  case 226:
#line 2364 "gram.y" /* yacc.c:1646  */
    {
                     tie_in_onblock(1);
             }
#line 4731 "gram.c" /* yacc.c:1646  */
    break;

  case 227:
#line 2370 "gram.y" /* yacc.c:1646  */
    {
                     tie_in_onblock(0);
             }
#line 4739 "gram.c" /* yacc.c:1646  */
    break;

  case 228:
#line 2376 "gram.y" /* yacc.c:1646  */
    {
                     struct pollarg arg;
                     
                     arg.kw = T_FOR;
                     arg.expr = (yyvsp[0].node);
                     memset(&(yyval.poll), 0, sizeof (yyval.poll));
                     set_poll_arg(&(yyval.poll), arg.kw, arg.expr);
             }
#line 4752 "gram.c" /* yacc.c:1646  */
    break;

  case 229:
#line 2385 "gram.y" /* yacc.c:1646  */
    {
                     struct pollarg arg;
                     
                     arg.kw = T_FOR;
                     arg.expr = (yyvsp[-1].node);
                     set_poll_arg(&(yyvsp[0].poll), arg.kw, arg.expr);
                     (yyval.poll) = (yyvsp[0].poll);
             }
#line 4765 "gram.c" /* yacc.c:1646  */
    break;

  case 230:
#line 2394 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.poll) = (yyvsp[0].poll);
             }
#line 4773 "gram.c" /* yacc.c:1646  */
    break;

  case 231:
#line 2400 "gram.y" /* yacc.c:1646  */
    {
                     memset(&(yyval.poll), 0, sizeof (yyval.poll));
                     set_poll_arg(&(yyval.poll), (yyvsp[0].pollarg).kw, (yyvsp[0].pollarg).expr);
             }
#line 4782 "gram.c" /* yacc.c:1646  */
    break;

  case 232:
#line 2405 "gram.y" /* yacc.c:1646  */
    {
                     set_poll_arg(&(yyvsp[-1].poll), (yyvsp[0].pollarg).kw, (yyvsp[0].pollarg).expr);
                     (yyval.poll) = (yyvsp[-1].poll);
             }
#line 4791 "gram.c" /* yacc.c:1646  */
    break;

  case 233:
#line 2412 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FOR;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4800 "gram.c" /* yacc.c:1646  */
    break;

  case 234:
#line 2417 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_HOST;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4809 "gram.c" /* yacc.c:1646  */
    break;

  case 235:
#line 2422 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_AS;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4818 "gram.c" /* yacc.c:1646  */
    break;

  case 236:
#line 2427 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.pollarg).kw = T_FROM;
                     (yyval.pollarg).expr = (yyvsp[0].node);
             }
#line 4827 "gram.c" /* yacc.c:1646  */
    break;

  case 237:
#line 2435 "gram.y" /* yacc.c:1646  */
    {
                     (yyval.case_list).head = (yyval.case_list).tail = (yyvsp[0].case_stmt);
             }
#line 4835 "gram.c" /* yacc.c:1646  */
    break;

  case 238:
#line 2439 "gram.y" /* yacc.c:1646  */
    {
                     (yyvsp[-1].case_list).tail->next = (yyvsp[0].case_stmt);
                     (yyvsp[-1].case_list).tail = (yyvsp[0].case_stmt);
                     (yyval.case_list) = (yyvsp[-1].case_list);
             }
#line 4845 "gram.c" /* yacc.c:1646  */
    break;

  case 239:
#line 2447 "gram.y" /* yacc.c:1646  */
    {
		     struct valist *p;

		     for (p = (yyvsp[-2].valist_list).head; p; p = p->next) {
			     if (p->value.type == dtype_string) {
                                     parse_error_locus(&(yylsp[-2]),
                                                       _("invalid data type, "
                                                         "expected number"));
				     /* Try to continue */
				     p->value.type = dtype_number;
				     p->value.v.number = 0;
			     }
		     }
		     
		     (yyval.case_stmt) = mu_alloc(sizeof *(yyval.case_stmt));
		     (yyval.case_stmt)->next = NULL;
		     mu_locus_range_init (&(yyval.case_stmt)->locus);
                     mu_locus_range_copy (&(yyval.case_stmt)->locus, &(yylsp[-3]));
		     (yyval.case_stmt)->valist = (yyvsp[-2].valist_list).head;
		     (yyval.case_stmt)->node = (yyvsp[0].stmtlist).head;
	     }
#line 4871 "gram.c" /* yacc.c:1646  */
    break;


#line 4875 "gram.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 2470 "gram.y" /* yacc.c:1906  */


int
yyerror(char const *s)
{
        parse_error("%s", s);
        return 0;
}

struct stream_state
{
	int mode;
	struct mu_locus_range loc;
	int sevmask;
};

void
stream_state_save(mu_stream_t str, struct stream_state *st)
{
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_GET_MODE, &st->mode);
	mu_locus_range_init(&st->loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_GET_LOCUS_RANGE, &st->loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_GET_SEVERITY_MASK,
			&st->sevmask);
}

void
stream_state_restore(mu_stream_t str, struct stream_state *st)
{
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_MODE, &st->mode);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_LOCUS_RANGE, &st->loc);
	mu_stream_ioctl(mu_strerr, MU_IOCTL_LOGSTREAM,
			MU_IOCTL_LOGSTREAM_SET_SEVERITY_MASK,
			&st->sevmask);
	mu_locus_range_deinit(&st->loc);
}

static void define_action_ids(void);

int
parse_program(char *name, int ydebug)
{
        int rc;
	struct stream_state st;
	int mode;

	stream_state_save(mu_strerr, &st);
	define_action_ids();

	mode = st.mode | MU_LOGMODE_LOCUS | MU_LOGMODE_SEVERITY;
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_SET_MODE, &mode);
	mode = MU_DEBUG_LEVEL_MASK (MU_DIAG_ERROR);
	mu_stream_ioctl (mu_strerr, MU_IOCTL_LOGSTREAM,
			 MU_IOCTL_LOGSTREAM_SET_SEVERITY_MASK,
			 &mode);
	
        yydebug = ydebug;
        if (lex_new_source(name, 0))
                return -1;
	/* Top module was initialized with NULL filename in init_symbols. */
	top_module->file = mf_strdup(name);
        outer_context = inner_context = context_none;
        catch_nesting = 0;
        rc = yyparse() + error_count;

	stream_state_restore(mu_strerr, &st);
	
        return rc;
}

static void
alloc_locus(struct mu_locus_range const *locus)
{
	struct literal *lit;
	if (locus->beg.mu_file) {
		lit = string_alloc(locus->beg.mu_file,
				   strlen(locus->beg.mu_file));
		lit->flags |= SYM_REFERENCED;
	}
	if (!mu_locus_point_same_file(&locus->beg, &locus->end)) {
		lit = string_alloc(locus->end.mu_file,
				   strlen(locus->end.mu_file));
		lit->flags |= SYM_REFERENCED;
	}
}

NODE *
alloc_node(enum node_type type, struct mu_locus_range const *locus)
{
        NODE *node = malloc(sizeof(*node));
        if (!node) {
                yyerror("Not enough memory");
                abort();
        }
        node->type = type;
        mu_locus_range_init (&node->locus);
        mu_locus_range_copy (&node->locus, locus);
	alloc_locus(locus);
        node->next = NULL;
	node->value_ignored = 0;
        return node;
}

void
free_node(NODE *node)
{
	mu_locus_range_deinit(&node->locus);
        free(node);
}

void
copy_node(NODE *dest, NODE *src)
{
        dest->type = src->type;
        mu_locus_range_copy (&dest->locus, &src->locus);
        dest->v = src->v;
}

void
free_subtree(NODE *node)
{
        /*FIXME*/
}

void
free_parser_data()
{
	/*FIXME*/
}


/* Print parse tree */

static void print_node_list(NODE *node, int indent);
static void print_node_list_reverse(NODE *node, int level);
static void print_node(NODE *node, int indent);
void print_stat(sfsistat stat);
static int dbg_setreply(void *data, char *code, char *xcode, char *message);
static void dbg_msgmod(void *data, struct msgmod_closure *clos);

static void
print_level(int level)
{
        level *= 2;
        printf("%*.*s", level, level, "");
}

static void
print_bin_op(enum bin_opcode opcode)
{
        char *p;
        switch (opcode) {
        case bin_and:
                p = "AND";
                break;
                
        case bin_or:
                p = "OR";
                break;
                
        case bin_eq:
                p = "EQ";
                break;
                
        case bin_ne:
                p = "NE";
                break;
                
        case bin_lt:
                p = "LT";
                break;
                
        case bin_le:
                p = "LE";
                break;
                
        case bin_gt:
                p = "GT";
                break;
                
        case bin_ge:
                p = "GE";
                break;
                
        case bin_match:
                p = "MATCH";
                break;
                
        case bin_fnmatch:
                p = "FNMATCH";
                break;

        case bin_add:
                p = "ADD";
                break;
                
        case bin_sub:
                p = "SUB";
                break;
                
        case bin_mul:
                p = "MUL";
                break;
                
        case bin_div:
                p = "DIV";
                break;

        case bin_mod:
                p = "MOD";
                break;

        case bin_logand:
                p = "LOGAND";
                break;
                
        case bin_logor:
                p = "LOGOR";
                break;
                
        case bin_logxor:
                p = "LOGXOR";
                break;

	case bin_shl:
		p = "SHL";
		break;

	case bin_shr:
		p = "SHR";
		break;
		
        default:
                p = "UNKNOWN_OP";
        }
        printf("%s", p);
}

static void
print_quoted_string(const char *str)
{
        for (; *str; str++) {
                if (mu_isprint(*str))
                        putchar(*str);
                else {
                        putchar('\\');
                        switch (*str) {
                        case '\a':
                                putchar('a');
                                break;
                        case '\b':
                                putchar('b');
                                break;
                        case '\f':
                                putchar('f');
                                break;
                        case '\n':
                                putchar('n');
                                break;
                        case '\r':
                                putchar('r');
                                break;
                        case '\t':
                                putchar('t');
                                break;
                        default:
                                printf("%03o", *str);
                        }
                }
        }
}

struct node_drv {
        void (*print) (NODE *, int);
        void (*mark) (NODE *);
        void (*code) (NODE *, struct mu_locus_range const **);
        void (*optimize) (NODE *);
};

static void traverse_tree(NODE *node);
static void code_node(NODE *node);
static void code_node(NODE *node);
static void optimize_node(NODE *node);
static void optimize(NODE *node);
static void record_switch(struct switch_stmt *sw);

#include "drivers.c"
#include "node-tab.c"

struct node_drv *
find_node_drv(enum node_type type)
{
        if (type >= NELEMS(nodetab)) {
                parse_error(_("INTERNAL ERROR at %s:%d, "
                              "unexpected node type %d"),
                            __FILE__, __LINE__,
                            type);
                abort();
        }
        return nodetab + type;
}

static void
print_node(NODE *node, int level)
{
        struct node_drv *nd = find_node_drv(node->type);
        if (nd->print)
                nd->print(node, level);
}       

static void
print_node_list(NODE *node, int level)
{
        for (; node; node = node->next)
                print_node(node, level);
}

static void
print_node_list_reverse(NODE *node, int level)
{
        if (node) {
                print_node_list_reverse(node->next, level);
                print_node(node, level);
        }
}

int
function_enumerator(void *sym, void *data)
{
        int i;
        struct function *fsym = sym;
        struct function *f =
                (struct function *)symbol_resolve_alias(&fsym->sym);
        struct module *mod = data;

        if (f->sym.module != mod)
                return 0;
        printf("function %s (", f->sym.name);
        for (i = 0; i < f->parmcount; i++) {
                printf("%s", type_to_string(f->parmtype[i]));
                if (i < f->parmcount-1)
                        putchar(',');
        }
        putchar(')');
        if (f->rettype != dtype_unspecified)
                printf(" returns %s", type_to_string(f->rettype));
        printf(":\n");
        print_node_list(f->node, 0);
        printf("END function %s\n", f->sym.name);
        return 0;
}
                
void
print_syntax_tree()
{
        struct module **modv;
        size_t i, modc;

        enum smtp_state tag;

        printf("State handlers:\n");
        printf("---------------\n");
        for (tag = smtp_state_startup; tag < smtp_state_count; tag++) {
                if (root_node[tag]) {
                        printf("%s:\n", xstate_to_string(tag));
                        print_node_list(root_node[tag]->v.progdecl.tree.head, 0);
                        putchar('\n');
                }
        }
        printf("User functions:\n");
        printf("---------------\n");
        
        collect_modules(&modv, &modc);
        for (i = 0; i < modc; i++) {
                size_t n;

                n = printf("Module %s (%s)\n", modv[i]->name,
                           modv[i]->file);
                while (n--)
                        putchar('-');
                putchar('\n');
                symtab_enumerate(MODULE_SYMTAB(modv[i], namespace_function),
                                 function_enumerator, modv[i]);
                putchar('\n');
        }
        free(modv);
}


/* Cross-reference support */

struct collect_data {
	mu_opool_t pool;
        size_t count;
};

static int
variable_enumerator(void *item, void *data)
{
        struct variable *var = item;
        struct collect_data *p = data;
        if (var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED)) {
                mu_opool_append(p->pool, &var, sizeof var);
                p->count++;
        }
        return 0;
}

int
print_locus(void *item, void *data)
{
        struct mu_locus_range *loc = item;
        struct mu_locus_range **prev = data;
        int c;

        if (!*prev) {
                *prev = loc;
                printf("%s", loc->beg.mu_file);
                c = ':';
        } else if (mu_locus_point_same_file (&(*prev)->beg, &loc->beg)) {
                *prev = loc;
                printf(", %s", loc->beg.mu_file);
                c = ':';
        } else
                c = ',';
        printf("%c%u", c, loc->beg.mu_line);
        return 0;
}

void
print_xref_var(struct variable *var)
{
        struct mu_locus_range *prev = NULL;
        var = (struct variable *)symbol_resolve_alias(&var->sym);
        printf("%-32.32s %6s %lu ",
               var->sym.name, type_to_string(var->type),
               (unsigned long)var->off);
        mu_list_foreach(var->xref, print_locus, &prev);
        printf("\n");
}

int
vp_comp(const void *a, const void *b)
{
        struct variable * const *va = a, * const *vb = b;
        return strcmp((*va)->sym.name, (*vb)->sym.name);
}

void
print_xref()
{
        struct collect_data cd;
        struct variable **vp;
        size_t i;
        
        mu_opool_create(&cd.pool, MU_OPOOL_ENOMEMABRT);
        cd.count = 0;
        symtab_enumerate(TOP_MODULE_SYMTAB(namespace_variable),
                         variable_enumerator, &cd);
        printf("Cross-references:\n");
        printf("-----------------\n");
        
        vp = mu_opool_finish(cd.pool, NULL);
        qsort(vp, cd.count, sizeof *vp, vp_comp);
        for (i = 0; i < cd.count; i++, vp++) 
                print_xref_var(*vp);
        mu_opool_destroy(&cd.pool);
}


static mu_list_t smtp_macro_list[gacopyz_stage_max];

static enum gacopyz_stage 
smtp_to_gacopyz_stage(enum smtp_state tag)
{
        switch (tag) {
        case smtp_state_connect:
                return gacopyz_stage_conn;
        case smtp_state_helo:
                return gacopyz_stage_helo;
        case smtp_state_envfrom:
                return gacopyz_stage_mail;
        case smtp_state_envrcpt:
                return gacopyz_stage_rcpt;
        case smtp_state_data:
        case smtp_state_header:
        case smtp_state_body:
                return gacopyz_stage_data;
        case smtp_state_eoh:
                return gacopyz_stage_eoh;
        case smtp_state_eom:
                return gacopyz_stage_eom;
        default:
                break;
        }
        return gacopyz_stage_none;
}

static int
compare_macro_names (const void *item, const void *data)
{
	const char *elt;
	size_t elen;
	const char *arg;
	size_t alen;

	elt = item;
	elen = strlen (elt);
	if (elt[0] == '{') {
		elt++;
		elen -= 2;
	}

	arg = data;
	alen = strlen (arg);
	if (arg[0] == '{') {
		arg++;
		alen -= 2;
	}

	if (alen != elen)
		return 1;
	return memcmp (elt, arg, alen);
}

void
register_macro(enum smtp_state state, const char *macro)
{
        enum gacopyz_stage ind = smtp_to_gacopyz_stage(state);
	
        if (ind == gacopyz_stage_none)
                return;
        if (!smtp_macro_list[ind]) {
                mu_list_create(&smtp_macro_list[ind]);
                mu_list_set_comparator(smtp_macro_list[ind],
				       compare_macro_names);
        }
        /* FIXME: MU: 2nd arg should be const? */
        if (mu_list_locate(smtp_macro_list[ind], (void*) macro, NULL)) {
		char *cmacro;
		if (macro[1] == 0 || macro[0] == '{')
			cmacro = mu_strdup (macro);
		else {
			size_t mlen = strlen (macro);
			cmacro = mu_alloc (mlen + 3);
			cmacro[0] = '{';
			memcpy (cmacro + 1, macro, mlen);
			cmacro[mlen + 1] = '}';
			cmacro[mlen + 2] = 0;
		}
                mu_list_append(smtp_macro_list[ind], cmacro);
	}
}

static int
print_macro(void *item, void *data)
{
        int *p = data;
        if (*p) {
                printf(" ");
                *p = 0;
        } else
                printf(", ");
        printf("%s", (char*) item);
        return 0;
}

void
print_used_macros()
{
        enum gacopyz_stage i;
        
        for (i = 0; i < gacopyz_stage_max; i++) {
                if (smtp_macro_list[i]) {
                        int n = 1;
                        printf("%s", gacopyz_stage_name[i]);
                        mu_list_foreach(smtp_macro_list[i], print_macro, &n);
                        printf("\n");
                }
        }
}

struct macro_acc {
        size_t size;
        char *buf;
};

static int
add_macro_size(void *item, void *data)
{
        char *macro = (char*) item;
        struct macro_acc *mp = data;
        mp->size += strlen(macro) + 1;
        return 0;
}

static int
concat_macro(void *item, void *data)
{
        char *macro = (char*) item;
        struct macro_acc *mp = data;
        size_t len = strlen(macro);
        char *pbuf = mp->buf + mp->size;
        memcpy(pbuf, macro, len);
        pbuf[len++] = ' ';
        mp->size += len;
        return 0;
}

char *
get_stage_macro_string(enum gacopyz_stage i)
{
        struct macro_acc acc;
        size_t size;
        mu_list_t list = smtp_macro_list[i];
        if (!list)
                return NULL;
                
        acc.size = 0;
        mu_list_foreach(list, add_macro_size, &acc);
        if (!acc.size)
                return NULL;
        size = acc.size;
                
        acc.size = 0;
        acc.buf = mu_alloc (size);
        mu_list_foreach(list, concat_macro, &acc);
        acc.buf[size-1] = 0;
        return acc.buf;
}


/* Code generation */

static void
code_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                static struct mu_locus_range const *old_locus;
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->code)
                        nd->code(node, &old_locus);
        }
}

static void
traverse_tree(NODE *node)
{
        for (; node; node = node->next)
                code_node(node);
}

static void
optimize_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->optimize)
                        nd->optimize(node);
        }
}

static void
optimize(NODE *node)
{
        for (; node; node = node->next)
                optimize_node(node);
}

static int
optimize_tree(NODE *node)
{
        if (optimization_level)
                optimize(node);
        return error_count;
}


static struct switch_stmt *switch_root;

static void
record_switch(struct switch_stmt *sw)
{
        sw->next = switch_root;
        switch_root = sw;
}


static struct exmask *exmask_root;

struct exmask *
exmask_create()
{
	struct exmask *p = mu_alloc(sizeof(*p));
	p->next = exmask_root;
	p->off = 0;
	p->all = 0;
	bitmask_init(&p->bm);
	exmask_root = p;
	return p;
}



static void
mark_node(NODE *node)
{
        if (!node) 
                error_count++;
        else {
                struct node_drv *nd = find_node_drv(node->type);
                if (nd->mark)
                        nd->mark(node);
        }
}

static void
mark(NODE *node)
{
        for (; node; node = node->next)
                mark_node(node);
}
        

static int
codegen(prog_counter_t *pc, NODE *node, struct exmask *exmask, size_t nautos)
{
	int save_mask;
	
	if (error_count)
		return 1;
	
	*pc = code_get_counter();
	jump_pc = 0;
	if (nautos) {
		code_op(opcode_stkalloc);
		code_immediate(nautos, uint);
	}
	save_mask = exmask && bitmask_nset(&exmask->bm);
	if (save_mask) {
		code_op(opcode_saveex);
		code_exmask(exmask);
	}
	traverse_tree(node);

	jump_fixup(jump_pc, code_get_counter());
	if (save_mask) 
		code_op(opcode_restex);
	
	code_op(opcode_return);
		
	return 0;
}

static void
action_hook_fixup(void)
{
	prog_counter_t hook_pc = entry_point[smtp_state_action];

	if (hook_pc != 0) {
		while (action_hook_pc) {
			prog_counter_t next = (prog_counter_t)mf_cell_c_value(code_peek(action_hook_pc), size);
			code_put(action_hook_pc, hook_pc, size);
			action_hook_pc = next;
		}
	}
}

static void
compile_tree(NODE *node)
{
        struct node_drv *nd;
        struct mu_locus_range const *plocus;

	/* slot 0 - emergency return point */
	code_op(opcode_nil);
	/* slot 1 - return point for main in --run mode */
	code_op(opcode_nil);
	/* traverse the tree producing code for each node */
        for (; node; node = node->next) {
                switch (node->type) {
                case node_type_progdecl:
                case node_type_funcdecl:
                        nd = find_node_drv(node->type);
                        if (!nd->code)
                                abort();
                        nd->code(node, &plocus);
                        break;
                        
                default:
                        parse_error_locus(&node->locus,
                                          _("INTERNAL ERROR at %s:%d, "
                                            "unexpected node type %d"),
                                          __FILE__, __LINE__,
                                          node->type);
                        break;
                }
        }
	
	action_hook_fixup();
}


enum regex_mode { regex_enable, regex_disable, regex_set };

static mf_stack_t regex_stack;

void
regex_push()
{
        if (!regex_stack)
                regex_stack = mf_stack_create(sizeof regex_flags, 0);
        mf_stack_push(regex_stack, &regex_flags);
}

void
regex_pop()
{
        if (!regex_stack || mf_stack_pop(regex_stack, &regex_flags))
                parse_error(_("nothing to pop"));
}

static void
pragma_regex(int argc, char **argv, const char *text)
{
        enum regex_mode mode = regex_set;
        int i = 1;

        if (strcmp(argv[i], "push") == 0) {
                regex_push();
                i++;
        } else if (strcmp(argv[i], "pop") == 0) {
                regex_pop();
                i++;
        }

        for (; i < argc; i++) {
                int bit;
                char *p = argv[i];
                switch (p[0]) {
                case '+':
                        mode = regex_enable;
                        p++;
                        break;

                case '-':
                        mode = regex_disable;
                        p++;
                        break;

                case '=':
                        mode = regex_set;
                        p++;
                        break;
                }

                if (strcmp (p, REG_EXTENDED_NAME) == 0)
                        bit = REG_EXTENDED;
                else if (strcmp (p, REG_ICASE_NAME) == 0)
                        bit = REG_ICASE;
                else if (strcmp (p, REG_NEWLINE_NAME) == 0)
                        bit = REG_NEWLINE;
                else {
                        parse_error(_("unknown regexp flag: %s"), p);
                        return;
                }

                switch (mode) {
                case regex_disable:
                        regex_flags &= ~bit;
                        break;
                case regex_enable:
                        regex_flags |= bit;
                        break;
                case regex_set:
                        regex_flags = bit;
                        break;
                }
        }
}       

static int
strtosize(const char *text, size_t *psize)
{
        unsigned long size;
	size_t factor = 1;
        char *p;

        size = strtoul(text, &p, 0);
	if (size == ULONG_MAX && errno == ERANGE) {
                parse_error(_("invalid size: numeric overflow occurred"));
                return 2;
	}
	
        switch (*p) {
        case 't':
        case 'T':
                factor = 1024;
        case 'g':
        case 'G':
                factor *= 1024;
        case 'm':
        case 'M':
                factor *= 1024;
        case 'k':
        case 'K':
                factor *= 1024;
                p++;
                if (*p && (*p == 'b' || *p == 'B'))
                        p++;
                break;

        case 0:
		factor = 1;
                break;
                
        default:
                parse_error(_("invalid size suffix (near %s)"), p);
                return 1;
        }

	if (((size_t)-1) / factor < size) {
                parse_error(_("invalid size: numeric overflow occurred"));
                return 2;
        }
        
        *psize = size * factor;

        return 0;
}

static void
pragma_stacksize(int argc, char **argv, const char *text)
{
        size_t size, incr = stack_expand_incr, max_size = stack_max_size;
        enum stack_expand_policy policy = stack_expand_policy;

        switch (argc) {
        case 4:
                if (strtosize(argv[3], &max_size))
                        return;
        case 3:
                if (strcmp(argv[2], "twice") == 0) 
                        policy = stack_expand_twice;
                else {
                        policy = stack_expand_add;
                                
                        if (strtosize(argv[2], &incr))
                                return;
                }
        case 2:
                if (strtosize(argv[1], &size))
			return;
        }

        stack_size = size;
        stack_expand_incr = incr;
        stack_expand_policy = policy;
        stack_max_size = max_size;
}

void
pragma_setup()
{
	install_pragma("regex", 2, 0, pragma_regex);
	install_pragma("stacksize", 2, 4, pragma_stacksize);
}



/* Test run */
struct sfsistat_tab {
        char *name;
	char *constname;
        sfsistat stat;
} sfsistat_tab[] = {
        { "accept",   "ACCEPT_ACTION",   SMFIS_ACCEPT },
        { "continue", "CONTINUE_ACTION", SMFIS_CONTINUE },
        { "discard",  "DISCARD_ACTION",  SMFIS_DISCARD },
        { "reject",   "REJECT_ACTION",   SMFIS_REJECT },
        { "tempfail", "TEMPFAIL_ACTION", SMFIS_TEMPFAIL },
        { NULL }
};

const char *
sfsistat_str(sfsistat stat)
{
        struct sfsistat_tab *p;
        for (p = sfsistat_tab; p->name; p++)
                if (p->stat == stat) 
                        return p->name;
        return NULL;
}

int
sfsistat_code(char const *stat)
{
        struct sfsistat_tab *p;
        for (p = sfsistat_tab; p->name; p++)
		if (strcmp(p->name, stat) == 0)
			return p->stat;
	return -1;
}
	
void
print_stat(sfsistat stat)
{
        struct sfsistat_tab *p;
        for (p = sfsistat_tab; p->name; p++)
                if (p->stat == stat) {
                        printf("%s", p->name);
                        return;
                }
        printf("%d", stat);
}

static void
define_action_ids(void)
{
        struct sfsistat_tab *p;
	struct mu_locus_range r = {
		{ __FILE__, __LINE__, 0 },
		{ __FILE__, __LINE__, 0 }
	};
	
        for (p = sfsistat_tab; p->name; p++) {
		struct value val = { .type = dtype_number };
		val.v.number = p->stat;
		define_constant(p->constname, &val, 0, &r);
	}
}

const char *
msgmod_opcode_str(enum msgmod_opcode opcode)
{
        switch (opcode) {
        case header_add:
                return "ADD HEADER";
                
        case header_replace:
                return "REPLACE HEADER";

        case header_delete:
                return "DELETE HEADER";

        case header_insert:
                return "INSERT HEADER";
                
        case rcpt_add:
                return "ADD RECIPIENT";

        case rcpt_delete:
                return "DELETE RECIPIENT";

        case quarantine:
                return "QUARANTINE";

        case body_repl:
                return "REPLACE BODY";

	case body_repl_fd:
		return "REPLACE BODY FROM FILE";
		
	case set_from:
		return "SET FROM";
		
        }
        return "UNKNOWN HEADER COMMAND";
}

static int
dbg_setreply(void *data, char *code, char *xcode, char *message)
{
        if (code) {
                printf("SET REPLY %s", code);
                if (xcode)
                        printf(" %s", xcode);
                if (message && message[0])
                        printf(" %s", message);
                printf("\n");
        }
        return 0;
}

static void
dbg_msgmod(void *data, struct msgmod_closure *clos)
{
	if (!clos)
		printf("clearing msgmod list\n");
	else
		printf("%s %s: %s %u\n", msgmod_opcode_str(clos->opcode),
		       SP(clos->name), SP(clos->value), clos->idx);
}

static const char *
dbg_dict_getsym (void *data, const char *str)
{
        return dict_getsym ((mu_assoc_t)data, str);
}

void
mailfromd_test(int argc, char **argv)
{
	int i;
	mu_assoc_t dict = NULL;
	eval_environ_t env;
	char *p, *end;
	long n;
	sfsistat status;
	char *args[9] = {0,0,0,0,0,0,0,0,0};
		
	dict_init(&dict);
	env = create_environment(NULL,
				 dbg_dict_getsym, dbg_setreply, dbg_msgmod,
				 dict);
	env_init(env);
	xeval_begin(env);
	
	for (i = 0; i < argc; i++) {
		if (p = strchr(argv[i], '=')) {
			char *ident = argv[i];
			*p++ = 0;
			if (mu_isdigit(*ident) && *ident != 0
				 && ident[1] == 0) 
				args[*ident - '0' - 1] = p;
			else {
				dict_install(dict, argv[i], p);
				if (strcmp(argv[i], "client_addr") == 0) {
					/* Deduce connection family. */
					if (dns_str_is_ipv6(p))
						env_save_socket_family(env, AF_INET6);
				}
			}
		}
	}

	env_init(env);
	for (i = state_parms[test_state].cnt; i--; ) {
		switch (state_parms[test_state].types[i]) {
		case dtype_string:
			env_push_string(env, args[i] ? args[i] : "");
			break;
		case dtype_number:
			if (args[i]) {
				n = strtol(args[i], &end, 0);
				if (*end) 
					mu_error(_("$%d is not a number"),
						 i+1);
			} else
				n = 0;
			env_push_number(env, n);
			break;
		default:
			abort();
		}
	}

	test_message_data_init(env);
	
	xeval(env, test_state);
	env_final_gc(env);

	status = environment_get_status(env);

	env_init(env);
	xeval(env, smtp_state_end);

	printf("State %s: ", xstate_to_string(test_state));
	print_stat(status);
	printf("\n");
	destroy_environment(env);
}

void
mailfromd_run(prog_counter_t entry_point, int argc, char **argv,
	      int macc, char **macv)
{
	int rc, i;
	mu_assoc_t dict = NULL;
	eval_environ_t env;

	dict_init(&dict);
	env = create_environment(NULL,
				 dbg_dict_getsym, dbg_setreply, dbg_msgmod,
				 dict);

	env_init(env);
	test_message_data_init(env);

	xeval_begin(env);

	for (i = 0; i < macc; i++) {
		char *p;
		if (p = strchr(macv[i], '=')) {
			*p++ = 0;
			dict_install(dict, macv[i], p);
			if (strcmp(macv[i], "client_addr") == 0) {
				/* Deduce connection family. */
				if (dns_str_is_ipv6(p))
					env_save_socket_family(env, AF_INET6);
			}
		}
	}
	
	env_init(env);
	env_push_number(env, 0);

	for (i = argc - 1; i >= 0; i--)
		env_push_string(env, argv[i]);
	env_push_number(env, argc);
	
	env_make_frame(env);
	set_milter_state(env, smtp_state_none);
	rc = eval_environment(env, entry_point);
	env_final_gc(env);
	rc = mf_c_val(env_get_reg(env), long);

	env_init(env);
	xeval(env, smtp_state_end);

	destroy_environment(env);
	exit(rc);
}

static char const *statenames[] ={
        [smtp_state_none]     = "none",
	[smtp_state_startup]  = "startup",
	[smtp_state_shutdown] = "shutdown",
        [smtp_state_begin]    = "begin",
	[smtp_state_end]      = "end",
        [smtp_state_connect]  = "connect",
        [smtp_state_helo]     = "helo",
        [smtp_state_envfrom]  = "envfrom",
        [smtp_state_envrcpt]  = "envrcpt",
        [smtp_state_data]     = "data",
        [smtp_state_header]   = "header",
        [smtp_state_eoh]      = "eoh",
        [smtp_state_body]     = "body",
        [smtp_state_eom]      = "eom",
	[smtp_state_action]   = "action",
};

enum smtp_state
string_to_state(const char *name)
{
	int i;

        for (i = 0; i < sizeof(statenames)/sizeof(statenames[0]); i++)
                if (strcasecmp(statenames[i], name) == 0)
                        return i;

        return smtp_state_none;
}

const char *
state_to_string(enum smtp_state state)
{
        if (state < sizeof(statenames)/sizeof(statenames[0]))
                return statenames[state];
        return NULL;
}

const char *
xstate_to_string(enum smtp_state state)
{
	const char *ret = state_to_string(state);
	if (!ret)
		abort();
	return ret;
}

static NODE *
_reverse(NODE *list, NODE **root)
{
        NODE *next;

        if (list->next == NULL) {
                *root = list;
                return list;
        }
        next = _reverse(list->next, root);
        next->next = list;
        list->next = NULL;
        return list;
}               

NODE *
reverse(NODE *in)
{
        NODE *root;
        if (!in)
                return in;
        _reverse(in, &root);
        return root;
}

size_t
nodelistlength(NODE *p)
{
	size_t len = 0;
	for (; p; p = p->next)
		len++;
	return len;
}

static NODE *
create_asgn_node(struct variable *var, NODE *expr,
		 struct mu_locus_range const *loc)
{
        NODE *node;
        data_type_t t = node_type(expr);

        if (t == dtype_unspecified) {
                parse_error(_("unspecified value not ignored as it should be"));
                return NULL;
        }
        node = alloc_node(node_type_asgn, loc);
        node->v.asgn.var = var;
        node->v.asgn.nframes = catch_nesting;
        node->v.asgn.node = cast_to(var->type, expr);
	var->initialized = 1;
        return node;
}


NODE *
function_call(struct function *function, size_t count, NODE *subtree)
{
        NODE *np = NULL;
        if (count < function->parmcount - function->optcount) {
                parse_error(_("too few arguments in call to `%s'"),
                            function->sym.name);
        } else if (count > function->parmcount && !function->varargs) {
                parse_error(_("too many arguments in call to `%s'"),
                            function->sym.name);
        } else {
                np = alloc_node(node_type_call, &yylloc);
                np->v.call.func = function;
                np->v.call.args = reverse(cast_arg_list(subtree,
                                                        function->parmcount,
                                                        function->parmtype,
                                                        function->vartype));
        }
        return np;
}       

data_type_t
node_type(NODE *node)
{
	switch (node->type) {
	case node_type_string:
	case node_type_symbol:
	case node_type_sed:
	case node_type_concat:
	case node_type_backref:
		return dtype_string;

	case node_type_argx:
		return node->v.argx.dtype;

	case node_type_number:
	case node_type_bin:
	case node_type_un:
	case node_type_sedcomp:
	case node_type_offset:
	case node_type_vaptr:
		return dtype_number;
		
	case node_type_if:
		return dtype_unspecified;
		
	case node_type_builtin:
		return node->v.builtin.builtin->rettype;
		
	case node_type_variable:
		return node->v.var_ref.variable->type;
		
	case node_type_arg:
		return node->v.arg.data_type;

	case node_type_call:
		return node->v.call.func->rettype;
		
	case node_type_return:
		if (node->v.node)
			return node_type(node->v.node);
		break;

	case node_type_cast:
		return node->v.cast.data_type;

	case node_type_noop:
		/* Nodes of this type are generated as placeholders in case
		   of various compilation errors and thus can represent any
		   data type. */
		return dtype_any;

	case node_type_result:
	case node_type_header:
	case node_type_asgn:
	case node_type_regex:
	case node_type_regcomp:
	case node_type_catch:
	case node_type_try:
	case node_type_throw:
	case node_type_echo:
	case node_type_switch:
	case node_type_funcdecl:
	case node_type_progdecl:
	case node_type_next:
	case node_type_break:
	case node_type_loop:
	case node_type_argv:
	case max_node_type:
		break;
	}
	return dtype_unspecified;
}

NODE *
cast_to(data_type_t type, NODE *node)
{
        NODE *np;
        data_type_t ntype = node_type(node);
        
        switch (ntype) {
        case dtype_string:
        case dtype_number:
        case dtype_pointer:
	case dtype_any:
                if (type == ntype)
                        return node;
                break;
                
        case dtype_unspecified:
                parse_error(_("cannot convert %s to %s"), type_to_string(ntype),
                            type_to_string(type));
                return NULL;

        default:
                abort();
        }
        np = alloc_node(node_type_cast, &yylloc);
        np->v.cast.data_type = type;
        np->v.cast.node = node;
        node->next = NULL;
        return np;
}

NODE *
cast_arg_list(NODE *args, size_t parmc, data_type_t *parmtype, data_type_t vartype)
{
        NODE *head = NULL, *tail = NULL;
        
        while (args) {
                NODE *next = args->next;
                NODE *p;
                data_type_t type;
                
		if (args->type == node_type_argv) {
			if (parmc) {
				parse_error_locus(&args->locus,
						  _("$@ cannnot be used to supply actual arguments for mandatory or optional parameters"));
				return NULL;
			} else {
				p = args;
			}
		} else {
			if (parmc) {
				type = *parmtype++;
				parmc--;
			} else if (vartype == dtype_any)
				type = node_type(args);
			else
				type = vartype;

			p = cast_to(type, args);
		}
		
                if (head)
                        tail->next = p;
                else
                        head = p;
                tail = p;
                args = next;
        }
        return head;
}

void
add_xref(struct variable *var, struct mu_locus_range const *locus)
{
        if (script_dump_xref) {
		/* FIXME: either change type to mu_locus_point, or
		   change print_locus above */
                struct mu_locus_range *elt = mu_zalloc(sizeof *elt);
                if (!var->xref)
                        mu_list_create(&var->xref);
                mu_locus_range_copy(elt, locus);
                mu_list_append(var->xref, elt);
        }
}

struct variable *
vardecl(const char *name, data_type_t type, storage_class_t sc,
        struct mu_locus_range const *loc)
{
        struct variable *var;
        const struct constant *cptr;
	
	if (!loc)
		loc = &yylloc;
        if (type == dtype_unspecified) {
                parse_error(_("cannot define variable of unspecified type"));
                return NULL;
        }
        var = variable_install(name);
        if (var->type == dtype_unspecified) {
                /* the variable has just been added: go straight to
                   initializing it */;
        } else if (sc != var->storage_class) {
                struct variable *vp;
                
                switch (sc) {
                case storage_extern:
                        parse_error(_("INTERNAL ERROR at %s:%d, declaring %s %s"),
                                    __FILE__, __LINE__,
                                    storage_class_str(sc), name);
                        abort();

                case storage_auto:
                        if (var->storage_class == storage_param) {
                                parse_warning_locus(loc,
						    _("automatic variable `%s' "
						      "is shadowing a parameter"),
						    var->sym.name);
                        } else
                                parse_warning_locus(loc,
						    _("automatic variable `%s' "
						      "is shadowing a global"), 
                                              var->sym.name);
                        unregister_auto(var);
                        break;

                case storage_param:
                        parse_warning_locus(loc,
					    _("parameter `%s' is shadowing a "
					      "global"),
					    name);
                }

                /* Do the shadowing */
                vp = variable_replace(var->sym.name, NULL);
                vp->shadowed = var;
                var = vp;
        } else {
                switch (sc) {
                case storage_extern:
                        if (var->type != type) {
                                parse_error_locus(loc,
						  _("redeclaring `%s' as different "
						    "data type"),
						  name);
                                parse_error_locus(&var->sym.locus,
                                            _("this is the location of the "
                                              "previous definition"));
                                return NULL;
                        }
                        break;

                case storage_auto:
                        if (var->type != type) {
                                parse_error_locus(loc,
						  _("redeclaring `%s' as different "
						    "data type"),
						  name);
                                parse_error_locus(&var->sym.locus,
						  _("this is the location of the "
						    "previous definition"));
                                return NULL;
                        } else {
                                parse_error_locus(loc,
						  _("duplicate variable: %s"),
						  name);
                                return NULL;
                        }
                        break;

                case storage_param:
                        parse_error_locus(loc, _("duplicate parameter: %s"),
					  name);
                        return NULL;
                }
        }

        /* FIXME: This is necessary because constants can be
           referred to the same way as variables. */
        if (cptr = constant_lookup(name)) {
                parse_warning_locus(loc,
				    _("variable name `%s' clashes with a constant name"),
                              name);
                parse_warning_locus(&cptr->sym.locus,
                                    _("this is the location of the "
                                      "previous definition"));
        }
        
        var->type = type;
        var->storage_class = sc;
        switch (sc) {
        case storage_extern:
                add_xref(var, loc);
                break;
        case storage_auto:
        case storage_param:
                register_auto(var);
        }
        mu_locus_range_copy(&var->sym.locus, loc);
        return var;
}

static int
cast_value(data_type_t type, struct value *value)
{
        if (type != value->type) {
                char buf[NUMERIC_BUFSIZE_BOUND];
                char *p;
                
                switch (type) {
                default:
                        abort();
                        
                case dtype_string:
                        snprintf(buf, sizeof buf, "%ld", value->v.number);
                        value->v.literal = string_alloc(buf, strlen(buf));
                        break;

                case dtype_number:
                        value->v.number = strtol(value->v.literal->text,
                                                 &p, 10);
                        if (*p) {
                                parse_error(_("cannot convert `%s' to number"),
                                            value->v.literal->text);
                                return 1;
                        }
                        break;
                }
                value->type = type;
        }
        return 0;
}

static struct variable *
externdecl(const char *name, struct value *value,
	   struct mu_locus_range const *loc)
{
        struct variable *var = vardecl(name, value->type, storage_extern, loc);
        if (!var)
                return NULL;
        if (initialize_variable(var, value, loc))
                return NULL;
        return var;
}


struct deferred_decl {
        struct deferred_decl *next;
        struct literal *name;
        struct value value;
	struct mu_locus_range locus;
};

struct deferred_decl *deferred_decl;

void
defer_initialize_variable(const char *arg, const char *val,
			  struct mu_locus_range const *ploc)
{
        struct deferred_decl *p;
        struct literal *name = string_alloc(arg, strlen(arg));
        for (p = deferred_decl; p; p = p->next)
                if (p->name == name) {
                        parse_warning_locus(NULL, _("redefining variable %s"),
                                            name->text);
                        p->value.type = dtype_string;
                        p->value.v.literal = string_alloc(val, strlen(val));
			mu_locus_range_copy (&p->locus, ploc);
                        return;
                }
        p = mu_alloc(sizeof *p);
        p->name = name;
        p->value.type = dtype_string;
        p->value.v.literal = string_alloc(val, strlen(val));
	mu_locus_range_init (&p->locus);
	mu_locus_range_copy (&p->locus, ploc);
        p->next = deferred_decl;
        deferred_decl = p;
}

static void
apply_deferred_init()
{
        struct deferred_decl *p;
        for (p = deferred_decl; p; p = p->next) {
                struct variable *var = variable_lookup(p->name->text);
                if (!var) {
			mu_diag_at_locus_range(MU_DIAG_ERROR,
					       &p->locus,
					       _("warning: no such variable: %s"),
					       p->name->text);
                } else if (initialize_variable(var, &p->value, &p->locus))
                        parse_error_locus(&p->locus,
					  _("error initialising variable %s: incompatible types"),
					  p->name->text);
        }
}


struct declvar {
        struct declvar *next;
        struct mu_locus_range locus;
        struct variable *var;
        struct value val;
};

static struct declvar *declvar;

void
set_poll_arg(struct poll_data *poll, int kw, NODE *expr)
{
        switch (kw) {
        case T_FOR:
                poll->email = expr;
                break;
                
        case T_HOST:
                poll->client_addr = expr;
                break;
                
        case T_AS:
                poll->mailfrom = expr;
                break;
                
        case T_FROM:
                poll->ehlo = expr;
                break;
                
        default:
                abort();
        }
}
                
int
initialize_variable(struct variable *var, struct value *val,
                    struct mu_locus_range const *locus)
{
        struct declvar *dv;

        if (cast_value(var->type, val))
                return 1;
        for (dv = declvar; dv; dv = dv->next)
                if (dv->var == var) {
                        if (dv->locus.beg.mu_file) {
                                parse_warning_locus(locus,
                                     _("variable `%s' already initialized"),
                                                    var->sym.name);
                                parse_warning_locus(&dv->locus,
                                     _("this is the location of the "
                                       "previous initialization"));
                        }
			
                        if (locus)
				mu_locus_range_copy (&dv->locus, locus);
                        else
				mu_locus_range_deinit (&dv->locus);
                        dv->val = *val;
                        return 0;
                }

        dv = mu_alloc(sizeof *dv);
        dv->next = declvar;
        dv->var = var;
	mu_locus_range_init (&dv->locus);
	if (locus)
		mu_locus_range_copy (&dv->locus, locus);
        dv->val = *val;
        declvar = dv;
	var->sym.flags |= SYM_INITIALIZED;
        return 0;
}

void
ds_init_variable(const char *name, void *data)
{
        struct declvar *dv;     
        struct variable *var = variable_lookup(name);

        if (!var) {
                mu_error(_("INTERNAL ERROR at %s:%d: variable to be "
                           "initialized is not declared"),
                         __FILE__, __LINE__);
                abort();
        }

        for (dv = declvar; dv; dv = dv->next)
                if (dv->var == var)
                        return;

        dv = mu_alloc(sizeof *dv);
	mu_locus_range_init(&dv->locus);
        dv->var = var;
        dv->next = declvar;
        declvar = dv;

	switch (var->type) {
	case dtype_string:
		dv->val.v.literal = string_alloc(data, strlen(data));
		break;
		
	case dtype_number:
		dv->val.v.number = *(long*)data;
		break;

	default:
                mu_error(_("INTERNAL ERROR at %s:%d: variable to be "
                           "initialized has wrong type"),
                         __FILE__, __LINE__);
		abort ();
	}
	dv->val.type = var->type;
}	

static int
_ds_variable_count_fun(void *sym, void *data)
{
        struct variable *var = sym;

        if ((var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
	    && !(var->sym.flags & SYM_PASSTOGGLE)) {
		var->sym.flags |= SYM_PASSTOGGLE;
                variable_count++;
                if (var->type == dtype_string) 
                        dataseg_reloc_count++;
                if (var->sym.flags & SYM_PRECIOUS)
                        precious_count++;
        }
        return 0;
}

static int
_ds_variable_fill_fun(void *sym, void *data)
{
        struct variable *var = sym;
        
        if (var->sym.flags & SYM_PASSTOGGLE) {
		var->sym.flags &= ~SYM_PASSTOGGLE;
                struct variable ***vtabptr = data;
                **vtabptr = var;
                ++*vtabptr;
        }
        return 0;
}

static int
_ds_reloc_fun(void *sym, void *data)
{
        struct variable *var = sym;
        size_t *pi = data;
        
        if ((var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
	    && !(var->sym.flags & SYM_PASSTOGGLE)
	    && var->type == dtype_string) {
		var->sym.flags |= SYM_PASSTOGGLE;
                dataseg_reloc[(*pi)++] = var->off;
	}
        return 0;
}

static int
_ds_literal_count_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        size_t *offset = data;
        if (!(lit->flags & SYM_VOLATILE) && (lit->flags & SYM_REFERENCED)) {
                lit->off = *offset;
                *offset += B2STACK(strlen(lit->text) + 1);
        }
        return 0;
}

static int
_ds_literal_copy_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        if (!(lit->flags & SYM_VOLATILE) && (lit->flags & SYM_REFERENCED)) 
                strcpy((char*)(dataseg + lit->off), lit->text);
        return 0;
}

static int
vtab_comp(const void *a, const void *b)
{
        const struct variable *vp1 = *(const struct variable **)a;
        const struct variable *vp2 = *(const struct variable **)b;

        if ((vp1->sym.flags & SYM_PRECIOUS)
            && !(vp2->sym.flags & SYM_PRECIOUS))
                return 1;
        else if ((vp2->sym.flags & SYM_PRECIOUS) 
                 && !(vp1->sym.flags & SYM_PRECIOUS))
                return -1;
        return 0;
}

static int
place_exc(const struct constant *cp, const struct literal *lit, void *data)
{
	STKVAL *tab = data;
	tab[cp->value.v.number] = (STKVAL) lit->off;
	return 0;
}

static void
dataseg_layout()
{
	struct declvar *dv;
	size_t i;
	struct switch_stmt *sw;
	struct variable **vtab, **pvtab;
	struct exmask *exmask;

	if (root_node[smtp_state_action]) {
		/* Make sure PROG_ACTION_NAME is registered before code
		   generation.  It will be used by code_type_result in
		   drivers.c */
		literal_lookup(PROG_ACTION_NAME)->flags |= SYM_REFERENCED;
	}
	
	/* Count used variables and estimate the number of relocations
	   needed */
	dataseg_reloc_count = 0;
	module_symtab_enumerate(namespace_variable,
				_ds_variable_count_fun,
				NULL);

	/* Fill variable pointer array and make sure precious variables
	   occupy its bottom part */
	vtab = mu_calloc(variable_count, sizeof(vtab[0]));
	pvtab = vtab;
	module_symtab_enumerate(namespace_variable,
				_ds_variable_fill_fun,
				&pvtab);
	qsort(vtab, variable_count, sizeof(vtab[0]), vtab_comp);

	/* Compute variable offsets. Offset 0 is reserved for NULL symbol */
	for (i = 0; i < variable_count; i++) {
		vtab[i]->off = i + 1;
		if (vtab[i]->addrptr)
			*vtab[i]->addrptr = vtab[i]->off;
	}

	/* Free the array */
	free(vtab);
	
	/* Mark literals used to initialize variables as referenced */
	for (dv = declvar; dv; dv = dv->next) {
		if ((dv->var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED))
		    && dv->var->type == dtype_string) {
			dv->val.v.literal->flags |= SYM_REFERENCED;
		}
	}

	datasize = variable_count + 1;
	dvarsize = datasize - precious_count;
	
	/* Count referenced literals and adjust the data size */
	symtab_enumerate(stab_literal, _ds_literal_count_fun, &datasize);

	/* Account for switch translation tables */
	for (sw = switch_root; sw; sw = sw->next) {
		sw->off = datasize;
		datasize += sw->tabsize;
	}

	/* Account for exception masks */
	for (exmask = exmask_root; exmask; exmask = exmask->next) {
		exmask->off = datasize;
		if (exmask->all) {
			size_t i;
			for (i = 0; i < exception_count; i++)
				bitmask_set(&exmask->bm, i);
		}
		datasize += exmask->bm.bm_size + 1;
	}

	/* Account for exception name table */
	datasize += exception_count;
	
	/* Allocate data segment and relocation table */
	dataseg = mu_calloc(datasize, sizeof(STKVAL));
	dataseg_reloc = mu_calloc(dataseg_reloc_count, sizeof *dataseg_reloc);

	/* Fill relocation table */
	i = 0;
	module_symtab_enumerate(namespace_variable, _ds_reloc_fun, &i);
	
	/* Initialize variables */
	for (dv = declvar; dv; dv = dv->next) {
		if (dv->var->sym.flags & (SYM_VOLATILE | SYM_REFERENCED)) {
			switch (dv->var->type) {
			case dtype_string:
				dataseg[dv->var->off] =
					(STKVAL) dv->val.v.literal->off;
				break;

			case dtype_number:
				dataseg[dv->var->off] =
					(STKVAL) dv->val.v.number;
				break;

			default:
				abort();
			}
		}
	}	

	/* Place literals */
	symtab_enumerate(stab_literal, _ds_literal_copy_fun, NULL);

	/* Initialize exception masks */
	for (exmask = exmask_root; exmask; exmask = exmask->next) {
		size_t i, off = exmask->off;
		
		dataseg[off++] = (STKVAL) exmask->bm.bm_size;
		for (i = 0; i < exmask->bm.bm_size; i++)
			dataseg[off++] = (STKVAL) exmask->bm.bm_bits[i++];
	}

	/* Initialize exception name table */
	enumerate_exceptions(place_exc, dataseg + EXTABIND);
}


static int
_regex_compile_fun(void *sym, void *data)
{
        struct literal *lit = sym;
        if (lit->regex) {
                struct sym_regex *rp;
                for (rp = lit->regex; rp; rp = rp->next) 
                        register_regex(rp);
        }
        return 0;
}

void
regex_layout()
{
        symtab_enumerate(stab_literal, _regex_compile_fun, NULL);
        finalize_regex();
}


static struct variable *auto_list;

static void
register_auto(struct variable *var)
{
        var->next = auto_list;
        auto_list = var;
}

static void
unregister_auto(struct variable *var)
{
        struct variable *p = auto_list, *prev = NULL;
        while (p) {
                struct variable *next = p->next;
                if (p == var) {
                        if (prev)
                                prev->next = next;
                        else
                                auto_list = next;
                        p->next = NULL;
                        return;
                }
                prev = p;
                p = next;
        }
}

/* FIXME: Redo shadowing via a separate table? */
static size_t
forget_autos(size_t nparam, size_t auto_count, size_t hidden_arg)
{
        size_t param_count = 0;
        struct variable *var = auto_list;
        while (var) {
                struct variable *next = var->next;
                switch (var->storage_class) {
                case storage_auto:
                        var->off = auto_count++;
                        break;
                case storage_param:
                        var->off = nparam - param_count++;
                        var->ord = var->off - (hidden_arg ? 1 : 0) - 1;
                        break;
                default:
                        abort();
                }
                while (var->storage_class != storage_extern) {
                        struct variable *shadowed = var->shadowed;
                        if (!shadowed) {
                                symtab_remove(TOP_MODULE_SYMTAB(namespace_variable),
                                              var->sym.name);
                                break;
                        }
                        var->shadowed = NULL;
                        var = variable_replace(var->sym.name, shadowed);
                }
                var = next;
        }
        auto_list = NULL;
        return auto_count;
}

const char *
storage_class_str(storage_class_t sc)
{
        switch (sc) {
        case storage_extern:
                return "extern";
        case storage_auto:
                return "auto";
        case storage_param:
                return "param";
        }
        return "unknown?";
}


const char *
function_name()
{
        switch (outer_context) {
        case context_function:
                return func->sym.name;
        case context_handler:
                return xstate_to_string(state_tag);
        default:
                return "";
        }
}

NODE *
declare_function(struct function *func, struct mu_locus_range const *loc,
		 size_t nautos)
{
        NODE *node = alloc_node(node_type_funcdecl, loc);
        node->v.funcdecl.func = func;
        node->v.funcdecl.auto_count = nautos;
        node->v.funcdecl.tree = func->node; 
        return node;
}


NODE *
create_node_variable(struct variable *var, struct mu_locus_range const *locus)
{
        NODE *node;

	variable_check_initialized(var, locus);
	node = alloc_node(node_type_variable, locus);
        node->v.var_ref.variable = var;
        node->v.var_ref.nframes = catch_nesting;
        return node;
}

NODE *
create_node_argcount(struct mu_locus_range const *locus)
{
        NODE *node;
        
        if (outer_context == context_function) {
                if (func->optcount || func->varargs) {
                        node = alloc_node(node_type_arg, locus);
                        node->v.arg.data_type = dtype_number;
                        node->v.arg.number = 1;
                } else {
                        node = alloc_node(node_type_number, locus);
                        node->v.number = parminfo[outer_context].parmcount();
                }
        } else {
                node = alloc_node(node_type_number, locus);
                node->v.number = parminfo[outer_context].parmcount();
        }
        return node;
}

NODE *
create_node_arg(long num, struct mu_locus_range const *locus)
{
        NODE *node = alloc_node(node_type_arg, locus);
	long argnum = num;
	data_type_t type;

	if (inner_context == context_function) {
		if (func->varargs) {
			argnum += PARMCOUNT() + FUNC_HIDDEN_ARGS(func);
			type = PARMTYPE(num);
		} else {
			parse_error_locus(locus,
					  _("$N is allowed only in functions "
					    "with variable number of arguments"));
			argnum = 0;
			type = dtype_string;
		}
        } else if (num > PARMCOUNT()) {
		parse_error_locus(locus, _("argument number too high"));
		num = argnum = 0;
		type = dtype_string;
	} else
		type = PARMTYPE(num);

	node->v.arg.data_type = type;
	node->v.arg.number = argnum;
        return node;
}

NODE *
create_node_string(char const *str, struct mu_locus_range const *locus)
{
        NODE *node = alloc_node(node_type_string, locus);
        node->v.literal = literal_lookup(str);
        return node;
}

NODE *
create_node_symbol(struct literal *lit, struct mu_locus_range const *locus)
{
        NODE *node;
        register_macro(state_tag, lit->text);
        node = alloc_node(node_type_symbol, locus);
        node->v.literal = lit;
        return node;
}

NODE *
create_node_backref(long num, struct mu_locus_range const *locus)
{
        NODE *node = alloc_node(node_type_backref, locus);
        node->v.number = num;
        return node;
}

static inline int
variable_is_initialized(struct variable *var)
{
	return var->storage_class != storage_auto || var->initialized;
}

void
variable_check_initialized(struct variable *var,
			   struct mu_locus_range const *loc)
{
	if (!variable_is_initialized(var)) {
		parse_warning_locus(loc,
				    _("use of uninitialized variable '%s'"),
				    var->sym.name);
		var->initialized = 1;
	}
}
