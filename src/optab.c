/* -*- buffer-read-only: t -*- vi: set ro: */
/* THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT. */
#line 2 "optab.opc"
/* This file is part of Mailfromd.
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include "mailfromd.h"
#include "prog.h"
#include "optab.h"

#line 19 "../src/opcodes"
extern void instr_locus(eval_environ_t); 
#line 21 "../src/opcodes"
extern void instr_stkalloc(eval_environ_t); 
#line 22 "../src/opcodes"
extern void instr_xchg(eval_environ_t); 
#line 23 "../src/opcodes"
extern void instr_pop(eval_environ_t); 
#line 24 "../src/opcodes"
extern void instr_dup(eval_environ_t); 
#line 25 "../src/opcodes"
extern void instr_dupn(eval_environ_t); 
#line 26 "../src/opcodes"
extern void instr_push(eval_environ_t); 
#line 27 "../src/opcodes"
extern void instr_symbol(eval_environ_t); 
#line 28 "../src/opcodes"
extern void instr_backref(eval_environ_t); 
#line 29 "../src/opcodes"
extern void instr_ston(eval_environ_t); 
#line 30 "../src/opcodes"
extern void instr_ntos(eval_environ_t); 
#line 31 "../src/opcodes"
extern void instr_adjust(eval_environ_t); 
#line 32 "../src/opcodes"
extern void instr_adjustx(eval_environ_t); 
#line 33 "../src/opcodes"
extern void instr_popreg(eval_environ_t); 
#line 34 "../src/opcodes"
extern void instr_pushreg(eval_environ_t); 
#line 36 "../src/opcodes"
extern void instr_bz(eval_environ_t); 
#line 37 "../src/opcodes"
extern void instr_bnz(eval_environ_t); 
#line 38 "../src/opcodes"
extern void instr_jmp(eval_environ_t); 
#line 39 "../src/opcodes"
extern void instr_cmp(eval_environ_t); 
#line 40 "../src/opcodes"
extern void instr_xlat(eval_environ_t); 
#line 41 "../src/opcodes"
extern void instr_xlats(eval_environ_t); 
#line 42 "../src/opcodes"
extern void instr_jreg(eval_environ_t); 
#line 44 "../src/opcodes"
extern void instr_regex(eval_environ_t); 
#line 45 "../src/opcodes"
extern void instr_regmatch(eval_environ_t); 
#line 46 "../src/opcodes"
extern void instr_fnmatch(eval_environ_t); 
#line 47 "../src/opcodes"
extern void instr_fnmatch_mx(eval_environ_t); 
#line 48 "../src/opcodes"
extern void instr_regmatch_mx(eval_environ_t); 
#line 49 "../src/opcodes"
extern void instr_regcomp(eval_environ_t); 
#line 51 "../src/opcodes"
extern void instr_not(eval_environ_t); 
#line 52 "../src/opcodes"
extern void instr_eqn(eval_environ_t); 
#line 53 "../src/opcodes"
extern void instr_eqs(eval_environ_t); 
#line 54 "../src/opcodes"
extern void instr_nen(eval_environ_t); 
#line 55 "../src/opcodes"
extern void instr_nes(eval_environ_t); 
#line 56 "../src/opcodes"
extern void instr_ltn(eval_environ_t); 
#line 57 "../src/opcodes"
extern void instr_lts(eval_environ_t); 
#line 58 "../src/opcodes"
extern void instr_len(eval_environ_t); 
#line 59 "../src/opcodes"
extern void instr_les(eval_environ_t); 
#line 60 "../src/opcodes"
extern void instr_gtn(eval_environ_t); 
#line 61 "../src/opcodes"
extern void instr_gts(eval_environ_t); 
#line 62 "../src/opcodes"
extern void instr_gen(eval_environ_t); 
#line 63 "../src/opcodes"
extern void instr_ges(eval_environ_t); 
#line 65 "../src/opcodes"
extern void instr_neg(eval_environ_t); 
#line 66 "../src/opcodes"
extern void instr_add(eval_environ_t); 
#line 67 "../src/opcodes"
extern void instr_sub(eval_environ_t); 
#line 68 "../src/opcodes"
extern void instr_mul(eval_environ_t); 
#line 69 "../src/opcodes"
extern void instr_div(eval_environ_t); 
#line 70 "../src/opcodes"
extern void instr_mod(eval_environ_t); 
#line 72 "../src/opcodes"
extern void instr_logand(eval_environ_t); 
#line 73 "../src/opcodes"
extern void instr_logor(eval_environ_t); 
#line 74 "../src/opcodes"
extern void instr_logxor(eval_environ_t); 
#line 75 "../src/opcodes"
extern void instr_lognot(eval_environ_t); 
#line 77 "../src/opcodes"
extern void instr_shl(eval_environ_t); 
#line 78 "../src/opcodes"
extern void instr_shr(eval_environ_t); 
#line 80 "../src/opcodes"
extern void instr_concat(eval_environ_t); 
#line 82 "../src/opcodes"
extern void instr_memstk(eval_environ_t); 
#line 83 "../src/opcodes"
extern void instr_xmemstk(eval_environ_t); 
#line 84 "../src/opcodes"
extern void instr_deref(eval_environ_t); 
#line 85 "../src/opcodes"
extern void instr_asgn(eval_environ_t); 
#line 86 "../src/opcodes"
extern void instr_builtin(eval_environ_t); 
#line 88 "../src/opcodes"
extern void instr_catch(eval_environ_t); 
#line 89 "../src/opcodes"
extern void instr_throw(eval_environ_t); 
#line 90 "../src/opcodes"
extern void instr_saveex(eval_environ_t); 
#line 91 "../src/opcodes"
extern void instr_restex(eval_environ_t); 
#line 93 "../src/opcodes"
extern void instr_echo(eval_environ_t); 
#line 94 "../src/opcodes"
extern void instr_return(eval_environ_t); 
#line 95 "../src/opcodes"
extern void instr_retcatch(eval_environ_t); 
#line 96 "../src/opcodes"
extern void instr_funcall(eval_environ_t); 
#line 98 "../src/opcodes"
extern void instr_next(eval_environ_t); 
#line 99 "../src/opcodes"
extern void instr_result(eval_environ_t); 
#line 100 "../src/opcodes"
extern void instr_header(eval_environ_t); 
#line 102 "../src/opcodes"
extern void instr_sedcomp(eval_environ_t); 
#line 103 "../src/opcodes"
extern void instr_sed(eval_environ_t); 
#line 26 "optab.opc"
extern void dump_result(prog_counter_t); 
extern void dump_regcomp(prog_counter_t); 
extern void dump_push(prog_counter_t); 
extern void dump_sedcomp(prog_counter_t); 
extern void dump_adjust(prog_counter_t); 
extern void dump_regex(prog_counter_t); 
extern void dump_adjust(prog_counter_t); 
extern void dump_stkalloc(prog_counter_t); 
extern void dump_funcall(prog_counter_t); 
extern void dump_builtin(prog_counter_t); 
extern void dump_branch(prog_counter_t); 
extern void dump_backref(prog_counter_t); 
extern void dump_symbol(prog_counter_t); 
extern void dump_memstk(prog_counter_t); 
extern void dump_branch(prog_counter_t); 
extern void dump_branch(prog_counter_t); 
extern void dump_catch(prog_counter_t); 
extern void dump_xlat(prog_counter_t); 
extern void dump_saveex(prog_counter_t); 
extern void dump_xlats(prog_counter_t); 
extern void dump_locus(prog_counter_t); 
extern void dump_throw(prog_counter_t); 
extern void dump_header(prog_counter_t); 
	  
struct optab optab[] = {
	{ "STOP", NULL, NULL, 0 },
#line 19 "../src/opcodes"
	{ "LOCUS", instr_locus, dump_locus, 2 }, 
#line 21 "../src/opcodes"
	{ "STKALLOC", instr_stkalloc, dump_stkalloc, 1 }, 
#line 22 "../src/opcodes"
	{ "XCHG", instr_xchg, NULL, 0 }, 
#line 23 "../src/opcodes"
	{ "POP", instr_pop, NULL, 0 }, 
#line 24 "../src/opcodes"
	{ "DUP", instr_dup, NULL, 0 }, 
#line 25 "../src/opcodes"
	{ "DUPN", instr_dupn, NULL, 1 }, 
#line 26 "../src/opcodes"
	{ "PUSH", instr_push, dump_push, 1 }, 
#line 27 "../src/opcodes"
	{ "SYMBOL", instr_symbol, dump_symbol, 1 }, 
#line 28 "../src/opcodes"
	{ "BACKREF", instr_backref, dump_backref, 1 }, 
#line 29 "../src/opcodes"
	{ "STON", instr_ston, NULL, 0 }, 
#line 30 "../src/opcodes"
	{ "NTOS", instr_ntos, NULL, 0 }, 
#line 31 "../src/opcodes"
	{ "ADJUST", instr_adjust, dump_adjust, 1 }, 
#line 32 "../src/opcodes"
	{ "ADJUSTX", instr_adjustx, dump_adjust, 1 }, 
#line 33 "../src/opcodes"
	{ "POPREG", instr_popreg, NULL, 0 }, 
#line 34 "../src/opcodes"
	{ "PUSHREG", instr_pushreg, NULL, 0 }, 
#line 36 "../src/opcodes"
	{ "BZ", instr_bz, dump_branch, 1 }, 
#line 37 "../src/opcodes"
	{ "BNZ", instr_bnz, dump_branch, 1 }, 
#line 38 "../src/opcodes"
	{ "JMP", instr_jmp, dump_branch, 1 }, 
#line 39 "../src/opcodes"
	{ "CMP", instr_cmp, NULL, 0 }, 
#line 40 "../src/opcodes"
	{ "XLAT", instr_xlat, dump_xlat, 2 }, 
#line 41 "../src/opcodes"
	{ "XLATS", instr_xlats, dump_xlats, 2 }, 
#line 42 "../src/opcodes"
	{ "JREG", instr_jreg, NULL, 0 }, 
#line 44 "../src/opcodes"
	{ "REGEX", instr_regex, dump_regex, 1 }, 
#line 45 "../src/opcodes"
	{ "REGMATCH", instr_regmatch, NULL, 0 }, 
#line 46 "../src/opcodes"
	{ "FNMATCH", instr_fnmatch, NULL, 0 }, 
#line 47 "../src/opcodes"
	{ "FNMATCH_MX", instr_fnmatch_mx, NULL, 0 }, 
#line 48 "../src/opcodes"
	{ "REGMATCH_MX", instr_regmatch_mx, NULL, 0 }, 
#line 49 "../src/opcodes"
	{ "REGCOMP", instr_regcomp, dump_regcomp, 1 }, 
#line 51 "../src/opcodes"
	{ "NOT", instr_not, NULL, 0 }, 
#line 52 "../src/opcodes"
	{ "EQN", instr_eqn, NULL, 0 }, 
#line 53 "../src/opcodes"
	{ "EQS", instr_eqs, NULL, 0 }, 
#line 54 "../src/opcodes"
	{ "NEN", instr_nen, NULL, 0 }, 
#line 55 "../src/opcodes"
	{ "NES", instr_nes, NULL, 0 }, 
#line 56 "../src/opcodes"
	{ "LTN", instr_ltn, NULL, 0 }, 
#line 57 "../src/opcodes"
	{ "LTS", instr_lts, NULL, 0 }, 
#line 58 "../src/opcodes"
	{ "LEN", instr_len, NULL, 0 }, 
#line 59 "../src/opcodes"
	{ "LES", instr_les, NULL, 0 }, 
#line 60 "../src/opcodes"
	{ "GTN", instr_gtn, NULL, 0 }, 
#line 61 "../src/opcodes"
	{ "GTS", instr_gts, NULL, 0 }, 
#line 62 "../src/opcodes"
	{ "GEN", instr_gen, NULL, 0 }, 
#line 63 "../src/opcodes"
	{ "GES", instr_ges, NULL, 0 }, 
#line 65 "../src/opcodes"
	{ "NEG", instr_neg, NULL, 0 }, 
#line 66 "../src/opcodes"
	{ "ADD", instr_add, NULL, 0 }, 
#line 67 "../src/opcodes"
	{ "SUB", instr_sub, NULL, 0 }, 
#line 68 "../src/opcodes"
	{ "MUL", instr_mul, NULL, 0 }, 
#line 69 "../src/opcodes"
	{ "DIV", instr_div, NULL, 0 }, 
#line 70 "../src/opcodes"
	{ "MOD", instr_mod, NULL, 0 }, 
#line 72 "../src/opcodes"
	{ "LOGAND", instr_logand, NULL, 0 }, 
#line 73 "../src/opcodes"
	{ "LOGOR", instr_logor, NULL, 0 }, 
#line 74 "../src/opcodes"
	{ "LOGXOR", instr_logxor, NULL, 0 }, 
#line 75 "../src/opcodes"
	{ "LOGNOT", instr_lognot, NULL, 0 }, 
#line 77 "../src/opcodes"
	{ "SHL", instr_shl, NULL, 0 }, 
#line 78 "../src/opcodes"
	{ "SHR", instr_shr, NULL, 0 }, 
#line 80 "../src/opcodes"
	{ "CONCAT", instr_concat, NULL, 0 }, 
#line 82 "../src/opcodes"
	{ "MEMSTK", instr_memstk, dump_memstk, 2 }, 
#line 83 "../src/opcodes"
	{ "XMEMSTK", instr_xmemstk, NULL, 0 }, 
#line 84 "../src/opcodes"
	{ "DEREF", instr_deref, NULL, 0 }, 
#line 85 "../src/opcodes"
	{ "ASGN", instr_asgn, NULL, 0 }, 
#line 86 "../src/opcodes"
	{ "BUILTIN", instr_builtin, dump_builtin, 2 }, 
#line 88 "../src/opcodes"
	{ "CATCH", instr_catch, dump_catch, 2 }, 
#line 89 "../src/opcodes"
	{ "THROW", instr_throw, dump_throw, 1 }, 
#line 90 "../src/opcodes"
	{ "SAVEEX", instr_saveex, dump_saveex, 1 }, 
#line 91 "../src/opcodes"
	{ "RESTEX", instr_restex, NULL, 0 }, 
#line 93 "../src/opcodes"
	{ "ECHO", instr_echo, NULL, 0 }, 
#line 94 "../src/opcodes"
	{ "RETURN", instr_return, NULL, 0 }, 
#line 95 "../src/opcodes"
	{ "RETCATCH", instr_retcatch, NULL, 0 }, 
#line 96 "../src/opcodes"
	{ "FUNCALL", instr_funcall, dump_funcall, 2 }, 
#line 98 "../src/opcodes"
	{ "NEXT", instr_next, NULL, 0 }, 
#line 99 "../src/opcodes"
	{ "RESULT", instr_result, dump_result, 1 }, 
#line 100 "../src/opcodes"
	{ "HEADER", instr_header, dump_header, 2 }, 
#line 102 "../src/opcodes"
	{ "SEDCOMP", instr_sedcomp, dump_sedcomp, 2 }, 
#line 103 "../src/opcodes"
	{ "SED", instr_sed, NULL, 0 }, 
#line 31 "optab.opc"
	{ NULL },
};

void
scan_code(prog_counter_t start, prog_counter_t end,
	  void (*fun)(prog_counter_t, struct optab *, void *), void *data)
{
	prog_counter_t i;

	for (i = start; i < end; i++) {
		if (mf_cell_c_value(prog[i], uint)) {
			struct optab *op;
			unsigned opcode = mf_cell_c_value(prog[i], uint);
			if (opcode > max_instr_opcode) {
				mu_error(_("%08lx: unknown opcode %u; aborting dump"),
					 (unsigned long) i, opcode);
				exit(1);
			}
			op = &optab[opcode];
			fun(i, op, data);
			i += op->length;
		} else 
			fun(i, &optab[opcode_nil], data);
	}
}

