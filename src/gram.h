/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 290 "gram.y" /* yacc.c:1909  */

#define YYLTYPE struct mu_locus_range

#line 48 "gram.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_ACCEPT = 258,
    T_REJECT = 259,
    T_TEMPFAIL = 260,
    T_CONTINUE = 261,
    T_DISCARD = 262,
    T_ADD = 263,
    T_REPLACE = 264,
    T_DELETE = 265,
    T_PROG = 266,
    T_IF = 267,
    T_FI = 268,
    T_ELSE = 269,
    T_ELIF = 270,
    T_ON = 271,
    T_HOST = 272,
    T_FROM = 273,
    T_AS = 274,
    T_DO = 275,
    T_DONE = 276,
    T_POLL = 277,
    T_MATCHES = 278,
    T_FNMATCHES = 279,
    T_MXMATCHES = 280,
    T_MXFNMATCHES = 281,
    T_WHEN = 282,
    T_PASS = 283,
    T_SET = 284,
    T_CATCH = 285,
    T_TRY = 286,
    T_THROW = 287,
    T_ECHO = 288,
    T_RETURNS = 289,
    T_RETURN = 290,
    T_FUNC = 291,
    T_SWITCH = 292,
    T_CASE = 293,
    T_DEFAULT = 294,
    T_CONST = 295,
    T_FOR = 296,
    T_LOOP = 297,
    T_WHILE = 298,
    T_BREAK = 299,
    T_NEXT = 300,
    T_ARGCOUNT = 301,
    T_ALIAS = 302,
    T_DOTS = 303,
    T_ARGX = 304,
    T_VAPTR = 305,
    T_PRECIOUS = 306,
    T_OR = 307,
    T_AND = 308,
    T_EQ = 309,
    T_NE = 310,
    T_LT = 311,
    T_LE = 312,
    T_GT = 313,
    T_GE = 314,
    T_NOT = 315,
    T_LOGAND = 316,
    T_LOGOR = 317,
    T_LOGXOR = 318,
    T_LOGNOT = 319,
    T_REQUIRE = 320,
    T_IMPORT = 321,
    T_STATIC = 322,
    T_PUBLIC = 323,
    T_MODULE = 324,
    T_BYE = 325,
    T_DCLEX = 326,
    T_SHL = 327,
    T_SHR = 328,
    T_SED = 329,
    T_ARGV = 330,
    T_VOID = 331,
    T_COMPOSE = 332,
    T_MODBEG = 333,
    T_MODEND = 334,
    T_STRING = 335,
    T_SYMBOL = 336,
    T_IDENTIFIER = 337,
    T_ARG = 338,
    T_NUMBER = 339,
    T_BACKREF = 340,
    T_BUILTIN = 341,
    T_FUNCTION = 342,
    T_TYPE = 343,
    T_TYPECAST = 344,
    T_VARIABLE = 345,
    T_BOGUS = 346,
    T_UMINUS = 347
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 294 "gram.y" /* yacc.c:1909  */

	struct literal *literal;
	struct stmtlist stmtlist;
	NODE *node;
	struct return_node ret;
	struct poll_data poll;
	struct pollarg {
		int kw;
		NODE *expr;
	} pollarg;
	struct arglist {
		NODE *head;
		NODE *tail;
		size_t count;
	} arglist;
	long number;
	const struct builtin *builtin;
	struct variable *var;
	enum smtp_state state;
	struct {
		int qualifier;
	} matchtype;
	data_type_t type;
	struct parmtype *parm;
	struct parmlist {
		struct parmtype *head, *tail;
		size_t count;
		size_t optcount;
		int varargs;
		data_type_t vartype;
	} parmlist;
	enum lexical_context tie_in;
	struct function *function;
	struct {
		struct valist *head, *tail;
	} valist_list;
	struct {
		int all;
		struct valist *valist;
	} catchlist;
	struct valist *valist;
	struct value value;
	struct {
		struct case_stmt *head, *tail;
	} case_list ;
	struct case_stmt *case_stmt;
	struct loop_node loop;
	struct enumlist {
		struct constant *cv;
		struct enumlist *prev;
	} enumlist;
	mu_list_t list;
	struct import_rule_list import_rule_list;
	char *string;

#line 209 "gram.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */
