/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#ifdef __DBGMOD_C_ARRAY
"engine",
#else
# define MF_SOURCE_ENGINE (mfd_debug_handle + 0)
#endif
#ifdef __DBGMOD_C_ARRAY
"main",
#else
# define MF_SOURCE_MAIN (mfd_debug_handle + 1)
#endif
#ifdef __DBGMOD_C_ARRAY
"pp",
#else
# define MF_SOURCE_PP (mfd_debug_handle + 2)
#endif
#ifdef __DBGMOD_C_ARRAY
"prog",
#else
# define MF_SOURCE_PROG (mfd_debug_handle + 3)
#endif
#ifdef __DBGMOD_C_ARRAY
"spf",
#else
# define MF_SOURCE_SPF (mfd_debug_handle + 4)
#endif
