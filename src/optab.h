/* -*- buffer-read-only: t -*- vi: set ro: */
/* THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT. */
#line 2 "optab.oph"
enum instr_opcode {
	opcode_nil,
#line 19 "../src/opcodes"
	opcode_locus, 
#line 21 "../src/opcodes"
	opcode_stkalloc, 
#line 22 "../src/opcodes"
	opcode_xchg, 
#line 23 "../src/opcodes"
	opcode_pop, 
#line 24 "../src/opcodes"
	opcode_dup, 
#line 25 "../src/opcodes"
	opcode_dupn, 
#line 26 "../src/opcodes"
	opcode_push, 
#line 27 "../src/opcodes"
	opcode_symbol, 
#line 28 "../src/opcodes"
	opcode_backref, 
#line 29 "../src/opcodes"
	opcode_ston, 
#line 30 "../src/opcodes"
	opcode_ntos, 
#line 31 "../src/opcodes"
	opcode_adjust, 
#line 32 "../src/opcodes"
	opcode_adjustx, 
#line 33 "../src/opcodes"
	opcode_popreg, 
#line 34 "../src/opcodes"
	opcode_pushreg, 
#line 36 "../src/opcodes"
	opcode_bz, 
#line 37 "../src/opcodes"
	opcode_bnz, 
#line 38 "../src/opcodes"
	opcode_jmp, 
#line 39 "../src/opcodes"
	opcode_cmp, 
#line 40 "../src/opcodes"
	opcode_xlat, 
#line 41 "../src/opcodes"
	opcode_xlats, 
#line 42 "../src/opcodes"
	opcode_jreg, 
#line 44 "../src/opcodes"
	opcode_regex, 
#line 45 "../src/opcodes"
	opcode_regmatch, 
#line 46 "../src/opcodes"
	opcode_fnmatch, 
#line 47 "../src/opcodes"
	opcode_fnmatch_mx, 
#line 48 "../src/opcodes"
	opcode_regmatch_mx, 
#line 49 "../src/opcodes"
	opcode_regcomp, 
#line 51 "../src/opcodes"
	opcode_not, 
#line 52 "../src/opcodes"
	opcode_eqn, 
#line 53 "../src/opcodes"
	opcode_eqs, 
#line 54 "../src/opcodes"
	opcode_nen, 
#line 55 "../src/opcodes"
	opcode_nes, 
#line 56 "../src/opcodes"
	opcode_ltn, 
#line 57 "../src/opcodes"
	opcode_lts, 
#line 58 "../src/opcodes"
	opcode_len, 
#line 59 "../src/opcodes"
	opcode_les, 
#line 60 "../src/opcodes"
	opcode_gtn, 
#line 61 "../src/opcodes"
	opcode_gts, 
#line 62 "../src/opcodes"
	opcode_gen, 
#line 63 "../src/opcodes"
	opcode_ges, 
#line 65 "../src/opcodes"
	opcode_neg, 
#line 66 "../src/opcodes"
	opcode_add, 
#line 67 "../src/opcodes"
	opcode_sub, 
#line 68 "../src/opcodes"
	opcode_mul, 
#line 69 "../src/opcodes"
	opcode_div, 
#line 70 "../src/opcodes"
	opcode_mod, 
#line 72 "../src/opcodes"
	opcode_logand, 
#line 73 "../src/opcodes"
	opcode_logor, 
#line 74 "../src/opcodes"
	opcode_logxor, 
#line 75 "../src/opcodes"
	opcode_lognot, 
#line 77 "../src/opcodes"
	opcode_shl, 
#line 78 "../src/opcodes"
	opcode_shr, 
#line 80 "../src/opcodes"
	opcode_concat, 
#line 82 "../src/opcodes"
	opcode_memstk, 
#line 83 "../src/opcodes"
	opcode_xmemstk, 
#line 84 "../src/opcodes"
	opcode_deref, 
#line 85 "../src/opcodes"
	opcode_asgn, 
#line 86 "../src/opcodes"
	opcode_builtin, 
#line 88 "../src/opcodes"
	opcode_catch, 
#line 89 "../src/opcodes"
	opcode_throw, 
#line 90 "../src/opcodes"
	opcode_saveex, 
#line 91 "../src/opcodes"
	opcode_restex, 
#line 93 "../src/opcodes"
	opcode_echo, 
#line 94 "../src/opcodes"
	opcode_return, 
#line 95 "../src/opcodes"
	opcode_retcatch, 
#line 96 "../src/opcodes"
	opcode_funcall, 
#line 98 "../src/opcodes"
	opcode_next, 
#line 99 "../src/opcodes"
	opcode_result, 
#line 100 "../src/opcodes"
	opcode_header, 
#line 102 "../src/opcodes"
	opcode_sedcomp, 
#line 103 "../src/opcodes"
	opcode_sed, 
#line 5 "optab.oph"
	max_instr_opcode
};
