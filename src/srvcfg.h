/* This file is part of Mailfromd.
   Copyright (C) 2005-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "srvman.h"

struct mf_srvcfg {
	const char *id;
	mu_url_t url;
	mu_acl_t acl;
	size_t max_children;
	mfd_server_func_t server;
	int single_process;
	int reuseaddr;
	mu_list_t options;
	int defopt;
	int backlog;
};

extern int server_flags;
extern char *mailfromd_state_dir;
extern char *syslog_tag;      /* Tag to mark syslog entries with. */
extern char *log_stream;
extern char *pidfile;                
extern int smtp_transcript;
extern struct mu_sockaddr *source_address;  /* Source address for TCP
					       connections */
extern size_t max_callout_mx;
extern time_t io_timeout;

/* Timeouts */
extern time_t smtp_timeout_soft[SMTP_NUM_TIMEOUT];
extern time_t smtp_timeout_hard[SMTP_NUM_TIMEOUT];

extern struct mu_tls_config tls_conf;

extern int mf_server_function(const char *key, struct mf_srvcfg *cfg);

void mf_srvcfg_init(const char *progname, const char *label);
void mf_srvcfg_add(const char *type, const char *urlstr);
void mf_srvcfg_flush(void);
mu_url_t parse_milter_url(const char *str);
void mf_srvcfg_log_setup(char const *stream);





