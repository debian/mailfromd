/* -*- buffer-read-only: t -*- vi: set ro:
 * THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
 */
#ifndef __MAILFROMD_GLOBAL_H
#define __MAILFROMD_GLOBAL_H
/* Global constants for Mailfromd.
 * Copyright (C) 2005-2024 Sergey Poznyakoff
 *
 * Mailfromd is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Mailfromd is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mailfromd.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Notes on format:
 * 1. Comments start with #.
 * 2. Comments and empty lines are ignored.
 * 3. A line starting with @ is reproduced verbatim in texinfo output
 *    and is suppressed in C output.
 * 4. A definition consists of at least two words: a keyword and its value.
 *    Definitions are converted to @set statements in texinfo output and
 *    to #define statements in C output.
 * 5. A value starting with @ is a reference to a C header file.  The definition
 *    of the keyword is then looked up in that file and is output only in
 *    texinfo format.
 * 6. Any part of a value can be enclosed in grave accents.  Such a part is
 *    executed as a shell command, and its output is pasted back to the
 *    resulting string.  If the command produces multiple lines, they are
 *    concatenated and separated by a single space character.
 */
#line 37 "global.def"
#define DEFAULT_REJECT_CODE "550"
#line 40 "global.def"
#define DEFAULT_TEMPFAIL_CODE "451"
#line 43 "global.def"
#define STACK_SIZE 4096
#line 44 "global.def"
#define STACK_INCR 4096
#line 53 "global.def"
#define MAX_E_EXCEPTIONS 22
#line 56 "global.def"
#define MAX_IOSTREAMS 1024
#line 59 "global.def"
#define MAX_MBOXES 64
#line 62 "global.def"
#define MAX_MSGS 1024
#line 65 "global.def"
#define MAX_DNS_A 64
#line 66 "global.def"
#define MAX_DNS_PTR 64
#line 68 "global.def"
#define MAX_DNS_MX 32
#line 71 "global.def"
#define MAX_MFMODS 16
#endif
