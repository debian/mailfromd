# This file is part of Mailfromd.
# Copyright (C) 2011-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {
	print "/* -*- buffer-read-only: t -*- vi: set ro:"
	print "   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT."
	print "*/"
	state = "init"
}
/^#prefix/ { prefix = $2; next }
/#/ { sub(/#/, "/*"); $0 = $0 " */" }
NF == 0 { next }

state == "init" && $1 == "const" {
	if (NF == 1) {
		state = "const"
		next
	} else {
		sub(/^const[ \t][ \t]*/, ("#define " prefix))
		print
	}
}

state == "const" && NF == 1 && $1 == "do" { state = "enum"; enum_val = 0; next }
state == "const" { print NR ": const syntax error" >> "/dev/stderr"; exit 1 }

state == "enum" && NF == 1 && $1 == "done" { state = "init"; next }
state == "enum" {
	if (NF == 2) {
		print "#define " prefix $1 " " $2
		enum_val = $2 + 1
	} else if (NF == 1) {
		print "#define " prefix $1 " " enum_val
		enum_val++
	} else {
		print NR ": const syntax error" >> "/dev/stderr"
		exit 1
	}
}
