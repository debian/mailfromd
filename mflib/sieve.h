/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define MF_SIEVE_FILE               0x00 /* script argument is a file name */
#define MF_SIEVE_TEXT               0x01 /* script argument is a Sieve text */
#define MF_SIEVE_LOG                0x02 /* log Sieve actions */
#define MF_SIEVE_DEBUG_TRACE        0x04 /* trace Sieve tests */
#define MF_SIEVE_DEBUG_INSTR        0x08 /* log every instruction run */
