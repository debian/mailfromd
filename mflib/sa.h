/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define SA_SYMBOLS    0
#define SA_REPORT     1
#define SA_LEARN_SPAM 2
#define SA_LEARN_HAM  3
#define SA_FORGET     4
