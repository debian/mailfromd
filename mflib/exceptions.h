/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#ifndef MAILFROMD_EXCEPTIONS_H
# define MAILFROMD_EXCEPTIONS_H
typedef enum mf_exception_code {
        mfe_success,
        mfe_not_found,
        mfe_failure,
        mfe_temp_failure,
        mfe_ston_conv,
        mfe_divzero,
        mfe_regcomp,
        mfe_invip,
        mfe_invcidr,
        mfe_invtime,
        mfe_dbfailure,
        mfe_range,
        mfe_url,
        mfe_too_many,
        mfe_io,
        mfe_macroundef,
        mfe_eof,
        mfe_exists,
        mfe_format,
        mfe_badmmq,
        mfe_ilseq,
        mfe_inval,
        mf_exception_count
} mf_exception;
#endif
