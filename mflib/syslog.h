/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define _MFL_LOG_EMERG	   0
#define _MFL_LOG_ALERT	   1
#define _MFL_LOG_CRIT	   2
#define _MFL_LOG_ERR	   3	
#define _MFL_LOG_WARNING  4	
#define _MFL_LOG_NOTICE   5	
#define _MFL_LOG_INFO	   6
#define _MFL_LOG_DEBUG	   7
#define _MFL_LOG_PRIMASK  0x07
#define _MFL_LOG_KERN	   0
#define _MFL_LOG_USER	   (1<<3)
#define _MFL_LOG_MAIL	   (2<<3)
#define _MFL_LOG_DAEMON   (3<<3)
#define _MFL_LOG_AUTH	   (4<<3)
#define _MFL_LOG_SYSLOG   (5<<3)
#define _MFL_LOG_LPR	   (6<<3)
#define _MFL_LOG_NEWS	   (7<<3)
#define _MFL_LOG_UUCP	   (8<<3)
#define _MFL_LOG_CRON	   (9<<3)
#define _MFL_LOG_AUTHPRIV (10<<3)
#define _MFL_LOG_FTP	   (11<<3)
#define _MFL_LOG_LOCAL0   (16<<3)
#define _MFL_LOG_LOCAL1   (17<<3)
#define _MFL_LOG_LOCAL2   (18<<3)
#define _MFL_LOG_LOCAL3   (19<<3)
#define _MFL_LOG_LOCAL4   (20<<3)
#define _MFL_LOG_LOCAL5   (21<<3)
#define _MFL_LOG_LOCAL6   (22<<3)
#define _MFL_LOG_LOCAL7   (23<<3)
