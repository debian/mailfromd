# This file is part of Mailfromd.
# Copyright (C) 2007-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {
	print "/* -*- buffer-read-only: t -*- vi: set ro:"
	print "   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT."
	print "*/"
	print "#ifndef MAILFROMD_EXCEPTIONS_H"
	print "# define MAILFROMD_EXCEPTIONS_H"
	print "typedef enum mf_exception_code {";
	indent = "        "
}

/dclex[ \t][ \t]*e_/ {
	printf("%smf%s,\n", indent, $2)
}

END {
	printf("%smf_exception_count\n", indent)
	print "} mf_exception;"
	print "#endif"
}




	
