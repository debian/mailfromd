/* -*- buffer-read-only: t -*- vi: set ro:
   THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
*/
#define _MFL_milter_state_none 0
#define _MFL_milter_state_startup 1
#define _MFL_milter_state_shutdown 2
#define _MFL_milter_state_begin 3
#define _MFL_milter_state_end 4
#define _MFL_milter_state_connect 5
#define _MFL_milter_state_helo 6
#define _MFL_milter_state_envfrom 7
#define _MFL_milter_state_envrcpt 8
#define _MFL_milter_state_data 9
#define _MFL_milter_state_header 10
#define _MFL_milter_state_eoh 11
#define _MFL_milter_state_body 12
#define _MFL_milter_state_eom 13
#define _MFL_milter_action 14
